module transforms
  ! NCS 2020 
  ! 
  ! A module for fundamental transforms on the data, including
  ! spectral transform and regridding from model to pressure levels.
  
  use ccc_data, only: ccc_data_array
  use utilities, only: timer_start, timer_stop
  use diag_sizes

  private

  public cofagg_inline, gsapl_inline, cwinds_inline
  contains

    subroutine cofagg_inline(cccdata, cccdata_result, kuv)
      ! NCS 2020
      ! Heavily modified from cofagg program
      !                                                                               
      !PURPOSE - READS A FILE OF GLOBAL SPECTRAL COEFFICIENTS (SPFILE),               
      !          CONVERTS THEM TO GLOBAL GAUSSIAN GRIDS, AND WRITES THEM ON           
      !          FILE GGFILE WITH PACKING DENSITY NPKGG.                              
      !          NOTE - ALL SPECTRAL FIELDS ON THE FILE MUST BE THE SAME SIZE.        
      !                                                                               
      !INPUTS                                                                  
      !      cccdata                                                                         
      !        cccdata object containing SPECTRAL COEFF                                    
      !                                                                               
      !OUTPUTS                                                                 
      !      cccdata_result                                                                         
      !        cccdata object containing GAUSSIAN GRID data                                   
      ! 
      !INPUT PARAMETERS...
      !                                                                               
      !      KUV   = 0 FOR NORMAL ANALYSIS                                            
      !            = 1 TO CONVERT MODEL WINDS TO REAL WINDS                           
      !      NPKGG = GRID PACKING DENSITY (0 DEFAULTS TO 2)                           
      !-------------------------------------------------------------------------- 
 
      ! load diag sizes
      use diag_sizes

      IMPLICIT REAL (A-H,O-Z)
      IMPLICIT INTEGER (I-N)

!     Load diag size values
      integer, parameter :: MAXW = SIZES_MAXLEV*                                 &
                            (2*(SIZES_LA+SIZES_LMTP1)+(SIZES_LONP1+1)*SIZES_LAT) 
      DATA MAXX,MAXLG,MAXL/SIZES_LONP1xLATxNWORDIO, SIZES_MAXLONP1LAT,SIZES_MAXLEV/  

      LOGICAL OK
      INTEGER IB(8,SIZES_MAXLEV) 
      INTEGER LSR(2,SIZES_LMTP1+1),IFAX(10) 
      REAL WRKS(64*SIZES_LONP2), WRKL(SIZES_MAXLEV*MAX(SIZES_LONP1+1,SIZES_LAT))
      REAL TRIGS(SIZES_MAXLONP1LAT)
      REAL*8 ALP(SIZES_LA+(2*SIZES_LMTP1)),EPSI(SIZES_LA+(2*SIZES_LMTP1))
      REAL*8 SL(SIZES_LAT),CL(SIZES_LAT),WL(SIZES_LAT), WOSSL(SIZES_LAT),RAD(SIZES_LAT)
      integer                                           :: it, il, ip  ! loop counter for time, level, points.
      integer                                           :: nt, nl, np  ! number of times, levels, points.
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/W/W(MAXW)
      type(ccc_data_array), intent(in)                  :: cccdata
      type(ccc_data_array), intent(out)                 :: cccdata_result
      real, allocatable, dimension(:)                   :: spec_coeffs
      !---------------------------------------------------------------------
      call timer_start('COFAGG')
      
      write(6,*) ""
      write(6,*) "COFAGG inline on:", cccdata%name
      write(6,*)
      
      NFF=4
!      open(unit=2, file='gg_from_spec', form='UNFORMATTED')
      ! NCS : below is the read of the input card. Replace with simple declaration
      ilg=128  ! number of lons
      ilat=64  ! number of lats
      !kuv=0    ! conversion to "real" winds
      npkgg=2  ! packing density (why not 0?).
      
      IF (ILG+2.GT.MAXLG)   THEN
          WRITE(6,*) "ILG ", ILG, "is too large relative to size MAXLG", MAXLG 
          CALL                                     XIT('COFAGG',-1) 
      ENDIF 

      MAXLG=ILG+2 
      IF(KUV.EQ.1) WRITE(6,*) "Doing conversion to real winds"
      ILATH=ILAT/2
      ILG1=ILG+1
      LGG=ILG1*ILAT 
      IF(NPKGG.EQ.0) NPKGG=2
  
!     * READ THE FIRST SPECTRAL FIELD TO GET THE SIZE (LR,LM).
      NR=0

      ! Instead just get this from the IBUF attribute of the data array
      IBUF = cccdata%IBUF
      IBUF(2) = cccdata%time(1)
      IBUF(4) = cccdata%level(cccdata%nlev)
      
      !CALL PRTLAB (IBUF)
  
!     * FIRST TIME ONLY, SET CONSTANTS FOR SPECTRAL-GRID TRANSFORMATION.
!     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
!     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
  
      LRLMT=IBUF(7) 
      IF(LRLMT.LT.100) CALL FXLRLMT (LRLMT,IBUF(5),IBUF(6),0)
      CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
      CALL PRLRLMT (LA,LR,LM,KTR,LRLMT)
  
      CALL EPSCAL(EPSI,LSR,LM)
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL FTSETUP(TRIGS,IFAX,ILG)
  
!     * FIND HOW MANY SP,GG PAIRS WE CAN FIT (MAX 45).
  
      NWDS=2*LA+LGG 
      MAXLEV=MAXW/NWDS
      IF(MAXLEV.GT.MAXL) MAXLEV=MAXL
      NGG1=2*LA*MAXLEV+1

      write(6,*) "MAXLEV", MAXLEV
      !WRITE(6,*) LR,LM,ILG1,ILAT,MAXLEV
      write(6,*) "ILG1, ILAt, ILEV", ILG1, ILAT, ILEV
      
      !CALL XIT('COFAFF_inline', 0)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
!     * READ AS MANY SPECTRAL FIELDS AS POSSIBLE (2*LA REAL WORDS EACH).

      nt = cccdata%nt
      nl = cccdata%nlev
      np = cccdata%np
      ILEV = nl
      write(6,*) 'COFAGG LA, np, ngg1:', LA, np, ngg1
      
      !allocate(spec_coeffs(nl*np), gg_result(nl*(ilg+1)*ilat))
      !spec_coeffs(:) = 0.0
      !allocate(gg_result((ilg+1)*ilat, nl, nt))
      call cccdata_result%init(cccdata%name, (ilg+1)*ilat, cccdata%time, cccdata%level, IBUF)
      
      do it = 1, nt
          NSP=1
          !W(1:np*nl) = pack(cccdata%data(:,:,it), .true.) ! not much faster
          do il=1,nl
             W(NSP:NSP+np) = cccdata%data(:,il,it)
             !write(6,*), 'COFAGG cccdata', (cccdata%data(:,il,it))
             IBUF = cccdata%IBUF
             IBUF(2) = cccdata%time(it)
             IBUF(4) = cccdata%level(il)
            ! IF (IBUF(5).NE.LA.OR.IBUF(7).NE.LRLMT) then
            !    CALL                                       XIT('COFAGG',-10)
            ! end if   
             DO I=1,8
                IB(I,il)=IBUF(I)                
             end do   
             NSP=NSP+2*LA
          end do   

          ilev = nl
    !      write(6,*) "ILEV before stagg", ilev          
    !     * CONVERT ILEV SPECTRAL FIELDS TO GAUSSIAN GRIDS. 
    !     * NORMAL SPEC FIELDS ARE (LR,LM) BUT WINDS ARE (LRW,LM).
          CALL STAGG (W(NGG1),ILG1,ILAT,ILEV,SL, W(1),LSR,LM,LA, &
                      ALP,EPSI,WRKS,WRKL,TRIGS,IFAX,MAXLG)
    !     * WRITE ALL THE GRIDS ONTO FILE 2.
    !     * KUV=1 CONVERTS MODEL WINDS TO REAL WINDS. 

          NGG=NGG1
          DO il=1,nl
              CALL SETLAB(IBUF,NC4TO8("GRID"),IB(2,il),IB(3,il),IB(4,il),ILG1,ILAT,0,NPKGG)
              IF(KUV.EQ.1) CALL LWBW2(W(NGG),ILG1,ILAT,CL,-1) 
              !CALL PUTFLD2(2,W(NGG),IBUF,MAXX)
              !call cccdata_result%add_data(cccdata%name, W(NGG:NGG+cccdata%np), IBUF(1:8))
              cccdata_result%data(:,il, it) = W(NGG:NGG+cccdata%np)
              cccdata_result%IBUF = IBUF
              !IF(NR.EQ.0) CALL PRTLAB (IBUF)
              NGG=NGG+LGG 
              NR=NR+1 
          END DO
       end do ! time loop
       
    call timer_stop('COFAGG')
    
end subroutine cofagg_inline    

subroutine gsapl_inline(cccdata_input, cccdata_lnsp, cccdata_result)
      ! NCS 2020
      ! Heavily modified from cofagg program
      !
      !PURPOSE - INTERPOLATES FROM ETA (SIGMA/HYBRID) LEVELS TO NPL                   
      !          PRESSURE LEVELS. THE INTERPOLATION IS LINEAR IN LN(SIGMA).           
      !          EXTRAPOLATION UP AND DOWN IS BY LAPSE RATES SPECIFIED BY             
      !          THE USER.                                                            
      !                                                                               
      !INPUTS...                                                                 
      !      cccdata_input                                                                          
      !          cccdata object containing ETA (SIGMA/HYBRID) LEVEL GRID DATA.
      !      cccdata_lnsp
      !          cccdata object containing GRIDS OF LN(SF PRES).                                 
      !                                                                               
      !OUTPUTS                                                                 
      !      cccdata_result                                                                         
      !          cccdata object containing PRESSURE LEVEL GRID DATA.                               
      ! 
      !INPUT PARAMETERS...
      !                                                                               J5
      !      NPL    = NUMBER OF REQUESTED PRESSURE LEVELS, (MAX $L$).                 J5
      !      RLUP   = LAPSE RATE USED TO EXTRAPOLATE UPWARDS.                         J5
      !      RLDN   = LAPSE RATE USED TO EXTRAPOLATE DOWNWARDS.                       J5
      !      ICOORD = 4H SIG/ 4H ETA FOR SIGMA/ETA VERTICAL COORDINATES.              J5
      !      PTOIT  = PRESSURE (PA) AT THE LID OF MODEL.                              J5
      !      PR     = PRESSURE LEVELS (MB)                                            J5
      !---------------------------------------------------------------------------- 

      ! load diag sizes
      use diag_sizes

      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      LOGICAL OK
  
      INTEGER LEV(SIZES_MAXLEV),LEVP(SIZES_MAXLEV),KBUF(8)
  
      REAL ETA(SIZES_MAXLEV)
      REAL A(SIZES_MAXLEV) 
      REAL B(SIZES_MAXLEV)
      
      REAL SIG (SIZES_MAXLEV)
      REAL FSIG(SIZES_MAXLEV)
      REAL DFLNSIG(SIZES_MAXLEV+1)
      REAL DLNSIG(SIZES_MAXLEV)
             
      REAL PR(SIZES_MAXLEV)
      REAL PRLOG(SIZES_MAXLEV)

      integer :: it 
      
      COMMON/BLANCK/F(SIZES_MAXLEVP1xLONP1xLAT)
      COMMON/ICOM  /IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
  
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/,  &
           MAXL/SIZES_MAXLEVP1xLONP1xLAT/, &
           MAXLEV/SIZES_MAXLEV/

      type(ccc_data_array), intent(in)                  :: cccdata_input
      type(ccc_data_array), intent(in)                  :: cccdata_lnsp      
      type(ccc_data_array), intent(out)                 :: cccdata_result
      !---------------------------------------------------------------------
      call timer_stop('gsapl')
      
      write(6,*) ""
      write(6,*) "COFAGG inline on:", cccdata_input%name
      write(6,*)
 
      !open(unit=3, file='gsapl', form='UNFORMATTED')
      !rewind(3)

      ! Must be replaced by namelist reads or subroutine args
      ptoit = 50.0
      icoord = NC4to8("ET15")
      npl =  44                ! requested number of levels
      rlup = 0.0               ! these two should be args to the subroutine
      rldn = 0.0 
      
      levp(1:44) = (/-100,-150,-200,-300,-500,-700,10,15,20,30,50,70,80,90,100,115,125, &
                      130,150,170,175,200,225,250,300,350,400,450,500,550,600,650,700,  &
                      750,775,800,825,850,870,900,925,950,975,1000 /)
              
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0) 
      ELSE
        PTOIT=MAX(PTOIT,SIZES_PTMIN)
      ENDIF 
      IF(NPL.GT.MAXLEV) CALL                       XIT('GSAPL',-1)
      
!     * DECODE LEVELS.
  
      CALL LVDCODE(PR,LEVP,NPL) 

!      CALL WRITLEV(PR,NPL,' PR ')
  
      DO L=2,NPL
          IF(PR(L).LE.PR(L-1)) CALL                    XIT('GSAPL',-2)
      end do
       
      DO L=1,NPL
          PRLOG(L)=LOG(PR(L))
      end do
       
!     * GET ETA VALUES FROM THE GSFLD FILE. 
      NSL =  cccdata_input%nlev   
      LEV(1:NSL) =  cccdata_input%level
      IBUF(1:8) = cccdata_input%IBUF   
      
      IF(NSL.LT.1 .OR. NSL.GT.MAXLEV) CALL         XIT('GSAPL',-3)
      NWDS = IBUF(5)*IBUF(6)
      IF((MAX(NSL,NPL)+1)*NWDS.GT.MAXL) CALL      XIT('GSAPL',-4)
      
      DO I=1,8
          KBUF(I)=IBUF(I)
      end do
       
      CALL LVDCODE(ETA,LEV,NSL)
       
      DO L=1,NSL
          ETA(L)=ETA(L)*0.001E0 
      end do
       
!     * EVALUATE THE PARAMETERS OF THE ETA VERTICAL DISCRETIZATION. 
      CALL COORDAB (A,B, NSL,ETA, ICOORD,PTOIT)
!---------------------------------------------------------------------
!     * GET NEXT SET FROM FILE GSFLD. 
  
      NSETS=0 
      nt = cccdata_input%nt
      np = cccdata_input%np

!      allocate(gg_result(np, npl, nt))
!      write(6,*) 'gsapl sizes', shape(gg_result), shape(cccdata_input%time), size(levp(1:44))
      call cccdata_result%init(cccdata_input%name, np, cccdata_input%time, &
                               levp(1:44), IBUF)
      
      ! Time loop over all times.
      do it = 1, cccdata_input%nt
         IBUF = cccdata_input%IBUF
         IBUF(2) = cccdata_input%time(nt)
         np = cccdata_input%np
         ! Stack all levels into 1D F array
         N=NWDS+1
         do il = 1,cccdata_input%nlev
            N = NWDS+1 + (il-1)*np
             F(N:N+np) = cccdata_input%data(:,il, it)
             IBUF(4) = cccdata_input%level(il)
         end do
              
          CALL CMPLBL (0,IBUF,0,KBUF,OK)
          IF(.NOT.OK) CALL                             XIT('GSAPL',-6)
          NAME=IBUF(3)
          NPACK=IBUF(8) 

    !     * GET LN(SF PRES) FOR THIS STEP, PUT AT BEGINNING OF COMMON BLOCK.

          NST= IBUF(2)
    !      CALL GETFLD2 (2, F ,NC4TO8("GRID"),NST,NC4TO8("LNSP"),1, &
          !                                                IBUF,MAXX,OK)
          F(1:cccdata_lnsp%np) = cccdata_lnsp%data(:,1,it)
          IBUF = cccdata_lnsp%IBUF
          IBUF(2) = cccdata_lnsp%time(it)
          
     !      IF(NSETS.EQ.0) WRITE(6,6035) IBUF 
     !     IF(.NOT.OK) CALL                             XIT('GSAPL',-7)
          CALL CMPLBL (0,IBUF,0,KBUF,OK)
          IF(.NOT.OK) CALL                             XIT('GSAPL',-8)

          !     * INTERPOLATE IN-PLACE FROM ETA TO PRESSURE.
          N=NWDS+1
          CALL EAPL  (F(N), NWDS,PRLOG,NPL, F(N),SIG,NSL ,F(1),RLUP,RLDN, &
                      A,B, NSL+1,FSIG,DFLNSIG,DLNSIG) 

    !     * WRITE THE PRESSURE LEVEL GRIDS ONTO FILE 3. 

          IBUF(3)=NAME
          IBUF(8)=NPACK 
          !CALL PUTSET2 (3,F(N),LEVP,NPL,IBUF,MAXX)

          N=NWDS+1
          do il = 1,NPL
             N = NWDS+1 + (il-1)*np
             IBUF(4) = levp(il)
             !call cccdata_result%add_data(cccdata_input%name, F(N:N+np), IBUF(1:8))
              cccdata_result%data(:,il, it) = F(N:N+np)
              cccdata_result%IBUF = IBUF
          end do

          NSETS=NSETS+1 
      end do ! end if it time loop

    call timer_stop('gsapl')
end subroutine gsapl_inline

    subroutine cwinds_inline(cccdata_vort, cccdata_div, cccdata_ssu, cccdata_ssv)
      ! Heavily modified from cwind program
      !
      !PURPOSE - CONVERTS SPECTRAL VORTICITY, (Q), AND DIVERGENCE, (D), TO            
      !          MODEL WIND COMPONENTS (U,V)COS(LAT)/A.                               
      !          THESE ARE SAVED ON FILES SSU AND SSV.                                
      !          ALL FIELDS ARE GLOBAL.                                               
      !                                                                               
      !INPUTS...                                                                 
      !      cccdata_vort                                                                         
      !          cccdata object containing SPECTRAL VORTICITY
      !      cccdata_div
      !          cccdata object containing SPECTRAL DIVERGENCE                               
      !                                                                               
      !OUTPUTS...                                                                
      !      cccdata_ssu                                                                         
      !          cccdata object containing SSU = SPECTRAL WIND COMPONENT U                           
      !      cccdata_ssu                                                                         
      !          cccdata object containing SSV = SPECTRAL WIND COMPONENT V                           
      !-------------------------------------------------------------------------- 

      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)

      integer, parameter :: MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      ! Q,D,U,V were "complex". Not sure if this is OK!
      real Q,D,U,V 
      COMMON/BLANCK/ Q(SIZES_LA),D(SIZES_LA), U(SIZES_LA+SIZES_LMTP1), &
                    V(SIZES_LA+SIZES_LMTP1)

      LOGICAL OK
      INTEGER LSR(2,SIZES_LMTP1+1)
      REAL*8 EPSI(SIZES_LA+SIZES_LMTP1)

      COMMON/ICOM/IBUF(8),IDAT(MAXX)
      COMMON/JCOM/JBUF(8),JDAT(MAXX)

      type(ccc_data_array), intent(in)                  :: cccdata_vort
      type(ccc_data_array), intent(in)                  :: cccdata_div      
      type(ccc_data_array), intent(out)                 :: cccdata_ssu
      type(ccc_data_array), intent(out)                 :: cccdata_ssv

      integer  :: it, il, np, nl, nt
!---------------------------------------------------------------------
      call timer_start('cwinds')
      
      IBUF = cccdata_vort%IBUF
      JBUF = cccdata_div%IBUF

      !  CALCULATE CONSTANTS. 
!      CALL PRTLAB (IBUF)
!      CALL PRTLAB (JBUF)
      LRLMT=IBUF(7) 
      IF(LRLMT.LT.100) CALL FXLRLMT (LRLMT,IBUF(5),IBUF(6),0)
      CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
      CALL PRLRLMT (LA,LR,LM,KTR,LRLMT)
      CALL EPSCAL(EPSI,LSR,LM)
      LAW=LSR(2,LM+1)-1 
      LRW=LR+1
      CALL FXLRLMT (LRWLMT,LRW,LM,KTR)

      ! Some indices giving sizes of data
      nt = cccdata_vort%nt
      np = cccdata_vort%np
      nl = cccdata_vort%nlev

      do it = 1, nt      ! it time loop
         do il = 1, nl   ! il level loop

            ! Update metadata for the current time and level
            IBUF = cccdata_vort%IBUF
            JBUF = cccdata_div%IBUF

            IBUF(2) = cccdata_vort%time(it)
            IBUF(4) = cccdata_div%level(il)
            JBUF(2) = cccdata_vort%time(it)
            JBUF(4) = cccdata_div%level(il)
            
            ! Assign vort and div records into Q and D arrays
            Q(1:np) = cccdata_vort%data(:,il, it)
            D(1:np) = cccdata_div%data(:,il, it)            

            ! Check for consistency between Q & D fields, probably depracted.
            CALL CMPLBL(0,IBUF,0,JBUF,OK) 
            IF(.NOT.OK)THEN 
              CALL PRTLAB (IBUF)
              CALL PRTLAB (JBUF)
              CALL                                       XIT('CWINDS',-3) 
            ENDIF 

            ! COMPUTE THE WIND COMPONENTS U,V FROM Q,D.
            CALL QDAWG2(U,V,Q,D,EPSI,LSR,LM)

            ! NOW PACK U,V ONTO FILES SSU,SSV.
            IBUF(5)=LAW 
            IBUF(6)=1 
            IBUF(7)=LRWLMT

            IBUF(3)=NC4TO8("   U")
            call cccdata_ssu%add_data("   U", U, IBUF(1:8))
            
            IBUF(3)=NC4TO8("   V")
            call cccdata_ssv%add_data("   V", V, IBUF(1:8))
            
          end do ! il level loop      
       end do ! it time loop
       call timer_stop('cwinds')
       
      end subroutine cwinds_inline

end module transforms

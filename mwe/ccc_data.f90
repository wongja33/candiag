module ccc_data
  ! This module defines the type ccc_data_array
  ! A ccc_data_array is a derived type, which holds
  ! the data and metadata associated with a variable.
  !
  ! ccc_data_arrays:
  !
  ! - contain self-describing metadata, such as dimension sizes.
  ! - stack data for a given variable by time and level. 
  ! - dynamically grow as data is added.
  ! - are used to pass data between subroutines, avoid file i/o.
  !
  ! Various functions provided to construct the data objects,
  ! to clear, and to output to cccma or netcdf format.
  !
  ! A cccdata array is created with e.g.:
  !
  !  type(ccc_data_array)                           :: cccdata_result
  !  call cccdata_result%add_data('ST', st_real_array, IBUF) 
  ! 
  ! - NCS September 2020
  !
  ! TODO
  ! - Introduce a function to grow ccc_data_array dynamically on demand
  !   as in the dimension of the containing array, not the x-y-z-t which is already dynamic. 
  ! - Check that subsequent "adds" of data have a consistent IBUF.
  ! - Introduce functions to transform data, e.g. from 1D to 2D.
  ! - Update to handle multiple levels
  ! - Introduce a "from_ccc" input option
  ! - Try to encapsulate the ccc format reading logic down to the
  !   lowest level, modernising it and allowing allocatable.
  ! - Use interface to generalize / handle different data types.
  ! - NetCDF writes are slow and do not support model levels.
  
  use netcdf
  use diag_sizes
  use utilities, only: timer_start, timer_stop
  
  type, public :: ccc_data_array
  ! This is the definition of the data type   
    character(len=4)                            :: name=''
    real(kind=4), allocatable, dimension(:,:,:)   :: data
    integer, allocatable, dimension(:)          :: time    ! array of times
    integer, allocatable, dimension(:)          :: level   ! array of levels
    integer, dimension(8)                       :: IBUF    ! First IBUF 
    integer                                     :: nt      ! number of timesteps
    integer                                     :: nlev    ! number of levels    
    integer                                     :: np      ! number of grid-points p lev/time
    integer                                     :: nlat   ! number of lats
    integer                                     :: nlon    ! number of lons    
  contains
    procedure :: add_data  => add_data  ! constructor that initializes and/or grows type
    procedure :: init      => init      ! Full initialization without dynamic growth
    procedure :: alloc     => alloc     ! Allocate data size without knowing dim details.
    procedure :: clear     => clear     ! reset / clear instance
    procedure :: to_netcdf => to_netcdf ! output instance to NetCDF format
    procedure :: to_cccma  => to_cccma  ! output instance to CCCma format    
  end type ccc_data_array

contains

  subroutine selectvar(cccdata_array, variable, cccdata_result, status)
    implicit none
    integer iv
    integer, intent(inout)                            :: status
    character (len=4)                                 :: variable
    type(ccc_data_array), dimension(:), intent(in  )  :: cccdata_array
    type(ccc_data_array)              , intent(inout) :: cccdata_result

    do iv = 1, size(cccdata_array)
       if (cccdata_array(iv)%name == variable) then
          cccdata_result = cccdata_array(iv)
          status=0
          write(6,*) "Selected: ", variable
          return
       end if
    end do
    ! If we reach this point, the variable has not been found
    status = -1
    write(6,*) "Variable ", variable, " not found"
  end subroutine selectvar
  
  subroutine clear(self)
    class(ccc_data_array), intent(inout)   :: self

    if(allocated(self%data)) then
       deallocate(self%data)
       deallocate(self%time)
       deallocate(self%level)
    end if
    self%name = ''
    self%IBUF   = 0
    self%nt     = 0
    self%nlev   = 0
    self%nlat   = 0
    self%nlon   = 0    
    self%np     = 0
  end subroutine clear

  subroutine init(self, varname, np, times, levels,IBUF)
    ! This a the fundamental constructor of the cccdata type
    ! It instantiates itself from a provided data array, and
    ! infers it properties in part from input  IBUF
    ! Unlike the add_data routine, the init routine set sizes
    ! from the input, and does not support dynamic growth.
    ! However add_data could conceivably be called after init
    ! to add levels, times etc.
    implicit none
    
    class(ccc_data_array), intent(inout)   :: self
    character(len=*)  , intent(in)         :: varname       !< Name of the variable
!    real, dimension(:,:,:), intent(in)     :: input_data    !> Data for the array
    integer, dimension(:), intent(in)      :: times, levels  
    integer, dimension(8), intent(in)      :: IBUF          !< Regular CCCma IBUF
    integer                                :: np, nt, nlev
    integer                                :: nc4to8
    
    if(.not. allocated(self%data)) then
        ! If unallocated, allocate with size 1 for time
        !np = IBUF(6) * IBUF(5)                   ! number of lat-lon grid points
       !if (IBUF(1) == nc4to8('SPEC')) np = np*2  ! 2x larger for spectral
        !np = size(input_data, 1)
        nt =   size(times)
        nlev = size(levels)

 !       if (size(input_data, 2) /= nlev .OR. size(input_data, 3) /= nt) then
 !          write(6,*) "cccdata init: input data does not match size of times or levels"
 !          return
 !       end if
        
        !        write(*,*) 'allocating first', size(input_data), np
        allocate(self%data(np,nlev,nt))
        allocate(self%time(nt))
        allocate(self%level(nlev))        
        self%time = times
        self%level = levels
!        self%data = 
        self%name = trim(varname)
        self%IBUF = IBUF
        self%nt = nt
        self%np = np
        self%nlev = nlev
        self%nlat = IBUF(6)
        self%nlon = IBUF(5)
     else
        write(6,*) "cccdata init: data already initialized, skipping"
     end if   
   end subroutine init
   
   subroutine alloc(self, varname, np, nlev, nt, IBUF)
     ! This is a f constructor of the cccdata type
     ! Unlike the add_data routine, the alloc routine only
     ! allocates the dimension sizes, without assigning any
     ! values
     implicit none
     class(ccc_data_array), intent(inout)   :: self
     character(len=*)  , intent(in)         :: varname       !< Name of the variable
     integer, dimension(8), intent(in)      :: IBUF          !< Regular CCCma IBUF
     integer                                :: np, nt, nlev
     integer                                :: nc4to8

     if(.not. allocated(self%data)) then
         ! If unallocated, allocate with size 1 for time
         !np = IBUF(6) * IBUF(5)                   ! number of lat-lon grid points
         !if (IBUF(1) == nc4to8('SPEC')) np = np*2  ! 2x larger for spectral
         allocate(self%data(np,nlev,nt))
         allocate(self%time(nt))
         allocate(self%level(nlev))        
         !self%time = times
         !self%level = levels
         self%name = trim(varname)
         self%IBUF = IBUF
         self%nt = nt
         self%np = np
         self%nlev = nlev
         self%nlat = IBUF(6)
         self%nlon = IBUF(5)
      else
         write(6,*) "cccdata alloc: data already allocated, skipping"
      end if   
    end subroutine alloc
    
  subroutine add_data(self, varname, input_data, IBUF)
    ! This is the fundamental constructor of the cccdata type
    ! It instantiates itself from a provided data array, and
    ! infers it properties in part from input  IBUF
    ! The can be mulitple calls to this routine, which will dynamically
    ! grow the data by time and/or level (only time currently supported)
    implicit none
    
    class(ccc_data_array), intent(inout)   :: self
    character(len=*)  , intent(in)         :: varname       !< Name of the variable
    real, dimension(:), intent(in)         :: input_data    !> Data for the array
    real, dimension(:,:,:), allocatable    :: data2         !> Used to grow data dynamically
    integer                                :: tsize         !> num of existing time indices
    integer, dimension(8), intent(in)      :: IBUF          !< Regular CCCma IBUF
    integer, dimension(:), allocatable     :: time2         !? used to grow time
    integer, dimension(:), allocatable     :: level2        !? used to grow level
    integer                                :: grid_size
    integer                                :: ii,il,it
    integer                                :: np, nt, nlev
    logical                                :: grow_time, grow_level, fill_level
    integer                                :: nc4to8
    integer                                :: this_level, this_time
    
    if(.not. allocated(self%data)) then
        ! If unallocated, allocate with size 1 for time
        np = IBUF(6) * IBUF(5)                   ! number of lat-lon grid points
        if (IBUF(1) == nc4to8('SPEC')) np = np*2  ! 2x larger for spectral
!        write(*,*) 'allocating first', size(input_data), np
        allocate(self%data(np,1,1))
        allocate(self%time(1))
        allocate(self%level(1))        
        self%time = IBUF(2)
        self%level = IBUF(4)
        self%data(1:np,1,1) = input_data(1:np)
        self%name = trim(varname)
        self%IBUF = IBUF
        self%nt = 1
        self%np = np
        self%nlev = 1
        self%nlat = IBUF(6)
        self%nlon = IBUF(5)
!        write(6,*) 'Array size', self%name, self%np 
    else
        ! If already allocated, grow the time or level dimension if needed, else fill.
        nt = self%nt
        np = self%np
        nlev = self%nlev

        ! Decide if we need to grow the level or time dimension.
        ! If either the timestep or the level do not already exist, then grow that
        ! dimension by 1. If both already exist, fill data.
        grow_time = .TRUE.
        grow_level = .FALSE.
!        write(*,*) "grow time", nt, nlev, self%time, IBUF(1), IBUF(2), self%name
        do it = 1,nt
           if(self%time(it) == IBUF(2)) then
              grow_time = .FALSE.
              grow_level = .TRUE.
              this_time = it
           end if   
        end do   
        do il = 1,nlev
           if(self%level(il) == IBUF(4)) then
              grow_level = .FALSE.
              this_level = il
           end if   
        end do
        
        fill_level = .FALSE.
        if(.not. grow_time .and. .not. grow_level) then
           ! check if we just need to fill the level
           !if(nlev>1) fill_level = .TRUE.
           fill_level = .TRUE.           
        endif   
        
        if (grow_time) then
            !write(*,*) 'growing time', nlev, nt           
            ! Grow data in time dimension
            allocate(data2(np,nlev,nt+1))    ! new larger array larger in time dim
            do it = 1, nt
               do il = 1,nlev
                  do ii = 1,np
                     data2(ii,il,it) = self%data(ii, il, it) ! swap old data into new
                  end do
               end do
            end do
            data2(:,this_level, nt+1) = input_data(1:np)   ! add new, incoming data
            deallocate(self%data)                 
            call move_alloc(data2, self%data)     ! Return larger array to old name

            ! increment the timestep array
            allocate(time2(nt+1))
            time2(1:nt) = self%time
            time2(nt+1) = IBUF(2)
            deallocate(self%time)
            call move_alloc(time2, self%time)
            self%nt = self%nt + 1 ! increase the time-dimension count
         else if (grow_level) then
           !write(*,*) 'growing levels', nlev, nt   
           ! grow data in level dimension
            allocate(data2(np,nlev+1,nt))      ! new array larger in level dimension
            do it = 1, nt
               do il = 1,nlev
                  do ii = 1,np
                     data2(ii,il,it) = self%data(ii, il, it) ! swap old data into new
                  end do
               end do
            end do
            data2(:,nlev+1, this_time) = input_data(1:np)   ! add new, incoming data
            deallocate(self%data)                 
            call move_alloc(data2, self%data)     ! Return larger array to old name

            ! increment level array
            allocate(level2(nlev+1))
            level2(1:nlev) = self%level
            level2(nlev+1) = IBUF(4)
            deallocate(self%level)
            call move_alloc(level2, self%level)
            self%nlev = self%nlev + 1 ! increase the time-dimension count
         else if (fill_level) then
            ! In this case, the time has already been grown, and all levels exist
            ! so just fill the level. This happens for timestep 2 and on for 3d fields
            !write(*,*) "Fill level" !data2(:,nlev, nt)
            self%data(:,this_level, this_time) = input_data(1:np)   ! add new, incoming data
         else
            write(*,*) 'Repeated record'
            write(*,*) self%time
            write(*,*) self%level
            write(*,*) self%time(it), IBUF(2), self%name, self%level, IBUF(4)
            call prtlab(IBUF)
            write(*,*) ' '
         end if
         ! no need to deallocate data2, time2, level2, as they have been moved.
     end if
  end subroutine add_data

  subroutine to_netcdf(self, file_name)
    ! Write a cccdat object out to netcdf
    ! Currently only supports 3D data (lon-lat-time)
    
    implicit none

    class(ccc_data_array), intent(inout)   :: self

    ! This is the name of the data file we will create.
    character (len = *), intent(in) :: file_name 

    integer :: ncid

    ! We are writing 3D data (lon-lat-lev-time)
    integer, parameter :: NDIMS = 4
    integer  :: NLATS, NLONS, NLEVS, NRECS
    integer  :: rec
    character (len = *), parameter :: LAT_NAME = "lat"
    character (len = *), parameter :: LON_NAME = "lon"
    character (len = *), parameter :: LEV_NAME = "lev"
    character (len = *), parameter :: REC_NAME = "time"
    integer :: lon_dimid, lat_dimid, lev_dimid, rec_dimid

    ! The start and count arrays will tell the netCDF library where to
    ! write our data.
    integer :: start(NDIMS), count(NDIMS)

    ! These program variables hold the latitudes and longitudes.
    real, allocatable, dimension(:) :: lats, lons, levs
    integer :: lon_varid, lat_varid, lev_varid, time_varid

    ! We will create two netCDF variables, one each for temperature and
    ! pressure fields.
    character (len = 4)  :: variable_name
    integer :: varid
    integer :: dimids(NDIMS)

    ! We recommend that each variable carry a "units" attribute.
    character (len = *), parameter :: UNITS = "units"
    character (len = *), parameter :: LAT_UNITS = "degrees_north"
    character (len = *), parameter :: LON_UNITS = "degrees_east"
    character (len = 128)            :: LEV_UNITS
    
    real, dimension(:,:,:), allocatable :: reshaped_data
    real step
    integer i
    real time_hours

    call timer_start('to_netcdf')
    
    ! Assign some variables and allocate as needed
    NRECS = self%nt
    NLONS = self%nlon      
    NLATS = self%nlat
    NLEVS = self%nlev
    
    variable_name = trim(adjustl(self%name))
    write(6,*) "NetCDF name:", variable_name
    allocate(lats(nlats), lons(nlons), levs(nlevs))

    ! hard code grid, only valid for T63 linear grid.
    ! No lons and lats are saved in CCCma files so need to generate.
    ! Should be generalized in the future
    
    step = 360. / nlons
    do i = 1, nlons
       lons(i) = 0 + (i-1)*2.8125 ! This multiplier would change for different grids.
    end do
    step = 180. / nlats
    do i = 1, nlats
       lats(i) = -87.86 + (i-1)*2.767266
    end do

    ! Make the hard and unrealistic assumption we only ever write
    ! pressure level data if nlev>1. This should be generalized to include model levels.
    if (nlevs == 1) then
       levs = 1.0
       lev_units = ""
    else if (nlevs == 44) then
       lev_units = 'hPa'
       levs = (/ 1.,1.5,2.,3.,5.,7.,10.,15.,20.,30.,50.,70.,80.,90.,100.,115.,125., &
                 130.,150.,170.,175.,200.,225.,250.,300.,350.,400.,450.,500.,       &
                 550.,600.,650.,700., 750.,775.,800.,825.,850.,870.,900.,925.,950., &
                 975.,1000. /)
    else
       write(6,*) "Levels unregognized, cannot write netcdf for ", variable_name
       return
    end if
    
    ! reshape to an assumed dimension (needs to be generalized for levels).
!    allocate(reshaped_data(nlons, nlats, nrecs))
!    do rec = 1,nrecs
!       reshaped_data(:,:,rec) = reshape(self%data(:,1,rec),(/nlons,nlats/))
!    end do

    ! Create the file. 
    call check( nf90_create(FILE_NAME, nf90_clobber, ncid) )

    ! Define the dimensions. The record dimension is defined to have
    ! unlimited length - it can grow as needed. In this example it is
    ! the time dimension.
    call check( nf90_def_dim(ncid, LAT_NAME, NLATS, lat_dimid) )
    call check( nf90_def_dim(ncid, LON_NAME, NLONS, lon_dimid) )
    call check( nf90_def_dim(ncid, LEV_NAME, NLEVS, lev_dimid) )    
    call check( nf90_def_dim(ncid, REC_NAME, NF90_UNLIMITED, rec_dimid) )

    ! Define the coordinate variables. We will only define coordinate
    ! variables for lat and lon.  Ordinarily we would need to provide
    ! an array of dimension IDs for each variable's dimensions, but
    ! since coordinate variables only have one dimension, we can
    ! simply provide the address of that dimension ID (lat_dimid) and
    ! similarly for (lon_dimid).
    call check( nf90_def_var(ncid, LAT_NAME, NF90_REAL, lat_dimid, lat_varid) )
    call check( nf90_def_var(ncid, LON_NAME, NF90_REAL, lon_dimid, lon_varid) )
    call check( nf90_def_var(ncid, LEV_NAME, NF90_REAL, lev_dimid, lev_varid) )    
    call check( nf90_def_var(ncid, REC_NAME, NF90_REAL , rec_dimid, time_varid) )
    
    ! Assign units attributes to coordinate variables.
    call check( nf90_put_att(ncid, lat_varid, UNITS, LAT_UNITS) )
    call check( nf90_put_att(ncid, lon_varid, UNITS, LON_UNITS) )
    call check( nf90_put_att(ncid, lev_varid, UNITS, LEV_UNITS) )    
    call check( nf90_put_att(ncid, time_varid,UNITS, 'Days since 0001-01-01') )
    call check( nf90_put_att(ncid, time_varid,'calendar', 'noleap') )    
    ! The dimids array is used to pass the dimids of the dimensions of
    ! the netCDF variables. 
    dimids = (/ lon_dimid, lat_dimid, lev_dimid, rec_dimid /)

    ! Define the netCDF variable
    call check( nf90_def_var(ncid, variable_name, NF90_REAL, dimids, varid) )

    ! Assign units attributes to the netCDF variables.

    ! End define mode.
    call check( nf90_enddef(ncid) )

    ! Write the coordinate variable data. This will put the latitudes
    ! and longitudes of our data grid into the netCDF file.
    call check( nf90_put_var(ncid, lat_varid, lats) )
    call check( nf90_put_var(ncid, lon_varid, lons) )
    call check( nf90_put_var(ncid, lev_varid, levs) )
    
    count = (/ NLONS, NLATS, NLEVS, 1 /)
    start = (/ 1, 1, 1, 1 /)

    ! Write the data.
    write(6,*), 'netcdf write', shape(self%data(:,:, rec))
    do rec = 1, NRECS
       start(4) = rec
       call check( nf90_put_var(ncid, varid, self%data(:,:, rec), start = start, &
            count = count) )
       time_hours = self%time(rec)/(24*4)
!       write(*,*) 'hours:', time_hours
       call check( nf90_put_var(ncid, time_varid, time_hours, start= (/ rec /) ))
    end do

    ! Close the file. This causes netCDF to flush all buffers and make
    ! sure your data are really written to disk.
    call check( nf90_close(ncid) )
    call timer_stop('to_netcdf')
    
  end subroutine to_netcdf  

  subroutine check(status)
    ! check status of netcdf calls
    integer, intent ( in) :: status

    if(status /= nf90_noerr) then 
       print *, trim(nf90_strerror(status))
       stop "Stopped"
    end if
  end subroutine check

  subroutine to_cccma(self, file_name)
    ! Write a cccdat object out to CCCma format
    ! Currently only supports 1D data (linear lat-lon) with no
    ! time stacking (i.e. only single timesteps or means).
    
    implicit none

    class(ccc_data_array), intent(inout)   :: self

    ! This is the name of the data file we will create.
    character (len = *), intent(in) :: file_name 

    !CCC format related
    integer MAXX, K
    integer               :: output_funit ! output file unit    
    integer MACHINE,INTSIZE,ME32O64,INTEFLT,IDUMMY
    INTEGER IBUF,IDAT
    real output_array(SIZES_MAXLEVxBLONP1xBLAT)
    COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
    integer it, il, ii
    
    call timer_start('to_cccma')
    
    ! open the output file for writing
    output_funit=1
    open(unit=output_funit, file=file_name, form='UNFORMATTED', action='WRITE')
    rewind(output_funit)

    IBUF = self%IBUF

    ! Some required variables for low level i/o routines
    MACHINE = ME32O64(IDUMMY)
    INTSIZE = INTEFLT(IDUMMY)
    MAXX=SIZES_BLONP1xBLATxNWORDIO

    do it = 1, self%nt
       do il = 1, self%nlev
           output_array(:) = 0
           IBUF(4) = self%level(il)
           IBUF(2) = self%time(it)
           CALL PUTFLD2(output_funit, self%data(1:self%np,il, it), IBUF, MAXX)
       end do    
    end do
    close(output_funit)
    call timer_stop('to_cccma')
    
  end subroutine to_cccma


  
end module ccc_data

module operations
  ! Apply statistical and other such operations to data. This is where the
  ! processing occurs that would historically have been codified in decks.
  ! TODO:
  !  ccc_data_arrays should grow dynamically
  !  read variables to average from namelist. Also read whether to do other ops or not.
  !  I/O to file should probably be collected into one location, not distributed.
  
  use ccc_data, only: ccc_data_array, selectvar
  use diag_sizes
  use stats, only: time_average
  use transforms, only: cofagg_inline, gsapl_inline, cwinds_inline
  
  implicit none
  private
  public apply_processing
  
contains

  subroutine apply_processing(cccdata_input, cccdata_output, year, month)
    type(ccc_data_array), dimension(:), intent(in)    :: cccdata_input
    type(ccc_data_array), dimension(:), intent(inout) :: cccdata_output
    integer                           , intent(in)    :: year, month
    integer                                           :: ivar
    
    ! Do operations here
    write(*,*) 'Applying operations...'

    call ss_to_gp_temp(cccdata_input, year, month)
    call time_average_2d(cccdata_input, year, month)
    call ss_to_gp_winds(cccdata_input, year, month)
  end subroutine apply_processing

  subroutine ss_to_gp_temp(cccdata_input, year, month)
    ! Convert spectral temperature on model levels to Gausian Grid and
    ! pressure levels, and saves the ouput
    implicit none
    type(ccc_data_array)                              :: sstemp        ! spectral temp
    type(ccc_data_array)                              :: sslnsp        ! spectal lnsp
    type(ccc_data_array)                              :: gstemp        ! gg temp
    type(ccc_data_array)                              :: gslnsp        ! gg lnsp
    type(ccc_data_array)                              :: gptemp        ! ggtemp on plevs  
    type(ccc_data_array), dimension(:), intent(in)    :: cccdata_input ! all resbuilt vars
    integer                                           :: status
    integer                           , intent(in)    :: year, month
    character (len=128)                               :: name

    write(6,*) "ss_to_gp_temp"
    write(6,*) " "
    
    ! Select spectal TEMP and LNSP (log surface pressure)
    call selectvar(cccdata_input, 'TEMP', sstemp, status)
    if (status /= 0) write(6,*) "Failed to select TEMP" 
    
    call selectvar(cccdata_input, 'LNSP', sslnsp, status)
    if (status /= 0) write(6,*) "Failed to select LNSP" 
    
    ! Apply the transform to gausin grid
    call cofagg_inline(sstemp, gstemp, kuv=0) ! TEMP
    call cofagg_inline(sslnsp, gslnsp, kuv=0) ! LNSP    
    call gsapl_inline(gstemp, gslnsp, gptemp)

    ! write out the result to CCCma format (should this be here or in a global return?)
    write (name, "(A, I0.4,I0.2)") trim('gptemp')//'_6hr_', year, month
    name = trim(name)
    call gptemp%to_netcdf(trim(name)//'.nc')
    !call gptemp%to_cccma(trim(name)//'.cccma')
  end subroutine ss_to_gp_temp

  subroutine ss_to_gp_winds(cccdata_input, year, month)
    ! Convert spectral temperature on model levels to Gausian Grid and
    ! pressure levels, and saves the ouput
    implicit none
    type(ccc_data_array)                              :: ssvort        ! spectral divergence
    type(ccc_data_array)                              :: ssdiv         ! spectal vorticity
    type(ccc_data_array)                              :: sslnsp        ! spectal surface p
    type(ccc_data_array)                              :: ssu           ! spectal U
    type(ccc_data_array)                              :: ssv           ! spectal V
    type(ccc_data_array)                              :: gslnsp        ! Gausian pressure
    type(ccc_data_array)                              :: gsu           ! Gausian U on mod lev
    type(ccc_data_array)                              :: gsv           ! Gausian V on mod lev
    type(ccc_data_array)                              :: gpu           ! Gausian U on plev
    type(ccc_data_array)                              :: gpv           ! Gausian V on plev
    type(ccc_data_array), dimension(:), intent(in)    :: cccdata_input ! all rebuilt vars
    integer                                           :: status
    integer                           , intent(in)    :: year, month
    character (len=128)                               :: name

    write(6,*) "ss_to_gp_winds"
    write(6,*) " "
    
    ! Select spectal vort and div
    call selectvar(cccdata_input, 'VORT', ssvort, status)
    call selectvar(cccdata_input, ' DIV', ssdiv, status)
    call selectvar(cccdata_input, 'LNSP', sslnsp, status)

    ! Convert to real winds
    call cwinds_inline(ssvort, ssdiv, ssu, ssv) 
    
    ! Apply the transform to gausin grid
    call cofagg_inline(ssu, gsu, kuv=1) 
    call cofagg_inline(ssv, gsv, kuv=1)
    call cofagg_inline(sslnsp, gslnsp, kuv=0)
    
    call gsapl_inline(gsu, gslnsp, gpu)
    call gsapl_inline(gsv, gslnsp, gpv)
    
    ! write out the result to CCCma format (should this be here or in a global return?)
    write (name, "(A, I0.4,I0.2)") trim('gpu')//'_6hr_', year, month
    name = trim(name)
    call gpu%to_netcdf(trim(name)//'.nc')
    !call gpu%to_cccma(trim(name)//'.cccma')

    write (name, "(A, I0.4,I0.2)") trim('gpv')//'_6hr_', year, month
    name = trim(name)
    !call gptemp%to_netcdf(trim(name)//'.nc')
    !call gpv%to_cccma(trim(name)//'.cccma')
  end subroutine ss_to_gp_winds
  
  subroutine time_average_2d(cccdata_input, year, month)
    ! Do time averaging of selected 2D surface fields
    implicit none
    type(ccc_data_array), dimension(:), intent(in)    :: cccdata_input  ! all resbuilt vars 
    type(ccc_data_array), dimension(:), allocatable   :: cccdata_to_ave !
    type(ccc_data_array), dimension(:), allocatable   :: cccdata_from_ave !    
    integer                                           :: status, cumstatus
    integer                                           :: nv, iv, ic
    character(len=4)    , dimension(2)                :: variables
    integer                           , intent(in)    :: year, month
    character (len=128)                               :: name
    character (len=4)                                 :: varname

    write(6,*) "time_average_2d"
    write(6,*) " "

   ! do iv = 1, size(cccdata_input)
   !    write(6,*) cccdata_input(iv)%name
   ! end do
    
    variables = (/ '  ST', 'CLDT' /)    

    !nv = size(variables)
    nv = size(cccdata_input)
    allocate(cccdata_to_ave(nv), cccdata_from_ave(nv))

    ic = 1 ! index counter for successful selects
    
    do iv = 1, size(cccdata_input)
       call selectvar(cccdata_input, cccdata_input(iv)%name, cccdata_to_ave(ic), status)
       if (status == 0) then
          ic = ic + 1
       else   
          write(6,*) "Error selecting ", cccdata_input(iv)%name, " for 2D time average"
       end if
    end do
    
    if (ic > 1) then
        call time_average(cccdata_to_ave(1:ic-1), cccdata_from_ave(1:ic-1))
     end if
     
    do iv = 1, ic - 1
       varname = trim(cccdata_from_ave(iv)%name)
       write (name, "(A, I0.4,I0.2)") trim(adjustl(varname))//'_mon_', year, month
       name = trim(name)
       call cccdata_from_ave(iv)%to_netcdf(trim(name)//'.nc')
       !call cccdata_from_ave(iv)%to_cccma(trim(name)//'.cccma')
    end do
  end subroutine time_average_2d
  
end module operations

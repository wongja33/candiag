module utilities
  ! NCS 2020 basic utilities for timing etc
  implicit none
  
  private
  public timer_start, timer_stop
  
  real, save :: t1, t2
  integer, save :: timer_index=0
  character (len=4) :: name

  
  type, private :: named_timer
     character(len=1024)    :: name=''
     real                   :: t1, t2
  end type named_timer
  
  type(named_timer), dimension(999) :: timer_array
  
  contains

    
    subroutine timer_start(name)
      character (len=*), intent(in) :: name
      integer                       :: status
      type(named_timer)             :: this_timer
      
      call cpu_time(t1)

 !      call get_timer(name, timer_array, this_timer, status)
!       if (status == 0) then
!          write(6,*) "TIMER ERROR: already started timer under name: ", name
!       else
!          timer_index= timer_index + 1
!          timer_array(timer_index)%name=name
!          timer_array(timer_index)%t1=t1
!       end if   
    end subroutine timer_start
    
    subroutine timer_stop(name)
      character (len=*), intent(in) :: name
      integer                       :: status
      type(named_timer)             :: this_timer
      
      call cpu_time(t2)
      write(6,"(A12,A1,F10.2)") trim(name),':', t2 -t1

      ! call get_timer(name, timer_array, this_timer, status)
      ! if (status == -1) then
      !    write(6,*) "TIMER ERROR: No timer started under name: ", name
      ! else
      !    this_timer%t2=t2
      !    write(6,"(A12,A1,F10.2)") 'dt'//trim(name),':', this_timer%t2 - this_timer%t1
      ! end if   
    end subroutine timer_stop

    subroutine get_timer(name, timer_array, named_timer_result, status)
      implicit none
      integer                                           :: iv
      integer                        , intent(out)      :: status
      character (len=*)                                 :: name
      type(named_timer), dimension(:), intent(in  )     :: timer_array
      type(named_timer)              , intent(out)      :: named_timer_result

      do iv = 1, size(timer_array)
         if (timer_array(iv)%name == name) then
            named_timer_result = timer_array(iv)
            status=0
            return
         end if
      end do
      ! If we reach this point, the named timer has not been found
      status = -1
  end subroutine get_timer
      
end module utilities

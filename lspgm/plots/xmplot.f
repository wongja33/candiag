      PROGRAM XMPLOT
C     PROGRAM XMPLOT (XIN,       INPUT,       OUTPUT,                   )       A2
C    1          TAPE1=XIN, TAPE5=INPUT, TAPE6=OUTPUT,
C    2                                  TAPE8       )
C     -----------------------------------------------                           A2
C
C     FEB 02/10 - F.MAJAESS (ADD "NCURRCOLRESET" TO "CCCAGCHCU" COMMON BLOCK)   A2
C     NOV 20/07 - F.MAJAESS (MAKE "INTPR" COMMON BLOCK DECLARATION CONSISTENT   
C                            WITH THAT IN THE OTHER SOURCE CODE)                
C     AUG 23/07 - B.MIVILLE (NEW FONT, MORE COLOR LINES)                        
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)                            
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS, TEST, TEST, TEST)
C     AUG 31/96 - T. GUI (UPDATE TO NCAR GRAPHICS V3.2)
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)           
C     NOV 19/93 - F.MAJAESS (USE DEFAULT TENSION, POSSIBLY INTERPOLATE)         
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)                    
C     SEP 22/93 - M. BERKLEY (changed to save data files instead of            
C                             generating meta code.)                          
C     SEP  1/92 - T. JANG   (add format to write to unit 44)                 
C     JUL 20/92 - T. JANG  (changed variable "MAX" to "MAXX" so not to
C                           conflict with the generic function "MAX")
C     JUL  4/91 - S. WILSON (ADD LABEL FOR SCIENTIFIC NOTATION, CHANGE X-AXIS
C                            LABELS FOR ZONAL PLOTS AND CHANGE DASH PATTERNS)
C     OCT  9/90 - S. WILSON (CONVERT TO AUTOGRAPH PLOTTING ROUTINES)
C     APR  1/90 - C. BERGSTEIN (INCLUDE FT44 AAASOVL) TO CONTROL OVERLAYS )
C     SEP 21/89 - F.MAJAESS (MODIFIED TO HANDLE BOTH ZONAL AND MERIDIONAL FLDS)
C     JUL 14/89 - H.REYNOLDS (ADAPT TO NCAR GRAPHICS V2.0)
C     NOV 06/86 - F.ZWIERS                                                      A2
C                                                                               A2
CXMPLOT  - UP TO 26 SINGLE LEVEL MERIDIONAL CROSS SECTION CURVES        1  1 C GA1
C                                                                               A3
CAUTHOR  - F.ZWIERS                                                             A3
C                                                                               A3
CPURPOSE - PLOTS UP TO 26 GLOBAL ZONALLY AVERAGED SINGLE LEVEL FIELDS           A3
C          VERSUS LATITUDE.                                                     A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      XIN = FILE CONTAINING CURVES TO BE PLOTTED.                              A3
C
CINPUT PARAMETERS...
C                                                                               A5
C      CARD 1:                                                                  A5
C      -------                                                                  A5
C      READ(5,5010,END=900) NSTEP,NAME,LEVEL,YLO,YHI,MS,NC,NZMS,IOPT            A5
C 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,3I5,14X,I1)                               A5
C                                                                               A5
C      NSTEP   = TIMESTEP NUMBER                                                A5
C      NAME    = NAME OF VARIABLE TO BE PLOTTED                                 A5
C                THE NAMES NC4TO8('NEXT') AND NC4TO8('SKIP') ARE RECOGNIZED     A5
C                BUT FILE XIN IS REWOUND BEFORE EACH SEARCH.  THIS ALLOWS       A5
C                FIELDS TO BE IN ANY ORDER ON THE FILE, BUT NC4TO8('NEXT')      A5
C                ALWAYS SELECTS THE FIRST FIELD AND NC4TO8('SKIP') IS A NULL    A5
C                OPERATION.                                                     A5
C      LEVEL   = LEVEL NUMBER                                                   A5
C      YLO,YHI = LOWER,UPPER LIMITS OF VERTICAL Y-AXIS.                         A5
C                IF YLO = YHI, THEN THE RANGE WILL AUTOMATICALLY BE             A5
C                DETERMINED BY EXAMINING THE DATA.  THE NUMBER OF MAJOR         A5
C                TICK MARKS WILL VARY, DEPENDING ON THE RANGE OF THE            A5
C                PLOT.  BUT, THERE IS ALWAYS 1 MINOR TICK MARK BETWEEN          A5
C                THE MAJOR TICK MARKS.                                          A5
C      MS      = PRINTER PLOT PARAMETER.                                        A5
C                IF MS.NE.0 IT PRODUCES A LINE PRINTER GRAPH.                   A5
C      NC      = NUMBER OF CURVES/PLOT (DEFAULT = 1, MAX = 26)                  A5
C      NZMS    = 0/1 FOR ZONAL/MERIDIONAL PLOT.                                 A5
C      IOPT    = 8, DRAW CURVES IN COLOUR, USING SHADES ON CARD 3.              A5
C                                                                               A5
C      LABEL   = 80 CHARACTER LABEL PRINTED ABOVE PLOT.                         A5
C                                                                               A5
C      CARD 2:                                                                  A5
C      -------                                                                  A5
C      READ(5,5012,END=902)(LABEL(I),I=1,60),(JAX(J),J=1,20)                    A5
C 5012 FORMAT(60A1,20A1)                                                        A5
C                                                                               A5
C      TITLE = 60 CHAR. MAIN LABEL OF PLOT                                      A5
C      JAX   = 20 CHAR. Y AXIS LABEL                                            A5
C                                                                               A5
C      NOTE - ALL LEADING AND TRAILING SPACES TO THE LABELS ARE STRIPPED        A5
C             AND THEY ARE CENTERED AUTOMATICALLY.                              A5
C                                                                               A5
C      CARD 3:                                                                  A5
C      -------                                                                  A5
C                                                                               A5
C      IOPT .NE. 0 THEN NEED CARD 3;CURVES CAN BE COLOURED. UP TO 26 COLORS.    A5
C                                                                               A5
C      READ(5,5050) NCURVCOL,(ICURVCOL(I),I=1,7)                                A5
C 5050 FORMAT(10X,1X,I4,7I5)                                                    A5
C      ...                                                                      A5
C      READ(5,5052) (ICURVCOL(I),I=21,NCURVCOL)                                 A5
C 5052 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      NCURVCOL           NUMBER OF COLOURS (1 <= NCURVCOL <= 26)               A5
C      ICURVCOL(NCURVCOL) NCURVCOL COLOUR SHADING PATTERN CODES                 A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C* XMPLOT         -1 NEXT  500    -1000.     1500.    0   20    0              8A5
C* FJ - JAN Z(500) - 12W TO 21W                                                 A5
C             2  100  120                                                       A5
C-----------------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_MAXBLONP1BLAT,
     &                       SIZES_NWORDIO
      integer, parameter :: 
     & MAXX = max(SIZES_BLONP1,SIZES_BLAT)*SIZES_NWORDIO

      LOGICAL OK,LAT
      CHARACTER*63 ALABEL
      CHARACTER*1 LABEL(63),JAX(23)
      CHARACTER PAT(26)*16
      CHARACTER*8 PLTNAM
      CHARACTER*23 AJAX
      INTEGER MAXCRV
      EQUIVALENCE (JAX,AJAX)
      REAL SBN(11)

      REAL X(SIZES_MAXBLONP1BLAT), 
     & Y(SIZES_MAXBLONP1BLAT,26), YHI, YLO

C     SHADES FOR COLOUR CURVES
      LOGICAL COLOURCURVES
      INTEGER NCURVCOL,MAXNCURVCOL,ICURVCOL(26)
      INTEGER IPATNE,NCURRCOLRESET
      COMMON /CCCAGCHCU/ COLOURCURVES,NCURVCOL,MAXNCURVCOL,ICURVCOL,
     1     PAT,IPATNE,NCURRCOLRESET

C     Array to pass user defined colours to dfclrs - unused in XMPLOT
      REAL HSVV(3, 16)

C     * ARRAYS FOR GETTING INFO ABOUT THE NUMERIC LABELS AND SCALING OF THE
C     * Y AXIS.
      REAL NMLBDT(11), TCKDAT(3), FSCALE

C     * STRINGS FOR THE SCALING LABEL.
      CHARACTER STRING*24, TMP*8

C     * SPECIAL VALUE
      REAL SPV
      PARAMETER (SPV=1.0E38)

C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(1)
      PARAMETER (LUNOFFSET=10,
     1     LUNFIELD1=LUNOFFSET+1)

      COMMON/BLANCK/ G(SIZES_MAXBLONP1BLAT)
      COMMON /ICOM/ IBUF(8),IDAT(MAXX)
C
C     *NCAR COMMON WHICH CONTROLS SMOOTHING PARAMETERS INCLUDING TENSION
C
      COMMON /INTPR/
     1     IPAU,FPART,TENSN,NP,SMALL,L1,ADDLR,ADDTB,MLLINE,ICLOSE
      INTEGER IPAU,NP,L1,MLLINE,ICLOSE
      REAL FPART,TENSN,SMALL,ADDLR,ADDTB

C     * COMMON BLOCK FOR PCHIQU - NEEDED TO CHANGE FONTS
      COMMON /PUSER/ MODE

C
C     * COMMON BLOCK TO TELL AGCHNL WHICH PROGRAM IS CALLING IT AND
C     * WHAT TYPE OF X-AXIS (ZONAL/MERIDIONAL) IS BEING DRAWN.
C
      COMMON / AGNMLB / PLTNAM, LAT

C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI
C
      EXTERNAL AGCHCU
      EXTERNAL AGCHNL
      EXTERNAL AGPWRT
C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL AGCHCU_BD
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD
C
C     * COMMON BLOCK FOR SETTING LINE THICKNESSES.
C
      COMMON /AGLW/ LWBG, LWMJ, LWMN

C
C    * NCAR COMMON
      COMMON /DASHD1/  NCP1(2),  ISIZE,  NCP2(203)

C     * THESE VARIABLES ARE FOR DRAWING THE ZERO LINE.
      REAL UCOORD(4), FXLO, FXHI, FYLO, FYHI

      EQUIVALENCE (LABEL,ALABEL)
      EQUIVALENCE (UCOORD(1), FXLO), (UCOORD(2), FXHI)
      EQUIVALENCE (UCOORD(3), FYLO), (UCOORD(4), FYHI)

      DATA IMAXX/SIZES_MAXBLONP1BLAT/,MAXCRV/26/
C
C     * SET TRESHOLD FOR THE MINIMUM NUMBER OF POINTS
C     * PASSED TO NCAR PLOTTING ROUTINES.
C
      MINPTS=144
C
C     * INITIALIZE COLOURCURVES TO FALSE
      COLOURCURVES=.FALSE.
C
C
C     * SET NCAR TENSN TO TURN CURVE SMOOTHING OFF
C
C     TENSN=9.0

C     * CHANGE DEFAULT MAX SEGMENT LENGTH
C
      MLLINE=43

C
C     * SET THE PLOT NAME FOR AGCHNL AND SET THE LINE THICKNESSES FOR
C     * AGCHCU.
C
      PLTNAM = 'XMPLOT'
      LWBG = 2000
      LWMJ = 4000
      LWMN = 2000

      NFF=3
      CALL JCLPNT(NFF,LUNFIELD1,5,6)

C     * OPEN NCAR GRAPHICS PACKAGE
      CALL PSTART
C     * TELL PCHIQU TO USE DUPLEX CHARACTER FONT
      CALL PCSETI('FN', 21)
      CALL PCSETI('OF', 1)
      CALL PCSETI('OL', 1)
      CALL PCSETR('CL',.2)

      CALL BFCRDF(0)
C     * DOUBLE THE LINE WIDTH
      CALL SETUSV('LW', LWBG)
      CALL GSLWSC(2.0)

C     * SET THE LENGTH OF THE DASH-PATTERN STRINGS & THE SIZE OF THE
C     * CHARACTERS DRAWN IN THE DASHED-LINES.
      CALL AGSETF('DASH/LENGTH.', 16.0)
      CALL AGSETF('DASH/CHARACTER.', 0.020)

C     * SET SPECIAL VALUE
      CALL AGSETF('NULL/1.',SPV)

C     *  MAKE THE NUMERIC LABELS BIGGER.
C     *  (NOTE:  THIS DOES NOT SET THE CHARACTERS TO A FIXED SIZE.
C     *  IF THE NUMBERS BECOME TOO LONG, EZMXY WILL SHRINK THE
C     *  CHARACTER SIZES SO THAT THEY FIT.)
      CALL AGSETF('LEFT/WIDTH/MANTISSA.', 0.02)
      CALL AGSETF('LEFT/WIDTH/EXPONENT.', 0.013333)
      CALL AGSETF('BOTTOM/WIDTH/MANTISSA.', 0.02)
      CALL AGSETF('BOTTOM/WIDTH/EXPONENT.', 0.013333)

C     * MAKE THE TOP LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'T')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C     * MAKE THE BOTTOM LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'B')
      CALL AGSETF('LINE/NUMBER.', -100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C     * SET UP THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C     * SCIENTIFIC NOTATION.
      CALL AGSETC('LABEL/NAME.', 'SCALE')
      CALL AGSETF('LABEL/BASEPOINT/X.', 0.0)
      CALL AGSETF('LABEL/BASEPOINT/Y.', 0.9)
      CALL AGSETF('LABEL/OFFSET/X.', -0.015)
      CALL AGSETF('LABEL/ANGLE.', 90.0)

C     * MAKE THE SCALE LABEL BIGGER.
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C
C     * SHUT OFF INFORMATION LABEL SCALING.  AUTOGRAPH WILL ONLY BE ABLE TO
C     * PREVENT INFORMATION LABELS FROM OVERLAPPING BY MOVING THEM.
C
      CALL AGSETF('LABEL/CONTROL.', 1.0)


C     * SUPPRESS THE FRAME ADVANCE SO THAT THE ZERO LINE CAN BE DRAWN
C     * IF NEEDED.
      CALL AGSETF('FRAME.', 2.)


      REWIND LUNFIELD1
      NPLOT=0

C     * READ THE CONTROL CARDS.
C     * SIMULATE A 'WHILE NOT(EOF)' STATEMENT
  150 READ(5,5010,END=900) NSTEP,NAME,LEVEL,YLO,YHI,MS,NC,NZMS,IOPT            

C
C        * IF WE ARE DRAWING A ZONAL PLOT, SET LAT TO .TRUE.
C
         IF (NZMS .EQ. 0) THEN
              LAT = .TRUE.
         ELSE
              LAT = .FALSE.
         ENDIF


         IF(NC.LT.1) NC=1
         IF(NC.GT.MAXCRV) NC=MAXCRV
C        * READ MAIN TITLE
         READ(5,5012,END=902)(LABEL(I),I=1,60),(JAX(J),J=1,20)                  
         WRITE (6,6012) (LABEL(I),I=1,60),(JAX(J),J=1,20)
C
         CALL SHORTN(JAX, 23, .TRUE., .TRUE., JLEN)
C
C          * CHECK IF A COLOURED CURVES ARE TO BE PRODUCED.
         IF(IOPT.EQ.8) THEN
            COLOURCURVES=.TRUE.
              READ(5,5050) NCURVCOL,(ICURVCOL(I),I=1,7)
            IF(NCURVCOL.GT.7.AND.NCURVCOL.LE.14) THEN
              READ(5,5052) (ICURVCOL(I),I=8,NCURVCOL)
            ELSEIF(NCURVCOL.GT.14.AND.NCURVCOL.LE.21) THEN
              READ(5,5052) (ICURVCOL(I),I=8,14)
              READ(5,5052) (ICURVCOL(I),I=15,NCURVCOL)
            ELSEIF(NCURVCOL.GT.21.AND.NCURVCOL.LE.26) THEN
              READ(5,5052) (ICURVCOL(I),I=8,14)
              READ(5,5052) (ICURVCOL(I),I=15,21)
              READ(5,5052) (ICURVCOL(I),I=21,NCURVCOL)
           ENDIF
            CALL DFCLRS(-NCURVCOL,HSVV,ICURVCOL)
         ENDIF


         NPLOT=NPLOT+1

C        * GET THE FIELD.

         IBASE=0
         DO 170 J=1,NC
            CALL GETFLD2(LUNFIELD1,
     1           G,NC4TO8('ZONL'),NSTEP,NAME,LEVEL,IBUF,MAXX,OK)
            IF(.NOT.OK) THEN
               IF(J.EQ.1)THEN
                  WRITE(6,6010) NAME,NSTEP,LEVEL
                  CALL                             PXIT('XMPLOT',-101)
               ELSE
                  NCRV=J-1
                  GOTO 175
               ENDIF
            ENDIF
            IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)

C        * COPY INTO ARRAY Y, (REVERSING ARRAY ELEMENTS FOR LATITUDES).

            NPTS=IBUF(5)
            IF (NZMS .EQ. 0) THEN
                 DO 160 I = 1, NPTS
                      Y(I, J) = G(NPTS - I + 1)
  160            CONTINUE
            ELSE
                 DO 165 I = 1, NPTS
                      Y(I, J) = G(I)
  165            CONTINUE
            ENDIF
            NCRV=J

  170    CONTINUE
  175    CONTINUE
         NC=NCRV

         IF (NZMS .EQ. 0) THEN
              DO 180 I = 1, NPTS
                   X(I) = 90.0 - (180.0 * (I - 1)) / (NPTS - 1)
  180         CONTINUE
         ELSE
              DO 185 I = 1, NPTS
                   X(I) = (360.0 * (I - 1)) / (NPTS - 1)
  185         CONTINUE
         ENDIF

C
C        *  DOUBLE RESOLUTION BY INTERPOLATION IF THE NUMBER OF
C        *  POINTS TO PLOT IS LESS THAN MINPTS.
C
         NOPTS=NPTS
  200    IF ( NPTS .GE. MINPTS ) GO TO 300

         DO 270 J=1,NC
           Y(2*NPTS-1,J)=Y(NPTS,J)
           DO 260 I = NPTS-1, 1, -1
            IF ( Y(I,J).NE.SPV .AND. Y(I+1,J).NE.SPV) THEN
              Y(2*I,J)   = 0.5*(Y(I,J)+Y(I+1,J))
            ELSE
              Y(2*I,J)   = SPV
            ENDIF
            Y(2*I-1,J) = Y(I,J)
  260      CONTINUE
  270    CONTINUE
         X(2*NPTS-1)=X(NPTS)
         DO 280 I = NPTS-1, 1, -1
          X(2*I)   = 0.5*(X(I)+X(I+1))
          X(2*I-1) = X(I)
  280    CONTINUE
         NPTS=2*NPTS-1
         GO TO 200
C
C        *  CHANGE THE LINE-END CHARACTER TO '!'.
C
  300    CALL AGSETC('LINE/END.','!')

C
C        *  TURN ON WINDOWING AND DO NOT FORCE ENDPOINTS AT MAJOR
C        *  TICK MARKS IF A SPECIFIC RANGE HAS BEEN REQUESTED
C        *  OTHERWISE, SET YHI AND YLO TO 'NULL 1' SO AUTOGRAPH WILL
C        *  CALCULATE THE ACTUAL MIN AND MAX VALUES FOR Y.
C
         IF (YHI .GT. YLO) THEN
              CALL AGSETI('WINDOWING.', 1)
              CALL AGSETF('Y/NICE.', 0.0)
         ELSE
              CALL AGSETI('WINDOWING.', 0)
              CALL AGSETF('Y/NICE.', -1.0)
              YHI = SPV
              YLO = SPV
         ENDIF
         CALL AGSETF('Y/MAXIMUM.', YHI)
         CALL AGSETF('Y/MINIMUM.', YLO)

C
C        *  SET THE X MIN AND MAX
C
         CALL AGSETI('BOTTOM/NUMERIC/TYPE.',3)
         CALL AGSETI('BOTTOM/NUMERIC/FRAC.',-1)
         CALL AGSETI('BOTTOM/NUMERIC/EXPO.',0)
         CALL AGSETI('X/NICE.', 0)
         IF (NZMS .EQ. 0) THEN
              CALL AGSETI('X/ORDER.', 1)
              CALL AGSETF('X/MAXIMUM.', 90.0)
              CALL AGSETF('X/MINIMUM.', -90.0)
         ELSE
              CALL AGSETF('X/MAXIMUM.', 360.0)
              CALL AGSETF('X/MINIMUM.', 0.0)
         ENDIF

C
C        *  SET BOTTOM AXIS PARAMETERS
C
         

C
C        *  SET THE LEFT AND RIGHT TICKS
C
         CALL AGSETI('LEFT/MINOR/SPACING.', 1)
         CALL AGSETI('RIGHT/MINOR/SPACING.', 1)

C
C        *  SET THE TOP AND BOTTOM TICKS
C
         IF (NZMS .EQ. 0) THEN
              CALL AGSETF('TOP/MAJOR/BASE.', 30.0)
              CALL AGSETF('BOTTOM/MAJOR/BASE.', 30.0)
              CALL AGSETI('TOP/MINOR/SPACING.', 2)
              CALL AGSETI('BOTTOM/MINOR/SPACING.', 2)
         ELSE
              CALL AGSETF('TOP/MAJOR/BASE.', 30.0)
              CALL AGSETF('BOTTOM/MAJOR/BASE.', 30.0)
              CALL AGSETI('TOP/MINOR/SPACING.', 0)
              CALL AGSETI('BOTTOM/MINOR/SPACING.', 0)
         ENDIF
         CALL AGSETF('TOP/NUMERIC/TYPE.', 0.0)

C
C        *  SET UP THE BOTTOM AND LEFT LABELS (THE LEFT LABEL IS JUST
C        *  A SPACE) AND SET THE LINE PATTERNS IN THE @ANOTAT' CALL.
C
         IF(COLOURCURVES) THEN
            NPAT=0
         ELSE
            NPAT=MAXCRV
         ENDIF

         IF (NZMS .EQ. 0) THEN
              CALL ANOTAT('LATITUDE!',AJAX(4:JLEN)//'!',0,0,NPAT,PAT)
           ELSE
              CALL ANOTAT('LONGITUDE!',AJAX(4:JLEN)//'!',0,0,NPAT,PAT)

              CALL AGSETC('LABEL/NAME.','EAST')
              CALL AGSETF('LABEL/BASEPOINT/X.', .999999)
              CALL AGSETF('LABEL/BASEPOINT/Y.', 0.0)
              CALL AGSETF('LABEL/OFFSET/Y.', -0.015)
              CALL AGSETI('LINE/NUMBER.', -100)
              CALL AGSETF('LINE/CHARACTER.', 0.020)
              CALL AGSETC('LINE/TEXT.', 'E!')
         ENDIF

C
C        *  CHANGE THE MAXIMIMUM LINE LENGTH.
C
         CALL AGSETF('LINE/MAXIMUM.', 80.0)



C
C        * RUN AGSTUP TO MAKE AUTOGRAPH CALCULATE THE NUMERIC LABEL
C        * SETTINGS.  WE CAN CHECK THE 'SECONDARY' PARAMETERS TO MAKE
C        * SURE THE THE LABELS WILL NOT USE SCIENTIFIC NOTATION OR HAVE
C        * LABELS OF THE FORM 0.00001, 0.00002, 0.00003, ETC.  WHERE THERE
C        * ARE TOO MANY ZEROS.
C
         CALL AGSTUP(X,1,IMAXX,NPTS,1,Y,NC,IMAXX,NPTS,1)

C
C        * GET THE NUMERIC LABEL INFORMATION ON THE Y AXIS.
C
         CALL AGGETP('SECONDARY/LEFT/NUMERIC.', NMLBDT, 11)
         CALL AGGETP('SECONDARY/LEFT/TICKS.', TCKDAT, 3)

C
C        * GET THE USER COORDINATES OF THE GRID WINDOW.
C
         CALL AGGETP('SECONDARY/USER.', UCOORD, 4)
         IF (FYLO * FYHI .NE. 0) THEN
              FSCALE = MAX(ANINT(LOG10(ABS(FYLO))),
     1                       ANINT(LOG10(ABS(FYHI))))
         ELSE
              FSCALE = ANINT(LOG10(MAX(ABS(FYLO), ABS(FYHI))))
         ENDIF


C
C        * IF (THE NUMERIC LABEL USES SCIENTIFIC NOTATION) OR (THE
C        * NUMERIC LABEL *DOESN'T* USE SCIENTIFIC NOTATION *AND*
C        * THE LARGEST ABSOLUTE VALUE IS LESS THAN 0.01) THEN
C        * SCALE THE DATA ACCORDINGLY AND SHOW THE SCALING LABEL
C        * ALONG THE Y AXIS.
C
         IF ((NMLBDT(1) .EQ. 2.0 .AND. TCKDAT(1) .EQ. 1.0) .OR.
     1       (NMLBDT(1) .EQ. 3.0 .AND. FSCALE .LT. -2.0)) THEN

C
C             * IF SCIENTIFIC NOTATION WAS USED, THE LOG OF THE
C             * SCALING FACTOR IS STORED IN NMLBDT(2) (WHICH IS ACTUALLY
C             * THE VALUE OF 'LEFT/EXPONENT.')
C
              IF (NMLBDT(1) .EQ. 2.0 .AND. TCKDAT(1) .EQ. 1.0) THEN
                   WRITE(TMP, 6000) INT(NMLBDT(2))
                   FSCALE = 10.0**(-1.0*NMLBDT(2))

C
C             * IF SCIENTIFIC NOTATION WASN'T USED, THEN WE'VE ALREADY
C             * CALCULATED THE LOG OF THE SCALING FACTOR IN FSCALE.
C
              ELSE
                   WRITE(TMP, 6000) INT(FSCALE)
                   FSCALE = 10.0**(-1.0*FSCALE)
              ENDIF


C
C             * HERE IS WHERE WE SCALE THE DATA.
C
              DO 410 J = 1, NC
                   DO 400 I = 1, NPTS
                        IF (Y(I, J) .NE. SPV) THEN
                             Y(I, J) = Y(I, J)*FSCALE
                        ENDIF
  400              CONTINUE
  410         CONTINUE

C
C             * CHANGE THE YHI AND YLO SETTINGS IF THEY WERE SPECIFIED
C             * BY THE USER
C
              IF (YLO .NE. SPV) THEN
                   CALL AGSETF('Y/MAXIMUM.', YHI*FSCALE)
                   CALL AGSETF('Y/MINIMUM.', YLO*FSCALE)
              ENDIF



C
C             * SET NUMERIC LABEL TYPE TO 'NO-EXPONENT' NOTATION.
C
              CALL AGSETF('LEFT/TYPE.', 3.)

C
C             * TURN ON THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C             * SCIENTIFIC NOTATION.
C
              CALL AGSETC('LABEL/NAME.', 'SCALE')
              CALL AGSETF('LABEL/SUPPRESSION.', 0.0)

C
C             * SET THE TEXT OF THE LABEL.
C
              CALL AGSETI('LINE/NUMBER.', 100)
              CALL SHORTN(TMP, 8, .TRUE., .TRUE., ILEN)
              STRING='''L1''X10''H-7'''//TMP(4:ILEN)//'''H7''!'
              CALL AGSETC('LINE/TEXT.', STRING)


         ELSE
C
C             * MAKE SURE THE SCALE LABEL IS TURNED OFF.
C
              CALL AGSETC('LABEL/NAME.', 'SCALE')
              CALL AGSETF('LABEL/SUPPRESSION.', 1.0)

         ENDIF


C
C        * FIX UP THE TOP LABEL SO EZXY WILL CENTER IT PROPERLY
C        * AND DO SOME CRUDE SCALING TO MAKE IT FIT IF IT@S TOO LONG.
C
         CALL SHORTN(LABEL, 63, .TRUE., .TRUE., ILEN)
         IF (ILEN - 3 .GT. 40) THEN
              CALL AGSETC('LABEL/NAME.', 'T')
              CALL AGSETF('LINE/NUMBER.', 100.0)

              IF (ILEN - 3 .LE. 60) THEN
                   CALL AGSETF('LINE/CHARACTER.', 0.020)
              ELSE
                   CALL AGSETF('LINE/CHARACTER.', 0.015)
              ENDIF
         ENDIF

C
C        * PLOT THE CURVE
C
         CALL EZMXY(X, Y, IMAXX, NC, NPTS, ALABEL(4:ILEN)//'!')

C        * ACTIVATE "NCURRCOLRESET=1" LINE,IF NEED BE,TO 
C        * SETUP FOR RESETTING THE CURVE PATTERNS 

C        NCURRCOLRESET=1
C
         CALL GSPLCI(1)
C
C        * GET THE USER COORDINATES OF THE GRID WINDOW.
         CALL AGGETP('SECONDARY/USER.', UCOORD, 4)

C        * DRAW Y=0 LINE IF 0 IS IN THE RANGE
         IF(FYLO*FYHI .LE. 0.0) THEN
              CALL LINE (FXLO, 0.0, FXHI, 0.0)
         ENDIF

C        *  ADVANCE TO THE NEXT FRAME & RESET ALL THE GRID POINTS TO
C        *  UNDRAWN.
         CALL FRAME
         CALL RESET


C        * IF MS.NE.0 DRAW A GRAPH ON THE PRINTER.
CCCC         IF(MS.NE.0)THEN
CCCC  750     IF(NPTS.EQ.NOPTS) GO TO 800
CCCC          IF(NPTS.LT.NOPTS) CALL               PXIT('XMPLOT',-102)
CCCC          NNPTS=(NPTS+1)/2
CCCC          DO 780 J=1,NC
CCCC           DO 775 I=1,NNPTS
CCCC            Y(I,J)=Y(2*I-1,J)
CCCC  775      CONTINUE
CCCC  780     CONTINUE
CCCC          DO 785 I=1,NNPTS
CCCC           X(I)=X(2*I-1)
CCCC  785     CONTINUE
CCCC          NPTS=NNPTS
CCCC          GO TO 750
CCCC
CCCC  800      CALL SPLAT(Y,IMAXX,NC,NPTS,1,YLO,YHI)
CCCC           WRITE(6,6012) (LABEL(I),I=4,83)
CCCC         ENDIF




C        * E.O.F. ON INPUT.  A.K.A. 'END WHILE LOOP'
         GO TO 150
  900 CONTINUE
      IF(NPLOT.EQ.0) CALL                          PXIT('XMPLOT',-1)
      CALL                                         PXIT('XMPLOT',0)
  902 CALL                                         PXIT('XMPLOT',-2)
C-----------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,3I5,14X,I1)                                
 5012 FORMAT(60A1,20A1)                                                         
 5050 FORMAT(10X,1X,I4,7I5)                                                     
 5052 FORMAT(15X,7I5)                                                           
 6000 FORMAT(I8)
 6010 FORMAT(' UNABLE TO FIND-',A4,' AT TIMESTEP-',I10,' AND LEVEL-',
     1       I5)
 6012 FORMAT(' ',60A1,' ',20A1)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
      END

      PROGRAM BLT
C     PROGRAM BLT(CORD21,       VARM1,       VARO1,                             A2
C                 CORD22,       VARM2,       VARO2,                             A2
C                 CORD23,       VARM3,       VARO3,                             A2
C                 CORD24,       VARM4,       VARO4,                             A2
C                               INPUT,      OUTPUT,                     )       A2
C    1     TAPE11=CORD21, TAPE12=VARM1, TAPE13=VARO1,
C    2     TAPE14=CORD22, TAPE15=VARM2, TAPE16=VARO2,
C    3     TAPE17=CORD23, TAPE18=VARM3, TAPE19=VARO3,
C    4     TAPE20=CORD24, TAPE21=VARM4, TAPE22=VARO4,
C                         TAPE5=INPUT, TAPE6=OUTPUT,)         
C     -----------------------------------------------                           A2
C                                                                               A2
C     AUG 23/07 - B.MIVILLE - ORIGINAL VERSION                                  A2
C                                                                               A2
CBLT     - PRODUCES A BLT PLOT                                         14  1 C  A1
C                                                                               A3
CAUTHOR  - B. MIVILLE                                                           A3
C                                                                               A3
CPURPOSE - PRODUCES A BLT PLOT                                                  A3
C                                                                               A3
CINPUT FILES...                                                                 A3
C                                                                               A3
C   CORD21 = CORRELATION OR MEAN SQUARE DIFFERENCE (N1 POINTS)                  A3
C   VARM1  = VARIANCE OF MODEL (STANDARD DEVIATION SQUARE) (N1 POINTS)          A3
C   VARO1  = VARIANCE OF OBSERVATIONS (STANDARD DEVIATION SQUARE) (1 POINT      A3
C            DEFAULT OR IF IVAROB=1 FROM INPUT CARD THAN N1 POINTS)             A3
C   CORD22 = CORRELATION OR MEAN SQUARE DIFFERENCE (N2 POINTS)                  A3
C   VARM2  = VARIANCE OF MODEL (STANDARD DEVIATION SQUARE) (N2 POINTS)          A3
C   VARO2  = VARIANCE OF OBSERVATIONS (STANADRD DEVIATION SQUARE) (1 POINT      A3
C            DEFAULT OR IF IVAROB=1 FROM INPUT CARD THAN N2 POINTS)             A3
C   CORD23 = CORRELATION OR MEAN SQUARE DIFFERENCE (N3 POINTS)                  A3
C   VARM3  = VARIANCE OF MODEL (STANDARD DEVIATION SQUARE) (N3 POINTS)          A3
C   VARO3  = VARIANCE OF OBSERVATIONS (STANDARD DEVIATION SQUARE) (1 POINT      A3
C            DEFAULT OR IF IVAROB=1 FROM INPUT CARD THAN N3 POINTS)             A3
C   CORD24 = CORRELATION OR MEAN SQUARE DIFFERENCE (N4 POINTS)                  A3
C   VARM4  = VARIANCE OF MODEL (STANDARD DEVIATION SQUARE) (N4 POINTS)          A3
C   VARO4  = VARIANCE OF OBSERVATIONS (STANDARD DEVIATION SQUARE) (1 POINT      A3
C            DEFAULT OR IF IVAROB=1 FROM INPUT CARD THAN N4 POINTS)             A3
C                                                                               A3
COUTPUT FILES...                                                                A3
C                                                                               A3
C   PRODUCES A GMETA FILE THAT CAN BE LOOKED AT WITH IDT OR NIDT                A3
C                                                                               A3
CCARDS READ...                                                                  A5
C                                                                               A5
C      CARD 1- MAIN CARD - NEEDS TO BE PRESENT                                  A5
C      ------                                                                   A5
C                                                                               A5
C      READ(5,5000) IINPUT,IVAROB,ICOLOR,ILABEL,IGROUP,                         A5
C     1             IG(1),IG(2),IG(3),IG(4),ISCALE                              A5
C 5000 FORMAT(11X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1)     A5
C                                                                               A5
C      IINPUT     FILE TYPE FOR FOR FILE CORD2                                  A5
C                 = 0 FILE CORD2 WILL BE THE CORRELATIONS (DEFAULT)             A5
C                 = 1 FILE CORD2 WILL BE THE MEAN SQUARE DIFFERENCE.            A5
C                                                                               A5
C                 THE INPUT FILES CAN BE NORMALIZED OR NOT (FIELDS PRE-DIVIDED  A5
C                 BY THE OBSERVED VARIANCE MEANING THAT THE FILE VARO WOULD BE  A5
C                 ONLY ONES BUT CORRELATION WOULD REMAIN AS IS.                 A5
C                                                                               A5
C      IVAROB     RECORD DESCRIPTION FOR FILE VARO                              A5
C                 = 0 INPUT FILE VARO HAS ONLY ONE RECORD (DEFAULT)             A5
C                 = 1 INPUT FILE VARO HAS SAME NUMBER OF RECORDS AS VARM        A5
C                                                                               A5
C      ICOLOR     COLOR OF DIAGRAM                                              A5
C                 = 0 BLACK AND WHITE (DEFAULT)                                 A5
C                 = 1 COLOR GRAPH                                               A5
C                                                                               A5
C      ILABEL     LABEL TYPE FOR POINTS IN DIAGRAM                              A5
C                 = 0 NO LABEL DRAWN (DEFAULT)                                  A5
C                 = 1 DRAWS INCREASING NUMBER FOR A SAME GROUP                  A5
C                 = 2 DRAWS NAME BASED ON INPUT CARD                            A5
C                 = 3 INTERCONNECTS POINTS BY A LINE FOR A SAME GROUP AND       A5
C                     WRITES INCREASING NUMBER BESIDE MARKERS.                  A5
C                                                                               A5
C      IGROUP     NUMBER OF GROUPS TO BE PLOTTED                                A5
C                 = 1 to 4                                                      A5
C                 = 0 NO NAME OR MARKERS WILL BE PLOTTED. NO NEED TO            A5
C                     HAVE INPUT CARDS FOR THE NAME IT WILL NOT BE READ.        A5
C                     THE BACKGROUND LINES WILL BE PLOTTED.                     A5
C                                                                               A5
C      IG(1 TO 4) SYMBOL TO BE USED BY A GROUP:                                 A5
C                 = 1 CIRCLE (MARKERS SHAPE)                                    A5
C                 = 2 TRIANGLE                                                  A5
C                 = 3 SQUARE                                                    A5
C                 = 4 PLUS SIGN                                                 A5
C                 = 5 TRANSPARENT CIRCLE FOR TESTING PURPOSES                   A5
C                                                                               A5
C      ISCALE     FORMAT AND SCALE OF DIAGRAM                                   A5
C                 = 0  ORIGINAL FORMAT (100% Y AXIS SCALE)                      A5
C                 = 1  ORIGINAL FORMAT (200% Y AXIS SCALE)                      A5
C                 = 2  MODIFIED FORMAT (100% Y AXIS SCALE)                      A5
C                 = 3  MODIFIED FORMAT (150% Y AXIS SCALE)                      A5
C                 = 4  MODIFIED FORMAT (150% Y AXIS SCALE, SQUARE DIAGRAM)      A5
C                 = 5  MANUAL FORMAT. IT WILL READ AN EXTRA LINE RIGHT AFTER    A5
C                      INPUT CARD ONE FOR "RY" AND "RX":                        A5
C                                                                               A5
C                      READ(5,5020) RY,RX                                       A5
C                 5020 FORMAT(10X,2E10.0)                                       A5
C                                                                               A5
C                      RY= MAXIMUM VALUE OF Y AXIS (RATIO OF VARIANCE) IN       A5
C                          PERCENT                                              A5
C                      RX= MAXIMUM VALUE OF RATIO OF VARIANCE IN PERCENT        A5
C                          CROSSING THE X-AXIS.                                 A5
C                                                                               A5
C      CARD 2- TITLE                                                            A5
C      ------                                                                   A5
C                                                                               A5
C      READS THE TOP TITLE OF THE GRAPHIC (1 LINE ALLOWED FOR NOW)              A5
C      CARD DOES NOT NEED TO BE PRESENT WHEN NO NAME CARDS ARE PROVIDED.        A5
C                                                                               A5
C      READ(5,5005) (TOPLABEL(I),I=1,68)                                        A5
C 5005 FORMAT(11X,68A1)                                                         A5
C                                                                               A5
C      TOPLABEL   = NAME OF TOP TITLE (UP TO 68 CHARACTERS)                     A5
C                                                                               A5
C      CARD 3 AND UP- MARKERS NAME                                              A5
C      -------------                                                            A5
C                                                                               A5
C      READS THE NAME OF THE MARKER IN SAME ORDER AS IN THE FILES CORD2 FILE    A5
C      NUMBER OF NAMES NEED TO MATCH NUMBER OF RECORDS IN INPUT FILES.          A5
C                                                                               A5
C      READ(LINEIN,5012) MG                                                     A5
C 5012 FORMAT(11X,I1)                                                           A5
C                                                                               A5
C      READ(LINEIN,5014) AMNAME(MG,IC),  MCOLOR(MG,IC),  MPOS(MG,IC),           A5
C     1                AMNAME(MG,IC+1),MCOLOR(MG,IC+1),MPOS(MG,IC+1),           A5
C     2                AMNAME(MG,IC+2),MCOLOR(MG,IC+2),MPOS(MG,IC+2),           A5
C     3                AMNAME(MG,IC+3),MCOLOR(MG,IC+3),MPOS(MG,IC+3)            A5
C 5014 FORMAT(13X,A10,1X,I1,1X,A2,1X,A10,1X,I1,1X,A2,1X,A10,1X,I1,              A5
C     1        1X,A2,1X,A10,1X,I1,1X,A2)                                        A5
C                                                                               A5
C      THE GROUP NUMBER MG IS READ FIRST FROM THE LINE OF NAMES. EACH LINE      A5
C      MAY HAVE UP TO 4 NAMES BELONGING TO THE SAME GROUP NUMBER.               A5
C                                                                               A5
C      MG         GROUP NUMBER (ONE PER LINE OF 4 MEMBERS)                      A5
C      AMNAME     NAME OF THE MARKER TO BE PLOTTED (MAXIMUM 10 CHARACTERS)      A5
C      MCOLOR     COLOR OF THE TEXT                                             A5
C                 = 0  NOT PLOTTED                                              A5
C                 = 1  BLACK                                                    A5
C                 = 2  RED                                                      A5
C                 = 3  GREEN                                                    A5
C                 = 4  BLUE                                                     A5
C                 = 5  PURPLE                                                   A5
C      MPOS       POSITION OF THE TEXT RELATIVE TO THE MARKER (RIGHT JUSTIFIED) A5
C                 =  R RIGHT OF MARKER DEFAULT                                  A5
C                 =  L LEFT OF MARKER                                           A5
C                 =  T TOP OF MARKER                                            A5
C                 =  B BOTTOM OF MARKER                                         A5
C                 = BL BOTTOM LEFT OF MARKER                                    A5
C                 = BR BOTTOM RIGHT OF MARKER                                   A5
C                 = TL TOP LEFT OF MAKER                                        A5
C                 = TR TOP RIGHT OF MARKER                                      A5
C                                                                               A5
C                         T                                                     A5
C                       TL TR                                                   A5
C                      L  +  R                                                  A5
C                       BL BR                                                   A5
C                         B                                                     A5
C                                                                               A5
CEXAMPLES OF INPUT CARDS...                                                     A5
C                                                                               A5
C THIS EXAMPLE PLOTS TWO GROUPS                                                 A5
C                                                                               A5
C*     BLT 0 0 1 2 2 1 2 0 0 0                                                  A5
C*     BLT Sea Surface Temperature                                              A5
C*     BLT 1        MRI 1        NCARWM 2         CSIRO 1  B       BMRC 1       A5
C*     BLT 1     ECHAM4 2  B    NCARCSM 1          UKMO 1          GFDL 1  R    A5
C*     BLT 1     ECHAM3 1  T     CCCMA2 2  B     CCCMA1 1  T                    A5
C*     BLT 2        MRI 1  L     NCARWM 2 BR      CSIRO 1  B       BMRC 1 BL    A5
C*     BLT 2     ECHAM4 2  B    NCARCSM 1  L       UKMO 1  L       GFDL 1  R    A5
C*     BLT 2     ECHAM3 1  T     CCCMA2 2 TL     CCCMA1 1  R                    A5
C                                                                               A5  
C THIS EXAMPLE USES THE MANUAL OPTION FOR THE SCALE AND FORMAT                  A5 
C                                                                               A5
C*     BLT 0 1 1 2 2 1 2 0 0 5                                                  A5
C*     BLT    150.0     200.0                                                   A5
C*     BLT BLT EXAMPLE USING ISCALE=5                                           A5
C*     BLT 1      CCCMA 1          CCRS 1 TR    CERFACS 2          COLA 2  T    A5
C*     BLT 1      CSIRO 1 BR       GFDL 1  L      GISSM 2  L      GISSR 2 UR    A5
C*     BLT 1        LMD 2       MPIE3/L 1  B    MPIE4/O 1  L        MRI 1       A5
C*     BLT 1    NCARCSM 2        NCARWM 2  L       UKMO 1  L       MEAN 1       A5
C------------------------------------------------------------------------------
C
C     * DEFINE ERROR FILE, FORTRAN UNIT NUMBER AND WORKSTATION TYPE AND ID.
C
      use diag_sizes, only : SIZES_NWORDIO

      PARAMETER (IERRF=6, LUNIT=2, IWTYPE=1, IWKID=1)
C
C     * GRAPHIC PARAMETERS AND ARRAYS
C
      PARAMETER (NPTS=101, NGRPS=1, NC=200)
      INTEGER IAREA(NGRPS),IGRP(NGRPS)
      REAL X1(NPTS),Y1(NPTS),Y2
      REAL XC(NC),YC(NC),XM1,YM1,XCN,X,Y
      REAL RY,RX
C
C     * CORRELATION LINES TO BE DRAWN
C
      REAL RCOR(7),RANG(7)
      DATA RCOR / 0.99 , 0.95 , 0.90 , 0.80 , 0.70 , 0.50, 0.25 /
C
C     * INPUT/OUTPUT FILES PARAMETERS
C
      PARAMETER (IMX=300,JMX=7000,IJM=IMX*JMX,IJMV=IJM*SIZES_NWORDIO)
      LOGICAL OK
      INTEGER IBUF,IDAT,MAXX
      COMMON/ICOM/IBUF(8),IDAT(IJMV)
      DATA MAXX/IJMV/
      REAL VARO(20000),VARM(20000),CORD2(20000)
C
C     * INPUT CARD PARAMETERS
C
      CHARACTER*1 TOPLABEL(68),CMNAME(10)
      CHARACTER*75 TOPT
      CHARACTER*2 BLANK,T,L,R,B,BL,BR,TL,TR,MPOS(4,200)
      CHARACTER*10 AMNAME(4,200),CMNAMET,B10
      CHARACTER*80 LINEIN
      CHARACTER*7 RTXT
      EQUIVALENCE (TOPLABEL,TOPT),(CMNAME,CMNAMET)
      INTEGER MCOLOR(4,200),IG(4),ICOUNT(4),ICF(4)
      
C
C     EXTERNAL FILL
      EXTERNAL GNCRCL
      EXTERNAL GNTRG
C
C     * CONVERT DEGREES TO RADIANS FOR DRAWING CIRCLES.
C
      DATA D2R / .017453292519943 /

C
C-------------------------------------------------------------------------
C
      NF=14
      CALL JCLPNT(NF,11,12,13,14,15,16,17,18,19,20,21,22,5,6)
      REWIND 11
      REWIND 12
      REWIND 13
      REWIND 14
      REWIND 15
      REWIND 16
      REWIND 17
      REWIND 18
      REWIND 19
      REWIND 20
      REWIND 21
      REWIND 22
C
C     * TEST VALUES
C
      TOPT=' '
      T=' T'
      B=' B'
      L=' L'
      R=' R'
      TL='TL'
      TR='TR'
      BL='BL'
      BR='BR'
      BLANK='  '
      B10='          '
C
C     * DEFAULT VALUES
C
      ITITLE=0
      IMNAME=0
      ILABEL=0
      ISCALE=0
      IGROUP=0
      IINPUT=0
      IVAROB=0
      ICOLOR=0
      ICOUNT(1)=0
      ICOUNT(2)=0
      ICOUNT(3)=0
      ICOUNT(4)=0
      ICF(1)=0
      ICF(2)=0
      ICF(3)=0
      ICF(4)=0
      IG(1)=1
      IG(2)=2
      IG(3)=3
      IG(4)=4
      ICL1=1
      ICL2=1
      ICL3=1
      ICL4=1
C
C     * READ INPUT PARAMETERS FROM INPUT CARD
C
      READ(5,5000,END=900) IINPUT,IVAROB,ICOLOR,ILABEL,IGROUP,
     1             IG(1),IG(2),IG(3),IG(4),ISCALE
C
      IF(ISCALE.EQ.5) THEN
         READ(5,5020) RY,RX
      ENDIF
C
C     * READ TOP TITLE
C
      READ(5,5005,END=350) (TOPLABEL(I),I=1,68)
C
C     * COUNTER
C
      IC=0
      MGOLD=0
      IMG=0
C
C     * READ MARKERS NAME WILL SKIP IF NOT PROVIDED
C
 300  READ(5,5010,END=350) LINEIN
      IMNAME=1
      READ(LINEIN,5012) MG
      IF(MG.NE.MGOLD) THEN
         IF((MGOLD.NE.0).AND.(IB.NE.1)) ICOUNT(MGOLD)=IC
         IC=1
         IMG=IMG+1
      ELSE
         IC=IC+4
      ENDIF
      IB=0
      READ(LINEIN,5014) AMNAME(MG,IC),MCOLOR(MG,IC),MPOS(MG,IC),
     1            AMNAME(MG,IC+1),MCOLOR(MG,IC+1),MPOS(MG,IC+1),
     2            AMNAME(MG,IC+2),MCOLOR(MG,IC+2),MPOS(MG,IC+2),
     3            AMNAME(MG,IC+3),MCOLOR(MG,IC+3),MPOS(MG,IC+3)
      IF(AMNAME(MG,IC).EQ.B10)THEN
         IF(IC.EQ.1)THEN
            ICOUNT(MG)=0
         ELSE
            ICOUNT(MG)=IC
         ENDIF
         IB=1
         GOTO 300
      ELSEIF(AMNAME(MG,IC+1).EQ.B10)THEN
         ICOUNT(MG)=IC
         IB=1
         GOTO 300
      ELSEIF(AMNAME(MG,IC+2).EQ.B10)THEN
         ICOUNT(MG)=IC+1
         IB=1
         GOTO 300
      ELSEIF(AMNAME(MG,IC+3).EQ.B10)THEN
         ICOUNT(MG)=IC+2
         IB=1
         GOTO 300
      ELSE
         MGOLD=MG
         GOTO 300
      ENDIF
C
 350  CONTINUE
C
      IF(IB.NE.1)ICOUNT(MG)=IC+3
C
C     * CHECK IF NAMES WERE PROVIDED WHEN ILABEL=2
C
      IF((ILABEL.EQ.2).AND.(IMNAME.EQ.0)) THEN
         WRITE(6,*) 'NO NAMES PROVIDED FOR MARKERS, CHANGE ILABEL'
         CALL                                      XIT('BLT',-1)
      ENDIF
C
C     * CHANGED DEFAULT COLOR SCHEME BASED ON ICOLOR=1
C
      IF(ICOLOR.EQ.1)THEN
         ICL1=4
         ICL2=3
         ICL3=2
         ICL4=5
      ELSEIF(ICOLOR.EQ.0) THEN
         DO J=1,IGROUP
            DO I=1,ICOUNT(J)
               MCOLOR(J,I)=1
            ENDDO
         ENDDO
      ENDIF
C
C     * OPEN GKS, OPEN AND ACTIVATE WORKSTATION.
C
      CALL PSTART
      CALL BFCRDF(0)
C      CALL GOPKS (IERRF, ISZDM)
C      CALL GOPWK (IWKID, LUNIT, IWTYPE)
C      CALL GACWK (IWKID)
C
C     * CREATE COLOR INDEX
C
      CALL SETUSV('PB',2)
C
C     * WHITE BACKGROUND
C
      CALL GSCR (IWKID,0,1.,1.,1.)
C
C     * BLACK FOREGROUND
C
      CALL GSCR (IWKID,1,0.,0.,0.)
C
C     * RED
C
      CALL GSCR (IWKID,2,1.,0.,0.)
C
C     * GREEN
C
      CALL GSCR (IWKID,3,0.,1.,0.)
C
C     * BLUE
C 
      CALL GSCR (IWKID,4,0.,0.,1.)
C
C     * PURPLE
C 
      CALL GSCR (IWKID,5,1.,0.,1.)
C
C     * SET UP PLOTTING SPACE PARAMETERS
C
C     * UNITS SPACE
      XMN=0.0
      XMX=1.0
      YMN=0.0
      YMX=0.75
      SC=YMX/SQRT(100.0)
C     * PLOTTING COORDINATE RESTRICTED SPACE
      VXMN=0.1
      VXMX=0.9
      VYMN=0.2
      VYMX=0.8
      IF(ISCALE.EQ.1)THEN
         SC=YMX/SQRT(200.0)
         XMN=0.35
         VXMN=0.38
      ELSEIF(ISCALE.EQ.3)THEN
         SC=YMX/SQRT(150.0)         
      ELSEIF(ISCALE.EQ.4)THEN
         SC=YMX/SQRT(150.0)         
         XMN=0.25
         VXMN=XMN*(VXMX-VXMN)+VXMN
      ELSEIF(ISCALE.EQ.5) THEN
         SC=YMX/SQRT(RY)
         XMN=XMX-(SQRT(RX)*(YMX-YMN)/SQRT(RY))
         VXMN=XMN*(VXMX-VXMN)+VXMN
      ENDIF
C
C
      CALL SET(VXMN,VXMX,VYMN,VYMX,XMN,XMX,YMN,YMX,1)
      CALL CPSETI('SET',0)
      CALL GSCLIP (1)
C
C     * DRAW CORRELATION LINES
C
      CALL GSPLCI(ICL4)
C
C     * SET LINE WIDTH 
C 
      CALL GSLWSC(1.) 
C
C     * DEFINE COORDINATES OF LINES
C
      NLINES=6
C
C     * NUMBER OF CORRELATION LINES 
C
      IF((ISCALE.EQ.2).OR.(ISCALE.EQ.3).OR.(ISCALE.EQ.4).OR.
     1   (ISCALE.EQ.5)) NLINES=7
C
      DO I=1,NLINES
C
C        * GET ANGLE FROM CORRELATION
C
         ANG=ACOS(RCOR(I))
         RANG(I)=360.0-ANG/D2R
         Y2=((XMX-XMN)/RCOR(I))*SIN(ANG)
         IF(Y2.GT.YMX) THEN
            X2=XMX-YMX/TAN(ANG)
            Y2=YMX
         ELSE
            X2=XMN
         ENDIF
Cwrite(6,*) 'COR ',I,NLINES,RCOR(I),ANG,D2R,RANG(I),XMX,XMAX,
C     1                Y2,SIN(ANG),ACOS(RCOR(I))

         write(6,*) 'COR ', I,XMX,YMN,X2,Y2
C
C        * DRAW LINES
C
         CALL LINE(XMX,YMN,X2,Y2)
C
      ENDDO
C
C     * DRAW THE MEAN SQUARE DIFFERENCE CIRCLES (X AXIS)
C
C     * CENTER OF CIRCLE
C
      XCN=XMX-SQRT(100.0)*SC
C
      CALL GSPLCI(ICL1)
C
      RAD=SQRT(10.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(25.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(50.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(100.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(150.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(200.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(250.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(300.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(350.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
      RAD=SQRT(400.0)*SC
      CALL GNCRCL(XCN,YMN,RAD,NPTS,X1,Y1)
      CALL CURVED(X1,Y1,NPTS)
C
C     * DRAW THE RATIO OF VARIANCES CIRCLES (Y AXIS)
C
      CALL GSPLCI(ICL2)
      CALL GSLN(2)
      RAD=SQRT(400.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(350.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(300.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(250.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(200.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(150.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      CALL GSLWSC(5.) 
      RAD=SQRT(100.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      CALL GSLWSC(1.) 
      RAD=SQRT(50.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
      RAD=SQRT(25.0)*SC
      CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
      CALL CURVE(X1,Y1,NPTS)
C
C     * NEED EXTRA CIRCLE IF CORRELATION SPACE IS REDUCED
C
      IF((ISCALE.EQ.2).OR.(ISCALE.EQ.3).OR.(ISCALE.EQ.4).OR.
     1    (ISCALE.EQ.5))THEN
         RAD=SQRT(10.0)*SC
         CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
         CALL CURVED(X1,Y1,NPTS)
      ENDIF         
C
C     * DRAW FILL CIRCLE IN LOWER RIGHT CORNER
C     * THIS IS TO ERASE ANY LINES PASSING IN CIRCLE
C
      IF((ISCALE.EQ.0).OR.(ISCALE.EQ.1))THEN
         RAD=SQRT(25.0)*SC     
         CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
         CALL GSFACI(0)
         CALL GSFAIS(1)
         CALL GFA(NPTS,X1,Y1)
C
C        * DRAW OUTER EDGE OF CIRCLE
C
         CALL GSPLCI(ICL2)
         CALL CURVED(X1,Y1,NPTS)
         CALL GSLN(1)
      ELSEIF((ISCALE.EQ.2).OR.(ISCALE.EQ.3).OR.(ISCALE.EQ.4).OR.
     1    (ISCALE.EQ.5))THEN
         RAD=SQRT(1.0)*SC     
         CALL GNCRCL(XMX,YMN,RAD,NPTS,X1,Y1)
         CALL GSFACI(0)
         CALL GSFAIS(1)
         CALL GFA(NPTS,X1,Y1)
C
C        * DRAW OUTER EDGE OF CIRCLE
C
         CALL GSPLCI(ICL2)
         CALL CURVED(X1,Y1,NPTS)
         CALL GSLN(1)
C
      ENDIF

C
C     * DRAW BACKGROUND PERIMETER 
C
      CALL GSPLCI(1)
      CALL FRSTPT( XMN, YMN)
      CALL VECTOR( XMX, YMN)
      CALL VECTOR( XMX, YMX)
      CALL VECTOR( XMN, YMX)
      CALL VECTOR( XMN, YMN)
C
C     * RED CIRCLE AT ZERO
C
      CALL GSCLIP(0)
      DELTA=0.01
      CALL GSFACI(ICL3)
      IF(XCN.GE.XMN.AND.XCN.LE.XMX) THEN
         CALL GNCRCL(XCN,YMN,DELTA,NPTS,X1,Y1)
         CALL GSFAIS(1)
         CALL GFA(NPTS,X1,Y1)
      ENDIF
      CALL GSCLIP (1)
C
C     * NO MORE CLIPPING OUTSIDE FRAME
C
      CALL GSCLIP(0)
C
C     * REGULAR BLT GRAPH WITH SCALE AT 100 ON Y AXIS
C
      IF(ISCALE.EQ.0)THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL GSFACI(ICL4)
         CALL PLCHHQ(0.654,0.04,':F21:99',0.017, 0., 0.)
         CALL PLCHHQ(0.67,0.10,':F21:95',0.017, 0., 0.)
         CALL PLCHHQ(0.69,0.15,':F21:90',0.017, 0., 0.)
         CALL PLCHHQ(0.72,0.21,':F21:80',0.017, 0., 0.)
         CALL PLCHHQ(0.76,0.25,':F21:70',0.017, 0., 0.)
         CALL PLCHHQ(0.82,0.30,':F21:50',0.017, 0., 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(0.012929,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.25,-.035,':F21:0',0.017, 0., 0.)
         CALL PLCHHQ(0.48717,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.625,-.035,':F21:25',0.017, 0., 0.)
C
C        * WRITE RIGHT AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         CALL PLCHHQ(0.97,.35,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.97,.50,':F21:50',0.017, 0., 0.)
         CALL PLCHHQ(0.96,.72,':F21:100',0.017, 0., 0.)
         CALL PLCHHQ(0.45,.70,':F21:150',0.017, 0., 0.)
         CALL PLCHHQ(0.24,.70,':F21:200',0.017, 0., 0.)
C
C        * WRITE LEFT AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(-0.030,.28,':F21:25',0.017, 90., 0.)
         CALL PLCHHQ(-0.030,.47,':F21:50',0.017, 90., 0.)
         CALL PLCHHQ(-0.030,.70,':F21:100',0.017, 90., 0.)
C
C        * EXPERIMENTAL BLT GRAPH WITH SCALE AT 200 ON Y AXIS
C
      ELSEIF(ISCALE.EQ.1)THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL GSFACI(ICL4)
         CALL PLCHHQ(0.76,0.035,':F21:99',0.017, 0., 0.)
         CALL PLCHHQ(0.77,0.075,':F21:95',0.017, 0., 0.)
         CALL PLCHHQ(0.785,0.105,':F21:90',0.017, 0., 0.)
         CALL PLCHHQ(0.806,0.145,':F21:80',0.017, 0., 0.)
         CALL PLCHHQ(0.835,0.175,':F21:70',0.017, 0., 0.)
         CALL PLCHHQ(0.88,0.21,':F21:50',0.017, 0., 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         XRAD=1.0-SQRT(100.0)*SC
         CALL PLCHHQ(XRAD,-.035,':F21:0',0.017, 0., 0.)
         RAD=SQRT(50.0)*SC
         RAD=SQRT(25.0)*SC
         CALL PLCHHQ(XRAD+RAD,-.035,':F21:25',0.017, 0., 0.)
         RAD=SQRT(10.0)*SC
         CALL PLCHHQ(XRAD+RAD,-.035,':F21:10',0.017, 0., 0.)
C     
C        * WRITE RIGHT AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         CALL PLCHHQ(0.97,.24,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.97,.35,':F21:50',0.017, 0., 0.)
         CALL PLCHHQ(0.96,.50,':F21:100',0.017, 0., 0.)
         CALL PLCHHQ(0.96,.62,':F21:150',0.017, 0., 0.)
         CALL PLCHHQ(0.96,.72,':F21:200',0.017, 0., 0.)
         CALL PLCHHQ(0.44,.69,':F21:300',0.017, 0., 0.)
C
C        * WRITE LEFT AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(XMN-0.030,.11,':F21:10',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.23,':F21:25',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.35,':F21:50',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.51,':F21:100',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.63,':F21:150',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.73,':F21:200',0.017, 90., 0.)
C     
      ELSEIF(ISCALE.EQ.2) THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL GSFACI(ICL4)
         CALL PLCHHQ(0.025,0.155,':F21:99',0.014, 0., 0.)
         CALL PLCHHQ(0.025,0.34,':F21:95',0.014, 0., 0.)
         CALL PLCHHQ(0.025,0.50,':F21:90',0.014, 0., 0.)
         CALL PLCHHQ(0.12,0.685,':F21:80',0.014, 0., 0.)
         CALL PLCHHQ(0.335,0.705,':F21:70',0.014, 0., 0.)
         CALL PLCHHQ(0.62,0.705,':F21:50',0.014, 0., 0.)
         CALL PLCHHQ(0.85,0.685,':F21:25',0.014, 0., 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(0.012929,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.25,-.035,':F21:0',0.017, 0., 0.)
         CALL PLCHHQ(0.48717,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.625,-.035,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.785,-.035,':F21:50',0.017, 0., 0.)
C
C        * WRITE RIGHT AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         CALL PLCHHQ(1.03,.08,':F21:1',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.24,':F21:10',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.37,':F21:25',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.53,':F21:50',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.75,':F21:100',0.017, 270., 0.)
         CALL PLCHHQ(0.47,.78,':F21:150',0.017, 0., 0.)
         CALL PLCHHQ(0.25,.78,':F21:200',0.017, 0., 0.)
C
C        * WRITE LEFT AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(-0.023,.27,':F21:25',0.017, 90., 0.)
         CALL PLCHHQ(-0.023,.46,':F21:50',0.017, 90., 0.)
         CALL PLCHHQ(-0.023,.70,':F21:100',0.017, 90., 0.)
C
      ELSEIF(ISCALE.EQ.3) THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL GSFACI(ICL4)
         CALL PLCHHQ(0.025,0.155,':F21:99',0.014, 0., 0.)
         CALL PLCHHQ(0.025,0.34,':F21:95',0.014, 0., 0.)
         CALL PLCHHQ(0.09,0.46,':F21:90',0.014, 0., 0.)
         CALL PLCHHQ(0.025,0.71,':F21:80',0.014, 0., 0.)
         CALL PLCHHQ(0.335,0.705,':F21:70',0.014, 0., 0.)
         CALL PLCHHQ(0.63,0.69,':F21:50',0.014, 0., 0.)
         CALL PLCHHQ(0.85,0.685,':F21:25',0.014, 0., 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(0.08,-.035,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.19,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.385,-.035,':F21:0',0.017, 0., 0.)
         CALL PLCHHQ(0.58,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.695,-.035,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.825,-.035,':F21:50',0.017, 0., 0.)
C
C        * WRITE RIGHT AND TOP AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         CALL PLCHHQ(1.03,.06,':F21:1',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.195,':F21:10',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.305,':F21:25',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.43,':F21:50',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.61,':F21:100',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.75,':F21:150',0.017, 270., 0.)
         CALL PLCHHQ(0.57,.78,':F21:200',0.017, 0., 0.)
         CALL PLCHHQ(0.25,.78,':F21:300',0.017, 0., 0.)
C
C        * WRITE LEFT AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(-0.023,.19,':F21:50',0.017, 90., 0.)
         CALL PLCHHQ(-0.023,.47,':F21:100',0.017, 90., 0.)
         CALL PLCHHQ(-0.023,.63,':F21:150',0.017, 90., 0.)

      ELSEIF(ISCALE.EQ.4) THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL GSFACI(ICL4)
         CALL PLCHHQ(0.375,0.105,':F21:99',0.014,RANG(1), 0.)
         CALL PLCHHQ(0.375,0.22,':F21:95',0.014,RANG(2), 0.)
         CALL PLCHHQ(0.375,0.32,':F21:90',0.014,RANG(3), 0.)
         CALL PLCHHQ(0.389,0.477,':F21:80',0.014,RANG(4), 0.)
         CALL PLCHHQ(0.467,0.566,':F21:70',0.014,RANG(5), 0.)
         CALL PLCHHQ(0.628,0.680,':F21:50',0.014,RANG(6), 0.)
         CALL PLCHHQ(0.837,0.705,':F21:25',0.014,RANG(7), 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(0.385,-.035,':F21:0',0.017, 0., 0.)
         CALL PLCHHQ(0.585,-.035,':F21:10',0.017, 0., 0.)
         CALL PLCHHQ(0.695,-.035,':F21:25',0.017, 0., 0.)
         CALL PLCHHQ(0.825,-.035,':F21:50',0.017, 0., 0.)
C
C        * WRITE RIGHT AND TOP AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         CALL PLCHHQ(1.03,.06,':F21:1',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.195,':F21:10',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.305,':F21:25',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.43,':F21:50',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.61,':F21:100',0.017, 270., 0.)
         CALL PLCHHQ(1.03,.75,':F21:150',0.017, 270., 0.)
         CALL PLCHHQ(0.57,.78,':F21:200',0.017, 0., 0.)
C
C        * WRITE LEFT AND TOP AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         CALL PLCHHQ(XMN-0.030,.185,':F21:10',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.30,':F21:25',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.435,':F21:50',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.61,':F21:100',0.017, 90., 0.)
         CALL PLCHHQ(XMN-0.030,.75,':F21:150',0.017, 90., 0.)

      ELSEIF(ISCALE.EQ.5) THEN
C
C        * WRITE CORRELATION AXIS LABEL VALUES
C
         CALL PCSETI ('BF - BOX FLAG',7)
         CALL PCSETI ('BL - BOX LINE WIDTH',2)
         CALL PCSETR ('BM - BOX MARGIN',.10)
         CALL PCSETR ('BX - BOX SHADOW X OFFSET',0.)
         CALL PCSETR ('BY - BOX SHADOW Y OFFSET',0.)
         CALL PCSETI ('BC(1) - BOX COLOR - BOX OUTLINE    ',0)
         CALL PCSETI ('BC(2) - BOX COLOR - BOX FILL       ',0)
         CALL PCSETI ('BC(3) - BOX COLOR - BOX SHADOW FILL',0)
         CALL GSFACI(ICL4)
         DO I=1,NLINES
            ANG=ACOS(RCOR(I))          
            X=XMX-SQRT(7.)*SC*RCOR(I)
            Y=SQRT(7.)*SC*SIN(ANG)
            WRITE(RTXT,7000) INT(RCOR(I)*100.+0.5)
            CALL PLCHHQ(X,Y,RTXT,0.012, RANG(I) , 0.)            
         ENDDO
         CALL PCSETI ('BF - BOX FLAG',0)
C
         RAD=SQRT(1.)*SC
         Y=SQRT(RAD*RAD/8.)
         X=XMX-Y
         CALL PLCHHQ(X,Y,':F21:r',0.019, 0., 0.)
C
C        * WRITE BOTTOM AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         Y=-0.025
         RAD=SQRT(0.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:0',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:0',0.017, 0., 0.)
         RAD=SQRT(10.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:10',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:10',0.017, 0., 0.)
         RAD=SQRT(25.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:25',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:25',0.017, 0., 0.)
         RAD=SQRT(50.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:50',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:50',0.017, 0., 0.)
         RAD=SQRT(100.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:100',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:100',0.017, 0., 0.)
         RAD=SQRT(150.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:150',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:150',0.017, 0., 0.)
         RAD=SQRT(200.0)*SC
         X=XCN+RAD
         IF(X.LE.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:200',0.017, 0., 0.)
         X=XCN-RAD
         IF(X.GE.XMN.AND.X.LE.XMX) 
     1        CALL PLCHHQ(X,Y,':F21:200',0.017, 0., 0.)
C
C        * WRITE LEFT AXIS LABEL VALUES
C
         CALL GSFACI(ICL1)
         X=XMN-0.020
         RAD=SQRT(10.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:10',0.017, 90., 0.)
         RAD=SQRT(25.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:25',0.017, 90., 0.)
         RAD=SQRT(50.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:50',0.017, 90., 0.)
         RAD=SQRT(100.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:100',0.017, 90., 0.)
         RAD=SQRT(150.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:150',0.017, 90., 0.)
         RAD=SQRT(200.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:200',0.017, 90., 0.)
         RAD=SQRT(250.0)*SC
         Y=SQRT(RAD*RAD-((XCN-XMN)*(XCN-XMN)))
         IF(Y.GE.YMN.AND.Y.LE.YMX) 
     1        CALL PLCHHQ(X,Y,':F21:250',0.017, 90., 0.)

C
C        * WRITE RIGHT AXIS LABEL VALUES
C
         CALL GSFACI(ICL2)
         X=XMX+0.02
         RAD=SQRT(1.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:1',0.017, 270., 0.)
         RAD=SQRT(10.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:10',0.017, 270., 0.)
         RAD=SQRT(25.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:25',0.017, 270., 0.)
         RAD=SQRT(50.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:50',0.017, 270., 0.)
         RAD=SQRT(100.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:100',0.017, 270., 0.)
         RAD=SQRT(150.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:150',0.017, 270., 0.)
         RAD=SQRT(200.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:200',0.017, 270., 0.)
         RAD=SQRT(250.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:250',0.017, 270., 0.)
         RAD=SQRT(300.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:300',0.017, 270., 0.)
         RAD=SQRT(350.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:350',0.017, 270., 0.)
         RAD=SQRT(400.0)*SC         
         Y=YMN+RAD
         IF(Y.LE.YMX) CALL PLCHHQ(X,Y,':F21:400',0.017, 270., 0.)
C
C        * WRITE TOP AXIS LABEL VALUES
C
         Y=YMX+0.02
         RAD=SQRT(10.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:10',0.017, 0., 0.)
         RAD=SQRT(25.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:250',0.017, 0., 0.)
         RAD=SQRT(50.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:50',0.017, 0., 0.)
         RAD=SQRT(100.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:100',0.017, 0., 0.)
         RAD=SQRT(150.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:150',0.017, 0., 0.)
         RAD=SQRT(200.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:200',0.017, 0., 0.)
         RAD=SQRT(250.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:250',0.017, 0., 0.)
         RAD=SQRT(300.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:300',0.017, 0., 0.)
         RAD=SQRT(350.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:350',0.017, 0., 0.)
         RAD=SQRT(400.)*SC
         X=XMX-SQRT(RAD*RAD-YMX*YMX)
         IF(X.LT.XMX.AND.X.GE.XMN) 
     1        CALL PLCHHQ(X,Y,':F21:400',0.017, 0., 0.)
C
      ENDIF
C
C     * WRITE OUTSIDE PLOTTING FRAME
C
      CALL SET(0.,1.,0.,1.,0.,1.,0.,1.,1)
C
C     * WRITE BOTTOM AXIS LABEL TEXT
C
      CALL GSFACI(ICL1)
      XP=(VXMX+VXMN)/2.
      YP=0.1
      CALL PLCHHQ(XP,YP,':F21:Mean square difference :F21:d:SF21:2:N:/:
     1            F33:s:S1F21:2:BF21:ref:N: (percent)',0.017, 0., 0.)
C
C     * MEAN BAR OVER d
C
      CALL GSPLCI(ICL1)
      CALL GSLWSC(1.5)
      CALL FRSTPT(XP+0.05,0.12)
      CALL VECTOR(XP+0.07,0.12)
C
C     * WRITE RIGHT AXIS LABEL
C
      CALL GSFACI(ICL2)
      CALL PLCHHQ(.97,.5,':F21:Ratio of variances :F33:s:S1F21:2:BF21:
     1   :N:/:F33:s:S1F21:2:BF21:ref:N: (percent)',0.017, 270., 0.)
C
C     * WRITE CORRELATION LABEL r
C
      CALL GSFACI(ICL4)
      IF(ISCALE.EQ.0)THEN
         CALL PLCHHQ(.74,.355,':F21:Correlation',0.017, 45., 0.)
      ELSEIF(ISCALE.EQ.1)THEN
         CALL PLCHHQ(.82,.28,':F21:Correlation',0.017, 45., 0.)
      ELSEIF(ISCALE.EQ.2)THEN
         CALL PLCHHQ(.88,.23,':F21:r',0.019, 0., 0.)
      ELSEIF(ISCALE.EQ.3.OR.ISCALE.EQ.4)THEN
         CALL PLCHHQ(.88,.22,':F21:r',0.019, 0., 0.)
      ENDIF
C
C     * FONT
C
      CALL PCSETI('FN', 21)
C
      CALL GSFACI(1)
      CALL GSTXCI(1)
      CALL PLCHHQ (XP,.87,
     1     TOPT(LSTRBEG(TOPT):LSTREND(TOPT)),
     2     .02,0.,0.)
C
C     * PLOT MARKERS
C
      CALL SET(VXMN,VXMX,VYMN,VYMX,XMN,XMX,YMN,YMX,1)
C
C     * DRAW MARKERS
C
      DELTAC=0.01
      DELTA=0.012
      CALL GSFACI(1)
      CALL GSFAIS(1)
      I1=11
      I2=12
      I3=13
      DO J=1,IGROUP
C
         I=0
 400     CONTINUE
         I=I+1
C
C           * GET INPUT FIELD
C
C           * ONLY THE FIRST VALUE IN THE ARRAY IS VALID
C
            CALL GETFLD2 (I1,CORD2,-1,-1,-1,-1,IBUF,MAXX,OK)
            IF ((.NOT.OK).AND.(I.NE.0)) THEN
               ICF(J)=I-1
               IF((IMNAME.EQ.1).AND.(ICF(J).NE.ICOUNT(J)))THEN
                  WRITE(6,*) 'NUMBER OF RECORDS DOES NOT MATCH ',
     1                       'THE NUMBER OF NAMES.  RECORDS: '
     2                       ,ICF(J),'   NAMES: ',ICOUNT(J)
                  CALL                             XIT('BLT',-2)
               ENDIF
               GOTO 410
            ELSEIF(.NOT.OK)THEN
               CALL                                XIT('BLT',-3)
            ENDIF
            CALL GETFLD2 (I2,VARM,-1,-1,-1,-1,IBUF,MAXX,OK)
            IF(IVAROB.EQ.1)THEN
               CALL GETFLD2 (I3,VARO,-1,-1,-1,-1,IBUF,MAXX,OK)
               IF (.NOT.OK) CALL                   XIT('BLT',-4)
            ELSEIF(I.EQ.1)THEN
               CALL GETFLD2 (I3,VARO,-1,-1,-1,-1,IBUF,MAXX,OK)
               IF (.NOT.OK) CALL                   XIT('BLT',-5)
            ENDIF
C           * CORRELATION
            RC=-CORD2(1)
            IF(IINPUT.EQ.1)THEN
               RC=-(VARO(1)+VARM(1)-CORD2(1))/
     1             (2.*SQRT(VARO(1)*VARM(1)))
            ENDIF
C           * VARIANCE RATIO IN PERCENT
            SRATIO=(VARM(1)/VARO(1))*100.0
            CALL GSFACI(1)
            RAD=SQRT(SRATIO)*SC
            XCNTR=XMX
            XM1=RAD*RC+XCNTR
            YM1=RAD*SQRT(1.-RC**2)+YMN
C
C           * JOIN GROUP MARKERS IF REQUESTED
C
            IF(ILABEL.EQ.3)THEN
               CALL GSPLCI(1)
               IF(I.GT.1)THEN
                  CALL FRSTPT(XMOLD,YMOLD)
                  CALL VECTOR(XM1,YM1)
               ENDIF
               XMOLD=XM1
               YMOLD=YM1
            ENDIF
C
C           * MARKERS
C
            IF(IG(J).EQ.1) THEN
               CALL GNCRCL(XM1,YM1,DELTAC,NPTS,X1,Y1)
               CALL GFA(NPTS,X1,Y1)
            ELSEIF(IG(J).EQ.2)THEN
               CALL GNTRG(XM1,YM1,DELTA,X1,Y1)
               CALL GFA(4,X1,Y1)
            ELSEIF(IG(J).EQ.3)THEN
               CALL GNSQR(XM1,YM1,DELTAC,X1,Y1)
               CALL GFA(5,X1,Y1)
            ELSEIF(IG(J).EQ.4)THEN
               CALL GSMK(2)
               CALL GSPMCI(1)
               CALL GSMKSC(1.7)
               X1(1)=XM1
               Y1(1)=YM1
               CALL GPM(1,X1,Y1)
            ELSEIF(IG(J).EQ.5)THEN
               CALL GSMK(4)
               CALL GSPMCI(1)
               CALL GSMKSC(1.)
               X1(1)=XM1
               Y1(1)=YM1
               CALL GPM(1,X1,Y1)
            ENDIF
C
C           * WRITE NAME OR NUMBERS
C
            CALL GSFACI(MCOLOR(J,I))
            DXM=0.015
            DYM=0.0
            CNTR=-1.0
            IF(IMNAME.EQ.1)THEN
               IF((MPOS(J,I).EQ.BLANK).OR.(MPOS(J,I).EQ.R))THEN
                  DXM=0.015
                  DYM=0.0
                  CNTR=-1.0
               ELSEIF(MPOS(J,I).EQ.L)THEN
                  DXM=-0.015
                  DYM=0.0
                  CNTR=1.0
               ELSEIF(MPOS(J,I).EQ.B)THEN
                  DXM=0.0
                  DYM=-0.023
                  CNTR=0.0
               ELSEIF(MPOS(J,I).EQ.T)THEN
                  DXM=0.0
                  DYM=0.023
                  CNTR=0.0
               ELSEIF(MPOS(J,I).EQ.BL)THEN
                  DXM=-0.015
                  DYM=-0.015
                  CNTR=1.0
               ELSEIF(MPOS(J,I).EQ.BR)THEN
                  DXM=0.015
                  DYM=-0.015
                  CNTR=-1.0
               ELSEIF(MPOS(J,I).EQ.TL)THEN
                  DXM=-0.015
                  DYM=0.015
                  CNTR=1.0
               ELSEIF(MPOS(J,I).EQ.TR)THEN
                  DXM=0.015
                  DYM=0.015
                  CNTR=-1.0
               ENDIF
            ENDIF
C
            IF((ILABEL.EQ.2).AND.(MCOLOR(J,I).NE.0))THEN
               READ(AMNAME(J,I),5016) (CMNAME(IJ),IJ=1,10)            
               CALL GSFACI(MCOLOR(J,I))
               CALL PLCHHQ (XM1+DXM,YM1+DYM,
     1              CMNAMET(LSTRBEG(CMNAMET):LSTREND(CMNAMET)),
     2              .01,0.,CNTR)
            ELSEIF((ILABEL.EQ.1).OR.(ILABEL.EQ.3))THEN
               CALL GSFACI(1)
               WRITE(CMNAMET,6000) I
               CALL PLCHHQ (XM1+DXM,YM1+DYM,
     1              CMNAMET(LSTRBEG(CMNAMET):LSTREND(CMNAMET)),
     2              .01,0.,CNTR)
            ENDIF
C
            GOTO 400
C
 410        CONTINUE
            I1=I1+3
            I2=I2+3
            I3=I3+3
C
         ENDDO
C
C
C     * FINISH PLOTTING
C
      CALL FRAME
C
      CALL                                         PXIT('BLT',0)
C
C     * IF NO INPUT CARDS WERE PROVIDED
C
 900  WRITE(6,*) 'NO INPUT CARDS PROVIDED'
      CALL                                         PXIT('BLT',-6)
C
C------------------------------------------------------------------------------
 5000 FORMAT(11X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,1X,I1,
     1        1X,I1,1X,I1,1X,I1)
 5005 FORMAT(11X,68A1)
 5010 FORMAT(A80)
 5012 FORMAT(11X,I1)
 5014 FORMAT(13X,A10,1X,I1,1X,A2,1X,A10,1X,I1,1X,A2,1X,A10,1X,I1,
     1       1X,A2,1X,A10,1X,I1,1X,A2)
 5016 FORMAT(10A1)
 5020 FORMAT(10X,2E10.0)
 6000 FORMAT(I5)
 7000 FORMAT(':F21:',I2)
C
      END

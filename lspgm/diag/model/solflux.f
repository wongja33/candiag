      PROGRAM SOLFLUX 
C     PROGRAM SOLFLUX (FSG,       FSO,       INPUT,       OUTPUT,       )       J2
C    1           TAPE2=FSG, TAPE1=FSO, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               J2
C                                                                               J2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C                                                                               J2
CSOLFLUX - RE-COMPUTE THE INCIDENT SOLAR FLUX AT THE TOP OF THE GCM     1  1 C  J1
C                                                                               J3
CAUTHOR  - R.LAPRISE                                                            J3
C                                                                               J3
CPURPOSE - RE-COMPUTE THE INCIDENT SOLAR FLUX AT THE TOP OF THE GCM (FSO)       J3
C          FOR THE TIMESTEPS AND ON THE SAME GRID THAT FSG IS AVAILABLE.        J3
C          NOTE - IT IS ADAPTED FROM SOLFLX WRITTEN BY J.P.BLANCHET.            J3
C                                                                               J3
CINPUT FILE...                                                                  J3
C                                                                               J3
C      FSG = SOLAR FLUX ABSORBED BY THE SURFACE                                 J3
C                                                                               J3
COUTPUT FILES...                                                                J3
C                                                                               J3
C      FSO = INCIDENT SOLAR FLUX (W M-2)                                        J3
C 
CINPUT PARAMETERS...
C                                                                               J5
C     DAY    = INITIAL (JULIAN) DAY OF THE PERIOD                               J5
C     GMT    = INITIAL G.M.TIME (HR)                                            J5
C     DELT   = LENGTH OF TIMESTEP OF MODEL RUN (S)                              J5
C     INCDAY = 1,0 TO INCREMENT THE DECLINATION ANGLE OR NOT                    J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C*SOLFLUX   182   12      1800    1                                             J5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON/BLANCK/ FSO(SIZES_LONP1xLAT)
C 
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C 
      LOGICAL OK
      REAL*8 W(SIZES_LAT),WOCS(SIZES_LAT),SINL(SIZES_LAT),
     & COSL(SIZES_LAT),RAD(SIZES_LAT)  
C 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
      DATA S/1370.E0/,PI/3.141592654E0/ 
      DATA COSMIN/0.001E0/
C-----------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,2,1,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ INPUT CARD.
C 
      READ(5,5000,END=901)DAY,GMT,DELT,INCDAY                                   J4
      WRITE(6,6000)DAY,GMT,DELT,INCDAY
C 
C     * SCAN FSG FOR TIMESTEP INFORMATION.
C 
      NT3=1 
      NSTEPS=0
    1 CALL RECGET(2,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NSTEPS.EQ.0) CALL                       XIT('SOLFLUX',-1)
        NT2=IBUF(2) 
        GO TO 3 
      ENDIF 
      NSTEPS=NSTEPS+1 
      IF(NSTEPS.EQ.1)THEN 
        NPACK=MIN(2,IBUF(8))
        NT1=IBUF(2) 
        ILG1=IBUF(5)
        ILAT=IBUF(6)
      ENDIF 
      IF(NSTEPS.EQ.2) NT3=IBUF(2)-NT1 
      GO TO 1 
C 
C     * PREPARE TO COMPUTE INCIDENT FLUX. 
C 
    3 CONTINUE
      GMT0=GMT
      DAYFIX=DAY
      ILG=ILG1-1
      NSIZE=ILG1*ILAT 
      ILATH=ILAT/2
      STEP=FLOAT(NT3)*DELT/3600.E0
      RADTIM=2.E0*PI/24.E0
      RADLON=2.E0*PI/FLOAT(ILG) 
      IBUF(3)=NC4TO8(" FSO")
      IBUF(8)=NPACK 
C 
      DO 5 I=1,NSIZE
    5 FSO(I)=0.E0 
C 
C     *** SET GAUSSIAN GRID AND SOLAR DECLINATION.
C 
      CALL GAUSSG(ILATH,SINL,W,COSL,RAD,WOCS) 
      CALL TRIGL (ILATH,SINL,W,COSL,RAD,WOCS) 
      NT=0
C 
C     *** LOOP ON TIME. 
C 
      DO 50 NTIME=NT1,NT2,NT3 
C 
      NT=NT+1 
      IBUF(2)=NTIME 
      CALL SDET(COSD,SIND,MONTH,DAY)
C 
C     *** LOOP ON LATITUDE. 
C 
      DO 20 I=1,ILAT
C 
C     *** LOOP ON LONGITUDE.
C 
      DO 20 J=1,ILG1
C 
      N=J+(I-1)*ILG1
      COSH=-COS(GMT*RADTIM+(J-1)*RADLON)
      COSZ=SINL(I)*SIND+COSL(I)*COSD*COSH 
      IF(COSZ.LE.COSMIN) COSZ=0.E0
      FSO(N)=S*COSZ 
   20 CONTINUE
C 
C     *** PUT DATA IN FILE "FSO". 
C 
      CALL PUTFLD2(1,FSO,IBUF,MAXX)
      IF(NTIME.EQ.NT1 .OR. NTIME.EQ.NT2) WRITE(6,6005)IBUF
C 
C     *** ADJUST TIME.
C 
      GMTOLD=GMT+STEP 
      GMT=MOD(GMTOLD,24.E0)
      DAY=DAY+(GMTOLD-GMT)/24.E0
      IF(INCDAY.EQ.0) DAY=DAYFIX
   50 CONTINUE
C 
      WRITE(6,6001) NSTEPS,NT 
      WRITE(6,6004) NT1,NT2,NT3 
      EPSILN=0.1E0
      GMTM=GMT0-EPSILN
      GMTP=GMT0+EPSILN
      WRITE(6,6003) DAYFIX,GMT0,DAY,GMT 
      IF(GMT.LT.GMTM.OR.GMT.GT.GMTP) WRITE(6,6002)
C 
      CALL                                         XIT('SOLFLUX',0)
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('SOLFLUX',-2) 
C-----------------------------------------------------------------------
 5000 FORMAT(10X,2F5.0,F10.0,I5)                                                J4
 6000 FORMAT(' DAY,GMT,DELT,INCDAY...',3F10.2,I6)
 6002 FORMAT(1X,70('*'),/,5X,'THIS PERIOD IS NOT A MULTIPLE OF 24 HOUR',
     1                    /,1X,70('*'))
 6003 FORMAT(' THE PERIOD IS FROM DAY ',F5.1,' - ',F4.0,'Z TO DAY ',
     1 F5.1,' - ',F4.0,'Z')
 6004 FORMAT(' NT1,NT2,NT3 =',3I10)
 6001 FORMAT(1X,I10,' INPUT  RECORDS OF FSG',/,
     1       1X,I10,' OUTPUT RECORDS OF FSO')
 6005 FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END

      PROGRAM GSHLVL
C     PROGRAM GSHLVL (IN,       OUT,       OUTPUT,                      )       J2
C    1          TAPE1=IN, TAPE2=OUT, TAPE6=OUTPUT)
C     --------------------------------------------                              J2
C                                                                               J2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 12/83 - R.LAPRISE.                                                    
C     DEC 18/80 - J.D.HENDERSON 
C                                                                               J2
CGSHLVL  - GRID FILE INTERPOLATION FROM FULL TO HALF SIGMA LEVELS.      1  1    J1
C                                                                               J3
CAUTHOR  - J.D.HENDERSON                                                        J3
C                                                                               J3
CPURPOSE - INTERPOLATES A GRID FILE OF SIGMA LEVEL DATA FROM FULL               J3
C          LEVELS TO HALF LEVELS.                                               J3
C                                                                               J3
CINPUT FILE...                                                                  J3
C                                                                               J3
C      IN = GRID FILE OF MULTI-LEVEL DATA ON FULL SIGMA LEVELS.                 J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      OUT = FILE IN INTERPOLATED TO HALF SIGMA LEVELS.                         J3
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/G(SIZES_LONP1xLAT,SIZES_MAXLEV) 
C 
      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV),LS(SIZES_MAXLEV),LH(SIZES_MAXLEV)
      REAL S(SIZES_MAXLEV) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_MAXLEV/ 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * GET THE FULL LEVELS FROM THE FIRST SET ON THE FILE. 
C     * COMPUTE HALF LEVELS.
C 
      CALL FILEV(LEV,NLEV,IBUF,1) 
      IF(NLEV.LT.1)THEN 
        WRITE(6,6020) 
        CALL                                       XIT('GSHLVL',-1) 
      ENDIF 
      IF(NLEV.GT.MAXL) CALL                        XIT('GSHLVL',-2) 
      REWIND 1
      CALL LVDCODE(S,LEV,NLEV)
      DO 120 L=1,NLEV 
  120 S(L)=S(L)*0.001E0 
      CALL ISIGL(LS,LH,S,NLEV)
C 
C     * READ THE NEXT GRID, ALL LEVELS. 
C 
      NSETS=0 
  150 DO 200 L=1,NLEV 
      CALL GETFLD2(1,G(1,L),NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6010) NSETS 
        CALL                                       XIT('GSHLVL',0)
      ENDIF 
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
  200 CONTINUE
C 
C     * LAST HALF LEVEL IS SET TO LAST FULL LEVEL.
C     * INTERPOLATE TO HALF-LEVELS. 
C 
      NPTS=IBUF(5)*IBUF(6)
      NLEV1=NLEV-1
      DO 300 L=1,NLEV1
      DO 250 I=1,NPTS 
  250 G(I,L)=(G(I,L)+G(I,L+1))*0.5E0 
      IBUF(4)=LH(L) 
      CALL PUTFLD2(2,G(1,L),IBUF,MAXX) 
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
  300 CONTINUE
      IBUF(4)=LH(NLEV)
      CALL PUTFLD2(2,G(1,NLEV),IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
      NSETS=NSETS+1 
      GO TO 150 
C-----------------------------------------------------------------------
 6010 FORMAT(' ',I6,' SETS PROCESSED')
 6020 FORMAT('0... GSHLVL INPUT FILE IS EMPTY')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END

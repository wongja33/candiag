      PROGRAM BINAPAK 
C     PROGRAM BINAPAK (BIN,       PAK,       INPUT,       OUTPUT,       )       B2
C    1           TAPE1=BIN, TAPE2=PAK, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 15/84 - R. LAPRISE                                                
C                                                                               B2
CBINAPAK - CONVERT SIMPLE BINARY FILE TO CCRN (PACKED) FORM             1  1 C  B1
C                                                                               B3
CAUTHOR  - R.LAPRISE                                                            B3
C                                                                               B3
CPURPOSE - CONVERT SIMPLE BINARY FILE (WRITTEN BY FORTRAN WRITE)                B3
C          TO CCRN (PACKED) LABELLED FORMAT (WRITTEN WITH PUTFLD2).             B3
C          A REDUNDANT CYCLIC LONGITUDE IS ADDED IN THE CCRN TRADITION.         B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      BIN = BINARY FILE.                                                       B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      PAK = CCRN (PACKED) FILE.                                                B3
C 
CINPUT PARAMETERS...
C                                                                               B5
C     IBUF(1) TO (8) TAKE THE USUAL MEANING.                                    B5
C     IBUF(5) IS THE NUMBER OF LONGITUDES RESULTING AFTER ADDING                B5
C                THE CYCLIC LONGITUDE.                                          B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*BINAPAK  GRID         0 INCR         0       193        96         1         2B5
C-------------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON // GG(SIZES_BLONP1xBLAT),G1(SIZES_BLONP1xBLAT) 
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NF=4
      CALL JCLPNT(NF,1,2,5,6) 
C 
C     * INPUT LABEL AND SIZE INFORMATION. 
C 
      READ(5,5000,END=901) IBUF                                                 B4
      WRITE(6,5000) IBUF
      ISIZ=(IBUF(5)-1) *IBUF(6) 
      ISIZ1=IBUF(5)*IBUF(6) 
      IF(ISIZ1.GT.MAXX) CALL                       XIT('BINAPAK',-1)
C 
C     * INPUT BINARY DATA.
C 
      READ(1,END=903) (GG(I),I=1,ISIZ)
C 
C     * ADD CYCLIC LONGITUDE TO FIELD.
C 
      LON = IBUF(5)-1 
      LON1= IBUF(5) 
      LAT = IBUF(6) 
      IND = 0 
      IND1= 0 
      DO 200 J=1,LAT
      DO 100 I=1,LON
         IND = IND +1 
         IND1= IND1+1 
  100    G1(IND1)=GG(IND) 
         IND1= IND1+1 
         G1(IND1)=G1(IND1-LON)
  200 CONTINUE
C 
C     * OUTPUT DATA IN CCRN FORMAT. 
C 
      CALL PUTFLD2(2,G1,IBUF,MAXX) 
      CALL                                         XIT('BINAPAK',0) 
C 
C     E.O.F. ON READING.
C 
  901 CALL                                         XIT('BINAPAK',-2)
  903 CALL                                         XIT('BINAPAK',-3)
C------------------------------------------------------------------------ 
 5000 FORMAT(10X,1X,A4,I10,1X,A4,5I10)                                          B4
      END

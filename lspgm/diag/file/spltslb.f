      PROGRAM SPLTSLB
C     PROGRAM SPLTSLB (FILELIST,       VARLIST,      OUTPUT,            )       B2
C    1           TAPE1=FILELIST, TAPE2=VARLIST,TAPE6=OUTPUT)                    B2
C     ------------------------------------------------------                    B2
C                                                                               B2
C     APR 05/16 - D.PLUMMER (ADJUST FOR DIFFERENT SUPERLABELS ORDER)            B2
C     SEP 22/14 - S.KHARIN (CLOSE UNIT 3 AFTER REACHING END OF FILE)            
C     FEB 29/12 - S.KHARIN (FIX A BUG WHEN INPUT FILES DO NOT HAVE SUPERLABELS)
C     JUL 14/11 - S.KHARIN (CORRECT SUPERLABEL CHECK BY VERIFYING AGAINST THOSE
C                           FOUND IN THE FIRST INPUT FILE)
C     APR 26/10 - S.KHARIN (CORRECT SUPERLABEL CHECK FOR A SINGLE ENTRY CASE)
C     JAN 06/10 - S.KHARIN (ABORT IF INPUT FILES ARE DIFFERENT)
C     NOV 25/09 - S.KHARIN (ADD "NVARS"/"LINFO" INPUT PARAMETERS IN SUPPORT OF
C                           ADDED FUNCTIONALITIES)
C     APR 24/09 - S.KHARIN (LEAVE PACKING DENSITY OF PARM RECORDS UNCHANGED.
C                           ADD OPTION FOR KEEPING TIME STEPS OF INPUT FILES.)
C     APR 06/09 - S.KHARIN.
C                                                                               B2
CSPLTSLB - SPLITS SUPERLABELED FILES INTO TIME SERIES.                  1  1  C B1
C                                                                               B3
CAUTHOR  - S.KHARIN                                                             B3
C                                                                               B3
CPURPOSE - SPLITS SUPERLABELED FILES INTO TIME SERIES.                          B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      FILELIST = ASCII TEXT FILE CONTAINING INPUT SUPER-LABELLED FILE NAMES    B3
C                 AND THE CORRESPONDING DATE, ONE PER LINE.                     B3
C                 FOR EXAMPLE,                                                  B3
C X001 197001                                                                   B3
C X002 197002                                                                   B3
C ...                                                                           B3
C                 IF THE DATE IS EQUAL TO -1 THEN KEEP TIME STEPS UNCHANGED.    B3
C X001 -1                                                                       B3
C X002 -1                                                                       B3
C ...                                                                           B3
C      VARLIST  = ASCII TEXT FILE CONTAINING OUTPUT FILE NAMES,                 B3
C                 ONE PER LINE.                                                 B3
C                                                                               B3
CINPUT PARAMETERS...
C                                                                               B5
C11:15 LSKIP  = 1, SKIP (CO)VARIANCES SUCH AS X"Y", X"2, ETC. AND DX/DT.        B5
C                  OTHERWISE CREATE TIME SERIES FOR ALL SUPERLABELS.            B5
C16:20 LNAME  = 1, USE IBUF(3) FOR FILE NAMES.                                  B5
C             =-1, INPUT FILES DO NOT HAVE SUPERLABELS.                         B5
C                  USE IBUF(3) FOR FILE NAMES.                                  B5
C                  OTHERWSIE, DERIVE NAME FROM SUPERLABEL.                      B5
C21:25 LSLAB  = 1, ADD SUPERLABEL TO OUTPUT FILES.                              B5
C25:30 NPACK  = 2, PACKING DENSITY OF OUTPUT FILES.                             B5
C               0, LEAVE THE PACKING DENSITY AS IT IS IN INPUT FILES.           B5
C31:35 NVARS  = 0, CREATE TIME SERIES FOR ALL VARIABLES.                        B5
C             = 1, CREATE TIME SERIES FOR A SUBSET OF VARIABLES.                B5
C                  THE VARIABLE NAMES/SUPERLABELS ARE SPECIFIED IN EXTRA        B5
C                  INPUT CARDS, ONE FOR EACH VARIABLE.                          B5
C                  THE FORMAT IS THE SAME AS IN XFIND, THAT IS,                 B5
C                  VARIABLE NAMES/SUPERLABEL START FROM COLUMN 15.              B5
C36:40 LINFO  = 0, SAVE MODEL INFO RECORDS IN ONE FILE DATA_DESCRIPTION.        B5
C             = 1, SAVE MODEL INFO RECORDS IN SEPARATE FILES.                   B5
C                                                                               B5
CEXAMPLES:                                                                      B5
C                                                                               B5
CSPLTSLB      1    1    1    0                                                  B5
C(CONSTRUCT TIME SERIES FOR ALL VARAIBLES. SKIP 2ND ORDER STATISTICS.           B5
C USE IBUF(3) FOR FILE NAMES. KEEP PACKING UNCHANGED.)                          B5
C                                                                               B5
CSPLTSLB      1    1    1    0    1                                             B5
CSPLTSLB      ST                                                                B5
CSPLTSLB      PCP                                                               B5
CSPLTSLB      SIC                                                               B5
CSPLTSLB      SICN                                                              B5
CSPLTSLB      PMSL                                                              B5
CSPLTSLB      TEMP                                                              B5
CSPLTSLB      GZ                                                                B5
C                                                                               B5
C(CONSTRUCT TIME SERIES FOR 7 VARIABLES)                                        B5
C----------------------------------------------------------------------------
C
C     MAXVAR = MAXIMAL NUMBER OF VARIABLES IN INPUT FILES.
C     MAXUN  = MAXIMAL NUMBER OF ALLOWED I/O UNITS.
C
C      IMPLICIT REAL (A-H,O-Z),
C     +INTEGER (I-N)
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT NONE
      INTEGER MAXSL
      PARAMETER (MAXSL=2000)

      INTEGER NF,NLAB,I,I1,J,ILAB,NREC,NINP,LFN,LSL,LSL2,LLAB,NC4TO8,
     +     LOUT,LPARM,LTDIF,IDATE,NPRIM,NFIL,NOUT,NLAB1,
     +     LSKIP,LNAME,LSLAB,NPACK,LREAD,NVARS,LINFO,OPN3

      LOGICAL OK
C
C     INAM  = TABLE FOR VARIABLE NAME, TIME STEP RANGE AND LEVEL RANGE
C     IUN = ARRAY TO HOLD THE UNIT NUMBERS AVAILABLE FOR OUTPUT
C

      CHARACTER*64 SLAB(MAXSL),SLAB1(MAXSL),SLABL,SLABL2,SLABSEL(MAXSL)
      CHARACTER*512 FLAB(MAXSL)
      CHARACTER*4 NAME,NAME0
      INTEGER IUN(MAXSL)
      CHARACTER*512 FNAME
C
      REAL X(SIZES_BLONP1xBLAT)
      INTEGER IBUF,IDAT,MAXX
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      EQUIVALENCE (SLABL,IDAT)

      INTEGER JBUF,JDAT
      COMMON/JCOM/JBUF(8),JDAT(20)
      EQUIVALENCE (SLABL2,JDAT)

      EQUIVALENCE (NAME,IBUF(3))

      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NF=4
      CALL JCLPNT(NF,-1,-2,5,6)
      NF=NF-2
      IF (NF.LT.1) CALL                            XIT('SPLTSLB',-1)

C     * READ-IN DIRECTIVE CARD.

      READ(5,'(10X,6I5)',END=900) LSKIP,LNAME,LSLAB,NPACK,NVARS,LINFO
      WRITE(6,'(6(A,I5))')' LSKIP=',LSKIP,' LNAME=',LNAME,
     1     ' LSLAB=',LSLAB,' NPACK=',NPACK,' NVARS=',NVARS,
     2     ' LINFO=',LINFO
      IF(LSKIP.EQ.1)THEN
        WRITE(6,*)'SKIP 2ND ORDER STATISTICS.'
      ENDIF
      IF(LNAME.EQ.1.OR.LNAME.EQ.-1)THEN
        WRITE(6,*)'DERIVE FILE NAMES FROM NAME=IBUF(3).'
      ELSE
        WRITE(6,*)'DERIVE FILE NAMES FROM SUPERLABELS.'
      ENDIF
      IF(LSLAB.EQ.1)THEN
        WRITE(6,*)'WRITE SUPERLABELS TO OUTPUT FILES.'
      ELSE
        WRITE(6,*)'DO NOT WRITE SUPERLABELS TO OUTPUT FILES.'
      ENDIF
      WRITE(6,*)'NPACK=',NPACK
C
C     * READ NAMES TO BE SELECTED, IF SPECIFIED (NVARS!=0)
C
      IF(NVARS.NE.0)THEN
        NVARS=1
 90     READ(5,'(14X,A60)',END=95)SLABSEL(NVARS)
        WRITE(6,'(14X,A60)')SLABSEL(NVARS)
        NVARS=NVARS+1
        GOTO 90
 95     CONTINUE
        NVARS=NVARS-1
        IF(NVARS.LT.1)CALL                         XIT('SPLTSLB',-2)
        WRITE(6,*)' FOUND NVARS=',NVARS,' NAMES.'
      ENDIF
      IF(LINFO.EQ.1)THEN
        WRITE(6,*)'WRITE MODEL INFO RECORDS TO SEPARATE OUTPUT FILES.'
      ELSE
        WRITE(6,*)'WRITE MODEL INFO RECORDS TO ONE OUTPUT FILE.'
      ENDIF

      CALL SETLAB(JBUF,NC4TO8("LABL"),0,NC4TO8("LABL"),0,10,1,0,1)
      OPN3=0
C
C     * GET FILE NAME FROM FILELIST
C
      NFIL=0
 100  CONTINUE
      NLAB=0
      READ(1,*,END=300)FNAME,IDATE
      LFN=LEN_TRIM(FNAME)
      NFIL=NFIL+1
      WRITE(6,'(A,I5,2A)')' READING FILE',NFIL,': ',FNAME(1:LFN)
C
C     * OPEN INPUT FILE

      IF(OPN3.EQ.1) CLOSE(3)
      OPEN(3,FILE=FNAME, FORM='UNFORMATTED')
      REWIND 3
      OPN3=1
C
C     * PROCESS ALL RECORDS
C
      NAME0='    '
      LREAD=1
      NREC=0
      NINP=0
      NOUT=0
 200  CONTINUE
      IF(LREAD.EQ.1)CALL RECGET(3,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        WRITE(6,*)' READ-IN',NREC,' RECORDS.'
        WRITE(6,*)' FOUND  ',NLAB,' SUPERLABELS.'
        IF (NREC.EQ.0) THEN
          CALL                                     XIT('SPLTSLB',-3)
        ENDIF
        CLOSE(3)
        OPN3=0
        WRITE(6,*)' WRITTEN',NOUT,' RECORDS.'
        IF(NFIL.EQ.1)THEN
C
C         * COPY THE SUPERLABELS FOUND IN THE FIRST FILE
C
          NLAB1=NLAB
          DO I=1,NLAB
            SLAB1(I)=SLAB(I)
          ENDDO
        ELSE
C
C         * COMPARE SUPERLABELS IN THE CURRENT FILE TO THOSE IN THE FIRST FILE
C
          IF(NLAB1.NE.NLAB)THEN
            WRITE(6,'(3A)')'***ERROR: SUPERLABELS IN FILE ',
     +           FNAME(1:LFN),' ARE DIFFERENT FROM THOSE IN FILE 1.'
            CALL                                   XIT('SPLTSLB',-4)
          ENDIF
          DO I=1,NLAB
            DO I1=1,NLAB1
              IF(SLAB(I).EQ.SLAB1(I1))GOTO 205
            ENDDO
            WRITE(6,'(5A)')'***ERROR: SUPERLABEL ',SLAB(I),
     +           ' IN FILE ',FNAME(1:LFN),' IS NOT FOUND IN FILE 1.'
            CALL                                   XIT('SPLTSLB',-5)
 205        CONTINUE
          ENDDO
        ENDIF
        GOTO 100
      ENDIF
      NINP=NINP+1
      IF(NINP.EQ.1.AND.IBUF(1).NE.NC4TO8("LABL"))THEN
C
C       * THE INPUT FILE DO NOT HAVE SUPERLABELS
C       * USE IBUF(3) FOR FILE NAMES
C
        LNAME=-1
        WRITE(6,*)'NO SUPERLABELS IN INPUT FILE.'
        WRITE(6,*)'DERIVE FILE NAMES FROM IBUF(3).'
      ENDIF

      LREAD=1
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * FIND SUPERLABELS
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      IF (LNAME.NE.-1.AND.IBUF(1).EQ.NC4TO8("LABL")) THEN
        LSL=LEN_TRIM(SLABL)
        LPARM=0
C
C       *  DATA DESCRIPTION RECORDS
C
        IF((SLABL(5:MIN(20,LSL)).EQ.'DATA DESCRIPTION'.OR.
     +      SLABL(5:MIN(16,LSL)).EQ.'OCEAN MODEL '))LPARM=1
C
C       * DATA DESCRIPTION RECORDS ARE SAVED FROM THE FIRST FILE ONLY
C
        LOUT=1
        IF(LPARM.EQ.1.AND.NFIL.GT.1)THEN
          LOUT=0
        ENDIF
C
C       * CHECK IF THE SUPERLABEL IS IN THE SUBSET VARIABLE LIST
C
        IF(NVARS.GE.1)THEN
          DO I=1,NVARS
            LSL2=LEN_TRIM(SLABSEL(I))
            IF(SLABL(5:LSL).EQ.SLABSEL(I)(1:LSL2))GOTO 115
          ENDDO
          LOUT=0
          GOTO 200
        ENDIF
 115    CONTINUE
C
C       * CHECK IF THE SUPERLABEL HAS BEEN ALREADY FOUND PREVIOUSLY
C
        DO I=1,NLAB
          IF (SLABL(5:LSL).EQ.SLAB(I)) THEN
            IF(NFIL.EQ.1) THEN
              ILAB=I
            ELSE
              ILAB=-1
              DO J=1,NLAB1
                IF(SLABL(5:LSL).EQ.SLAB1(J)) ILAB=J
              ENDDO
              IF(ILAB.LT.0) CALL                   XIT('SPLTSLB',-6)
            ENDIF
            GOTO 200
          ENDIF
        ENDDO
C
C       * SKIP (CO)VARIANCE TERMS, IF REQUESTED
C
        IF(LSKIP.EQ.1) THEN
          NPRIM=0
          DO I=5,LSL
C           * X"Y" OR X"2
            IF(SLABL(I:I).EQ.'"') THEN
              NPRIM=NPRIM+1
              I1=MIN(I+1,LSL)
              IF (SLABL(I1:I1).EQ.'2') THEN
                NPRIM=NPRIM+1
              ENDIF
            ENDIF
          ENDDO
          LTDIF=0
          IF(LSL.GE.9.AND.SLABL(5:5).EQ.'D'.AND.
     +         SLABL((LSL-2):LSL).EQ.'/DT')LTDIF=1
          IF(NPRIM.EQ.2.OR.LTDIF.EQ.1)THEN
            LOUT=0
            GOTO 200
          ENDIF
        ENDIF
C
C       * NEW SUPERLABEL IS FOUND
C
        NLAB=NLAB+1
        IF(NLAB.GT.MAXSL)CALL                      XIT('SPLTSLB',-7)
        SLAB(NLAB)=SLABL(5:LSL)
C
C       * ASSIGN UNIT NUMBER ON THE FIRST PASS OR FIND THE
C       * UNIT NUMBER USED EARLIER
C
        IF(NFIL.EQ.1) THEN
          IF(NLAB.EQ.1)THEN
            IUN(NLAB)=10
          ELSE
            IUN(NLAB)=IUN(NLAB-1)+1
C
C         * SKIP UNITS 100-102 (RESERVED ON POLLUX)
C
            IF(IUN(NLAB).GE.100.AND.IUN(NLAB).LE.102)
     +           IUN(NLAB)=103
          ENDIF
          ILAB=NLAB
        ELSE
          ILAB=-1
          DO J=1,NLAB1
            IF(SLAB(NLAB).EQ.SLAB1(J)) ILAB=J
          ENDDO
          IF(ILAB.LT.0) CALL                       XIT('SPLTSLB',-8)
        ENDIF
C
        IF(LSLAB.EQ.1)THEN
C
C         * STORE SUPERLABEL
C
          SLABL2=SLABL
        ENDIF
C
C       * DERIVE FILE NAME ENDING FROM NAME OR SUPELABEL
C
        IF(LNAME.EQ.1.AND.LPARM.NE.1)THEN
C
C         * GET NAME FROM IBUF(3) OF THE NEXT RECORD
C
          CALL RECGET(3,-1,-1,-1,-1,IBUF,MAXX,OK)
          IF (.NOT.OK) THEN
            CALL                                   XIT('SPLTSLB',-9)
          ENDIF
          LREAD=0
          LLAB=0
          DO I=1,4
            IF(NAME(I:I).NE.' ')THEN
              LLAB=LLAB+1
              FLAB(NLAB)(LLAB:LLAB)=NAME(I:I)
            ENDIF
          ENDDO
        ELSE
C
C         * GET NAME FROM THE SUPERLABEL
C
          LLAB=0
          DO I=5,LSL
            LLAB=LLAB+1
            IF(SLABL(I:I).EQ.' ')THEN
              IF(LLAB.EQ.1)THEN
                LLAB=LLAB-1
              ELSE IF(FLAB(NLAB)((LLAB-1):(LLAB-1)).EQ.'_')THEN
                LLAB=LLAB-1
              ELSE
                FLAB(NLAB)(LLAB:LLAB)='_'
              ENDIF
            ELSE IF(SLABL(I:I).EQ.'"')THEN
              FLAB(NLAB)(LLAB:LLAB)='P'
C            ELSE IF(SLABL(I:I).EQ.'*')THEN
C              FLAB(NLAB)(LLAB:LLAB)='S'
C            ELSE IF(SLABL(I:I).EQ.'-')THEN
C              FLAB(NLAB)(LLAB:LLAB)='D'
            ELSE IF(SLABL(I:I).EQ.'/')THEN
              FLAB(NLAB)(LLAB:(LLAB+5))='_OVER_'
              LLAB=LLAB+5
            ELSE IF(SLABL(I:I).EQ.'.')THEN
              LLAB=LLAB-1
            ELSE IF(SLABL(I:I).EQ.'(')THEN
              LLAB=LLAB-1
            ELSE IF(SLABL(I:I).EQ.')')THEN
              LLAB=LLAB-1
            ELSE
              FLAB(NLAB)(LLAB:LLAB)=SLABL(I:I)
            ENDIF
          ENDDO
        ENDIF
C
C       * MAKE SURE THAT FILE NAME IS UNIQUE
C
        DO I=1,NLAB-1
          IF (FLAB(NLAB).EQ.FLAB(I)) THEN
            WRITE(6,*)'DUPLICATE NAME ',FLAB(NLAB)(1:LLAB)
C
C           * MAKE IT UNIQUE
C
            LLAB=LLAB+2
            I1=1
 110        WRITE(FLAB(NLAB)((LLAB-1):LLAB),'(A,I1)')'_',I1
            DO J=1,NLAB-1
              IF (FLAB(NLAB).EQ.FLAB(J)) THEN
                I1=I1+1
                IF(I1.GT.9)THEN
                  WRITE(6,*)' ***ERROR: TOO MANY DUPLICATE NAMES'
                  CALL                             XIT('SPLTSLB',-10)
                ENDIF
                GOTO 110
              ENDIF
            ENDDO
            WRITE(6,*)'CHANGE IT TO ',FLAB(NLAB)(1:LLAB)
          ENDIF
        ENDDO
C
C       * PUT ALL MODEL PARAMETERS RECORDS INTO ONE DATA_DESCRIPTION FILE
C       * (DATA_DESCRIPTION IS ASSUMED TO BE THE FIRST RECORD)
C
        IF(LINFO.NE.1.AND.LPARM.EQ.1.AND.
     1       SLABL(5:MIN(20,LSL)).NE.'DATA DESCRIPTION')THEN
          ILAB=1
C
C         * WRITE OUT SUPERLABEL
C
          IF(LSLAB.EQ.1)CALL RECPUT(IUN(ILAB),JBUF)
          GOTO 200
        ENDIF
C
C       * OPEN NEW UNIT AND PROCEED
C
        WRITE(6,'(I5,1X,A)')NLAB,FLAB(NLAB)(1:LLAB)
        IF(NFIL.EQ.1)THEN
          OPEN(IUN(NLAB),FILE=FLAB(NLAB)(1:LLAB),
     1         FORM='UNFORMATTED',ERR=910)
          REWIND IUN(NLAB)
          WRITE(2,'(A)')FLAB(NLAB)(1:LLAB)
          ILAB=NLAB
        ENDIF
C
C       * WRITE OUT SUPERLABEL
C
        IF(LSLAB.EQ.1)CALL RECPUT(IUN(ILAB),JBUF)
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       * LNAME=-1, NEW NAME
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      ELSE IF (LNAME.EQ.-1)THEN
        IF(NAME.NE.NAME0) THEN
          NAME0=NAME
C
C         * CHECK IF NAME IS ALREADY IN THE LIST
C
          DO I=1,NLAB
            IF (NAME.EQ.SLAB(I)(1:4)) THEN
              IF(NFIL.EQ.1) THEN
                ILAB=I
              ELSE
                ILAB=-1
                DO J=1,NLAB1
                  IF(NAME.EQ.SLAB1(J)(1:4)) ILAB=J
                ENDDO
                IF(ILAB.LT.0) CALL                 XIT('SPLTSLB',-11)
              ENDIF
              GOTO 210
            ENDIF
          ENDDO
          NLAB=NLAB+1
          IF(NLAB.GT.MAXSL)CALL                    XIT('SPLTSLB',-12)
          SLAB(NLAB)=NAME
          LLAB=0
          DO I=1,4
            IF(NAME(I:I).NE.' ')THEN
              LLAB=LLAB+1
              FLAB(NLAB)(LLAB:LLAB)=NAME(I:I)
            ENDIF
          ENDDO
C
C       * ASSIGN UNIT NUMBER ON THE FIRST PASS OR FIND THE
C       * UNIT NUMBER USED EARLIER
C
          IF(NFIL.EQ.1) THEN
            IF(NLAB.EQ.1)THEN
              IUN(NLAB)=10
            ELSE
              IUN(NLAB)=IUN(NLAB-1)+1
C
C         * SKIP UNITS 100-102 (RESERVED ON POLLUX)
C
              IF(IUN(NLAB).GE.100.AND.IUN(NLAB).LE.102)
     +             IUN(NLAB)=103
            ENDIF
            ILAB=NLAB
          ELSE
            ILAB=-1
            DO J=1,NLAB1
              IF(SLAB(NLAB).EQ.SLAB1(J)) ILAB=J
            ENDDO
            IF(ILAB.LT.0) CALL                     XIT('SPLTSLB',-13)
          ENDIF
C
C         * OPEN NEW UNIT AND PROCEED
C
          WRITE(6,'(I5,1X,A)')NLAB,FLAB(NLAB)(1:LLAB)
          IF(NFIL.EQ.1)THEN
            OPEN(IUN(NLAB),FILE=FLAB(NLAB)(1:LLAB),FORM='UNFORMATTED',
     1           ERR=920)
            REWIND IUN(NLAB)
            WRITE(2,'(A)')FLAB(NLAB)(1:LLAB)
            ILAB=NLAB
          ENDIF
        ENDIF
 210    CONTINUE
        NREC=NREC+1
        IF(IDATE.NE.-1)IBUF(2)=IDATE
        IF(NPACK.NE.0.AND.NPACK.GT.IBUF(8))THEN
          CALL RECUP2(X,IBUF)
          IBUF(8)=NPACK
          CALL RECPK2(IBUF,MAXX,X)
        ENDIF
        CALL RECPUT(IUN(ILAB),IBUF)
        NOUT=NOUT+1
      ELSE ! LABL
C
C       * WRITE OUT THE RECORD INTO THE APPROPRIATE FILE...
C
        NREC=NREC+1
        IF(LPARM.EQ.1)THEN
          IBUF(2)=0
        ELSE
          IF(IDATE.NE.-1)IBUF(2)=IDATE
        ENDIF
        IF(LOUT.EQ.1)THEN
          IF(LPARM.NE.1.AND.NPACK.NE.0.AND.NPACK.GT.IBUF(8))THEN
            CALL RECUP2(X,IBUF)
            IBUF(8)=NPACK
            CALL RECPK2(IBUF,MAXX,X)
          ENDIF
          CALL RECPUT(IUN(ILAB),IBUF)
          NOUT=NOUT+1
        ENDIF ! LOUT
      ENDIF ! LABL
      GOTO 200
C
C     * CLOSE ALL OUTPUT FILES
C
 300  CONTINUE
      DO I=1,NLAB
        CLOSE(IUN(I))
      ENDDO
      CALL                                         XIT('SPLTSLB',0)
 900  CALL                                         XIT('SPLTSLB',-14)
 910  CALL                                         XIT('SPLTSLB',-15)
 920  CALL                                         XIT('SPLTSLB',-16)
C---------------------------------------------------------------------
      END

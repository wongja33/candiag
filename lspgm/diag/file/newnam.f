      PROGRAM NEWNAM
C     PROGRAM NEWNAM (IN,       OUT,       INPUT,       OUTPUT,         )       B2
C    1          TAPE1=IN, TAPE2=OUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 14/93 - E. CHAN (END READ USING FBUFFIN IF K.GE.0)                    
C     JAN 27/92 - E.CHAN (REPLACE BUFFERED I/O AND CONVERT HOLLERITH            
C                         LITERALS TO ASCII)                                   
C     MAY 13/83 - R.LAPRISE.                                          
C     JUL 10/81 - J.D.HENDERSON.
C                                                                               B2
CNEWNAM  - CHANGES THE NAME IN IBUF(3) OF ALL RECORDS ON A FILE         1  1 C  B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - COPIES FILE IN TO FILE OUT RE-SETTING THE THIRD WORD OF              B3
C          THE LABEL IN EACH RECORD TO A NEW NAME READ FROM A CARD.             B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN  = FILE TO BE RENAMED.                                                B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      OUT = COPY OF IN WITH ALL RECORDS RENAMED                                B3
C 
CINPUT PARAMETER... 
C                                                                               B5
C      NAME = NEW NAME TO BE INSERTED INTO LABEL WORD 3.                        B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*  NEWNAM VORT                                                                 B5
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: LA = 10+SIZES_BLONP1xBLATxNWORDIO
      COMMON/ICOM/IBUF(LA)
C
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEW NAME FROM A CARD.
C 
      READ(5,5010,END=910) NAME                                                 B4
C 
C     * COPY THE FILE AND INSERT THE NEW NAME.
C
      NR=0
  100 CALL FBUFFIN(1,IBUF,LA,K,LEN)
      IF (K .GE. 0) GO TO 900
      IBUF(3)=NAME
      CALL FBUFOUT(2,IBUF,LEN,K)
      NR=NR+1 
      GO TO 100 
C 
C     * E.O.F. ON INPUT.
C 
  900 IF (NR.NE.0) THEN
        WRITE (6,6020) NR,NAME 
        CALL                                       XIT('NEWNAM',0)
      ELSE
        WRITE (6,6010)
        CALL                                       XIT('NEWNAM',-1) 
      ENDIF
C
  910 CALL                                         XIT('NEWNAM',-2) 
C---------------------------------------------------------------------
 5010 FORMAT(11X,A4)                                                            B4
 6010 FORMAT(' FILE EMPTY')
 6020 FORMAT(' ',I6,' LABELS CHANGED TO NEW NAME  ',A4)
      END

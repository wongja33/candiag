      PROGRAM INITSS9R
C     PROGRAM INITSS9RLX(ICTL,       SPPHIS,       GSINIT,       START,         I2
C    1                                                         OUTPUT,  )       I2
C    1            TAPE99=ICTL, TAPE1=SPPHIS, TAPE2=GSINIT, TAPE3=START,
C    2                                                   TAPE6=OUTPUT)
C     ----------------------------------------------------------------          I2
C
C     NOV 15/17 - S.KHARIN. MODIFIED FOR CREATING RELAXATION REFERENCE FILES    I2
C     APR 08/08 - M.LAZARE. NEW VERSION BASED ON INITSS8.                       I2
C                           DETERMINES ANALYSIS GRID SIZE FROM ACTUAL           I2
C                           READ GRID RECORD RATHER THAN CONTROL FILE.          I2
C     SEP 25/06 - F.MAJAESS WRITE "DATA" RECORDS WITH PACKING DENSITY OF "1".
C     FEB 20/06 - M.LAZARE. NEW VERSION BASED ON INITSS7, WITH:
C                           - CLEAN-UP DIMENSIONS USING PARAMETER
C                             STATEMENTS, FOR BETTER UNDERSTANDING.
C                           - BUGFIX TO HAVE ALP,EPSI ALSO AS REAL*8.
C                           - {WRK1,WRK2} -> {WRKS,WRKL} AS IN ICNTRLB.
C     JUL 03/03 - A.IRWIN,F.MAJAESS. PREVIOUS VERSION INITSS7.
C     NOV 06/95 - R.HARVEY PREVIOUS VERSION INITSS6.
C                                                                               I2
CINITSS9RLX - CONVERTS SIGMA LEVEL GAUSSIAN GRID DATASET TO SPHERICAL           I1
C          HARMONICS (OPTIMAL TOPOGRAPHY PHIS FIELD SAVED DIRECTLY)     3  1    I1
C                                                                               I3
CAUTHOR  - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - PERFORMS GLOBAL SPECTRAL ANALYSIS OF ILEV SIGMA LEVELS               I3
C          OF GLOBAL GAUSSIAN GRIDS FOR INPUT TO THE HYBRID G.C.M.              I3
C          WINDS ARE CONVERTED TO VORTICITY AND DIVERGENCE.                     I3
C          A GLOBALLY-AVERAGED SURFACE PRESSURE (AVGPS) IS CALCULATED BASED     I3
C          ON THE INPUT LNSP FIELD FROM THE PROGRAM INITGSC, AND IS PASSED TO   I3
C          THE START FILE. THE MODEL SUBROUTINE INGCM5 READS THIS VALUE WHICH   I3
C          IS SUBSEQUENTLY USED TO EXPLICITY CORRECT FOR ANY TENDENCY FOR NON-  I3
C          CONSERVATION OF MASS DUE TO THE CHOICE OF LNSP AS A MODEL VARIABLE.  I3
C          NOTE: OPTIMAL TOPOGRAPHY PHIS FIELD READ IN AND SAVED DIRECTLY,      I3
C                RATHER THAN USING FIELD ON CONTROL FILE AND CONVERTING         I3
C                FROM GG->SP.                                                   I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL   = INITIALIZATION CONTROL DATASET (SEE ICNTRL6)                    I3
C      SPPHIS = INITIALIZATION OPTIMAL SPECTRAL TOPOGRAPHY DATASET              I3
C      GSINIT = SIGMA/HYBRID LEVEL GAUSSIAN GRIDS CONTAINING THE FIELDS:        I3
C               GZS,LNSP,T(ILEV),((U,V)(ILEV)),ES(ILEV)                         I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      START  = SIGMA/HYBRID LEVEL SPECTRAL FIELDS OF...                        I3
C                                                                               I3
C                NAME   VARIABLE           UNITS     LEVELS                     I3
C                                                                               I3
C                LNSP   LN(SF.PRES.)       PA           1  SFC                  I3
C                TEMP   TEMPERATURE        DEG K     ILEV  SH                   I3
C                VORT   VORTICITY          1./SEC    ILEV  SG                   I3
C                 DIV   DIVERGENCE         1./SEC    ILEV  SG                   I3
C                  ES   MOISTURE VARIABLE            ILEV  SH                   I3
C                                                                               I3
C                THIS DATASET IS HEADED BY TWO SHORT RECORDS OF                 I3
C                MODEL CONTROL INFORMATION...                                   I3
C                NOTE THAT THESE SET CERTAIN RESOLUTION PARAMETERS              I3
C                WHICH ARE USED BY THE GCM WHEN IT STARTS UP.                   I3
C                                                                               I3
C                1. LABL,KSTART,ILEV,(SG(L),L=1,ILEV),(SH(L),L=1,ILEV),         I3
C                   LAY,ICOORD,PLID,MOIST                                       I3
C                2. LABL,LRLMT,ILGM,ILATM,IDAY,GMT                              I3
C                                                                               I3
C                LABL = 4 CHAR LABEL WORD.                                      I3
C                KSTART = INITIAL TIMESTEP NUMBER (SET TO 0).                   I3
C                LRLMT = SPECTRAL RESOLUTION (ONE WORD CONTAINING               I3
C                        LR, LM, AND KTR).                                      I3
C                                                                               I3
C-----------------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * THE FOLLOWING PARAMETER STATEMENTS ARE SET UP FOR A T128 MODEL
C     * RESOLUTION IN SPECTRAL SPACE AND A 256X128 INPUT ANALYSIS SIZE
C     * WITH 100 LEVELS.
c     * NOTE THAT "MAXX" IS USED FOR BOTH GRID AND SPECTRAL I/O SIZES.
C     * SINCE IT IS DEFINED FOR THE GRID SIZE AND THIS IS LARGER THAN
C     * THE SPECTRAL SIZE!
C     * ** HIGHER FUTURE RESOLUTIONS WILL REQUIRE CORRESPONDING CHANGES!! **
C
      PARAMETER(LMTOTAL=128, MAXLEV=100, NLON=256, NLAT=128,
     1     LMP=LMTOTAL+1, LATOTAL=(LMTOTAL*LMP)/2, IRAM=LATOTAL+LMTOTAL,
     2     MAXLG=NLON+2, IGRID=(NLON+1)*NLAT, MAXX=IGRID,
     3     IGRIDL=IGRID*MAXLEV, IWRKL=(NLON+1)*MAXLEV, IWRKS=MAXLG)

      REAL*8 SL(NLAT),CL(NLAT),WL(NLAT),WOSSL(NLAT),RAD(NLAT)
      REAL*8 SL2(NLAT),CL2(NLAT),WL2(NLAT),WOSSL2(NLAT),RAD2(NLAT)
      REAL*8 ALP(IRAM),DALP(IRAM),EPSI(IRAM)
      REAL SG(MAXLEV),SH(MAXLEV),TRIGS1(MAXLG),TRIGS2(MAXLG)
      INTEGER LSR(2,LMP),IFAX1(10),IFAX2(10)
      INTEGER IBU(8,MAXLEV),IBV(8,MAXLEV)

C     * GG,GGX ARE WORK FIELDS FOR GAUSSIAN GRIDS (ILG1,ILAT).
C     * FACTOR OF TWO FOR DQ,WRKL,WRKS COMES FROM {Q,D} PACKED
C     * INTO SINGLE ARRAYS.

      COMPLEX DQ(IRAM*MAXLEV*2)
      COMPLEX Q(IRAM)
      REAL U(IGRIDL),V(IGRIDL)
      REAL WRKL(MAXLG*2*MAXLEV),WRKS(MAXLG*2*MAXLEV)

      LOGICAL OK

      COMMON /BLANCK/ GG (IGRID),GGX (IGRID)
      COMMON /ICOM  / IBUF(8),IDAT(MAXX)

      DATA NF0,NF1,NF2,NPACK/ 1,2,3,2/
C-----------------------------------------------------------------------
      NFIL=5
      CALL JCLPNT (NFIL,99,1,2,3,6)

C     * GET PARAMETERS FROM CONTROL FILE.

      REWIND 99
      READ(99,END=903) LABL,ILEV,
     1                 (SG(L),L=1,ILEV),
     2                 (SH(L),L=1,ILEV),
     3                 LAY,ICOORD,PTOIT,MOIST,SREF,SPOW
      IF((ILEV.LT.1).OR.(ILEV.GT.MAXLEV)) CALL     XIT('INITSS9RLX',-1)
      READ(99,END=904) LABL,III,JJJ,ILGM,ILATM,LRLMT,IDAY,GMT
      WRITE(6,6015)    LRLMT
      WRITE(6,6100)    LAY,ICOORD,PTOIT
      WRITE(6,6110)    MOIST,SREF,SPOW

C
C     * DETERMINE SPECTRAL FIELD PARAMETERS.
C
      CALL DIMGT  (LSR,LA,LR,LM,KTR,LRLMT)
      CALL EPSCAL (EPSI,LSR,LM)
      NREC=0
 100  CONTINUE
C-----------------------------------------------------------------------
C     * LOG OF SURFACE PRESSURE (MILLIBARS).
C     * IN ADDITION TO PERFORMING SPECTRAL TRANSFORM, CALCULATE GLOBALLY-
C     * AVERAGED SURFACE PRESSURE AND SAVE ON START FILE.
C
      CALL GETFLD2(NF1,GG ,NC4TO8("GRID"),-1,NC4TO8("LNSP"),-1,
     +                                           IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        IF(NREC.EQ.0)CALL                          XIT('INITSS9RLX',-2)
        WRITE(6,'(A,I10,A)')'PROCESSED',NREC,' RECORDS.'
        CALL                                       XIT('INITSS9RLX',0)
      ENDIF
      ILG=IBUF(5)-1
      ILAT=IBUF(6)
      IDATE=IBUF(2)
      WRITE(6,6017)    ILG,ILAT,ILGM,ILATM
      CALL PRTLAB (IBUF)
      NPACK=MIN(NPACK,IBUF(8))
      NWDS=IBUF(5)*IBUF(6)
C
C     * CALCULATE SET-UP FIELDS FOR TRANFORMS.
C
      ILATH=ILAT/2
      ILG1 =ILG+1
      ILH  =(ILG1+1)/2
      CALL GAUSSG (ILATH,SL,WL,CL,RAD,WOSSL)
      CALL  TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
      CALL FTSETUP(TRIGS1,IFAX1,ILG)
C
C     * CONVERT TO SURFACE PRESSURE AND CALCULATE GLOBAL AVERAGE.
C
C      DO 150 I=1,NWDS
C        GGX(I)=EXP(GG(I))
C  150 CONTINUE
C      AVGPS=0.E0
C      DO 190 J=1,ILAT
C        N = ILG1 * J
C        GGX(N) = 0.E0
C        DO 180 I=1,ILG
C          IJ = (J - 1) * ILG1 + I
C          GGX(N) = GGX(N) + GGX(IJ) * WL(J) * 0.5E0
C  180   CONTINUE
C        AVGPS = AVGPS + GGX(N) / ILG
C  190 CONTINUE
C      AVGPS=AVGPS*100.E0
C      WRITE(6,6020) (IBUF(N),N=1,4),AVGPS
C      CALL SETLAB (IBUF,NC4TO8("DATA"),KSTART,NC4TO8("DATA"),1,2,1,0,1)
C      CALL FBUFOUT (NF2,IBUF,-8,K)
C      WRITE(NF2) REAL(LABL),AVGPS

      CALL GGAST (Q,LSR,LM,LA, GG,ILG1,ILAT,1,SL,WL,
     1            ALP,EPSI,WRKS,WRKL,TRIGS1,IFAX1,MAXLG)
      CALL SETLAB(IBUF,NC4TO8("SPEC"),IDATE,NC4TO8("LNSP"),1,
     +                                  LA,1,LRLMT,NPACK)
      CALL PUTFLD2(NF2,Q,IBUF,MAXX)
      WRITE(6,6026) IBUF
C--------------------------------------------------------------------
C     * MOUNTAINS (M/SEC)**2. (FROM OPTIMAL TOPOGRAPHY FILE).
C
      CALL GETFLD2(-NF0,Q,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INITSS9RLX',-3)
      CALL PRTLAB (IBUF)
      CALL SETLAB (IBUF,NC4TO8("SPEC"),IDATE,NC4TO8("PHIS"),1,
     +                                  LA,1,LRLMT,NPACK)
      CALL PUTFLD2(NF2,Q,IBUF,MAXX)
      WRITE(6,6026) IBUF

C--------------------------------------------------------------------
C     * TEMPERATURE (DEG K).

      DO 250 L=1,ILEV
        CALL GETFLD2 (NF1,GG ,NC4TO8("GRID"),IDATE,NC4TO8("TEMP"),-1,
     +                                              IBUF,MAXX,OK)
        CALL PRTLAB  (IBUF)
        CALL GGAST   (Q,LSR,LM,LA, GG,ILG1,ILAT,1,SL,WL,
     1                ALP,EPSI,WRKS,WRKL,TRIGS1,IFAX1,MAXLG)
        CALL SETLAB  (IBUF,NC4TO8("SPEC"),IDATE,NC4TO8("TEMP"),-1,
     +                                       LA,1,LRLMT,NPACK)
        CALL PUTFLD2 (NF2,Q,IBUF,MAXX)
        WRITE(6,6026) IBUF
  250 CONTINUE
C--------------------------------------------------------------------
C     * WIND COMPONENTS (U,V)*COS(LAT)/(EARTH RADIUS).
C     * CONVERT TO VORTICITY AND DIVERGENCE (1./SEC).

      NX = 1
      DO 300 L=1,ILEV
        CALL GETFLD2 (NF1,U(NX) ,NC4TO8("GRID"),IDATE,NC4TO8("   U"),-1,
     +                                              IBUF,MAXX,OK)
        CALL PRTLAB (IBUF)
        DO N=1,8
          IBU(N,L)=IBUF(N)
        ENDDO
C
        CALL GETFLD2 (NF1,V(NX) ,NC4TO8("GRID"),IDATE,NC4TO8("   V"),-1,
     +                                              IBUF,MAXX,OK)
        CALL PRTLAB (IBUF)
        DO N=1,8
          IBV(N,L)=IBUF(N)
        ENDDO
C
        NX = NX + NWDS
  300 CONTINUE
C
C     * COMPUTE VORT AND DIV FROM MODEL WINDS.
C
      CALL GWAQD (DQ,LSR,LA,LM,U,V,ILG1,ILH,ILAT,SL,WOSSL,ALP,
     1            DALP,EPSI,WRKL,WRKS,ILEV,IFAX1,TRIGS1)
C
C     * SAVE RESULTS.
C
      ND=1
      NQ=LA*ILEV+1
      DO 350 L=1,ILEV
        LVL=IBU(4,L)
        CALL SETLAB  (IBUF,NC4TO8("SPEC"),IDATE,NC4TO8("VORT"),LVL,
     +                                       LA,1,LRLMT,NPACK)
        CALL PUTFLD2 (NF2,DQ(NQ),IBUF,MAXX)
        WRITE(6,6026) IBUF
C
        LVL=IBV(4,L)
        CALL SETLAB  (IBUF,NC4TO8("SPEC"),IDATE,NC4TO8(" DIV"),LVL,
     +                                       LA,1,LRLMT,NPACK)
        CALL PUTFLD2 (NF2,DQ(ND),IBUF,MAXX)
        WRITE(6,6026) IBUF
C
        NQ=NQ+LA
        ND=ND+LA
  350 CONTINUE
C--------------------------------------------------------------------
C     * MOISTURE VARIABLE.

      IF(MOIST.EQ.NC4TO8("SL3D") .OR. MOIST.EQ.NC4TO8("SLQB"))THEN

        WRITE(6,6123) MOIST

C     * MOISTURE FIELD MUST BE WRITTEN IN GRID FORM TO START FILE
C     * LE SINUS DES LATITUDES GAUSSIENNES DOIVENT ETRE RECALCULE
C     * (DANS SL2) POUR LA GRILLE ILATM, SL EST VALIDE POUR LA
C     * GRILLE ILAT=128

        CALL GAUSSG (ILATM/2,SL2,WL2,CL2,RAD2,WOSSL2)
        CALL  TRIGL (ILATM/2,SL2,WL2,CL2,RAD2,WOSSL2)
        CALL FTSETUP(TRIGS2,IFAX2,ILGM)

        DO 440 L=1,ILEV
           CALL GETFLD2 (NF1,GG ,NC4TO8("GRID"),IDATE,NC4TO8("  ES"),-1,
     +                                                 IBUF,MAXX,OK)
           CALL PRTLAB (IBUF)
           CALL GGAST (Q,LSR,LM,LA, GG,ILG1,ILAT,1,SL,WL,
     1                   ALP,EPSI,WRKS,WRKL,TRIGS1,IFAX1,MAXLG)
           CALL STAGG (GG,ILGM+1,ILATM,1,SL2,Q,LSR,LM,LA,
     1                   ALP,EPSI,WRKS,WRKL,TRIGS2,IFAX2,ILGM+2)

C     * ELIMINATE NEGATIVE VALUES BEFORE SAVING THE MOISTURE FIELD

           DO 400 NN=1,(ILGM+1)*ILATM
              GG(NN)=MAX(GG(NN),0.E0)
 400       CONTINUE
           CALL SETLAB  (IBUF,NC4TO8("GRID"),IDATE,NC4TO8("  ES"),-1,
     +                                      ILGM+1,ILATM,IDATE,NPACK)
           CALL PUTFLD2 (NF2,GG,IBUF,MAXX)
           WRITE(6,6026) IBUF
 440    CONTINUE

      ELSE

        DO 450 L=1,ILEV
          CALL GETFLD2 (NF1,GG ,NC4TO8("GRID"),IDATE,NC4TO8("  ES"),-1,
     +                                                IBUF,MAXX,OK)
          CALL PRTLAB (IBUF)
          CALL GGAST   (Q,LSR,LM,LA, GG,ILG1,ILAT,1,SL,WL,
     1                  ALP,EPSI,WRKS,WRKL,TRIGS1,IFAX1,MAXLG)
          CALL SETLAB  (IBUF,NC4TO8("SPEC"),IDATE,NC4TO8("  ES"),-1,
     +                                         LA,1,LRLMT,NPACK)
          CALL PUTFLD2 (NF2,Q,IBUF,MAXX)
          WRITE(6,6026) IBUF
 450    CONTINUE
      ENDIF

      NREC=NREC+1
      GOTO 100

      CALL                                         XIT('INITSS9RLX',0)

C     * E.O.F. ON FILE ICTL.

  903 CALL                                         XIT('INITSS9RLX',-4)
  904 CALL                                         XIT('INITSS9RLX',-5)
C-----------------------------------------------------------------------
 5010 FORMAT (10X,5I5)
 6015 FORMAT ('0 LRLMT =',I10)
 6017 FORMAT ('0ANALYSIS GRID =',2I5,5X,'MODEL GRID =',2I5/)
 6020 FORMAT(' FROM ',A4,2X,I10,2X,A4,2X,I10,' THE SURFACE MEAN IS ',
     1       E12.6)
 6026 FORMAT (' ',60X,A4,I10,2X,A4,I10,4I8)
 6100 FORMAT(' LAY=',I5,', COORD=',A4,', P.LID(PA)=',F10.3)
 6110 FORMAT(' MOIST=',A4,', SREF=',E12.4,', SPOW=',F10.3)
 6123 FORMAT(' MOIST=',4A,' GRIDDED MOISTURE FIELD *NOT* CONVERTED',
     1       ' TO SPRECTRAL FORM')
      END

      PROGRAM T2TEST
C     PROGRAM T2TEST (U1BAR, VU1, V1BAR, VV1, COV1,                             H2
C    1                U2BAR, VU2, V2BAR, VV2, COV2,                             H2
C    2                DU, DV, AMP, FVALUE, KVALUE,  DVALUE,      MASK,          H2
C    3                                              INPUT,       OUTPUT,)       H2
C    4         TAPE11=U1BAR,  TAPE12=VU1,    TAPE13=V1BAR,  TAPE14=VV1, 
C    5                                                      TAPE15=COV1,
C    6         TAPE16=U2BAR,  TAPE17=VU2,    TAPE18=V2BAR,  TAPE19=VV2, 
C    7                                                      TAPE20=COV2,
C    8                        TAPE21=DU,     TAPE22=DV,     TAPE23=AMP, 
C    9         TAPE24=FVALUE, TAPE25=KVALUE, TAPE26=DVALUE, TAPE27=MASK,
C    A                                        TAPE5=INPUT, TAPE6=OUTPUT)
C     ------------------------------------------------------------------        H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     AUG 21/00 - F.MAJAESS (ENSURE FIELD DIMENSION CHECK VERSUS "MAXRSZ")      
C     JUN 05/94 - B.DENIS  (MODIFY CALCULATION OF A1SCAL/A2SCAL)                
C     MAR 07/94 - F.MAJAESS (MODIFY DATA SECTION)
C     AUG 04/92 - E. CHAN   (MODIFY EXTRACTION/CALCULATION OF XMAX, XMIN,       
C                            AND XSCALE DUE TO IMPLEMENTATION OF NEW PACKER)    
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     OCT 11/89 - F.MAJAESS (ALLOW SUPPLYING VARIANCE OR UNBIASED              
C                            ESTIMATES OF THE STANDARD DEVIATION)             
C     APR 29/88 - F. MAJAESS (REPLACE ABORT EXIT BY WARNING EXIT
C                             FOR UNMATCHED MEAN FIELD NAMES) 
C 
C     NOV 06/87 - F. MAJAESS
C                                                                               H2
CT2TEST  - CONDUCT A TWO SAMPLE HOTELLING'S T**2 STATISTICAL TEST.     10  7 C  H1
C                                                                               H3
CAUTHORS - F. ZWIERS, F. MAJAESS.                                               H3
C                                                                               H3
CPURPOSE - CONDUCT A TWO SAMPLE HOTELLING'S T**2 STATISTICAL TEST.              H3
C          UPON RETURN, THE PROGRAM REPORTS THE COMPUTED STATISTICAL            H3
C          VALUES IN FVALUE AS WELL AS RETURNING THE K-VALUE AND                H3
C          D-VALUE WHICH ARE RESPECTIVELY THE TRANSFORMED SIGNIFICANCE          H3
C          LEVELS AND THE ACCEPTANCE/REJECTION FLAG. ALSO RETURNED ARE          H3
C          THE DIFFERENCES EXPERIMENT-CONTROL OF THE TWO COMPONENTS IN          H3
C          DU AND DV AS WELL AS THE AMPLITUDE OF THE VECTOR DIFFERENCE          H3
C          IN AMP.                                                              H3
C                                                                               H3
C          NOTE - TESTS ARE  NOT PERFORMED  WHEN ONE  OR  MORE  OF  THE         H3
C                 "POOLED VARIANCES" AND/OR THE CORRELATION COEFFICIENT         H3
C                 IS APPROXIMATELY EQUAL TO  THE  NOISE INDUCED  BY THE         H3
C                 PACKING ALGORITHM.                                            H3
C                 INTERPRETATION OF VU1, VV1,  VU2  AND  VV2  FILES  IS         H3
C                 CONTROLED BY INPUT PARAMETER IVAR.                            H3
C                 MASK FILE NEED NOT BE SPECIFIED IN THE PROGRAM CALLING        H3
C                 SEQUENCE. (SEE OUTPUT FILES SECTION BELOW).                   H3
C                                                                               H3
CINPUT FILES...                                                                 H3
C                                                                               H3
C  CONTROL DATASET STATISTICS FOR W1(U1,V1):                                    H3
C  =========================================                                    H3
C    U1BAR= FIELDS OF MEANS OF THE 'U1-CONTROL' VARIABLE.                       H3
C           SUM(U1(I))/N1, I=1,...,N1.                                          H3
C   IF (IVAR.EQ.0) THEN                                                         H3
C    VU1  = THE VARIANCE OF U1. SUM(U1(I)-U1BAR)**2/N1, I=1,...,N1.             H3
C   OTHERWISE,                                                                  H3
C    VU1  = THE UNBIASED ESTIMATES OF THE STANDARD DEVIATIONS OF U1.            H3
C           SQRT(SUM(U1(I)-U1BAR)**2/(N1-1)), I=1,...,N1.                       H3
C                                                                               H3
C    V1BAR= SAME AS U1BAR EXCEPT FOR THE 'V1-CONTROL' VARIABLE.                 H3
C    VV1  = SAME AS VU1   EXCEPT FOR THE 'V1-CONTROL' VARIABLE.                 H3
C    COV1 = COVARIANCE OF THE CONTROL COMPONENTS.                               H3
C           SUM((U1(I)-U1BAR)*(V1(I)-V1BAR))/N1, I=1,...,N1.                    H3
C                                                                               H3
C  EXPERIMENT DATASET STATISTICS FOR W2(U2,V2):                                 H3
C  ============================================                                 H3
C    U2BAR= FIELDS OF MEANS OF THE 'U2-EXPERIMENT' VARIABLE.                    H3
C           SUM(U2(J))/N2, J=1,...,N2.                                          H3
C   IF (IVAR.EQ.0) THEN                                                         H3
C    VU2  = THE VARIANCE OF U2. SUM(U2(J)-U2BAR)**2/N2, J=1,...,N2.             H3
C   OTHERWISE,                                                                  H3
C    VU2  = THE UNBIASED ESTIMATES OF THE STANDARD DEVIATIONS OF U2.            H3
C           SQRT(SUM(U2(J)-U2BAR)**2/(N2-1)), J=1,...,N2.                       H3
C                                                                               H3
C    V2BAR= SAME AS U2BAR EXCEPT FOR THE 'V2-EXPERIMENT' VARIABLE.              H3
C    VV2  = SAME AS VU2   EXCEPT FOR THE 'V2-EXPERIMENT' VARIABLE.              H3
C    COV2 = COVARIANCE OF THE EXPERIMENT COMPONENTS.                            H3
C           SUM((U2(J)-U2BAR)*(V2(J)-V2BAR))/N2, J=1,...,N2.                    H3
C                                                                               H3
COUTPUT FILES...                                                                H3
C                                                                               H3
C    DU    = FIELDS OF MEAN DIFFERENCES FOR THE FIRST COMPONENTS                H3
C            COMPUTED AS DU=U2BAR-U1BAR.                                        H3
C                                                                               H3
C    DV    = FIELDS OF MEAN DIFFERENCES FOR THE SECOND COMPONENTS               H3
C            COMPUTED AS DV=V2BAR-V1BAR.                                        H3
C                                                                               H3
C    AMP   = FIELDS OF THE AMPLITUDE OF THE MEAN DIFFERENCES                    H3
C            COMPUTED AS AMP=SQRT(DU**2+DV**2).                                 H3
C                                                                               H3
C    FVALUE= FIELDS OF 'F-VALUES' STANDARDIZED DIFFERENCES OF MEANS.            H3
C            USING N1,N2,DU,DV,VU1,VU2,COV1,COV2 AS DEFINED ABOVE,              H3
C            THIS FIELD IS COMPUTED AS FOLLOWS:                                 H3
C                                                                               H3
C                 F=FACT3*T22                                                   H3
C                                                                               H3
C                 FACT3=(N1+N2-2-1)/(2*(N1+N2-2))                               H3
C                                                                               H3
C                 T22=FACT2*SDIF/(1-ROP**2)                                     H3
C                                                                               H3
C                 FACT2=N1*N2/(N1+N2)                                           H3
C                                                                               H3
C                 SDIF=SU**2 - 2*ROP*SU*SV + SV**2                              H3
C                                                                               H3
C                 SU=DU/S1P                                                     H3
C                                                                               H3
C                 SV=DV/S2P                                                     H3
C                                                                               H3
C                 S1P=SQRT(S1P2)                                                H3
C                                                                               H3
C                 S1P2=FACT1*(N1*VU1+N2*VU2)                                    H3
C                                                                               H3
C                 FACT1=1/(N1+N2-2)                                             H3
C                                                                               H3
C                 S2P=SQRT(S2P2)                                                H3
C                                                                               H3
C                 S2P2=FACT1*(N1*VV1+N2*VV2)                                    H3
C                                                                               H3
C                 ROP=FACT1*(N1*COV1+N2*COV2)/(S1P*S2P)                         H3
C                                                                               H3
C                                                                               H3
C            T22 ABOVE IS THE TWO SAMPLE HOTELLING'S T**2 STATISTICAL VALUE.    H3
C                                                                               H3
C    KVALUE= FIELDS OF 'K-VALUES' OF TRANSFORMED SIGNIFICANCE LEVELS.           H3
C            COMPUTED AS K IN:                                                  H3
C                                                                               H3
C                       P=ALPHA/(5**(K-1))                                      H3
C                                                                               H3
C            OR         K=LOG (ALPHA/P)+1                                       H3
C                            5                                                  H3
C                                                                               H3
C            WHICH INDICATES THAT THE OBSERVED STATISTIC IS                     H3
C            SIGNIFICANT AT THE                                                 H3
C                       ALPHA/(5**(K-1))                                        H3
C            SIGNIFICANCE LEVEL.                                                H3
C            THUS K=1 INDICATES THAT THE OBSERVED STATISTIC IS JUST             H3
C            SIGNIFICANT AT THE ALPHA SIGNIFICANCE LEVEL.                       H3
C            IF PLOTTED WITH UNIT CONTOUR INTERVALS, SUCCESSIVE                 H3
C            CONTOURS WILL ENCLOSE REGIONS WHERE LOCALLY IT IS                  H3
C            FIVE TIMES AS UNLIKELY THAT VALUES OF THE OBSERVED                 H3
C            STATISTICS ARE CONSISTENT WITH THE NULL HYPOTHESIS                 H3
C            THAN IN REGIONS OUTSIDE THE NEXT LOWER CONTOUR.                    H3
C                                                                               H3
C    DVALUE= FIELDS OF 'D-VALUES' COMPUTED AS:                                  H3
C                       __                                                      H3
C                       ! 0  IF THE NULL HYPOTHESIS IS ACCEPTED                 H3
C                   D = !                                                       H3
C                       ! 1  IF THE NULL HYPOTHESIS IS REJECTED                 H3
C                       --                                                      H3
C                                                                               H3
C                                                                               H3
C    MASK  = FIELDS OF 'MASK-VALUES' COMPUTED AS:                               H3
C                       __                                                      H3
C                       ! 1  IF THE TEST WAS     CONDUCTED                      H3
C                MASK = !                                                       H3
C                       ! 0  IF THE TEST WAS NOT CONDUCTED                      H3
C                       --                                                      H3
C                                                                               H3
C            THE MASK FILE IS NOT RETURNED IF THE PROGRAM IS NOT                H3
C            CALLED WITH OUTPUT FILE MASK.                                      H3
C 
CINPUT PARAMETERS...
C                                                                               H5
C     N1=INT(AN1) & N2=INT(AN2)                                                 H5
C                                                                               H5
C     WHERE,                                                                    H5
C                                                                               H5
C     ALPHA = THE SIGNIFICANCE LEVEL OF THE TEST.                               H5
C     N1    = THE NUMBER OF OBSERVATIONS IN THE CONTROL    DATA SET,            H5
C     N2    = THE NUMBER OF OBSERVATIONS IN THE EXPERIMENT DATA SET.            H5
C             IF N1 >= 1 , N2 >= 1 AND N1+N2 > 3 THEN                           H5
C                N1 AND N2 READ FROM THE INPUT CARD ARE USED IN THE             H5
C                COMPUTATION OF THE NUMBER OF DEGREES OF FREEDOM.               H5
C             OTHERWISE,                                                        H5
C                THE N1 AND N2 VALUES NEEDED IN THE COMPUTATION ARE             H5
C                OBTAINED RESPECTIVELY FROM THE LABELS OF THE FILES             H5
C                U1BAR AND U2BAR.                                               H5
C     IVAR  = A FLAG USED TO DETERMINE THE CONTENTS OF VU1, VV1, VU2            H5
C             AND VV2 FILES. THAT IS :                                          H5
C             IF (IVAR.EQ.0) THEN                                               H5
C               VU1, VV1, VU2 AND VV2 FILES CONTAIN THE VARIANCES OF            H5
C                U1,  V1,  U2 AND  V2 RESPECTIVELY.                             H5
C             OTHERWISE,                                                        H5
C               VU1, VV1, VU2 AND VV2 FILES CONTAIN THE UNBIASED                H5
C                ESTIMATES  OF  THE   STANDARD   DEVIATIONS   OF                H5
C                U1,  V1,  U2 AND  V2 RESPECTIVELY.                             H5
C                                                                               H5
CEXAMPLE OF INPUT CARD...                                                       H5
C                                                                               H5
C*T2TEST.      5.E-2  10.   5.    1                                             H5
C-------------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     &INTEGER (I-N)
      DIMENSION IBUF1(8),IBUF2(8),A1(SIZES_LONP1xLAT),
     & A2(SIZES_LONP1xLAT), DU(SIZES_LONP1xLAT),DV(SIZES_LONP1xLAT),
     & S1P2(SIZES_LONP1xLAT),S2P2(SIZES_LONP1xLAT),
     & P(SIZES_LONP1xLAT),T22(SIZES_LONP1xLAT),
     & F(SIZES_LONP1xLAT),D(SIZES_LONP1xLAT),
     & AMP(SIZES_LONP1xLAT),WK1(SIZES_LONP1xLAT),WK2(SIZES_LONP1xLAT),
     & WK3(SIZES_LONP1xLAT), FMASK(SIZES_LONP1xLAT)
      REAL S1P,S2P,ROP,SU,SV,SDIF,K(SIZES_LONP1xLAT) 
      COMMON/BLANCK/DU,DV,AMP,F,P,A1,FMASK
      COMMON/MACHTYP/MACHINE,INTSIZE
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      EQUIVALENCE (DU(1),WK1(1)),(DV(1),WK2(1)),
     &            (F(1),T22(1),A2(1),D(1)),(P(1),S2P2(1),K(1)), 
     &            (AMP(1),WK3(1),S1P2(1)) 
      LOGICAL OK,LN1N2,NRMLXIT,MASK,MATCH,VAR 
C     DATA NGRID/4HGRID/,NZONL/4HZONL/,NDU/4H  DU/,NDV/4H  DV/
C     DATA NAMP /4H AMP/,NF   /4H   F/,NK   /4H   K/
C     DATA ND   /4H   D/,NMASK/4HMASK/
      DATA NBW/64/, MAXX/SIZES_LONP1xLATxNWORDIO/
      DATA MAXRSZ /SIZES_LONP1xLAT/
C-------------------------------------------------------------------------------
C 
      NGRID=NC4TO8("GRID")
      NZONL=NC4TO8("ZONL")
      NDU=NC4TO8("  DU")
      NDV=NC4TO8("  DV")
      NAMP =NC4TO8(" AMP")
      NF   =NC4TO8("   F")
      NK   =NC4TO8("   K")
      ND   =NC4TO8("   D")
      NMASK=NC4TO8("MASK")
      NRMLXIT=.TRUE.
      MATCH=.TRUE.
      NFF=19
      CALL JCLPNT(NFF,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
     1               25,26,27,5,6)
      IF(NFF.GT.18)THEN 
        MASK=.TRUE. 
      ELSE
        MASK=.FALSE.
      ENDIF 
C 
      DO 1010 KK=11,20
         REWIND KK
 1010 CONTINUE
C 
C-----------------------------------------------------------------------
C     * GET ALPHA, N1 , N2 AND IVAR.
C 
      READ(5,5010,END=8000)ALPHA,AN1,AN2,IVAR                                   H4
      GO TO 8010
C 
C-----------------------------------------------------------------------
C     * EOF ON UNIT #5. 
C 
 8000 CONTINUE
      WRITE(6,6010) 
      CALL                                         XIT('T2TEST',-1) 
 8010 CONTINUE
      WRITE(6,6020)ALPHA,AN1,AN2,IVAR 
      N1=INT(AN1+0.0001E0)
      N2=INT(AN2+0.0001E0)
C 
C     * CHECK N1, N2 AND IVAR VALUES. 
C 
      IF((N1.LT.1).OR.(N2.LT.1).OR.(N1+N2.LE.3))THEN
        LN1N2=.TRUE.
      ELSE
        LN1N2=.FALSE. 
      ENDIF 
      IF(IVAR.EQ.0)THEN 
        VAR=.TRUE.
      ELSE
        VAR=.FALSE. 
      ENDIF 
C 
C-----------------------------------------------------------------------
C 
      IKIND=1 
      NREC=0
 1020 CONTINUE
C 
C     * OBTAIN U1BAR. 
C 
      CALL GETFLD2(11,A1,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         IF(NREC.EQ.0)THEN
            WRITE(6,6090) 
            CALL                                   XIT('T2TEST',-2) 
         ENDIF
         WRITE(6,6030) NREC 
         IF(NRMLXIT.AND.MATCH)THEN
           CALL                                    XIT('T2TEST',0)
         ELSE 
           CALL                                    XIT('T2TEST',-101) 
         ENDIF
      ENDIF 
      DO 1030 I=1,8 
         IBUF1(I)=IBUF(I) 
 1030 CONTINUE
      IF(NREC.EQ.0)WRITE(6,6040)IBUF1 
      IF(LN1N2)N1=IBUF1(2)
      IF(N1.LT.1)THEN 
         WRITE(6,6050)N1
         CALL                                      XIT('T2TEST',-3) 
      ENDIF 
C 
C     * OBTAIN U2BAR. 
C 
      CALL GETFLD2(16,A2,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         WRITE(6,6100)
         CALL                                      XIT('T2TEST',-4) 
      ENDIF 
      DO 1040 I=1,8 
         IBUF2(I)=IBUF(I) 
 1040 CONTINUE
      IF(NREC.EQ.0)WRITE(6,6040)IBUF2 
      IF(LN1N2)N2=IBUF2(2)
      IF(N2.LT.1)THEN 
         WRITE(6,6060)N2
         CALL                                      XIT('T2TEST',-5) 
      ENDIF 
      IF((N1+N2).LE.3)THEN
         WRITE(6,6070)N1,N2 
         CALL                                      XIT('T2TEST',-6) 
      ENDIF 
C 
C 
      IF( (IBUF1(1).NE.NGRID .AND. IBUF1(1).NE.NZONL) .OR.
     1    (IBUF2(1).NE.NGRID .AND. IBUF2(1).NE.NZONL) .OR.
     2    (IBUF1(1).NE.IBUF2(1)) .OR. (IBUF1(4).NE.IBUF2(4)) .OR. 
     3    (IBUF1(5).NE.IBUF2(5)) .OR. (IBUF1(6).NE.IBUF2(6)))THEN 
         WRITE(6,6080)
         WRITE(6,6040)IBUF1 
         WRITE(6,6040)IBUF2 
         CALL                                      XIT('T2TEST',-7) 
      ENDIF 
C 
      IF( (IBUF1(3).NE.IBUF2(3)) .AND. MATCH ) THEN 
         MATCH=.FALSE.
         WRITE(6,6210)
      ENDIF 
C 
      FN1=FLOAT(N1) 
      FN2=FLOAT(N2) 
      NDF=N1+N2-3 
      FACT1=1.0E0/(FN1+FN2-2.0E0) 
      FACT2=(FN1*FN2)/(FN1+FN2) 
      FACT3=(FN1+FN2-2.0E0-1.0E0)/(2.0E0*(FN1+FN2-2.0E0)) 
      NWDS=IBUF1(5)*IBUF1(6)
      IF(NWDS.GT.MAXRSZ)THEN 
         WRITE(6,6190)MAXRSZ,NWDS
         CALL                                      XIT('T2TEST',-8) 
      ENDIF 
C 
C     * COMPUTE DU. 
C 
      DO 1050 I=1,NWDS
        DU(I)=A2(I)-A1(I) 
 1050 CONTINUE
C 
C     * OBTAIN V1BAR. 
C 
      CALL GETFLD2(13,A1,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         WRITE(6,6110)
         CALL                                      XIT('T2TEST',-9) 
      ENDIF 
      DO 1060 I=1,8 
         IBUF1(I)=IBUF(I) 
 1060 CONTINUE
      IF(NREC.EQ.0)WRITE(6,6040)IBUF1 
C 
C     * OBTAIN V2BAR. 
C 
      CALL GETFLD2(18,A2,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
         WRITE(6,6120)
         CALL                                      XIT('T2TEST',-10)
      ENDIF 
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C 
      IF( (IBUF1(1).NE.NGRID .AND. IBUF1(1).NE.NZONL) .OR.
     1    (IBUF(1).NE.NGRID .AND. IBUF(1).NE.NZONL) .OR.
     2    (IBUF1(1).NE.IBUF(1)) .OR. (IBUF1(4).NE.IBUF(4)) .OR. 
     3    (IBUF1(5).NE.IBUF(5)) .OR. (IBUF1(6).NE.IBUF(6)))THEN 
         WRITE(6,6080)
         WRITE(6,6040)IBUF1 
         WRITE(6,6040)IBUF
         CALL                                      XIT('T2TEST',-11)
      ENDIF 
C 
      IF( (IBUF1(3).NE.IBUF(3)) .AND. MATCH ) THEN
         MATCH=.FALSE.
         WRITE(6,6210)
      ENDIF 
C 
C     * COMPUTE DV. 
C 
      DO 1080 I=1,NWDS
        DV(I)=A2(I)-A1(I) 
 1080 CONTINUE
C 
C     * IF(N1.LE.1)THEN SET VARIANCE OF U1 TO ZERO
C     * OTHERWISE, READ IN THE VARIANCE/S.D. OF U1. 
C 
      IF(N1.GT.1)THEN 
         CALL GETFLD2(12,A1,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6130) 
            CALL                                   XIT('T2TEST',-12)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A1MIN=DECODR(IDAT(1))
C        A1SCAL=1./DECODR(IDAT(2))
C        A1MAX=A1MIN+A1SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A1MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A1MAX)
         IF(.NOT.VAR)THEN 
           FACTSD1=(FN1-1.0E0)/FN1
           DO 1085 I=1,NWDS 
              A1(I)=FACTSD1*(A1(I)**2)
 1085      CONTINUE 
           A1MIN=FACTSD1*(A1MIN**2) 
           A1MAX=FACTSD1*(A1MAX**2) 
         ENDIF
         A1SCAL=(A1MAX-A1MIN)/(2.E0**(NBW/IBUF(8))-1.0E0)
         IF(A1MIN.LE.1.E-3*A1MAX)THEN 
           EPSA1=2.0E0*(A1MIN+A1SCAL) 
         ELSE 
           EPSA1=2.0E0*A1SCAL 
         ENDIF
      ELSE
         DO 1090 I=1,NWDS 
            A1(I)=0.0E0 
 1090    CONTINUE 
         EPSA1=1.0E0
      ENDIF 
C 
C     * IF(N2.LE.1)THEN SET VARIANCE OF U2 TO ZERO
C     * OTHERWISE, READ IN THE VARIANCE/S.D. OF U2. 
C 
      IF(N2.GT.1)THEN 
         CALL GETFLD2(17,A2,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6140) 
            CALL                                   XIT('T2TEST',-13)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A2MIN=DECODR(IDAT(1))
C        A2SCAL=1./DECODR(IDAT(2))
C        A2MAX=A2MIN+A2SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A2MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A2MAX)
         IF(.NOT.VAR)THEN 
           FACTSD2=(FN2-1.0E0)/FN2
           DO 1095 I=1,NWDS 
              A2(I)=FACTSD2*(A2(I)**2)
 1095      CONTINUE 
           A2MIN=FACTSD2*(A2MIN**2) 
           A2MAX=FACTSD2*(A2MAX**2) 
         ENDIF
         A2SCAL=(A2MAX-A2MIN)/(2.E0**(NBW/IBUF(8))-1.0E0)
         IF(A2MIN.LE.1.E-3*A2MAX)THEN 
           EPSA2=2.0E0*(A2MIN+A2SCAL) 
         ELSE 
           EPSA2=2.0E0*A2SCAL 
         ENDIF
      ELSE
         DO 1100 I=1,NWDS 
            A2(I)=0.0E0 
 1100    CONTINUE 
         EPSA2=1.0E0
      ENDIF 
      EPS1P=MIN(EPSA1,EPSA2)
C 
C     * COMPUTE S1P2. 
C 
      DO 1110 I=1,NWDS
        S1P2(I)=FACT1*(FN1*A1(I)+FN2*A2(I)) 
 1110 CONTINUE
C 
C     * IF(N1.LE.1)THEN SET VARIANCE OF V1 TO ZERO
C     * OTHERWISE, READ IN THE VARIANCE/S.D. OF V1. 
C 
      IF(N1.GT.1)THEN 
         CALL GETFLD2(14,A1,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6150) 
            CALL                                   XIT('T2TEST',-14)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A1MIN=DECODR(IDAT(1))
C        A1SCAL=1./DECODR(IDAT(2))
C        A1MAX=A1MIN+A1SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A1MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A1MAX)
         IF(.NOT.VAR)THEN 
           FACTSD1=(FN1-1.0E0)/FN1
           DO 1115 I=1,NWDS 
              A1(I)=FACTSD1*(A1(I)**2)
 1115      CONTINUE 
           A1MIN=FACTSD1*(A1MIN**2) 
           A1MAX=FACTSD1*(A1MAX**2) 
         ENDIF
         A1SCAL=(A1MAX-A1MIN)/(2.E0**(NBW/IBUF(8))-1.0E0)
         IF(A1MIN.LE.1.E-3*A1MAX)THEN 
           EPSA1=2.0E0*(A1MIN+A1SCAL) 
         ELSE 
           EPSA1=2.0E0*A1SCAL 
         ENDIF
      ELSE
         DO 1120 I=1,NWDS 
            A1(I)=0.0E0 
 1120    CONTINUE 
         EPSA1=1.0E0
      ENDIF 
C 
C     * IF(N2.LE.1)THEN SET VARIANCE OF V2 TO ZERO
C     * OTHERWISE, READ IN THE VARIANCE/S.D. OF V2. 
C 
      IF(N2.GT.1)THEN 
         CALL GETFLD2(19,A2,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6160) 
            CALL                                   XIT('T2TEST',-15)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A2MIN=DECODR(IDAT(1))
C        A2SCAL=1./DECODR(IDAT(2))
C        A2MAX=A2MIN+A2SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A2MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A2MAX)
         IF(.NOT.VAR)THEN 
           FACTSD2=(FN2-1.0E0)/FN2
           DO 1125 I=1,NWDS 
              A2(I)=FACTSD2*(A2(I)**2)
 1125      CONTINUE 
           A2MIN=FACTSD2*(A2MIN**2) 
           A2MAX=FACTSD2*(A2MAX**2) 
         ENDIF
         A2SCAL=(A2MAX-A2MIN)/(2.E0**(NBW/IBUF(8))-1.0E0)
         IF(A2MIN.LE.1.E-3*A2MAX)THEN 
           EPSA2=2.0E0*(A2MIN+A2SCAL) 
         ELSE 
           EPSA2=2.0E0*A2SCAL 
         ENDIF
      ELSE
         DO 1130 I=1,NWDS 
            A2(I)=0.0E0 
 1130    CONTINUE 
         EPSA2=1.0E0
      ENDIF 
      EPS2P=MIN(EPSA1,EPSA2)
C 
C     * COMPUTE S2P2. 
C 
      DO 1140 I=1,NWDS
        S2P2(I)=FACT1*(FN1*A1(I)+FN2*A2(I)) 
 1140 CONTINUE
C 
C     * IF(N1.LE.1)THEN SET THE COVARIANCE OF (U1,V1) TO ZERO 
C     * OTHERWISE, READ IN THE COVARIANCE OF (U1,V1). 
C 
      IF(N1.GT.1)THEN 
         CALL GETFLD2(15,A1,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6170) 
            CALL                                   XIT('T2TEST',-16)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A1MIN=DECODR(IDAT(1))
C        A1SCAL=1./DECODR(IDAT(2))
C        A1MAX=A1MIN+A1SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A1MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A1MAX)
         A1SCAL=(A1MAX-A1MIN)/(2.E0**(NBW/IBUF(8))-1.0E0) 
         IF(MIN(ABS(A1MIN),ABS(A1MAX)).LE.
     1                 1.E-3*MAX(ABS(A1MIN),ABS(A1MAX)))THEN
           EPSA1=2.0E0*(MIN(ABS(A1MIN),ABS(A1MAX))+A1SCAL)
         ELSE 
           EPSA1=2.0E0*A1SCAL 
         ENDIF
      ELSE
         DO 1150 I=1,NWDS 
            A1(I)=0.0E0 
 1150    CONTINUE 
         EPSA1=1.0E0
      ENDIF 
C 
C     * IF(N1.LE.1)THEN SET THE COVARIANCE OF (U2,V2) TO ZERO 
C     * OTHERWISE, READ IN THE COVARIANCE OF (U2,V2). 
C 
      IF(N2.GT.1)THEN 
         CALL GETFLD2(20,A2,-1,0,0,0,IBUF,MAXX,OK) 
         IF(.NOT.OK)THEN
            WRITE(6,6180) 
            CALL                                   XIT('T2TEST',-17)
         ENDIF
         IF(NREC.EQ.0)WRITE(6,6040)IBUF 
C        A2MIN=DECODR(IDAT(1))
C        A2SCAL=1./DECODR(IDAT(2))
C        A2MAX=A2MIN+A2SCAL*(2**(NBW/IBUF(8))-1.0)
         CALL DECODR2(IBUF(9),A2MIN)
         CALL DECODR2(IBUF(8+MACHINE+1),A2MAX)
         A2SCAL=(A2MAX-A2MIN)/(2.E0**(NBW/IBUF(8))-1.0E0) 
         IF(MIN(ABS(A2MIN),ABS(A2MAX)).LE.
     1                 1.E-3*MAX(ABS(A2MIN),ABS(A2MAX)))THEN
           EPSA2=2.0E0*(MIN(ABS(A2MIN),ABS(A2MAX))+A2SCAL)
         ELSE 
           EPSA2=2.0E0*A2SCAL 
         ENDIF
      ELSE
         DO 1160 I=1,NWDS 
            A2(I)=0.0E0 
 1160    CONTINUE 
         EPSA2=1.0E0
      ENDIF 
      EPSROP=MIN(EPSA1,EPSA2) 
      EPS=MIN(EPS1P,EPS2P,EPSROP) 
C 
C     * COMPUTE T22.
C 
      ICNT=0
      DO 1170 I=1,NWDS
        IF((S1P2(I).LE.EPS1P).OR.(S2P2(I).LE.EPS2P))THEN 
          S1P=EPS1P 
          S2P=EPS2P 
          ROP=1.0E0 
        ELSE
          S1P=SQRT(S1P2(I)) 
          S2P=SQRT(S2P2(I)) 
          ROP=FACT1*(FN1*A1(I)+FN2*A2(I))/(S1P*S2P) 
          IF(ABS(ROP).LE.EPSROP)ROP=1.0E0 
        ENDIF 
        IF((S1P.LE.EPS1P).OR.(S2P.LE.EPS2P) 
     1                   .OR.((1.E0-ABS(ROP)).LE.EPS))THEN
          FMASK(I)=0.0E0
          T22(I)=0.0E0
          ICNT=ICNT+1 
        ELSE
          FMASK(I)=1.0E0
          SU=DU(I)/S1P
          SV=DV(I)/S2P
          SDIF=SU**2 - 2.0E0*ROP*SU*SV + SV**2
          T22(I)=FACT2*SDIF/(1.0E0-(ROP**2))
        ENDIF 
 1170 CONTINUE
C 
      IF(ICNT.GE.1)THEN 
        NRMLXIT=.FALSE. 
        WRITE(6,6200)ICNT,NWDS,IBUF2(4) 
      ENDIF 
C 
C     * COMPUTE F-STATISTICS (F-VALUES).
C 
      DO 1190 I=1,NWDS
        F(I)=FACT3*T22(I) 
 1190 CONTINUE
C 
C     * COMPUTE AMP.
C 
      DO 1200 I=1,NWDS
        AMP(I)=SQRT(DU(I)**2+DV(I)**2)
 1200 CONTINUE
C 
C     * MODIFY THE BUFFER USED TO WRITE OUT THE RESULTS.
C 
      DO 1210 I=1,8 
        IBUF(I)=IBUF2(I)
 1210 CONTINUE
C 
C     * WRITE OUT DU. 
C 
      IBUF(3)=NDU 
      CALL PUTFLD2(21,DU,IBUF,MAXX)
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * WRITE OUT DV. 
C 
      IBUF(3)=NDV 
      CALL PUTFLD2(22,DV,IBUF,MAXX)
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * WRITE OUT AMP.
C 
      IBUF(3)=NAMP
      CALL PUTFLD2(23,AMP,IBUF,MAXX) 
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * WRITE OUT F-VALUES. 
C 
      IBUF(3)=NF
      CALL PUTFLD2(24,F,IBUF,MAXX) 
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * COMPUTE THE P-VALUES AS 
C     * 
C     *                     P=PROB(FISHER-F > OBSERVED F) 
C     * 
C     *          IE, P = PROB THAT F IS GREATER THAN THE OBSERVED VALUE 
C     *          ASSUMING THAT THE NULL HYPOTHESIS OF EQUALITY OF MEANS 
C     *          IS CORRECT.
C 
      CALL PROBF(F,2,NDF,NWDS,P,WK1,WK2,WK3)
C 
C     * COMPUTE K AND D-VALUES. 
C 
      CALL HTEST(P,NWDS,ALPHA,IKIND,K,D)
C 
C     * WRITE OUT K-VALUES. 
C 
      IBUF(3)=NK
      CALL PUTFLD2(25,K,IBUF,MAXX) 
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * WRITE OUT D-VALUES. 
C 
      IBUF(3)=ND
      CALL PUTFLD2(26,D,IBUF,MAXX) 
      IF(NREC.EQ.0)WRITE(6,6040)IBUF
C 
C     * WRITE OUT MASK-VALUES IF REQUIRED.
C 
      IF(MASK)THEN
        IBUF(3)=NMASK 
        CALL PUTFLD2(27,FMASK,IBUF,MAXX) 
        IF(NREC.EQ.0)WRITE(6,6040)IBUF
      ENDIF 
C 
      NREC=NREC+1 
      GOTO 1020 
C 
C-----------------------------------------------------------------------
C 
 5010 FORMAT(10X,E10.0,2F5.0,I5)                                                H4
 6010 FORMAT('0 THE T2TEST CARD IS MISSING.')
 6020 FORMAT('0T2TEST.   ',E10.1,2F5.0,I5)
 6030 FORMAT('0CONDUCTED F-TESTS ON ',I5,' PAIRS OF MEAN FIELDS.')
 6040 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6050 FORMAT('0ILLEGAL # OF CONTROL OBS. N1= ',I10)
 6060 FORMAT('0ILLEGAL # OF EXPERIMENTAL OBS. N2= ',I10)
 6070 FORMAT('0ILLEGAL # OF DATA POINTS, N1= ',I10,', N2= ',I10)
 6080 FORMAT('0LABELS ON MEAN FIELDS DO NOT MATCH.')
 6090 FORMAT('0FILE OF MEANS FOR VARIABLE U1 EMPTY.')
 6100 FORMAT('0UNEXPECTED EOF ON U2 MEANS FILE.')
 6110 FORMAT('0UNEXPECTED EOF ON V1 MEANS FILE.')
 6120 FORMAT('0UNEXPECTED EOF ON V2 MEANS FILE.')
 6130 FORMAT('0UNEXPECTED EOF ON U1 VARIANCE/S.D. FILE.')
 6140 FORMAT('0UNEXPECTED EOF ON U2 VARIANCE/S.D. FILE.')
 6150 FORMAT('0UNEXPECTED EOF ON V1 VARIANCE/S.D. FILE.')
 6160 FORMAT('0UNEXPECTED EOF ON V2 VARIANCE/S.D. FILE.')
 6170 FORMAT('0UNEXPECTED EOF ON (U1,V1) COVARIANCE FILE.')
 6180 FORMAT('0UNEXPECTED EOF ON (U2,V2) COVARIANCE FILE.')
 6190 FORMAT('0INSUFFICIENT ARRAY SIZE= ',I10,', FOR # PTS= ',I10)
 6200 FORMAT('0WARNING ',I6,' DETECTED VERY SMALL DENOMINATORS ',
     1       'OUT OF ',I6,' COMPUTED T**2-VALUES AT LEVEL ',I6)
 6210 FORMAT('0** NOTE - UNMATCHED MEAN FIELD NAMES **.')
C 
C-----------------------------------------------------------------------
C 
      END

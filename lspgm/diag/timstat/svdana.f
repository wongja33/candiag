      PROGRAM SVDANA
C     PROGRAM SVDANA(X,        Y,        XEOF,        YEOF,        SVD,         H2
C    1                                               INPUT,     OUTPUT, )       H2
C    2        TAPE11=X, TAPE12=Y, TAPE13=XEOF, TAPE14=YEOF, TAPE15=SVD, 
C    3                                         TAPE5=INPUT, TAPE6=OUTPUT)
C     -------------------------------------------------------------------       H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     MAR 09/99 - SLAVA KHARIN.                                                 
C                                                                               H2
CSVDANA  - PERFORMS SINGULAR VALUE DECOMPOSITION ANALYSIS.              4  1 C  H1
C                                                                               H3
CAUTHOR  - SLAVA KHARIN                                                         H3
C                                                                               H3
CPURPOSE - PERFORMS SINGULAR VALUE DECOMPOSITION OF TWO FIELDS.                 H3
C                                                                               H3
CINPUT FILES...                                                                 H3
C                                                                               H3
C     X    = INPUT FILE WITH FIELDS OF THE 1ST VARIABLE.                        H3
C     Y    = INPUT FILE WITH FIELDS OF THE 2ND VARIABLE.                        H3
C     XEOF = INPUT FILE WITH EOFS AND PRINCIPAL COMPONENTS OF THE 1ST VARIABLE  H3
C            EXACTLY AS THEY COME FROM THE MKEOF PROGRAM.                       H3
C            THE EOFS MUST BE CALCULATED FOR THE COVARIANCE MATRIX (ICOV=0 IN   H3
C            MKEOF.) EOF NORMALIZTION DOES NOT MATTER.                          H3
C     YEOF = THE SAME AS XEOF BUT FOR THE 2ND VARIABLE.                         H3
C                                                                               H3
COUTPUT FILES...                                                                H3
C                                                                               H3
C     SVD = OUTPUT FILE WITH SVD PATTERNS AND TIME SERIES IN CCRN FORMAT.       H3
C       1ST RECORD CONTAINS COVARIANCES.            (VARIABLE NAME = 4HSVCV)    H3
C       2ND RECORD CONTAINS 1ST SVD PATTERN OF X    (VARIABLE NAME = 4HSVPX)    H3
C       3RD RECORD CONTAINS ITS TIME SERIES         (VARIABLE NAME = 4HSVTX)    H3
C       4TH RECORD CONTAINS 1ST SVD PATTERN OF Y    (VARIABLE NAME = 4HSVPY)    H3
C       5TH RECORD CONTAINS ITS TIME SERIES         (VARIABLE NAME = 4HSVTY)    H3
C       ...                 2ND ...                                             H3
C                                                                               H3
C   NOTE - IF YOU USE AREA WEIGHTING OF INPUT FIELDS BEFORE RUNNING             H3
C          MKEOF+SVDANA, DO NOT FORGET TO REMOVE THIS WEIGHTING FROM SVD        H3
C          PATTERNS (SVPX AND SVPY) AFTERWARDS.                                 H3
C
CINPUT PARAMETERS:
C                                                                               H5
C     NEOFX   =    THE NUMBER OF EOFS FOR X.                                    H5
C             = 0  USE ALL EOFS FROM FILE EOFX.                                 H5
C     NEOFY   =    THE NUMBER OF EOFS FOR Y.                                    H5
C             = 0  USE ALL EOFS FROM FILE EOFY.                                 H5
C     SPVALX  =    REAL NUMBER USED TO INDICATE MISSING VALUES IN THE DATA      H5
C                  FIELDS OF X.                                                 H5
C     SPVALY  =    REAL NUMBER USED TO INDICATE MISSING VALUES IN THE DATA      H5
C                  FIELDS OF Y.                                                 H5
C     NORM    =0   THE SVD PATTERNS ARE NORMALIZED BY THE UNITY AND             H5
C                  THE CORRESPONDING TIME SERIES ARE NORMALIZED BY              H5
C                  THE SIGNAL AMPLITUDE.                                        H5
C                  OTHERWISE,                                                   H5
C                  THE TIME SERIES ARE NORMALIZED BY THE UNITY AND              H5
C                  THE CORRESPONDING SVD PATTERNS ARE NORMALIZED BY             H5
C                  THE SIGNAL AMPLITUDE.                                        H5
C                                                                               H5
CEXAMPLES OF INPUT CARDS...                                                     H5
C                                                                               H5
C*SVDANA     10   10    1.E+38    1.E+38    1                                   H5
C                                                                               H5
C (PERFORM SVD ANALYSIS FOR THE FIRST 10 EOFS/PCS OF THE FIRST VARIABLE         H5
C  AND FOR THE FIRST 10 EOFS/PCS OF THE SECOND VARIABLE. THE SVD PATTERNS       H5
C  ARE NORMALIZED BY THE SIGNAL AMPLITUDE).                                     H5
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_NWORDIO,
     &                       SIZES_TSL

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: MAXX = SIZES_BLONP1xBLATxNWORDIO
      integer, parameter :: MAXT = SIZES_TSL*SIZES_NWORDIO

      LOGICAL OK
C
C     * LENMAX = MAX LENGTH OF A DATA RECORD.
C     * NTMAX  = MAX NUMBER OF TIME STEPS.
C     * NMAX   = MAX DIMENSION OF SVD MATRIX.
C              THE NUMBER OF EOFS USED FOR BOTH VARIABLES MUST BE 
C              SMALLER THAN OR EQUALS TO NMAX.
C
      PARAMETER (NTMAX=SIZES_TSL,LENMAX=SIZES_BLONP1xBLAT,
     & NMAX=100,NSVDOUT=25)
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
      COMMON/JCOM/JBUF(8),JDAT(MAXT)
C
C     * ARRAYS FOR INPUT FIELDS
C
      REAL  X(LENMAX),Y(LENMAX),EVALX(NMAX),EVALY(NMAX)
     1     ,PCTX(NMAX),PCTY(NMAX),PCTCX(NMAX),PCTCY(NMAX)
     2     ,TAVGX(LENMAX),TAVGY(LENMAX),VARX(NMAX),VARY(NMAX)
     3     ,EVECX(LENMAX,NMAX),EVECY(LENMAX,NMAX)
     4     ,PCX(NTMAX,NMAX),PCY(NTMAX,NMAX)
      EQUIVALENCE (X(1),Y(1))
C
C     * ARRAYS FOR OUTPUT FIELDS
C
      REAL  SVD(NMAX),SVD0(NMAX),SVPX(LENMAX),SVPY(LENMAX)
     1     ,SVTX(NTMAX),SVTY(NTMAX)
C
C     * SAVE SPACE BY MAKING EQUIVALENCE
C
      EQUIVALENCE (SVPX(1),SVPY(1))
     1     ,(PCX(1,1),SVTX(1)),(PCY(1,1),SVTY(1))
C
C     INTERNAL ARRAYS
C
      REAL  COV(NMAX,NMAX),SNGX(NMAX,NMAX),SNGY(NMAX,NMAX)
      INTEGER NORDX(NMAX),NORDY(NMAX)
C
      DATA SMALL/1.E-12/,BIG/1.E+39/
C--------------------------------------------------------------------------
C
C     * ASSIGN FILES TO FORTRAN UNITS
C
      NFF=7
      CALL JCLPNT(NFF,11,12,13,14,15,5,6)
      REWIND 11
      REWIND 12
      REWIND 13
      REWIND 14
      REWIND 15
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,END=900) NEOFX, NEOFY, SPVALX, SPVALY, NORM                   H4
C
C     * WRITE OUT THE INPUT PARAMETERS TO STANDARD OUTPUT
C
      WRITE(6,6010) NEOFX,NEOFY,SPVALX,SPVALY,NORM
      IF (NORM.EQ.0) THEN
         WRITE(6,'(A)')
     1        ' THE SVD PATTERNS WILL BE NORMALIZED BY ONE.'
      ELSE
         WRITE(6,'(A)')
     1        ' THE TIME SERIES WILL BE STANDARDIZED BY ONE.'
      ENDIF
C
C     * READ EVS, EOFS AND PCS:
C
C     * FIRST COME EVS
C     
      CALL GETFLD2(13,EVALX,NC4TO8("TIME"),-1,NC4TO8("EVAL"),-1,
     +                                             IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-1)
      ENDIF
      CALL PRTLAB(IBUF)
      NEOFMX=IBUF(5)
      IF (NEOFX.EQ.0) THEN
         WRITE(6,'(A)') ' NEOFX=0. USE ALL INPUT EOFS.'
         NEOFX=NEOFMX
      ENDIF
      IF (NEOFX .GT. NMAX) THEN
         WRITE(6,'(A,I5,A)')
     1        ' *** ERROR: NUMBER OF EOFS NEOFX=',NEOFX,
     2        ' IS TOO LARGE.'
         CALL                                      XIT('SVDANA',-2)
      ENDIF
      IF (NEOFMX .LT. NEOFX) THEN
         WRITE(6,6020) NEOFMX
         NEOFX=NEOFMX
      ENDIF

      CALL GETFLD2(14,EVALY,NC4TO8("TIME"),-1,NC4TO8("EVAL"),-1,
     +                                             IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-3)
      ENDIF
      CALL PRTLAB(IBUF)
      NEOFMY=IBUF(5)
      IF (NEOFY.EQ.0)THEN
         WRITE(6,'(A)') ' NEOFY=0. USE ALL INPUT EOFS.'
         NEOFY=NEOFMY
      ENDIF
      IF (NEOFY .GT. NMAX) THEN
         WRITE(6,'(A,I5,A)')
     1        ' *** ERROR: NUMBER OF EOFS NEOFY=',NEOFY,
     2        ' IS TOO LARGE.'
         CALL                                      XIT('SVDANA',-4)
      ENDIF
      IF (NEOFMY .LT. NEOFY) THEN
         WRITE(6,6025) NEOFMY
         NEOFY=NEOFMY
      ENDIF

      NSVD=MIN(NEOFX,NEOFY)
C
C     * THEN PCT (THE PERCENTAGES OF EXPLAINED VARIANCE)
C
      CALL GETFLD2(13,PCTX,NC4TO8("TIME"),-1,NC4TO8(" PCT"),-1,
     +                                            IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-5)
      ENDIF
      CALL PRTLAB(IBUF)

      CALL GETFLD2(14,PCTY,NC4TO8("TIME"),-1,NC4TO8(" PCT"),-1,
     +                                            IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-6)
      ENDIF
      CALL PRTLAB(IBUF)
C
C     * THEN PCTC (THE CUMULATIVE PERCENTAGES)
C
      CALL GETFLD2(13,PCTCX,NC4TO8("TIME"),-1,NC4TO8("PCTC"),-1,
     +                                             IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-7)
      ENDIF
      CALL PRTLAB(IBUF)

      CALL GETFLD2(14,PCTCY,NC4TO8("TIME"),-1,NC4TO8("PCTC"),-1,
     +                                             IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-8)
      ENDIF
      CALL PRTLAB(IBUF)
C
C     * READ TIME MEAN
C
      CALL GETFLD2(13,TAVGX,-1,-1,NC4TO8("TAVG"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-9)
      ENDIF
      KINDX=IBUF(1)
      LEVX=IBUF(4)
      NLGX=IBUF(5)
      NLATX=IBUF(6)
      KHEMX=IBUF(7)
      NWDSX=NLGX*NLATX
      IF (KINDX.EQ.NC4TO8("SPEC") .OR. 
     +    KINDX.EQ.NC4TO8("FOUR")     ) NWDSX=NWDSX*2
      CALL PRTLAB(IBUF)

      CALL GETFLD2(14,TAVGY,-1,-1,NC4TO8("TAVG"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
         CALL                                      XIT('SVDANA',-10)
      ENDIF
      KINDY=IBUF(1)
      LEVY=IBUF(4)
      NLGY=IBUF(5)
      NLATY=IBUF(6)
      KHEMY=IBUF(7)
      NWDSY=NLGY*NLATY
      IF (KINDY.EQ.NC4TO8("SPEC") .OR. 
     +    KINDY.EQ.NC4TO8("FOUR")     ) NWDSY=NWDSY*2
      CALL PRTLAB(IBUF)
C
C     * EOFS & PCS
C
      DO NE=1,NEOFX
         CALL GETFLD2(13,EVECX(1,NE),-1,-1,NC4TO8("EVEC"),-1,
     +                                          IBUF,MAXX,OK)
         IF(.NOT.OK)THEN
            CALL                                   XIT('SVDANA',-11)
         ENDIF
         IF (IBUF(5) .NE. NLGX .OR. IBUF(6) .NE. NLATX)
     1        CALL                                 XIT('SVDANA',-12)
         IF(NE .EQ.1) CALL PRTLAB(IBUF)

         CALL GETFLD2(13,PCX(1,NE),NC4TO8("TIME"),-1,NC4TO8("  PC"),
     +                                              -1,JBUF,MAXT,OK)
         IF(.NOT.OK)THEN
            CALL                                   XIT('SVDANA',-13)
         ENDIF
         IF(NE .EQ.1) CALL PRTLAB(JBUF)
         NSTEP=JBUF(5)
      ENDDO

      DO NE=1,NEOFY
         CALL GETFLD2(14,EVECY(1,NE),-1,-1,NC4TO8("EVEC"),-1,
     +                                          IBUF,MAXX,OK)
         IF(.NOT.OK)THEN
            CALL                                   XIT('SVDANA',-14)
         ENDIF
         IF (IBUF(5) .NE. NLGY .OR. IBUF(6) .NE. NLATY)
     1        CALL                                 XIT('SVDANA',-15)
         IF(NE .EQ.1) CALL PRTLAB(IBUF)

         CALL GETFLD2(14,PCY(1,NE),NC4TO8("TIME"),-1,NC4TO8("  PC"),
     +                                              -1,JBUF,MAXT,OK)
         IF(.NOT.OK)THEN
            CALL                                   XIT('SVDANA',-16)
         ENDIF
         IF(NE .EQ.1) CALL PRTLAB(JBUF)
         IF (JBUF(5) .NE. NSTEP)
     1        CALL                                 XIT('SVDANA',-17)
      ENDDO
C
C     * CALCULATE TOTAL VARIANCES FROM EVAL(1) AND PCT(1)
C
      VARTOTX=EVALX(1)/PCTX(1)
      VARTOTY=EVALY(1)/PCTY(1)
C
C     * PRINT EVAL AND EXPLAIND VARIANCES
C
      WRITE(6,6030) VARTOTX,NEOFX
      DO NE=1,NEOFX
         WRITE(6,6035) NE,EVALX(NE),100.E0*PCTX(NE),100.E0*PCTCX(NE)
      ENDDO
      WRITE(6,6040) VARTOTY,NEOFY
      DO NE=1,NEOFY
         WRITE(6,6045) NE,EVALY(NE),100.E0*PCTY(NE),100.E0*PCTCY(NE)
      ENDDO
C
C     * NORMALIZE EOFS AND PCS PROPERLY
C     * (THE EOFS MUST BE NORMALIZED BY THE UNITY AND
C     *  THE PCS MUST BE NORMALIZED BY THE SQRT OF EIGENVALUES).
C
      DO NE=1,NEOFX
         SUM=0.E0
         DO IJ=1,NWDSX
            IF (ABS(EVECX(IJ,NE)-SPVALX).GT.SMALL) THEN         
               SUM=SUM+EVECX(IJ,NE)**2
            ENDIF
         ENDDO
         SUM=SQRT(SUM)
         DO IJ=1,NWDSX
            IF (ABS(EVECX(IJ,NE)-SPVALX).GT.SMALL) THEN         
               EVECX(IJ,NE)=EVECX(IJ,NE)/SUM
            ENDIF
         ENDDO
         DO NT=1,NSTEP
            PCX(NT,NE)=PCX(NT,NE)*SUM
         ENDDO
      ENDDO

      DO NE=1,NEOFY
         SUM=0.E0
         DO IJ=1,NWDSY
            IF (ABS(EVECY(IJ,NE)-SPVALY).GT.SMALL) THEN         
               SUM=SUM+EVECY(IJ,NE)**2
            ENDIF
         ENDDO
         SUM=SQRT(SUM)
         DO IJ=1,NWDSY
            IF (ABS(EVECY(IJ,NE)-SPVALY).GT.SMALL) THEN         
               EVECY(IJ,NE)=EVECY(IJ,NE)/SUM
            ENDIF
         ENDDO
         DO NT=1,NSTEP
            PCY(NT,NE)=PCY(NT,NE)*SUM
         ENDDO
      ENDDO
C
C     * CALCULATE COVARIANCE MATRIX <PCX,PCY>
C
      DO NE2=1,NEOFY
         DO NE1=1,NEOFX
            COV(NE1,NE2)=0.E0
         ENDDO
      ENDDO
      DO NE2=1,NEOFY
         DO NE1=1,NEOFX
            DO NT=1,NSTEP
               COV(NE1,NE2)=COV(NE1,NE2)+PCX(NT,NE1)*PCY(NT,NE2)
            ENDDO
            COV(NE1,NE2)=COV(NE1,NE2)/FLOAT(NSTEP)
         ENDDO
      ENDDO
C
C     * CALCULATE "SINGULAR" MATRIX FOR X
C
      DO NE2=1,NEOFX
         DO NE1=1,NEOFX
            SNGX(NE1,NE2)=0.E0
         ENDDO
      ENDDO
      DO NE2=1,NEOFX
         DO NE1=1,NEOFX
            DO NE3=1,NEOFY
               SNGX(NE1,NE2)=SNGX(NE1,NE2)+COV(NE1,NE3)*COV(NE2,NE3)
            ENDDO
         ENDDO
      ENDDO

C
C     * SOLVE EIGENVALUE PROBLEM
C
      CALL TRED2(SNGX,NEOFX,NMAX,SVD,PCTX)
      CALL TQLI1(SVD,PCTX,NEOFX,NMAX,SNGX)
C
C     * ORDER THE EIGENVALUES FROM MAX TO MIN
C
      ELAST=BIG
      DO NE=1,NEOFX
         EMAX=-BIG
         DO NE1=1,NEOFX
            IF (SVD(NE1).GT.EMAX .AND. SVD(NE1).LT.ELAST) THEN
               EMAX=SVD(NE1)
               NORDX(NE)=NE1
            ENDIF
         ENDDO
         ELAST=EMAX
      ENDDO
C
C     * CALCULATE "SINGULAR" MATRIX FOR Y
C
      DO NE2=1,NEOFY
         DO NE1=1,NEOFY
            SNGY(NE1,NE2)=0.E0
         ENDDO
      ENDDO
      DO NE2=1,NEOFY
         DO NE1=1,NEOFY
            DO NE3=1,NEOFX
               SNGY(NE1,NE2)=SNGY(NE1,NE2)+COV(NE3,NE1)*COV(NE3,NE2)
            ENDDO
         ENDDO
      ENDDO
C
C     * SOLVE EIGENVALUE PROBLEM
C
      CALL TRED2(SNGY,NEOFY,NMAX,SVD,PCTY)
      CALL TQLI1(SVD,PCTY,NEOFY,NMAX,SNGY)
C
C     * ORDER THE EIGENVALUES FROM MAX TO MIN
C
      ELAST=BIG
      DO NE=1,NEOFY
         EMAX=-BIG
         DO NE1=1,NEOFY
            IF (SVD(NE1).GT.EMAX .AND. SVD(NE1).LT.ELAST) THEN
               EMAX=SVD(NE1)
               NORDY(NE)=NE1
            ENDIF
         ENDDO
         ELAST=EMAX
      ENDDO
C
C     * SAVE SVD COVARIANCES (NAME=4HSVCV)
C
      DO NE=1,NSVD
         SVD0(NE)=SQRT(SVD(NORDY(NE)))
      ENDDO
      CALL SETLAB(IBUF,NC4TO8("TIME"),1,NC4TO8("SVCV"),0,NSVD,1,0,1)
      CALL PUTFLD2(15,SVD0,IBUF,MAXX)

C
C     * TRANSFORM SVD COEFFICIENTS FROM EOF SPACE TO PHYSICAL SPACE
C
      DO NE=1,NSVD
C
C     * X VARIABLE
C
         DO IJ=1,NWDSX
            SVPX(IJ)=0.E0
            DO NE1=1,NEOFX
               IF (ABS(EVECX(IJ,NE)-SPVALX).GT.SMALL) THEN
                SVPX(IJ)=SVPX(IJ)+SNGX(NE1,NORDX(NE))*EVECX(IJ,NE1)
               ELSE
                SVPX(IJ)=SPVALX
               ENDIF
            ENDDO
         ENDDO
C
C     * CALCULATE THE CORRESPONDING SVD TIME SERIES = <X,SVPX>
C
         REWIND 11
         DO NT=1,NSTEP
            CALL GETFLD2(11,X,-1,-1,-1,-1,IBUF,MAXX,OK)
            IF(.NOT.OK)THEN
               CALL                                XIT('SVDANA',-18)
            ENDIF
            IF (IBUF(5) .NE. NLGX .OR. IBUF(6) .NE. NLATX)
     1           CALL                              XIT('SVDANA',-19)
            SVTX(NT)=0.E0
            DO IJ=1,NWDSX
               IF ( ABS(SVPX(IJ)-SPVALX).GT.SMALL .AND. 
     1              ABS(   X(IJ)-SPVALX).GT.SMALL)
     2              SVTX(NT)=SVTX(NT)+SVPX(IJ)*(X(IJ)-TAVGX(IJ))
            ENDDO
         ENDDO
C
C     * RE-NORMALIZE TIME SERIES AND PATTERNS
C
         VARX(NE)=0.E0
         DO NT=1,NSTEP
            VARX(NE)=VARX(NE)+SVTX(NT)*SVTX(NT)
         ENDDO
         VARX(NE)=VARX(NE)/FLOAT(NSTEP)
         IF (NORM .NE. 0) THEN
            DO NT=1,NSTEP
               SVTX(NT)=SVTX(NT)/SQRT(VARX(NE))
            ENDDO
            DO IJ=1,NWDSX
               IF (ABS(SVPX(IJ)-SPVALX).GT.SMALL) 
     1              SVPX(IJ)=SVPX(IJ)*SQRT(VARX(NE))
            ENDDO
         ENDIF
C
C     * SAVE SVD PATTERN (NAME=4HSVPX)
C
         CALL SETLAB(IBUF,KINDX,NE,NC4TO8("SVPX"),LEVX,NLGX,
     +                                        NLATX,KHEMX,1)
         CALL PUTFLD2(15,SVPX,IBUF,MAXX)
C
C     * SAVE SVD TIME SERIES (NAME=4HSVTX)
C
         CALL SETLAB(JBUF,NC4TO8("TIME"),NE,NC4TO8("SVTX"),LEVX,
     +                                          NSTEP,1,KHEMX,1)
         CALL PUTFLD2(15,SVTX,JBUF,MAXT)

C
C     * Y VARIABLE
C
         DO IJ=1,NWDSY
            SVPY(IJ)=0.E0
            DO NE1=1,NEOFY
               IF (ABS(EVECY(IJ,NE1)-SPVALY).GT.SMALL) THEN
                SVPY(IJ)=SVPY(IJ)+SNGY(NE1,NORDY(NE))*EVECY(IJ,NE1)
               ELSE
                SVPY(IJ)=SPVALY
               ENDIF
            ENDDO
         ENDDO
C
C     * CALCULATE THE CORRESPONDING TIME SERIES OF Y = <Y,SVPY>
C
         REWIND 12
         DO NT=1,NSTEP
            CALL GETFLD2(12,Y,-1,-1,-1,-1,IBUF,MAXX,OK)
            IF(.NOT.OK)THEN
               CALL                                XIT('SVDANA',-20)
            ENDIF
            IF (IBUF(5) .NE. NLGY .OR. IBUF(6) .NE. NLATY)
     1           CALL                              XIT('SVDANA',-21)
            SVTY(NT)=0.E0
            DO IJ=1,NWDSY
               IF ( ABS(SVPY(IJ)-SPVALY).GT.SMALL .AND.
     1              ABS(   Y(IJ)-SPVALY).GT.SMALL)
     2              SVTY(NT)=SVTY(NT)+SVPY(IJ)*(Y(IJ)-TAVGY(IJ))
            ENDDO
         ENDDO
C
C     * RE-NORMALIZE TIME SERIES AND PATTERNS AND
C     * CHOOSE RIGHT SIGN TO HAVE A POSITIVE CORRELATION
C
         COVAR=0.E0
         VARY(NE)=0.E0
         DO NT=1,NSTEP
            COVAR=COVAR+SVTX(NT)*SVTY(NT)
            VARY(NE)=VARY(NE)+SVTY(NT)*SVTY(NT)
         ENDDO
         VARY(NE)=VARY(NE)/FLOAT(NSTEP)
         SGN=SIGN(1.E0,COVAR)
         IF (NORM .NE. 0)THEN
            DO NT=1,NSTEP
               SVTY(NT)=SVTY(NT)/SQRT(VARY(NE))*SGN
            ENDDO
            DO IJ=1,NWDSY
               IF ( ABS(SVPY(IJ)-SPVALY).GT.SMALL) THEN
                  SVPY(IJ)=SVPY(IJ)*SQRT(VARY(NE))*SGN
               ENDIF
            ENDDO
         ELSE
            DO NT=1,NSTEP
               SVTY(NT)=SVTY(NT)*SGN
            ENDDO
            DO IJ=1,NWDSY
               IF ( ABS(SVPY(IJ)-SPVALY).GT.SMALL) THEN
                  SVPY(IJ)=SVPY(IJ)*SGN
               ENDIF
            ENDDO
         ENDIF
C
C     * SAVE SVD PATTERN (NAME=4HSVPY)
C
         CALL SETLAB(IBUF,KINDY,NE,NC4TO8("SVPY"),LEVY,NLGY,
     +                                        NLATY,KHEMY,1)
         CALL PUTFLD2(15,SVPY,IBUF,MAXX)
C
C     * SAVE TIME SERIES (NAME=4HSVTY)
C
         CALL SETLAB(JBUF,NC4TO8("TIME"),NE,NC4TO8("SVTY"),LEVY,
     +                                          NSTEP,1,KHEMY,1)
         CALL PUTFLD2(15,SVTY,JBUF,MAXT)
      ENDDO
C
C     * CALCULATE TOTAL COVARIANCE SQUARED OF EOF TRUNCATED FIELDS
C
      SUM=0.E0
      DO NE=1,NSVD
         SUM=SUM+SVD(NORDY(NE))
      ENDDO
C
C     * PRINT COVARIANCES
C
      WRITE (6,6050) SQRT(SUM)
      WRITE (6,6055) NSVD 
      DO NE=1,MIN(NSVD,NSVDOUT)
        WRITE (6,6060)
     &     NE,SQRT(SVD(NORDY(NE))),SVD(NORDY(NE))/SUM*100.E0
     &     ,SQRT(SVD(NORDY(NE))/(VARX(NE)*VARY(NE)))
     &     ,VARX(NE)/VARTOTX*100.E0,VARY(NE)/VARTOTY*100.E0
      ENDDO
      CALL                                         XIT('SVDANA',0)
 900  CALL                                         XIT('SVDANA',-22)
C-------------------------------------------------------------------------
 5010 FORMAT (10X,2I5,2E10.0,I5)                                                H4
 6010 FORMAT (/' INPUT PARAMETERS ARE:'/
     1     ' NEOFX=',I5,' NEOFY=',I5,
     2     ' SPVALX=',1P1E12.5,' SPVALY=',1P1E12.5,' NORM=',I5)
 6020 FORMAT (
     1     /' THE NUMBER OF EOFS IN FILE XEOF IS SMALLER THAN NEOFX.'
     2     /' NEOFX IS SET TO ',I5/)
 6025 FORMAT (
     1     /' THE NUMBER OF EOFS IN FILE YEOF IS SMALLER THAN NEOFY.'
     2     /' NEOFY IS SET TO ',I5/)
 6030 FORMAT (/' TOTAL VARIANCE OF X: ',1P1E12.5
     1     /' THE FIRST ',I5,' EIGENVALUES OF X:'
     2     /'    NUMBER     EVAL      PCT(%)    CUM.PCT(%)')
 6035 FORMAT (5X,I5,1P1E12.4,0P2F10.3)
 6040 FORMAT (/' TOTAL VARIANCE OF Y: ',1P1E12.5
     1     /' THE FIRST ',I5,' EIGENVALUES OF Y:'
     2     /'    NUMBER     EVAL      PCT(%)    CUM.PCT(%)')
 6045 FORMAT (5X,I5,1P1E12.4,0P2F10.3)
 6050 FORMAT (
     1     ' TOTAL COVAR.^2 OF EOF TRUNCATED FIELDS=(',1P1E10.4,')^2')
 6055 FORMAT (
     1     /' FIRST ', I5, ' COVARIANCES '
     2     /' AND VARIANCES (%) EXPLAINED BY SVD PATTERNS:'
     3     /' NUMBER    COVAR.^2    COVAR.^2(%)  CORR.  ',
     4      ' VAR.X(%)  VAR.Y(%)')
 6060 FORMAT (1X,I3,2X,'(',1P1E10.4,')^2',0P4F10.3)
      END

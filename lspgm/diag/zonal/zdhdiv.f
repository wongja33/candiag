      PROGRAM ZDHDIV
C     PROGRAM ZDHDIV (ZXIN,       ZDH,       ZXOUT,       OUTPUT,       )       F2
C    1          TAPE1=ZXIN, TAPE2=ZDH, TAPE3=ZXOUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     NOV 07/80 - J.D.HENDERSON 
C                                                                               F2
CZDHDIV  - ZONAL CROSS-SECTION DELTA-HAT DIVIDER                        2  1    F1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - DIVIDES A CROSS-SECTION BY THE ZONAL MEAN OF DELTA-HAT.              F3
C          NOTE - ANY VALUE DIVIDED BY ZERO IS SIMPLY SET TO ZERO.              F3
C                                                                               F3
CINPUT FILES...                                                                 F3
C                                                                               F3
C      ZXIN  = ONE ZONAL CROSS-SECTION (MAX LATITUDES IS $BJ$)                  F3
C      ZDH   = ZONAL MEAN OF DELTA-HAT                                          F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXOUT = THE QUOTIENT ZXIN/ZDH  (ZXIN/0=0)                                F3
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      COMMON/BLANCK/F(SIZES_BLAT),DZM(SIZES_BLAT) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLATxNWORDIO)
      DATA MAXX/SIZES_BLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      DO 110 N=1,3
  110 REWIND N
C 
C     * READ THE NEXT LEVEL OF THE CROSS-SECTION FROM FILE 1. 
C 
      NR=0
  140 CALL GETFLD2(1,  F,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) NR
        IF(NR.EQ.0) CALL                           XIT('ZDHDIV',-1) 
        CALL                                       XIT('ZDHDIV',0)
      ENDIF 
      WRITE(6,6025) IBUF
C 
C     * READ THE NEXT LEVEL OF THE ZONAL MEAN OF DELTA-HAT (FILE 2).
C 
      CALL GETFLD2(2,DZM,NC4TO8("ZONL"),-1,-1,-1,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('ZDHDIV',-2) 
      WRITE(6,6025) JBUF
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK) CALL                             XIT('ZDHDIV',-3) 
C 
C     * COMPUTE THE QUOTIENT AND SAVE ON FILE 3.
C 
      NLAT=IBUF(5)
      DO 210 I=1,NLAT 
        IF(DZM(I).EQ.0.E0)THEN
          F(I)=0.E0 
        ELSE
          F(I)=F(I)/DZM(I)
        ENDIF 
  210 CONTINUE
C 
      CALL PUTFLD2(3,F,IBUF,MAXX)
      WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 140 
C---------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('0',I6,' RECORDS PROCESSED')
      END

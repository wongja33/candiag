      PROGRAM INCHEM2
C     PROGRAM INCHEM2 (ICTL,        MASK,       LLPHYS,       LLCHEM,           I2
C    1                                          GGCHEM,        OUTPUT,  )       I2
C    2          TAPE99=ICTL, TAPE88=MASK, TAPE77=LLPHYS,TAPE1=LLCHEM,
C    3                                    TAPE2=GGCHEM,  TAPE6=OUTPUT)
C     ----------------------------------------------------------------          I2
C     JAN 29/07 - X. MA COMBINE THE OLD EMISSION DATA FILE WITH AEROCOM        
C                                                                               I2
C     MAY 21/04 - M. LAZARE. - CHANGE THE DIMENSION OF "LONB(361),LATB(181)" TO I2
C                              "NLOMAX".                                        I2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     JUN  6/2000 - M.LAZARE.  NEW PROGRAM BASED ON GRID PHYSICS
C                              INITIALIZATION, EXCEPT DEDICATED TO
C                              CHEMISTRY INPUT FIELDS (SUCH AS SULFUR
C                              CYCLE.
C                                                                               I2
CINCHEM2 - GRID CHEMISTRY INITIALIZATION PROGRAM FOR GCM16+             2  1    I1
C                                                                               I3
CAUTHOR  - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - CONVERTS HIGH-RESOLUTION LAT-LONG SURFACE CHEMISTRY GRIDS TO         I3
C          GAUSSIAN GRIDS.                                                      I3
C          NOTE - ALL INITIAL SURFACE FIELDS ARE AT 1X1 DEGREE OFFSET           I3
C                 RESOLUTION. THE BLACK/ORGANIC CARBON AND DMS (O/L)            I3
C                 MUST BE AVAILABLE FOR ALL MONTHS. THE IPCC A2_2000            I3
C                 FIELD HAS ONE BASIC ANNUAL VALUE.                             I3
C                                                                               I3
C                 INITIAL FIELDS AT 1X1 DEGREE RESOLUTION ARE                   I3
C                 AREA-AVERAGED USING THE SUBROUTINE HRTOLR3.                   I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL   = CONTAINS GLOBAL LAT-LONG GRIDS WHICH ARE READ AND               I3
C               HORIZONTALLY SMOOTHED TO GLOBAL GAUSSIAN GRIDS. THE LAT-        I3
C               LONG GRIDS MAY BE IN ANY ORDER ON THIS FILE.                    I3
C                                                                               I3
C      MASK   = CONTAINS LAND MASK (NO SEA-ICE, I.E. -1/0) ON MODEL             I3
C               GAUSSIAN-GRID.                                                  I3
C                                                                               I3
C      LLPHYS = OFFSET LAT-LONG PHYSICS GRIDS(USED ONLY TO GET MASK)            I3
C                                                                               I3
C      LLCHEM_AEROCOM = CONTAINS THE HIGH-RES CHEMISTRY EMISSION GRIDS:         I3
C                                                                               I3
C      NAME            VARIABLE                   UNITS                         I3
C      ----            --------                   -----                         I3
C                                                                               I3
C       EBC   BLACK CARBON EMISSIONS            KG/M2-S                         I3
C       EOC   ORGANIC CARBON EMISSIONS          KG/M2-S                         I3
C      EOCF   ORGANIC FOREST CARBON EMISSIONS   KG/M2-S                         I3
C      DMSO   KETTLE OCEANIC DMS CONCENTRATIONS NANOMOL/LITRE                   I3
C      EDMS   LAND DMS FLUXES                   KG/M2-S                         I3
C      ESO2   SO2 EMISSIONS (ANNUAL ONLY)       KG/M2-S                         I3
C      EVOL   VOLCANIC EMISSIONS (ANNUAL ONLY)  KG/M2-S                         I3
C      HVOL   VOLCANO MAX HEIGHT (ANNUAL ONLY)  METRES ABOVE SEA LEVEL          I3
C                                                                               I3
C      NAME            VARIABLE                   UNITS                         I3
C      ----            --------                   -----                         I3
C                                                                               I3
C      EBWA   FOREST FIRE BC (1997-2002 mean)   KG/M2-S                         I3
C      EOWA   FOREST FIRE POM (1997-2002 mean)  KG/M2-S                         I3
C      ESWA   FOREST FIRE SO2 (1997-2002 mean)  KG/M2-S                         I3
C      EBBT   BIOFUEL BC EMISSIONS (2000)       KG/M2-S                         I3
C      EOBT   BIOFUEL POM EMISSIONS (2000)      KG/M2-S                         I3
C      EBFT   FOSSIL FUEL BC EMISSIONS (2000)   KG/M2-S                         I3
C      EOFT   FOSSIL FUEL POM EMISSIONS (2000)  KG/M2-S                         I3
C      ESDT   SO2 EMISSIONS - DOMESTIC (2000)   KG/M2-S                         I3
C      ESIT   SO2 EMISSIONS - INDUSTRIAL (2000) KG/M2-S                         I3
C      ESST   SO2 EMISSIONS - SHIPPING (2000)   KG/M2-S                         I3
C      ESOT   SO2 EMISSIONS - OFF-ROAD (2000)   KG/M2-S                         I3
C      ESPT   SO2 EMISSIONS - POWERPLANTS (2000)KG/M2-S                         I3
C      ESRT   SO2 EMISSIONS - ROADS (2000)      KG/M2-S                         I3
C      EOST   SECONDARY ORGANIC AEROSOLS        KG/M2-S                         I3
C      ESCV   SO2 FROM CONTINUOUS VOLCANOES     KG/M2-S                         I3
C      EHCV   HEIGHT OF CONTINUOUS VOLCANOES    M                               I3
C      ESEV   SO2 FROM EXPLOSIVE VOLCANOES      KG/M2-S                         I3
C      EHEV   HEIGHT OF EXPLOSIVE VOLCANOES     M                               I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      GGCHEM = GAUSSIAN GRID SURFACE PHYSICS FIELDS:                           I3
C                                                                               I3
C      NAME            VARIABLE                   UNITS                         I3
C      ----            --------                   -----                         I3
C                                                                               I3
C       EBC   BLACK CARBON EMISSIONS            KG/M2-S                         I3
C       EOC   ORGANIC CARBON EMISSIONS          KG/M2-S                         I3
C      EOCF   ORGANIC FOREST CARBON EMISSIONS   KG/M2-S                         I3
C      DMSO   KETTLE OCEANIC DMS CONCENTRATIONS NANOMOL/LITRE                   I3
C      EDMS   LAND DMS FLUXES                   KG/M2-S                         I3
C      ESO2   SO2 EMISSIONS (ANNUAL ONLY)       KG/M2-S                         I3
C      EVOL   VOLCANIC EMISSIONS (ANNUAL ONLY)  KG/M2-S                         I3
C      HVOL   VOLCANO MAX HEIGHT (ANNUAL ONLY)  METRES ABOVE GROUND             I3
C                                                                               I3
C      GGCHEM = GAUSSIAN GRID SURFACE PHYSICS FIELDS:                           I3
C                                                                               I3
C      NAME            VARIABLE                   UNITS                         I3
C      ----            --------                   -----                         I3
C                                                                               I3
C      EBWA   FOREST FIRE BC (1997-2002 mean)   KG/M2-S                         I3
C      EOWA   FOREST FIRE POM (1997-2002 mean)  KG/M2-S                         I3
C      ESWA   FOREST FIRE SO2 (1997-2002 mean)  KG/M2-S                         I3
C      EBBT   BIOFUEL BC EMISSIONS (2000)       KG/M2-S                         I3
C      EOBT   BIOFUEL POM EMISSIONS (2000)      KG/M2-S                         I3
C      EBFT   FOSSIL FUEL BC EMISSIONS (2000)   KG/M2-S                         I3
C      EOFT   FOSSIL FUEL POM EMISSIONS (2000)  KG/M2-S                         I3
C      ESDT   SO2 EMISSIONS - DOMESTIC (2000)   KG/M2-S                         I3
C      ESIT   SO2 EMISSIONS - INDUSTRIAL (2000) KG/M2-S                         I3
C      ESST   SO2 EMISSIONS - SHIPPING (2000)   KG/M2-S                         I3
C      ESOT   SO2 EMISSIONS - OFF-ROAD (2000)   KG/M2-S                         I3
C      ESPT   SO2 EMISSIONS - POWERPLANTS (2000)KG/M2-S                         I3
C      ESRT   SO2 EMISSIONS - ROADS (2000)      KG/M2-S                         I3
C      EOST   SECONDARY ORGANIC AEROSOLS        KG/M2-S                         I3
C      ESCV   SO2 FROM CONTINUOUS VOLCANOES     KG/M2-S                         I3
C      EHCV   HEIGHT OF CONTINUOUS VOLCANOES    M                               I3
C      ESEV   SO2 FROM EXPLOSIVE VOLCANOES      KG/M2-S                         I3
C      EHEV   HEIGHT OF EXPLOSIVE VOLCANOES     M                               I3
C-----------------------------------------------------------------------------
C     * IF NO HIGH-RESOLUTION POINTS ARE FOUND WITHIN A PARTICULAR LOW-
C     * RESOLUTION GRID SQUARE, EVEN AFTER CONSIDERING INFORMATION FROM
C     * NEAREST-NEIGHBOUR POINTS ON THE GAUSSIAN GRID, AN ABORT CONDITION
C     * IS GENERATED.

C     * HRMASK IS THE HIGH-RESOLUTION OFFSET LAND MASK (LAND=-1., ELSE 0.)
C     * WHILE MASK IS THE STANDARD LAND MASK AT GAUSSIAN GRID RESOLUTION
C     * (READ IN).

C     * GH IS GENERALLY THE RESULTING OUTPUT FIELD ARRAY FOR ALL FIELDS.
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LONP1xLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER (NLG=360,NLAT=180,IGND=3,NM=12,NM1=NM+1,
     & NLOMAX=SIZES_LONP1xLAT,
     & NHIMAX=NLG*NLAT,NLEV=6)
C
      REAL GLL(NHIMAX),HRMASK(NHIMAX)
      REAL GG(NLOMAX),  GH(NLOMAX),  MASK(NLOMAX)
      REAL DLAT(SIZES_BLAT), DLON(SIZES_BLONP1)
      REAL*8 SL(SIZES_BLAT), CL(SIZES_BLAT), 
     & WL(SIZES_BLAT), WOSSL(SIZES_BLAT), RAD(SIZES_BLAT) 
C
      INTEGER NFDM(NM),MMD(NM), LEVC(NLEV)

      LOGICAL OK

C     * WORK ARRAYS FOR SUBROUTINE GGFILL CALLED IN HRTOLR3.

      INTEGER LONB(NLOMAX), LATB(NLOMAX)
C
C     * WORK ARRAYS FOR SUBROUTINE HRTOLR3.
C     * NOTE THAT THE SIZES OF ARRAYS VAL,DIST,LOCAT,IVAL,LOCFST AND
C     * NMAX REALLY REPRESENT THE NUMBER OF HIGH-RESOLUTION GRID
C     * POINTS WITHIN A LOW-RESOLUTION GRID SQUARE. FOR CONVENIENCE
C     * SAKE, THEY ARE DIMENSIONED WITH SIZE "NLG" AND AN ABORT
C     * CONDITION IS GENERATED WITHIN THE SUBROUTINE.
C
      REAL GCHRTMP(NLG,NLAT)
      REAL RLON(NLG), RLAT(NLAT)

      REAL GCLRTMP(361,181)
      INTEGER LATL(181), LATH(181), LONL(361), LONH(361)

      REAL VAL(NLG), DIST(NLG)
      INTEGER LOCAT(NLG), IVAL(NLG), LOCFST(NLG), NMAX(NLG)

C
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)

      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/
      DATA  MMD/16,46,75,106,136,167,197,228,259,289,320,350/
      DATA LEVC/ 100,500,1000,2000,3000,6000/
      DATA MAXX,NPGG/SIZES_BLONP1xBLATxNWORDIO,+1/
C---------------------------------------------------------------------
      NFF=6
      CALL JCLPNT(NFF,99,88,77,1,2,6)

      INTERP=1

C     * READ DAY OF THE YEAR, GAUSSIAN GRID SIZE AND LAND SURFACE SCHEME
C     * CODE FROM CONTROL FILE.

      REWIND 99
      READ(99,END=910) LABL
      READ(99,END=911) LABL,IXX,IXX,ILG,ILAT,IXX,IDAY,IXX,IXX
      ILG1  = ILG+1
      ILATH = ILAT/2
      LGG   = ILG1*ILAT
      WRITE(6,6010)  IDAY,ILG1,ILAT

C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).

      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)

      DO 100 I=1,ILAT
          DLAT(I) = RAD(I)*180.E0/3.14159E0
  100 CONTINUE

C     * DEFINE LONGITUDE VECTOR FOR GAUSSIAN GRID.

      DGX=360.E0/(FLOAT(ILG))
      DO 120 I=1,ILG1
          DLON(I)=DGX*FLOAT(I-1)
  120 CONTINUE

      NMO=12
C
C     * DEFINE TARGET GRID TYPE FOR SUBROUTINE HRTOLR3 (GAUSSIAN GRID).
C
      IGRID=0
C
C     * READ-IN THE STANDARD LAND MASK ON THE MODEL GAUSSIAN GRID.
C     * IN CASE IT CONTAINS INFORMATION ON LAKES (GC=+2) AND IS PACKED,
C     * PROCESS THE FIELD TO GET ONLY VALUES OF (-1/0) FOR (LAND/NOLAND)
C     * BEFORE PROCESSING FURTHER.
C
      NDAY=0
      CALL GETFLD2(88,MASK,NC4TO8("GRID"),NDAY,NC4TO8("  GC"),0,
     +                                             IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-1)
      WRITE(6,6025) IBUF
C
      DO 125 I=1,LGG
          IVALM=NINT(MASK(I))
          MASK(I)=REAL(IVALM)
  125 CONTINUE
C
      DO 126 I=1,LGG
          MASK(I)=MIN(0.E0, MASK(I))
  126 CONTINUE
C
C     * SAVE HIGH-RES LAND MASK (-1./0.) INTO ARRAY "HRMASK", FOR USE
C     * IN PROCESSING SUBSEQUENT FIELDS.
C     * SET HIGH-RES LAND MASK TO GLACIER LAND SOUTH OF 78.5S TO
C     * MAKE ROSS AND WEDELL SEA ICE SHELVES "GLACIERS".
C
      CALL GETFLD2(-77,HRMASK,NC4TO8("GRID"),NDAY,NC4TO8("  GC"),
     +                                            1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-2)
      WRITE(6,6025) IBUF

      IGLAC=IBUF(5)*12
      LLL=IBUF(5)*IBUF(6)
C
      DO 127 I=1,LLL
          IF(I.LE.IGLAC) HRMASK(I)=-1.E0
  127 CONTINUE
C================================================================= OLD EMISSIONS
C=================================================================
C     * FIELDS FOR EACH OF TWELVE MID-MONTHS:
C
C     * LOOP OVER MONTHS.
C
      DO 150 N=1,NMO
        NDAY=MMD(N)
C---------------------------------------------------------------------
C       * GET BLACK CARBON EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8(" EBC"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-3)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-4)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8(" EBC"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
  150 CONTINUE
C------------------------------------------------------------------------
      DO 160 N=1,NMO
        NDAY=MMD(N)
C
C       * GET ORGANIC CARBON EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8(" EOC"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-5)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-6)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8(" EOC"),1,
     +                                       ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
  160 CONTINUE
C------------------------------------------------------------------------
      DO 170 N=1,NMO
        NDAY=MMD(N)
C
C       * GET ORGANIC CARBON FOREST EMISSIONS AND AREA-AVERAGE
C       * TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("EOCF"),1,
     +                                                IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-7)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-8)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("EOCF"),1,
     +                                       ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
  170 CONTINUE
C------------------------------------------------------------------------
       DO 180 N=1,NMO
        NDAY=MMD(N)
C
C       * GET OCEANIC DMS CONCENTRATIONS AND AREA-AVERAGE TO GAUSSIAN GRID.
C       * ENSURE DMS=0. OVER LAND AT HIGH RESOLUTION,

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("DMSO"),1,
     +                                                IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-9)
        WRITE(6,6025) IBUF
C
        DO 200 I=1,LLL
          GLL(I)=MAX(GLL(I),0.E0)
          IF(HRMASK(I).EQ.-1.E0) GLL(I)=0.E0
  200   CONTINUE
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-10)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("DMSO"),1,
     +                                       ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
  180 CONTINUE
C------------------------------------------------------------------------
       DO 190 N=1,NMO
        NDAY=MMD(N)
C
C       * GET LAND DMS FLUXES AND AREA-AVERAGE TO GAUSSIAN GRID.
C       * ENSURE DMS=0. OVER NON-LAND AT HIGH RESOLUTION,

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("EDMS"),1,
     +                                                IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-11)
        WRITE(6,6025) IBUF
C
        DO 210 I=1,LLL
          GLL(I)=MAX(GLL(I),0.E0)
          IF(HRMASK(I).EQ.0.E0) GLL(I)=0.E0
  210   CONTINUE
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-12)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("EDMS"),1,
     +                                       ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
C------------------------------------------------------------------------
  190 CONTINUE
C=====================================================================
C     * INVARIANT FIELDS:
C---------------------------------------------------------------------
C     * SO2 EMISSIONS.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESO2"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-13)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-14)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESO2"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * VOLCANIC EMISSIONS.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EVOL"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-15)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-16)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EVOL"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * GET THE "OPTIMAL TOPOGRAPHY" FIELD ON THE MODEL GAUSSIAN GRID
C     * FROM THE CONTROL FILE (GENERATED IN ICNTRLB).
C
      CALL GETFLD2(99,GG,NC4TO8("GRID"),-1,NC4TO8("PHIM"),1,
     1             IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-17)
      CALL PRTLAB (IBUF)
C---------------------------------------------------------------------
C     * VOLCANIC HEIGHT.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("HVOL"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-18)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-19)
      ENDIF
C
C     * CONVERT FROM HEIGHT IN METRES ABOVE SEA LEVEL (IN "GH") TO HEIGHT
C     * IN METRES ABOVE GROUND, BY SUBTRACTING OFF SURFACE HEIGHT (OBTAINED
C     * FROM "GG") AND CLIPPING NEGATIVE VALUES.
C
      GINV=1./9.80616
      DO 230 I=1,LGG
          GH(I)=MAX(0.E0, GH(I)-MAX(0.E0,GINV*GG(I)))
  230 CONTINUE
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("HVOL"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C================================================================= NEW EMISSIONS
C=================================================================
C     * FIELDS FOR EACH OF TWELVE MID-MONTHS:
C
C     * LOOP OVER MONTHS.
C
      DO 261 N=1,NMO
      DO 260 K=1,NLEV 
        NDAY=MMD(N)
C---------------------------------------------------------------------
C       * GET GFED BC EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("EBWA"),
     +                                        LEVC(K),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-20)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-21)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("EBWA"),
     +                                   LEVC(K),ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
 260    CONTINUE
 261  CONTINUE
C------------------------------------------------------------------------
      DO 271 N=1,NMO
      DO 270 K=1,NLEV
        NDAY=MMD(N)
C
C       * GET GFED POM EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("EOWA"),
     +                                         LEVC(K),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-22)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-23)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("EOWA"),
     +                                  LEVC(K),ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
 270    CONTINUE
 271  CONTINUE
C------------------------------------------------------------------------
      DO 281 N=1,NMO
      DO 280 K=1,NLEV
        NDAY=MMD(N)
C
C       * GET GFED SO2 EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("ESWA"),
     +                                          LEVC(K),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-24)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-25)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("ESWA"),
     +                                  LEVC(K),ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
 280    CONTINUE
 281  CONTINUE
C------------------------------------------------------------------------
      DO 282 N=1,NMO
         NDAY=MMD(N)
C------------------------------------------------------------------------
C       * GET SOA EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),MMD(N),NC4TO8("EOST"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-26)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-27)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),NDAY,NC4TO8("EOST"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF

 282    CONTINUE
C=====================================================================
C     * INVARIANT FIELDS:
C------------------------------------------------------------------------
C       * GET BIOFUEL BC EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EBBT"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-28)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-29)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EBBT"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
C------------------------------------------------------------------------
C       * GET BIOFUEL POM EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EOBT"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-30)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-31)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EOBT"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
C------------------------------------------------------------------------
C       * GET FOSSIL FUEL BC EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EBFT"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-32)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-33)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EBFT"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C       * GET FOSSIL FUEL POM EMISSIONS AND AREA-AVERAGE TO GAUSSIAN GRID.

        CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EOFT"),
     +                                            1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('INCHEM2',-34)
        WRITE(6,6025) IBUF
C
        IOPTION=1
        ICHOICE=1
        CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1               GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2               OK,LONBAD,LATBAD,
     3               GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4               VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
        IF(.NOT.OK)THEN
          WRITE(6,6030) LONBAD,LATBAD
          CALL                                     XIT('INCHEM2',-35)
        ENDIF
C
        CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EOFT"),
     +                                   1,ILG1,ILAT,0,NPGG)
        CALL PUTFLD2(2,GH,IBUF,MAXX)
        WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM DOMESTIC USE
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESDT"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-36)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-37)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESDT"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM INDUSTRIAL USE
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESIT"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-38)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-39)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESIT"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM INTERNATIONAL SHIPPING
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESST"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-40)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-41)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESST"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM OFF-ROAD USE
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESOT"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-42)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-43)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESOT"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM POWER PLANTS
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESPT"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-44)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-45)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESPT"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM ROADS
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESRT"),
     +                                     1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-46)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-47)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESRT"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM CONTINUOUS VOLCANOES.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESCV"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-48)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-49)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESCV"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * HEIGHT OF CONTINUOUS VOLCANOES.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EHCV"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-50)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=4
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-51)
      ENDIF
C
C     * CONVERT FROM HEIGHT IN METRES ABOVE SEA LEVEL (IN "GH") TO HEIGHT
C     * IN METRES ABOVE GROUND, BY SUBTRACTING OFF SURFACE HEIGHT (OBTAINED
C     * FROM "GG") AND CLIPPING NEGATIVE VALUES.
C
      GINV=1./9.80616
      DO 240 I=1,LGG
          GH(I)=MAX(0.E0, GH(I)-MAX(0.E0,GINV*GG(I)))
  240 CONTINUE
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EHCV"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * SO2 EMISSIONS FROM EXPLOSIVE VOLCANOES.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("ESEV"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-52)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=1
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-53)
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("ESEV"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * HEIGHT OF EXPLOSIVE VOLCANOES.
C
      CALL GETFLD2(-1,GLL,NC4TO8("GRID"),0,NC4TO8("EHEV"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INCHEM2',-54)
      WRITE(6,6025) IBUF
C
      IOPTION=1
      ICHOICE=4
      CALL HRTOLR3(GH,MASK,ILG1,ILAT,DLON,DLAT,ILG,
     1             GLL,HRMASK,NLG,NLAT,IOPTION,ICHOICE,IGRID,
     2             OK,LONBAD,LATBAD,
     3             GCLRTMP,GCHRTMP,RLON,RLAT,LATL,LATH,LONL,LONH,
     4             VAL,DIST,LOCAT,IVAL,LOCFST,LONB,LATB,NMAX)
C
      IF(.NOT.OK)THEN
        WRITE(6,6030) LONBAD,LATBAD
        CALL                                       XIT('INCHEM2',-55)
      ENDIF
C
C     * CONVERT FROM HEIGHT IN METRES ABOVE SEA LEVEL (IN "GH") TO HEIGHT
C     * IN METRES ABOVE GROUND, BY SUBTRACTING OFF SURFACE HEIGHT (OBTAINED
C     * FROM "GG") AND CLIPPING NEGATIVE VALUES.
C
      GINV=1./9.80616
      DO 245 I=1,LGG
          GH(I)=MAX(0.E0, GH(I)-MAX(0.E0,GINV*GG(I)))
  245 CONTINUE
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("EHEV"),1,
     +                                  ILG1,ILAT,0,NPGG)
      CALL PUTFLD2(2,GH,IBUF,MAXX)
      WRITE(6,6026) IBUF
C---------------------------------------------------------------------
C     * E.O.F. ON FILE LL.

      CALL                                         XIT('INCHEM2',0)

C     * E.O.F. ON FILE ICTL.

  910 CALL                                         XIT('INCHEM2',-56)
  911 CALL                                         XIT('INCHEM2',-57)
C---------------------------------------------------------------------
 6010 FORMAT('0IDAY,ILG1,ILAT =',3I6)
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
 6026 FORMAT(' ',60X,A4,I10,1X,A4,5I6)
 6030 FORMAT('0EXITING PROGRAM; NO POINTS FOUND WITHIN GRID SQUARE',
     1       ' CENTRED AT (',I3,',',I3,')')
      END

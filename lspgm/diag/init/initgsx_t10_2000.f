      program initgsx

!     program initgsx (ictl,      gpinit,     gpspec,       gsinit,  output)
!                   tape1=ictl, tape2=gpinit, tape3=gpspec, tape4=gsinit
!     ----------------------------------------------------------------
!
! Purpose: Convert a pressure-level gaussian-grid dataset of tracer model
!          variables to a sigma/hybrid-level grid. Based on subroutine
!          initgs8, but for species only.
!
! History:
! D. Plummer, October 28, 2016 
!      - remove seasonal background aerosol fields (BGA1 and BGA2) from 
!        initialization since these are no longer used.
!
! Input files:
!       ictl   = initialization control dataset (see icntrlh).
!       gpinit = pressure level gaussian grids for model variable. Used only
!                to extract surface pressure for vertical interpolation.
!       gpspec = pressure level gaussian grids for species (20)
!
!       NAME  VARIABLE          UNITS
!       ps    surface pressure  pa
!       x     species           vmr
!
! Output file:
!       gsinit = sigma/hybrid level gaussian grids of
!
!       NAME  VARIABLE          UNITS    LEVELS
!       x     species           vmr      ilev
!----------------------------------------------------------------------------

      use diag_sizes, only : sizes_lonp1Xlat,
     &                       sizes_lonp1XlatXnwordio,
     &                       sizes_maxlev,
     &                       sizes_maxlevXlonp1Xlat

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: jpntrci=34, jpmxlat=96
      integer, parameter :: jpmxa=2210, jpmxb=2145

      common /lat1/ sl(jpmxlat),cl(jpmxlat),wl(jpmxlat), wossl(jpmxlat)
      common /lat2/ rad(jpmxlat), alp(jpmxa), epsi(jpmxa)
      common /lat3/ ilg, ilat, la, lm, lrlmt, lsr(2,66)
      common /lat4/ wrks(546,2), wrkl(728)

      complex :: sstrac
      common /specsig/sstrac(jpmxb)

      common/icom/ ibuf(8), idat(sizes_lonp1XlatXnwordio)
      common/com1/ fa(sizes_maxlevXlonp1Xlat),xa(sizes_maxlevXlonp1Xlat)
     &            ,ya(sizes_maxlevXlonp1Xlat),za(sizes_maxlevXlonp1Xlat)
     &            ,sp(sizes_lonp1XlatXnwordio)

      logical :: ok

      integer :: kbuf(8)
      integer :: lp(sizes_maxlev), lg(sizes_maxlev), lh(sizes_maxlev)

      real :: ah(sizes_maxlev), bh(sizes_maxlev), sg(sizes_maxlev)
      real :: sh(sizes_maxlev), pr(sizes_maxlev), prlog(sizes_maxlev)
      real :: fpcol(sizes_maxlev), dfdlnp(sizes_maxlev+1)
      real :: dlnp(sizes_maxlev)

      data ij/sizes_lonp1XlatXnwordio/,
     &     ijk/sizes_maxlevXlonp1Xlat/, maxlev/sizes_maxlev/

      character(len=4) :: cspl(jpntrci)

!     Long lived species initialisation

      DATA  CSPL(1) /'  OX'/    ! Ox   : O3+O+O1D
      DATA  CSPL(2) /'  NX'/    ! NOx  : NO+NO2+NO3+2*N2O5
      DATA  CSPL(3) /'  C9'/    ! ClOx : Cl ClO + HOCl + OClO + ClONO2 + Cl2O2 
      DATA  CSPL(4) /'  BX'/    ! BrOX : Br+BrO+HOBr+BrONO2+BrCl
      DATA  CSPL(5) /'  N5'/   	! HNO3
      DATA  CSPL(6) /'  HX'/    ! HOX
      DATA  CSPL(7) /'  N7'/    ! HNO4
      DATA  CSPL(8) /'  T5'/    ! N2O  
      DATA  CSPL(9) /'  HB'/    ! HBr
      DATA  CSPL(10)/'  MM'/    ! CH4
      DATA  CSPL(11)/'  CO'/  	! CO
      DATA  CSPL(12)/'  HA'/    ! CH2O
      DATA  CSPL(13)/'  MH'/    ! CH3OOH
      DATA  CSPL(14)/'  HD'/    ! H2
      DATA  CSPL(15)/'  C7'/    ! HCl
      DATA  CSPL(16)/'  C4'/    ! ClONO2
      DATA  CSPL(17)/'  F1'/    ! F-11(CFCl3)
      DATA  CSPL(18)/'  F2'/    ! F-12(CF2Cl2)
      DATA  CSPL(19)/'  F3'/    ! CCl4
      DATA  CSPL(20)/'  F4'/    ! CH3CCl3
      DATA  CSPL(21)/'  F5'/    ! HCFC-22
      DATA  CSPL(22)/'  F6'/    ! CH3Cl
      DATA  CSPL(23)/'  CB'/    ! CH3Br
      DATA  CSPL(24)/'  BF'/    ! CHBr3
      DATA  CSPL(25)/'  DB'/    ! CH2Br2
      DATA  CSPL(26)/'  NY'/    ! NOy = NX + N5 + N7
      DATA  CSPL(27)/'  CY'/    ! Cl_y = C9 + C7 + C4
      DATA  CSPL(28)/'  CZ'/    ! Cl_tot = C9 + C7 + C4 + F1 + F2 + F3 + F4 + F5 + F6
      DATA  CSPL(29)/'  BZ'/    ! Brz = BX + HB + CB + 3BF + 2DB
      DATA  CSPL(30)/'  TA'/    ! Tropospheric NOx (hybrid)
      DATA  CSPL(31)/'  TB'/    ! Tropospheric HNO3 (hybrid)
      DATA  CSPL(32)/'  X1'/    ! Diagnostic tracer - strato. O3
      DATA  CSPL(33)/'  X2'/    ! Diagnostic tracer - e90
      DATA  CSPL(34)/'  X3'/    ! Diagnostic tracer - age of air
C-----------------------------------------------------------------------

! Call JCLPNT to associate filenames to unti numbers. NFIL is the number of
! files to which unit numbers are to be assigned. The integers following
! NFIL are the unit numbers to use. The files themselves are the arguments to
! the call to subroutine initgsx_t10_2000; see header notes above. 
      nfil = 5
      call jclpnt(nfil, 1, 2, 3, 11, 6)

! Get the pressure level values and the analysis grid size from the temperature
! fields in file GPINIT (tape 2 on call; unit=2 after JCLPNT).
      rewind 2
      call find(2,nc4to8("GRID"),-1,nc4to8("TEMP"),-1,ok)
      if (.not. ok) call xit('initgsx',-1)
      call filev(lp, npl, kbuf, -2)
      if ((npl < 1) .or. (npl > maxlev)) call xit('initgsx',-2)
      call lvdcode(pr, lp, npl)
      write(6,6005) npl,(pr(l),l=1,npl)
 6005 format('   pressure levels:',i5,20f5.0/('0',18x,20f5.0))

! Read model sigma values from the control file ICTL (tape 1 => unit=1).
      rewind 1
      read(1,end=903) labl,nsl,(sg(l),l=1,nsl)
     &                        ,(sh(l),l=1,nsl),lay,icoord,ptoit,moist
      read(1,end=904) labl,ilg,ilat,ilgm,ilatm,lrlmt,iday,gmt
      write(6,*) "                nsl=", nsl

      if ((nsl < 1) .or. (nsl > maxlev)) call xit('initgsx',-3)

      write(6,6015) lrlmt
 6015 format (' MARKER initgsx: lrlmt =', i10)
      write(6,6017) ilg, ilat, ilgm, ilatm
 6017 FORMAT ('        Analysis grid:', 2i5, 5x, 'Model grid:',2i5/)
      call writlev(sh, nsl, ' SH ')
      WRITE(6,6016) LAY,ICOORD,PTOIT,MOIST
C
      CALL SIGLAB2 (LG,LH, SG,SH, NSL)
      CALL COORDAB (AH,BH, NSL,SH, ICOORD,PTOIT)

      ILATH=ILAT/2

      CALL DIMGT  (LSR,LA,LR,LM,KTR,LRLMT)
      CALL EPSCAL (EPSI,LSR,LM)
      CALL GAUSSG (ILATH,SL,WL,CL,RAD,WOSSL)
      CALL  TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)

C     * STOP IF THERE IS NOT ENOUGH SPACE.

      LEN = ILAT*(ILG+1)
      IF(    LEN.GT.IJ ) CALL                      XIT('INITGSX',-4)
      IF(NPL*LEN.GT.IJK) CALL                      XIT('INITGSX',-5)
      IF(NSL*LEN.GT.IJK) CALL                      XIT('INITGSX',-6)

C     * PERFORM THE VERTICAL INTERPOLATION OF SPECIES

      CALL PTSIX   (2,3,11,FA,XA,YA,ZA,SP,LEN,NPL,PR,NSL,LP,LH,
     1             AH,BH,NPL+1,FPCOL,DFDLNP ,DLNP ,
     2             SG,PRLOG, IBUF,IJ+8,CSPL,JPNTRCI)


      CALL                                         XIT('INITGSX',0)

C     * E.O.F. ON FILE ICTL.

  903 CALL                                         XIT('INITGSX',-7)
  904 CALL                                         XIT('INITGSX',-8)
C-----------------------------------------------------------------------
 6010 FORMAT(I6,A4,/,(10X,10F6.3))
 6016 FORMAT(' LAY=',I5,', COORD=',A4,', P.LID=',F10.3,' (PA)',
     1       ', MOIST=',A4)
      END
C
      SUBROUTINE PTSIX
     1           (NF2,NF3,NFO,FA,XA,YA,ZA,SP,LEN,NPL,PR,NSL,
     2            LP,LH,AH,BH,NPL1,FPCOL,DFDLNP,DLNP , 
     3            SIG,PRLOG,IBUF,MAXP8,CSPL,JPNTRCI) 

C     * SEPT 00 - J. DE GRANDPRE. - PREVIOUS VERSION PTSH.
C     * AUG 13/90 - M.LAZARE. - PREVIOUS VERSION PTSH.
C 
C     * CONVERTS NPL PRESSURE LEVELS OF GAUSSIAN GRIDS (ILG,ILAT)
C     * ON FILE NF3 TO NSL ETA LEVELS ON FILE NFO FOR HYBRID MODEL. 
  
      PARAMETER (JPMXLAT=96, JPMXA=2210, JPMXB=2145)

      COMMON /LAT1/ SL(JPMXLAT),CL(JPMXLAT),WL(JPMXLAT),WOSSL(JPMXLAT)
      COMMON /LAT2/ RAD(JPMXLAT), ALP(JPMXA),EPSI(JPMXA)
      COMMON /LAT3/ ILG,ILAT,LA,LM,LRLMT,LSR(2,66)
      COMMON /LAT4/ WRKS(546,2), WRKL(728) 

      COMPLEX SSTRAC
      COMMON /SPECSIG/SSTRAC(JPMXB)

      LOGICAL OK
  
      REAL SP(LEN)
      REAL FA(LEN,NPL)
      REAL XA(LEN,NPL)
      REAL YA(LEN,NPL) ! special case to create Cloy
      REAL ZA(LEN,NPL) ! special case to create inorganic fraction of Cly

      REAL AH     (NSL) ,BH   (NSL) 
      REAL PR    (NPL ),FPCOL (NPL ),DFDLNP (NPL1),DLNP (NPL) 
      REAL SIG   (NSL ),PRLOG (NPL )
      INTEGER  LP (NPL), LH(NSL)
      INTEGER IBUF(MAXP8) 
      CHARACTER*4 CSPL(JPNTRCI)
C
C --- THE LOWER BOUNDARY CONDITION FOR THE SIX CHLORINE-CONTAINING
C     HALOCARBONS PLUS CH3BR USED TO CREATE THE INITIAL CONDITIONS
      REAL HALOIC(7)
      DATA HALOIC / 2.6345E-10, 6.6082E-10, 9.9370E-11,
     1              5.6148E-11, 2.1337E-10, 5.5001E-10,
     2              1.6653E-11 /
C
C --- SPECIFY THE TARGET LOWER BOUNDARY CONDITIONS FOR THE SAME
C     SEVERN HALOCARBONS
      REAL HALOTG(7)
      DATA HALOTG / 2.6345E-10, 6.6082E-10, 9.9370E-11,
     1              5.6148E-11, 2.1337E-10, 5.5001E-10,
     2              1.6653E-11 /
C-------------------------------------------------------------------- 
      MAXX = MAXP8-8 

      DO 110 L=1,NPL
         PRLOG(L) = LOG(PR(L)) 
  110 CONTINUE
C
C --- CALCULATE TOTAL INITIAL Cl FORCING
      XITLCL = 3.0*HALOIC(1) + 2.0*HALOIC(2) + 4.0*HALOIC(3) +
     1         3.0*HALOIC(4) + 1.0*HALOIC(5) + 1.0*HALOIC(6)
C
C --- CALCULATE THE TARGET INITIAL Cl FORCING
      XTTLCL = 3.0*HALOTG(1) + 2.0*HALOTG(2) + 4.0*HALOTG(3) +
     1         3.0*HALOTG(4) + 1.0*HALOTG(5) + 1.0*HALOTG(6)
C
C---------------------------------------------------------------------
C     * READ SURFACE PRESSURE AND CONVERT TO LSNP.
C 
      REWIND NF2
      CALL GETFLD2(NF2,SP,NC4TO8("GRID"),-1,NC4TO8("  PS"),
     1             1,IBUF,MAXX,OK) 
      IF(.NOT.OK) CALL XIT('PTSIX',-2)
      WRITE(6,6025) (IBUF(IND),IND=1,8) 
      DO 210 I=1,LEN
         SP(I)=LOG(SP(I))
 210  CONTINUE 
C
C---------------------------------------------------------------------
C     * initial work to calculate the [inorganic Cl]/[total Cl] fraction
C       to be used to set up the HCl/ClOy and halogens
C
      REWIND NF3

      CALL NOM(NAM,'  C9')
      DO L=1,NPL
        CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      CALL NOM(NAM,'  C7')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          XA(IL,L)=XA(IL,L)+YA(IL,L)
        ENDDO
      ENDDO
C
      CALL NOM(NAM,'  C4')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
C ---- initial ClOy = ClOx+HCl+ClONO2
C
      DO L=1,npl
        DO IL=1,len
          XA(IL,L)=XA(IL,L)+YA(IL,L)
        ENDDO
      ENDDO
C
C ---- calculate the total organic chlorine
C
C ---- read in CFC-11 and add to Clorg
      CALL NOM(NAM,'  F1')
      DO L=1,NPL
        CALL GETFLD2(NF3, ZA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = 3.0*ZA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in CFC-12 and add to Clorg
      CALL NOM(NAM,'  F2')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = ZA(IL,L) + 2.0*YA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in CCl4 and add to Clorg
      CALL NOM(NAM,'  F3')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = ZA(IL,L) + 4.0*YA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in CH3CCl3 and add to Clorg
      CALL NOM(NAM,'  F4')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = ZA(IL,L) + 3.0*YA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in HCFC-22 and add to Clorg
      CALL NOM(NAM,'  F5')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = ZA(IL,L) + YA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in CH3Cl and add to Clorg
      CALL NOM(NAM,'  F6')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = ZA(IL,L) + YA(IL,L)
        ENDDO
      ENDDO
C
C ---- calculate the inorganic/total fraction
      DO L=1,npl
        DO IL=1,len
          FA(IL,L) = XA(IL,L)/(XA(IL,L)+ZA(IL,L))
        ENDDO
      ENDDO
C
C ---- calculate the ratio of the initial total chlorine
C      to the idealized total chlorine
      DO L=1,npl
        DO IL=1,len
          ZA(IL,L) = XITLCL/(XA(IL,L)+ZA(IL,L))
        ENDDO
      ENDDO
C
C---------------------------------------------------------------------
C     * READ SPECIES.
  
      DO 300 N=1,JPNTRCI

        REWIND NF3

        IF (CSPL(N).NE.'  C9' .AND. CSPL(N).NE.'  C7' .AND.
     1      CSPL(N).NE.'  C4' .AND. CSPL(N).NE.'  F1' .AND.
     2      CSPL(N).NE.'  F2' .AND. CSPL(N).NE.'  F3' .AND.
     3      CSPL(N).NE.'  F4' .AND. CSPL(N).NE.'  F5' .AND.
     4      CSPL(N).NE.'  F6' .AND. 
     5      CSPL(N).NE.'  BX' .AND. CSPL(N).NE.'  HB' .AND.
     6      CSPL(N).NE.'  CB' .AND.
     7      CSPL(N).NE.'  NY' .AND. CSPL(N).NE.'  CY' .AND.
     8      CSPL(N).NE.'  CZ' .AND. CSPL(N).NE.'  BZ' .AND.
     9      CSPL(N).NE.'  TA' .AND. CSPL(N).NE.'  TB' .AND.
     a      CSPL(N).NE.'  X1' .AND. CSPL(N).NE.'  X2' .AND.
     b      CSPL(N).NE.'  X3'   ) THEN

          CALL NOM(NAM,CSPL(N))
          DO L=1,NPL
            CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                   NAM,LP(L),IBUF,MAXX,OK)
            IF(.NOT.OK) CALL XIT('PTSIX',-3) 
            WRITE(6,6025) (IBUF(IND),IND=1,8)
          ENDDO

        ELSE
C
C ------- for inorganic Cl, use the fraction of inorganic
C         to total scaled to the new target and put into HCl
C
          IF (CSPL(N).EQ.'  C9') THEN
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = 1.0E-16
              ENDDO
            ENDDO
          ENDIF
c
          IF (CSPL(N).EQ.'  C7') THEN
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = FA(IL,L)*XTTLCL
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  C4') THEN
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = 1.0E-16
              ENDDO
            ENDDO
          ENDIF
C
C ---- individual halocarbons are scaled to the target
C      amount and have a correction factor on any error
C      in total chlorine from the initial conditions
C
          IF (CSPL(N).EQ.'  F1') THEN
            CALL NOM(NAM,'  F1')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(1)/HALOIC(1)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  F2') THEN
            CALL NOM(NAM,'  F2')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(2)/HALOIC(2)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  F3') THEN
            CALL NOM(NAM,'  F3')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(3)/HALOIC(3)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  F4') THEN
            CALL NOM(NAM,'  F4')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(4)/HALOIC(4)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  F5') THEN
            CALL NOM(NAM,'  F5')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(5)/HALOIC(5)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  F6') THEN
            CALL NOM(NAM,'  F6')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(6)/HALOIC(6)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
C  ---- bromine is trickier because of the contribution from
C       VSLS so here a rough scaling is done taking into
C       account the contribution from CH3Br and an extra
C       5 pptv from the VSLS
C
          IF (CSPL(N).EQ.'  BX') THEN
            CALL NOM(NAM,'  BX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  HB') THEN
            CALL NOM(NAM,'  HB')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  CB') THEN
            CALL NOM(NAM,'  CB')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  NY')THEN
c
c  --------  construct the NOy family
c
            CALL NOM(NAM,'  NX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
c
            CALL NOM(NAM,'  N5')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
            DO IL=1,len
              DO L=1,npl
                xa(il,l) = xa(il,l) + ya(il,l)
              ENDDO
            ENDDO
c
            CALL NOM(NAM,'  N7')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = xa(il,l) + ya(il,l)
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (CSPL(N).EQ.'  CY')THEN
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = FA(IL,L)*XTTLCL
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (CSPL(N).EQ.'  CZ')THEN
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = XTTLCL
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (CSPL(N).EQ.'  BZ')THEN
c
c  --------  construct the Brz family using the same scaling
c            used earlier for the individual Br species
c
            CALL NOM(NAM,'  BX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = prx1*xa(il,l)
              ENDDO
            ENDDO
c
            CALL NOM(NAM,'  HB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = xa(il,l) + prx1*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  CB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = xa(il,l) + prx1*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  BF')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = xa(il,l) + 3.0*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  DB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,len
                xa(il,l) = xa(il,l) + 2.0*ya(il,l)
              ENDDO
            ENDDO

          ENDIF
c
          IF(CSPL(N).EQ.'  TA') THEN
C
C --- initialize the hybrid tracers for troposheric NOx
C   --- constants for hybridization must agree with model values
            XREF1=1.5E-08
            XPOW1=1.0
            SMIN = XREF1/
     1             ((1. + XPOW1*LOG(XREF1/1.0E-14))**(1.0/XPOW1))
            DO L=1,NPL
              DO I=1,LEN
                XA(I,L) = SMIN
              ENDDO
            ENDDO
          ENDIF

          IF(CSPL(N).EQ.'  TB') THEN
C
C --- initialize the hybrid tracers for troposheric HNO3
C   --- constants for hybridization must agree with model values
            XREF2=5.0E-09
            XPOW2=1.0
            SMIN = XREF2/
     1             ((1. + XPOW2*LOG(XREF2/1.0E-14))**(1.0/XPOW2))
C
            DO L=1,NPL
              DO I=1,LEN
                XA(I,L) = SMIN
              ENDDO
            ENDDO
          ENDIF
c ---
          IF (CSPL(N).EQ.'  X1') THEN
c
c  --------  stratospheric ozone
c
            CALL NOM(NAM,'  OX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
          ENDIF
C
          IF (CSPL(N).EQ.'  X2') THEN
c
c  -------- e90 tracer
c
            DO L=1,npl
              DO IL=1,len
                XA(IL,L)= 1.00E-07
              ENDDO
            ENDDO
          ENDIF
c
          IF (CSPL(N).EQ.'  X3') THEN
c
c  -------- SF6-like age of air tracer
c
            DO L=1,npl
              DO IL=1,len
                XA(IL,L)= 2.0E-12
              ENDDO
            ENDDO
          ENDIF
c
        ENDIF

C     * FROM DT/DP=(R*T)/(P*CP),  R/CP=2./7.,  AND T=280. 
C     * WE GET RLDN = DT/D(LN P) = 80.
C     * LTES = (0,1) TO PUT TEMP,ES ON (HALF,FULL) LEVELS.
  
        RLUP =  0.
        RLDN =  0.

        CALL PAELX  (XA,LEN,SIG,NSL, XA,PRLOG,NPL,SP,RLUP,RLDN, 
     1              AH,BH,NPL+1,FPCOL,DFDLNP ,DLNP )
  
C     * CONVERT TO SPECTRAL AND SAVE ADV. SPECIES

        DO 280 K=1,NSL
           CALL GGAST2(SSTRAC,LSR,LM,LA,XA(1,K),ILG+1,ILAT,
     1                 1,SL,WL,ALP,EPSI,WRKS,WRKL)
           CALL NOM(NAM,CSPL(N))
           CALL SETLAB(IBUF,NC4TO8("SPEC"),0,
     1                 NAM,LH(K),LA,1,LRLMT,2)

           CALL PUTFLD2(NFO,SSTRAC,IBUF,MAXX)
           WRITE(6,6026) (IBUF(IND),IND=1,8)
  280   CONTINUE

  300 CONTINUE
C-------------------------------------------------------------------- 
 6000 FORMAT(A4,'    ')
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
 6026 FORMAT(' ',60X,A4,I6,1X,A4,5I6) 
      END
      SUBROUTINE PAELX (FS, LEN,SIG,NSL, FP,PRLOG,NPL, PSLOG,RLUP,RLDN, 
     1                 A,B,NPL1,FPC,DFDLNP,DLNP)
  
C     * FEB 02/88 - R.LAPRISE.
C     * INTERPOLATES MULTI-LEVEL SET OF GRIDS FROM PRESSURE LEVELS
C     * TO ETA LEVELS FOR HYBRID MODEL. 
C     * THE INTERPOLATION IS LINEAR IN LN(PRES).
C 
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LEN POINTS).
C     * FP    = INPUT  GRIDS ON PRESSURE LEVELS.
C     * FS    = OUTPUT GRIDS ON ETA      LEVELS.
C     *     (NOTE THAT FP AND FS MAY BE EQUIVALENCED IN CALLING PROGRAM)
C     * PSLOG = INPUT  GRID OF LN(SURFACE PRESSURE IN MB).
C 
C     * PRLOG(NPL) = VALUES OF INPUT PRESSURE LEVEL (MB). 
C     * SIG(NSL) = VALUES OF ETA LEVELS OF OUTPUT FIELD.
C     *            (BOTH MUST BE MONOTONIC AND INCREASING). 
C     * NPL1  = NPL+1.
C 
C     * RLUP  = LAPSE RATE USED TO EXTRAPOLATE ABOVE PRLOG(1).
C     * RLDN  = LAPSE RATE USED TO EXTRAPOLATE BELOW PRLOG(NSL).
C     *         UNITS OF RLUP,RLDN ARE  DF/D(LN PRES).
C 
      REAL FP   (LEN,NPL),FS (LEN,NSL),PSLOG (LEN) 
      REAL PRLOG(   NPL)
  
      REAL A(NSL),B(NSL)
  
C     * WORK SPACE. 
  
      REAL FPC(NPL),DFDLNP(NPL1),DLNP(NPL),SIG(NSL) 
C---------------------------------------------------------------------

C     * PRECOMPUTE INVERSE OF DELTA LN PRES.
C 
      DO 180 L=1,NPL-1
         DLNP(L) =1. / (PRLOG(L+1)-PRLOG(L))
  180 CONTINUE
  
C     * LOOP OVER ALL HORIZONTAL POINTS.
  
      DO 500 I=1,LEN 
  
C     * GET A COLUMN OF FP (ON PRESSURE LEVELS).
  
      DO 210 L=1,NPL
         FPC(L)=FP(I,L) 
  210 CONTINUE
  
C     * COMPUTE VERTICAL DERIVATIVE OVER ALL PRESSURE INTERVALS.
  
      DO 260 L=1,NPL-1
  260 DFDLNP(L+1  ) = (FPC(L+1)-FPC(L)) *DLNP(L)
      DFDLNP(1    ) = RLUP
      DFDLNP(NPL+1) = RLDN
  
C     * COMPUTE THE LOCAL SIGMA VALUES. 
  
      CALL NIVCAL (SIG, A,B,100.*EXP(PSLOG(I)),NSL,1,1) 

      DO 300 L=1,NSL
         SIG(L)=LOG(SIG(L))
  300 CONTINUE
  
C     * LOOP OVER SIGMA LEVELS TO BE INTERPOLATED.
C     * X IS THE LN(PRES)=LN(PS)*LN(SIG) VALUE OF REQUIRED SIGMA LEVEL. 
  
      K=1 
      DO 350 N=1,NSL
      X=SIG(N)+PSLOG(I) 
  
C     * FIND WHICH PRESSURE INTERVAL WE ARE IN. 
  
      DO 310 L=K,NPL
      INTVL=L 
  310 IF(X.LT.PRLOG(L)) GO TO 320 
      INTVL=NPL+1 
  320 K=INTVL-1 
      IF(K.EQ.0) K=1
  
C     * NOW INTERPOLATE AT THIS POINT.
  
  350 FS(I,N)  = FPC(K)+DFDLNP(INTVL)*(X-PRLOG(K))
  
  500 CONTINUE
  
      RETURN
      END 

! -----------------------------------------------------------------------------
      subroutine nom (nam, cspl)

!     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.)
!     * Sous-routine qui définit le nom des champs associés aux différents
!     * traceurs en fonction du nombre de traceurs.
!     * Subroutine that defines field names corresponding to tracers, based
!     * on the number of tracers.
      character (len=8) :: nam
      character (len=4) :: cspl
!----------------------------------------------------------------------

      write (nam,1000) cspl
 1000 format(a4,'    ')

      return
      end subroutine nom

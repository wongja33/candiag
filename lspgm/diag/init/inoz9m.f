      PROGRAM INOZ9M
C     PROGRAM INOZ9M  (ICTL,       OZLX,       OZONE,       OUTPUT,     )       I2
C    1          TAPE99=ICTL, TAPE1=OZLX, TAPE2=OZONE, TAPE6=OUTPUT)
C     -------------------------------------------------------------             I2
C                                                                               I2
C     JAN 17/17 - F.MAJAESS (CHANGE UNDEFINED NLOMAX REFERENCE TO MOMAX)        I2
C     OCT 14/09 - M.LAZARE (BASED ON INOZ6M, BUT FOR RANDEL DATA)
C                                                                               I2
CINOZ9M  - PRODUCES MONTHLY GRIDS OF MEAN OZONE VALID AT MID-MONTH      2  1    I1
C                                                                               I3
CAUTHORS - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - INTERPOLATES INITIAL MONTHLY ZONAL CROSS-SECTIONS OF PPMV            I3
C          OZONE FROM EQUALLY-SPACED LATITUDES TO THE GAUSSIAN GRID,            I3
C          AND THEN FILLS 3-D GRID AND MULTIPLIES BY 1.E-6 TO CONVERT           I3
C          TO PPV. (RANDEL DATA)                                                I3
C          THE MODEL ROUTINE OZON13 USES THIS RESULT, ALONG WITH THE            I3
C          SURFACE PRESSURE, TO CALCULATE THE OZONE AMOUNT BETWEEN              I3
C          FULL MODEL SIGMA SURFACES.                                           I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL  = INITIALIZATION CONTROL DATASET (SEE ICNTRL5).                    I3
C      OZLX  = MONTHLY LATITUDINAL GLOBAL CROSS-SECTIONS OF OZONE IN            I3
C              PPMV (ON EQUALLY-SPACED LATITUDES AND A RELATIVELY COARSE        I3
C              PRESSURE GRID) OBTAINED FROM BILL RANDEL.                        I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      OZONE = MONTHLY GAUSSIAN GRIDS OF OZONE AMOUNT                           I3
C              (PPV) FOR EACH INPUT PRESSURE LEVEL, (ON LEVOZ LEVELS,           I3
C              WHERE LEVOZ IS THE NUMBER OF PRESSURE LEVELS IN THE              I3
C              ORIGINAL OZONE DATASET IN THE LLPHYS).                           I3
C              THERE ARE LEVOZ (=24) PRESSURE LEVEL (RECORDS) FOR EACH MONTH.   I3
C---------------------------------------------------------------------------
C
C
C     *    ILAT = SOUTH - NORTH NUMBER OF GRID POINTS.
C     *    ALAT = VECTOR OF EQUALLY-SPACED LATITUDES (S.P. TO N.P.) IN ORIGINAL
C     *           OZONE DATA.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER(ILGM=256, ILGM1=ILGM+1, ILATM=128, LEVOZ=24, NLAT=37,
     1          MOGRID=ILGM1*ILATM, MOMAX=MOGRID,
     2          IGRID=NLAT*LEVOZ)
      LOGICAL OK

      REAL GH(MOGRID)
      REAL*8 SL(ILATM),CL(ILATM),WL(ILATM),WOSSL(ILATM),RADL(ILATM)
      REAL   DLAT(ILATM)
C
      REAL ALAT(NLAT),P(LEVOZ),OZZX(NLAT,LEVOZ),O3(ILATM,LEVOZ)
C
      INTEGER NFDM(12),MMD(12),LBL(ILATM)
C
      COMMON/AOLD/G(IGRID)
      COMMON/ANEW/H(MOGRID)
      COMMON/ICOM/IBUF(8),IDAT(IGRID)
      COMMON/JCOM/JBUF(8),JDAT(MOMAX)
C
      EQUIVALENCE (G(1),OZZX(1,1))
C
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
C
C     * LATITUDE VALUES ARE IN DEGREES AND PRESSURE VALUES ARE IN PASCALS.
C
      DATA ALAT/-90.E0,-85.E0,-80.E0,-75.E0,-70.E0,
     &          -65.E0,-60.E0,-55.E0,-50.E0,-45.E0,
     &          -40.E0,-35.E0,-30.E0,-25.E0,-20.E0,
     &          -15.E0,-10.E0,- 5.E0,  0.E0,  5.E0,
     &           10.E0, 15.E0, 20.E0, 25.E0, 30.E0,
     &           35.E0, 40.E0, 45.E0, 50.E0, 55.E0,
     &           60.E0, 65.E0, 70.E0, 75.E0, 80.E0,
     &           85.E0, 90.E0/
      DATA    P/
     &  1.00E0, 1.50E0, 2.00E0, 3.00E0, 5.00E0,
     &  7.00E0, 10.0E0, 15.0E0, 20.0E0, 30.0E0,
     &  50.0E0, 70.0E0, 80.0E0, 100.E0, 150.E0,
     &  200.E0, 250.E0, 300.E0, 400.E0, 500.E0,
     &  600.E0, 700.E0, 850.E0,1000.E0/
      DATA MAXX/MOGRID/, MAXLAT/ILATM/
C-----------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,99,1,2,6)
C
C     * GET PARAMETERS FROM CONTROL FILE.
C
      REWIND 99
      READ(99,END=902) LABL,ILEV,(XXX,L=1,ILEV)
      READ(99,END=903) LABL,IXX,IXX,ILG,ILAT,IXX,IDAY,GMT
      ILG1=ILG+1
      WRITE(6,6010)  ILEV,ILG,ILAT,IDAY,GMT
      IF(ILAT*ILG1.GT.MOMAX) CALL                  XIT('INOZ9M',-1)
C
C     * GET LAT-HGT CROSS-SECTION FOR EACH MONTH (ARRAY NLATG X LEVOZ),
C     * WHERE NLATG IS THE NUMBER OF GAUSSIAN LATITUDES AND LEVOZ
C     * IS THE NUMBER OF PRESSURE LEVELS.
C
      DO 290 MONTH=1,12
      NDAY=NFDM(MONTH)
      CALL GETFLD2(-1,G,NC4TO8("ZONL"),NDAY,NC4TO8("OZR2"),0,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INOZ9M',-2)
      WRITE(6,6020) IBUF
      IF(NLAT.NE.IBUF(5)) CALL                     XIT('INOZ9M',-3)
      IF(LEVOZ.NE.IBUF(6)) CALL                    XIT('INOZ9M',-4)
C
      IF(ILAT.GT.MAXLAT.OR.NLAT.GT.MAXLAT) CALL    XIT('INOZ9M',-5)
C
C     * SET DLAT TO GAUSSIAN LATITUDES (DEG).
C
      ILATH=ILAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RADL,WOSSL)
      CALL  TRIGL(ILATH,SL,WL,CL,RADL,WOSSL)
      DO 110 J=1,ILAT
  110 DLAT(J)=RADL(J)*180.E0/3.14159E0
C
C     * PERFORM INTERPOLATION ON PRESSURE SURFACES TO MODEL GAUSSIAN LATITUDES.
C     * THE ARRAY LBL HOLDS THE POSITION IN THE ARRAY ALAT WHOSE CORRESPONDING
C     * LATITUDE IS CLOSEST TO, BUT SOUTH OF, (OR POSSIBLY EQUAL TO) THE
C     * GAUSSIAN LATITUDE DLAT(J); HENCE ONE VALUE FOR EACH GAUSSIAN LATITUDE.
C     * THE RESULTING ARRAY IS O3(ILAT,LEVOZ).
C
      DO 145 J=1,ILAT
        IB=0
        DO 140 I=1,NLAT
          IF(DLAT(J).LT.ALAT(I).AND.IB.EQ.0) IB=I
  140   CONTINUE
        IF(IB.EQ.0) THEN
          WRITE(6,6030) DLAT(J)
          CALL                                     XIT('INOZ9M',-6)
        ENDIF
        LBL(J)=IB-1
  145 CONTINUE
C
      DO 160 L=1,LEVOZ
        DO 150 J=1,ILAT
        K=LBL(J)
        CALL LININT(ALAT(K),OZZX(K,L),ALAT(K+1),OZZX(K+1,L),DLAT(J),
     1              O3(J,L))
  150 CONTINUE
  160 CONTINUE
C
C     * EVALUATE THE AMOUNT OF OZONE (IN VOLUME MIXING RATIO).
C
  170 CONTINUE
      MDAY=MMD(MONTH)
      CALL SETLAB(JBUF,NC4TO8("GRID"),MDAY,NC4TO8("  OZ"),
     1            0,ILG1,ILAT,0,1)
      DO 200 L=1,LEVOZ
        NIJ=0
        DO 180 J=1,ILAT
          H(J)=O3(J,L)*1.0E-6
          DO I=1,ILG1
            NIJ=NIJ+1
            GH(NIJ)=H(J)
          END DO
  180   CONTINUE
        JBUF(4)=L
        CALL PUTFLD2(2,GH,JBUF,MAXX)
        WRITE(6,6020) JBUF
  200 CONTINUE
  290 CONTINUE
C
      CALL                                         XIT('INOZ9M',0)
C
C     * E.O.F. ON FILE ICTL.
C
  902 CALL                                         XIT('INOZ9M',-7)
  903 CALL                                         XIT('INOZ9M',-8)
C--------------------------------------------------------------------
 6010 FORMAT('0 ILEV,ILG,ILAT,IDAY,GMT =',4I5,2X,F5.2)
 6020 FORMAT(' ',A4,I10,2X,A4,I10,4I10)
 6030 FORMAT(////,5X,'**** INTERPOLATION ATTEMPTED OUTSIDE THE RANGE ',
     1'OF THE LATITUDE GRID (-90 TO 90 DEG OF LAT) FOR LAT= ',F6.2,////)
      END

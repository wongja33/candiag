      PROGRAM ENSMERG
C     PROGRAM ENSMERG (MERG,        IN1,        IN2,        IN3,                C2
C    1                 IN4,         IN5,        IN6,        IN7,                C2
C    2                 IN8,         IN9,        IN10,       OUTPUT,     )       C2
C    3          TAPE20=MERG, TAPE21=IN1, TAPE22=IN2, TAPE23=IN3,
C    4          TAPE24=IN4,  TAPE25=IN5, TAPE26=IN6, TAPE27=IN7,
C    5          TAPE28=IN8,  TAPE29=IN9, TAPE30=IN10,TAPE6=OUTPUT)
C     ------------------------------------------------------------              C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     MAR 05/02 - F.MAJAESS  (REVISED TO HANDLE "CHAR" KIND RECORDS)            
C     JUL 22/99 - F. MAJAESS (CORRECT HANDLING OF SUPERLABEL RECORDS)           
C     JUN 23/93 - S. PIGGOTT                                                    
C                                                                               C2
CENSMERG - MERGES UP TO TEN FILES IN PREPARATION FOR ENSEMBLE                   C1
C          AVERAGING                                                   10  1    C1
C                                                                               C3
CAUTHOR  - S. PIGGOTT                                                           C3
C                                                                               C3
CPURPOSE - MERGES UP TO TEN ***IDENTICALLY STRUCTURED*** TIME ORDERED           C3
C          CCRD/N FILES IN PREPARATION FOR THE ENSAVG (ENSEMBLE AVERAGING)      C3
C          AND ENSDEV (ENSEMBLE DEVIATION) PROGRAMS.                            C3
C                                                                               C3
C          ENSEMBLE AVERAGING TAKES PLACE ACROSS FILES AND THERE IS AN UPPER    C3
C          LIMIT OF TEN FILES THAT CAN BE PROCESSED IN A SINGLE PASS OF THE     C3
C          PROGRAM. HOWEVER, THE MERGING PROCESS CAN BE REPEATED ANY NUMBER     C3
C          OF TIMES SO THAT AN ARBITRARY NUMBER OF FILES MAY BE PREPARED FOR    C3
C          ENSEMBLE AVERAGING.                                                  C3
C                                                                               C3
C          THE MERGING PROCESS IS AS FOLLOWS. THE FIRST SINGLE/MULTI-LEVEL      C3
C          SET FROM EACH INPUT FILE IS COPIED ONTO THE SPECIFIED OUTPUT FILE.   C3
C          THESE ARE FOLLOWED BY THE SECOND SINGLE/MULTI-LEVEL SET FROM EACH    C3
C          INPUT FILE. THE PROCESS CONTINUES UNTIL ALL TIME LEVELS HAVE BEEN    C3
C          COPIED ONTO THE OUTPUT FILE. ALL INPUT FILES MUST THEREFORE HAVE     C3
C          THE SAME NUMBER OF IDENTICALLY STRUCTURED AND TIME ORDERED           C3
C          SINGLE/MULTI-LEVEL SETS (ENSMERG WILL ABORT IF THIS IS *NOT* THE     C3
C          CASE). THE RESULTING OUTPUT FILE WILL HENCEFORTH BE REFERRED TO      C3
C          AS A "MERGED" FILE.                                                  C3
C                                                                               C3
C          MERGED FILES CAN ALSO BE INPUT FILES FOR THIS PROGRAM. THE MERGING   C3
C          PROCESS PRODUCES THE SAME KIND OF OUTPUT AS ABOVE.                   C3
C                                                                               C3
C          ANY COMBINATION OF MERGED AND ORIGINAL FILES MAY BE USED             C3
C          FOR INPUT.                                                           C3
C                                                                               C3
C          THE FINAL MERGED FILE MAY THEN PASSED ON TO THE ENSAVG PROGRAM       C3
C          TO PRODUCE A SINGLE/MULTILEVEL SET THAT IS THE AVERAGE OVER ALL      C3
C          THE SETS AT EACH TIME LEVEL. DEVIATIONS OF THE DATA IN THE MERGED    C3
C          FILE FROM THE ENSEMBLE AVERAGED DATA MAY BE CALCULATED USING THE     C3
C          ENSDEV PROGRAM.                                                      C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      IN1, ... ,IN10 = INPUT FILES TO BE MERGED (UP TO 10 MAY BE SPECIFIED)    C3
C                       ANY, ALL, OR NONE OF THE FILES MAY BE EMPTY.            C3
C                       A MIXTURE OF MERGED AND ORIGINAL FILES IS ALLOWED       C3
C                       AND THESE MAY BE IN ANY ORDER.                          C3
C                       ALL THE ORIGINAL FILES *MUST* HAVE IDENTICAL SIZE AND   C3
C                       STRUCTURE AND ALL THE MERGED FILES MUST HAVE BEEN       C3
C                       PRODUCED FROM ORIGINAL FILES WITH THE SAME SIZE AND     C3
C                       STRUCTURE.                                              C3
C                       EACH ORIGINAL FILE MAY CONTAIN DATA FOR MORE THAN ONE   C3
C                       VARIABLE AND MAY CONTAIN SINGLE/MULTI-LEVEL SETS OF     C3
C                       DIFFERENT SIZES AS LONG AS ALL THE ORIGINAL FILES ARE   C3
C                       IDENTICAL IN THESE RESPECTS.                            C3
C                       "LABL" OR "CHAR" KIND RECORDS (IF ANY) ARE SIMPLY       C3
C                       SKIPPED.                                                C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      MERG = MERGED FILE CONTAINING TIME LEVELS OF SINGLE/MULTILEVEL SETS      C3
C             TAKEN ACROSS ALL OF THE ABOVE INPUT FILES.                        C3
C                                                                               C3
C             NOTE: IBUF(2) IS ASSIGNED A NEGATIVE 9 DIGIT LABEL.               C3
C                   IBUF(2) IS MADE NEGATIVE TO ENABLE THIS PROGRAM TO          C3
C                   RECOGNIZE THE FILE AS A MERGED FILE IF IT IS USED FOR       C3
C                   INPUT.                                                      C3
C                                                                               C3
C                   -TTTFFFNNN                                                  C3
C                                                                               C3
C                   TTT = TIME LEVEL NUMBER                                     C3
C                   FFF = ORIGINAL INPUT FILE NUMBER                            C3
C                   NNN = TOTAL NUMBER OF ORIGINAL INPUT FILES MERGED           C3
C                                                                               C3
C                   NO LEADING ZEROS ARE DISPLAYED IF 'TTT' IS NOT A 3 DIGIT    C3
C                   NUMBER.                                                     C3
C-------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      INTEGER NAME,TIME,TTT
      INTEGER KBUF(8)
C
      COMMON/BLANCK/A(SIZES_BLONP1xBLAT),B(SIZES_BLONP1xBLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/MACHTYP/ MACHINE,INTSIZE
C
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C-------------------------------------------------------------------------------
      NF=12
      CALL JCLPNT(NF,20,21,22,23,24,25,26,27,28,29,30,6)
      NF=NF-2
      DO N=20,20+NF
       REWIND N
      ENDDO
C
C     * NTIM   = THE TIME LEVEL NUMBER IN THE OUTPUT FILE.
C     * NORFIL = THE ORIGINAL FILE NUMBER WITHIN EACH TIME LEVEL IN THE OUTPUT 
C     *          FILE.
C     * NFILT  = THE TOTAL NUMBER OF ORIGINAL FILES MERGED IN THE OUTPUT FILE.
C     * NINFIL = THE NUMBER OF THE CURRENT NON-EMPTY INPUT FILE WHETHER
C     *          IT IS MERGED OR ORIGINAL.
C     * NRSET  = THE NUMBER OF RECORDS TAKEN FROM THE CURRENT SET (OF AN
C     *          ORIGINAL INPUT FILE) OR THE CURRENT TIME LEVEL (OF A
C     *          MERGED INPUT FILE).
C     * NR     = THE TOTAL NUMBER OF RECORDS IN THE OUTPUT FILE.
C     * NFIN   = THE NUMBER OF CONNECTED FILES THAT ARE EITHER
C     *          EMPTY OR HAVE BEEN COMPLETELY READ (FINISHED).
C     * NMT    = THE NUMBER OF CONNECTED FILES THAT ARE EMPTY.
C
      NTIM=0
      NORFIL=1
      NFILT=0
      NINFIL=1
      NRSET=0
      NR=0
      NFIN=0
      NMT=0
      MAXLEN=MAXX+8
C-------------------------------------------------------------------------------
C
C     * DETERMINE IF THE INPUT FILES ARE MERGED FILES OR ORIGINAL
C     * FILES.
C     * IBUF(2) IS NEGATIVE IN MERGED FILES.
C     * IBUF(2) IS POSITIVE IN ORIGINAL FILES.
C     * COUNT THE TOTAL NUMBER OF ORIGINAL FILES CONTRIBUTED FROM
C     * THE INPUT FILES, IN VARIABLE NFILT.
C     * COUNT THE NUMBER OF EMPTY FILES IN NMT.
C
      DO 100 N=21,20+NF
        NN=N-20
   90   CALL FBUFFIN(-N,IBUF,-8,K,LEN)
        IF(K.GE.0) THEN
C
C         * THE CURRENT INPUT FILE IS EMPTY.
C
          WRITE(6,6010) NN
          NMT=NMT+1
          GOTO 100
        ENDIF
C
C       * SKIP "LABL" AND/OR "CHAR" RECORDS.
C
        IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR")) THEN
         CALL FBUFFIN(-N,IBUF,MAXLEN,K,LEN)
         IF(K.GT.0) CALL                           XIT('ENSMERG',-1)
         GO TO 90
        ENDIF
        WRITE(6,6020) NN
        CALL PRTLAB(IBUF)
        IF(IBUF(2).LT.0) THEN
C
C         * THE CURRENT INPUT FILE IS A MERGED FILE.
C         * NFILT IS INCREASED BY THE NUMBER OF ORIGINAL FILES MERGED TO
C         * FORM IT (TAKEN FROM THE LAST 3 DIGITS OF IBUF(2)).
C
          LAB2=IABS(IBUF(2))
          NFILT=NFILT+MOD(LAB2,1000)
        ELSE
C
C         * THE CURRENT INPUT FILE IS AN ORIGINAL FILE.
C         * THEREFORE, NFILT IS INCREASED BY ONLY 1.
C
          NFILT=NFILT+1
        ENDIF
        REWIND N
  100 CONTINUE
C
C     * EXIT WITH A WARNING IF NFILT = 0 (IF ALL INPUT FILES ARE EMPTY).
C
      IF(NFILT.EQ.0) THEN
        WRITE(6,6070)
        CALL                                       XIT('ENSMERG',-101)
      ENDIF
C-------------------------------------------------------------------------------
C
C     * DURING ONE CYCLE OF THE FOLLOWING LOOP, A COMPLETE TIME LEVEL
C     * IS TAKEN FROM THE INPUT FILES AND SENT TO THE OUTPUT FILE.
C
C     * NFIN KEEPS TRACK OF THE NUMBER OF INPUT FILES THAT ARE EITHER
C     * EMPTY OR COMPLETELY READ. WHEN THIS NUMBER EQUALS NF (THE # OF INPUT
C     * FILES ASSIGNED UNIT NUMBERS) THEN THE PROGRAM HAS FINISHED.
C     * IF NFIN COUNTS MORE THAN 'NMT' FILES BUT LESS THAN 'NF' IN ONE
C     * COMPLETE PASS OF THIS LOOP THEN ONE OR MORE OF THE NON-EMPTY INPUT
C     * FILES MUST BE SMALLER THAN THE OTHERS. IF SO THEN STOP.
C
  200 CONTINUE
      NTIM=NTIM+1
      IF(NFIN.GT.NMT) CALL                         XIT('ENSMERG',-2)
      NORFIL=1
      NINFIL=1
      NFIN=0
C
C     * DURING ONE CYCLE OF THE FOLLOWING LOOP ONE SINGLE/MULTI-LEVEL SET
C     * (OF AN ORIGINAL FILE) OR ONE TIME LEVEL (OF A MERGED FILE)
C     * IS COPIED FROM ONE OF THE INPUT FILES TO THE OUTPUT FILE. THIS IS
C     * DONE ONCE FOR EVERY NON-EMPTY INPUT FILE DURING ONE PASS OF THE ABOVE
C     * LOOP.
C
      DO 300 N=21,20+NF
        NN=N-20
        NR=NR+NRSET
        NRSET=0
C
C       * DURING ONE CYCLE OF THE FOLLOWING LOOP ONE RECORD IS COPIED
C       * FROM ONE OF THE INPUT FILES TO THE OUTPUT FILE.
C       * THIS IS DONE CONTINUALLY UNTIL ALL THE RECORDS OF THE CURRENT
C       * SET (OF AN ORIGINAL INPUT FILE) OR THE CURRENT TIME LEVEL (OF
C       * A MERGED INPUT FILE) ARE COPIED TO THE OUTPUT FILE DURING
C       * ONE PASS OF THE ABOVE LOOP.
C
  400   CALL FBUFFIN(-N,IBUF,-8,K,LEN)
        IF(K.GE.0) THEN
          NFIN=NFIN+1
C
C         * IF NFIN EQUALS NF THEN THE PROGRAM HAS FINISHED.
C
          IF(NFIN.EQ.NF) THEN
            NR=NR+NRSET
            WRITE(6,6040) NR
            IF(NR.EQ.0) CALL                       XIT('ENSMERG',-3)
            WRITE(6,6050) NTIM,NFILT
            CALL                                   XIT('ENSMERG',0)
          ENDIF
C
C         * IF FBUFFIN FAILED BECAUSE THE LAST RECORD OF THE CURRENT
C         * (NON-EMPTY) INPUT FILE WAS READ ON THE PREVIOUS PASS THEN
C         * GOTO 290 TO SEE IF THE CORRECT NUMBER OF RECORDS WERE TAKEN
C         * FROM THE FILE AND TO START THE DO LOOP OVER AGAIN FOR THE NEXT FILE.
C         * HOWEVER IF FBUFFIN FAILED SIMPLY BECAUSE THE CURRENT UNIT NUMBER
C         * IS ATTACHED TO AN EMPTY FILE THEN GOTO 300 TO START THE DO LOOP
C         * AGAIN FOR THE NEXT FILE.
C         * HOW CAN YOU TELL?
C         * IF THE CURRENT UNIT NUMBER IS ATTACHED TO A N0N-EMPTY FILE THAN
C         * RECORDS WILL HAVE BEEN READ FROM IT (NRSET WILL BE GREATER THAN 0).
C
          IF(NRSET.GT.0) THEN
            GOTO 290
          ELSE
            GOTO 300
          ENDIF
        ENDIF
C
C       * SKIP "LABL" AND/OR "CHAR" KIND RECORDS.
C
        IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR")) THEN
         CALL FBUFFIN(-N,IBUF,MAXLEN,K,LEN)
         IF(K.GT.0) CALL                           XIT('ENSMERG',-4)
         GO TO 400
        ENDIF
C
C       * DECIDE IF THE CURRENT RECORD STARTS A NEW SET (IN AN ORIGINAL
C       * FILE) OR A NEW TIME LEVEL (IN A MERGED FILE).
C       * IF THE FILE IS AN ORIGINAL FILE A NEW SET STARTS WHEN THE TIME
C       * STEP NUMBER (IBUF(2)) OR VARIABLE NAME (IBUF(3)) CHANGES FROM ONE
C       * RECORD TO THE NEXT.
C       * IF THE FILE IS A MERGED FILE THEN A NEW TIME LEVEL STARTS WHEN THE
C       * TIME LEVEL NUMBER PART OF IBUF(2), TTT, INCREMENTS BY 1 FROM ONE
C       * RECORD TO THE NEXT.
C
C       * IF THE CURRENT RECORD STARTS A NEW SET/TIME LEVEL THEN
C       * GOTO 290 TO SEE IF THE CORRECT NUMBER OF RECORDS WERE TAKEN.
C
C       * IF IT IS NOT A MERGED INPUT FILE DO THIS.
C
        IF(IBUF(2).GE.0) THEN
          IF(NRSET.EQ.0) THEN
            NAME=IBUF(3)
            TIME=IBUF(2)
            NSET=1
          ELSE
            IF(IBUF(3).NE.NAME.OR.IBUF(2).NE.TIME) THEN
              BACKSPACE N
              GOTO 290
            ENDIF
          ENDIF
C
C       * IF IT IS A MERGED INPUT FILE DO THIS.
C       * USE THE ABSOLUTE VALUE OF IBUF(2), LAB2, IN CALCULATIONS.
C
        ELSE
          LAB2=IABS(IBUF(2))
          IF(NRSET.EQ.0) THEN
            TTT=LAB2/1000000
            TIME=LAB2
            NSET=MOD(TIME,1000)
          ELSE
            J=LAB2/1000000
            IF(J.NE.TTT) THEN
              BACKSPACE N
              GOTO 290
            ENDIF
C
C           * DETERMINE IF A NEW SET (DIFFERENT ORIGINAL FILE NUMBER) WITHIN
C           * THE CURRENT TIME LEVEL HAS BEEN REACHED.
C           * THIS HAPPENS WHEN THE ORIGINAL FILE NUMBER PART OF
C           * IBUF(2), FFF, INCREMENTS FROM ONE RECORD TO THE NEXT.
C           * WE CAN JUST TEST FOR A CHANGE IN IBUF(2) (OR LAB2) SINCE THE NNN
C           * PART IS FIXED AND THE TTT PART HAS ALREADY BEEN TESTED FOR A 
C           * CHANGE.
C           * IF A NEW ORIGINAL FILE NUMBER HAS BEEN REACHED THEN INCREMENT
C           * NORFIL BY +1.
C
            IF(LAB2.NE.TIME) THEN
              TIME=LAB2
              NORFIL=NORFIL+1
            ENDIF
          ENDIF
        ENDIF
C
C       * COMPARE THE 8 WORD LABEL OF THE FIRST RECORD IN CORRESPONDING
C       * SETS/TIME LEVELS ACROSS DIFFERENT FILES TO MAKE SURE THAT THEY
C       * HAVE THE SAME SIZE, TYPE AND VARIABLE NAME.
C
        IF(NRSET.EQ.0) THEN
          IF(NORFIL.EQ.1) THEN
            DO 600 I=1,8
              KBUF(I)=IBUF(I)
  600       CONTINUE
          ELSE
            CALL CMPLBL(0,KBUF,0,IBUF,OK)
            IF(.NOT.OK.OR.KBUF(3).NE.IBUF(3)) THEN
              CALL PRTLAB(KBUF)
              CALL PRTLAB(IBUF)
              CALL                                 XIT('ENSMERG',-5)
            ENDIF
          ENDIF
        ENDIF
C
C       * THE CURRENT RECORD BELONGS TO THE CURRENT SET (OF AN ORIGINAL FILE)
C       * OR THE CURRENT TIME LEVEL (OF A MERGED FILE) SINCE
C       * THE ABOVE CONDITIONS WERE NOT MET. THEREFORE SEND THE RECORD TO THE
C       * OUTPUT FILE. INCREMENT COUNTER NRSET BY +1.
C       * THEN GO BACK TO 400 TO GET THE NEXT RECORD FROM THE CURRENT
C       * INPUT FILE.
C
        CALL FBUFFIN(-N,IBUF,MAXLEN,K,LEN)
        IF(K.GT.0) CALL                            XIT('ENSMERG',-6)
        IBUF(2)=-(NTIM*1000000+NORFIL*1000+NFILT)
        CALL FBUFOUT(20,IBUF,LEN,K)
        NRSET=NRSET+1
        GOTO 400
C
C       * CHECK TO SEE THAT THE SETS (ONE FROM EACH ORIGINAL FILE)
C       * THAT MAKE UP THE CURRENT TIME LEVEL ALL HAVE THE SAME NUMBER
C       * OF RECORDS.
C
  290   IF(NINFIL.EQ.1) THEN
          NCHEK1=NRSET/NSET
        ELSE
          NCHEK2=NRSET/NSET
          IF(NCHEK1.NE.NCHEK2) THEN
            WRITE(6,6800) NCHEK1,NCHEK2,NR
            CALL                                   XIT('ENSMERG',-7)
          ENDIF
        ENDIF
        NINFIL=NINFIL+1
        NORFIL=NORFIL+1
  300 CONTINUE
      GOTO 200
C-------------------------------------------------------------------------------
 6010 FORMAT('0  INPUT FILE ',I2,' IS EMPTY.')
 6020 FORMAT('0  FIRST RECORD OF INPUT FILE ',I2,':')
 6040 FORMAT(' ',I7,' TOTAL RECORDS IN THE OUTPUT FILE.')
 6050 FORMAT('0 THE OUTPUT FILE CONTAINS ',I7,' TIME LEVELS FROM ',
     1        I7,' ORIGINAL FILES.')
 6070 FORMAT('0 WARNING: ALL THE INPUT FILES ARE EMPTY.')
 6800 FORMAT(3I7)
      END

      PROGRAM GSUB
C     PROGRAM GSUB (X,       Y,       Z,       OUTPUT,                  )       C2
C    1        TAPE1=X, TAPE2=Y, TAPE3=Z, TAPE6=OUTPUT)
C     ------------------------------------------------                          C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     DEC 15/97 - BASED ON "GMLT"                                               
C                                                                               C2
CGSUB    - SUBTRACTS THE FIRST RECORD IN THE SECOND FILE FROM EACH              C1
C                                          RECORD IN THE FIRST FILE     2  1    C1
C                                                                               C3
CAUTHOR  - F.MAJAESS                                                            C3
C                                                                               C3
CPURPOSE - FILE ARITHMETIC PROGRAM THAT SUBTRACTS FROM EACH RECORD IN THE FIRST C3
C          INPUT FILE(X) THE FIRST RECORD IN THE SECOND INPUT FILE(Y) AND PUTS  C3
C          THE RESULTS ON FILE Z. (Z = X - (FIRST RECORD OF) Y )                C3
C          NOTE - NO TYPE CHECKING IS DONE EXCEPT FOR THE REQUIREMENT           C3
C                 THAT THE TWO FILES HAVE SAME TYPE OF RECORDS.                 C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = ANY DATA FILE WITH CCRN LABEL                                        C3
C      Y = ANY DATA FILE WITH CCRN LABEL                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Z = OUTPUT FILE WHOSE CONTENT IS: X - (FIRST REC. OF) Y                  C3
C---------------------------------------------------------------------------
C 
  
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/ A(SIZES_LONP1xLAT), B(SIZES_LONP1xLAT) 
      CHARACTER*4 CTYP
      INTEGER TYPE
      LOGICAL OK
  
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
  
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
  
C---------------------------------------------------------------------
      NF = 4
      CALL JCLPNT(NF,1,2,3,6) 
      REWIND 1
      REWIND 2
      REWIND 3
  
C     * READ THE FIRST RECORD OF Y. 
  
      CALL GETFLD2(2,B,-1,-1,-1,-1,IBUF,MAXX,OK) 
  
      IF (.NOT.OK) CALL                            XIT('GSUB',-1) 
  
      TYPE = IBUF(1)
      ILG  = IBUF(5)
      ILAT = IBUF(6)
      NWDS = ILG*ILAT 
      WRITE(CTYP,0001) TYPE 
      IF (CTYP.EQ.'SPEC' .OR. CTYP.EQ.'FOUR') 
     1    NWDS = NWDS*2 
  
C     * READ THE NEXT FIELD.
  
      NR = 0
  100 CALL GETFLD2(1,A,TYPE,-1,-1,-1,IBUF,MAXX,OK) 
  
          IF (.NOT.OK)                                         THEN 
              IF(NR.EQ.0) CALL                     XIT('GSUB',-2) 
              WRITE(6,6010) NR
              CALL                                 XIT('GSUB',0)
          END IF
  
          IF (NR.EQ.0) CALL PRTLAB (IBUF)
  
C         * MAKE SURE THAT A AND B ARE THE SAME SIZE. 
  
          IF (IBUF(5).NE.ILG .OR. IBUF(6).NE.ILAT)             THEN 
              CALL PRTLAB (IBUF)
              CALL                                 XIT('GSUB',-3) 
          END IF
  
C         * SUBTRACT THE FIELDS.
  
          DO 200 I=1,NWDS 
              A(I) = A(I)-B(I)
  200     CONTINUE
  
C         * SAVE THE RESULT ON FILE C.
  
          CALL PUTFLD2(3,A,IBUF,MAXX)
          IF (NR.EQ.0) CALL PRTLAB (IBUF)
  
      NR = NR+1 
      GOTO 100
  
C---------------------------------------------------------------------
 0001 FORMAT(A4)
 6010 FORMAT('0',I6,' RECORDS PROCESSED.')
      END

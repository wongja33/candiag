      PROGRAM CONJMLT 
C     PROGRAM CONJMLT (CA,       CB,       CC,       OUTPUT,            )       C2
C    1           TAPE1=CA, TAPE2=CB, TAPE3=CC, TAPE6=OUTPUT)
C     ------------------------------------------------------                    C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 06/83 - R.LAPRISE.                                                  
C     FEB 14/80 - J.D.HENDERSON 
C                                                                               C2
CCONJMLT - COMPLEX CONJUGATE MULTIPLICATION OF TWO COMPLEX FILES        2  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - MULTIPLIES A COMPLEX FILE CA BY THE COMPLEX CONJUGATE OF A           C3
C          SECOND COMPLEX FILE CB AND PUTS THE RESULT IN CC FILE.               C3
C          CA AND CB ARE BOTH COMPLEX.                                          C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      CA = FIRST  INPUT FILE                                                   C3
C      CB = SECOND INPUT FILE                                                   C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      CC = CA*CONJG(CB)                                                        C3
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX CA,CB 
      COMMON/BLANCK/CA(int((SIZES_LONP1xLAT + 1)/2.e0)),
     & CB(int((SIZES_LONP1xLAT + 1)/2.e0)) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * READ THE NEXT PAIR OF FIELDS. 
C 
      NR=0
  140 CALL GETFLD2(1,CA, -1 ,0,0,0,IBUF,MAXX,OK) 
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('CONJMLT',-1)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('CONJMLT',0) 
        ENDIF 
      ENDIF 
C 
      CALL GETFLD2(2,CB, -1 ,0,0,0,JBUF,MAXX,OK) 
      IF(.NOT.OK) CALL                             XIT('CONJMLT',-2)
C 
C     * MAKE SURE THAT THE FIELDS ARE COMPLEX AND THE SAME SIZE.
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,JBUF 
        CALL                                       XIT('CONJMLT',-3)
      ENDIF 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(.NOT.SPEC) CALL                           XIT('CONJMLT',-4)
C 
C     * MULTIPLY THE FIELDS.
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS 
  210 CA(I)=CA(I)*CONJG(CB(I))
C 
C     * SAVE THE RESULT ON FILE C.
C 
      IBUF(3)=NC4TO8("CMLT")
      CALL PUTFLD2(3,CA,IBUF,MAXX) 
      NR=NR+1 
      GO TO 140 
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,'  PAIRS OF RECORDS PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END

      PROGRAM DELHATO 
C     PROGRAM DELHATO (PHIS,       PHI,       DEL,       OUTPUT,        )       D2
C    1           TAPE1=PHIS, TAPE2=PHI, TAPE3=DEL, TAPE6=OUTPUT)
C     ----------------------------------------------------------                D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 06/83 - R.LAPRISE.                                                    
C     NOV 24/81 - J.D.HENDERSON 
C                                                                               D2
CDELHATO - COMPUTES DELTA-HAT FOR OBSERVED HEIGHTS                      2  1    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES DELTA-HAT FOR SETS OF GEOPOTENTIAL PHI AND SURFACE          D3
C          GEOPOTENTIAL PHIS.                                                   D3
C          DELTA-HAT IS 1 AT EVERY POINT ABOVE THE MOUNTAIN(S)                  D3
C                   AND 0                BELOW IT.                              D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      PHIS = SURFACE GEOPOTENTIAL FIELD                                        D3
C      PHI  = ONE GRID SET OF PRESSURE LEVEL GEOPOTENTIALS.                     D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      DEL  = OUTPUT SET OF THE DELTA-HAT FUNCTION                              D3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/PHIS(SIZES_LONP1xLAT),X(SIZES_LONP1xLAT)
C 
      LOGICAL OK
      INTEGER KBUF(8) 
      COMMON /ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * GET THE MOUNTAINS.
C 
      CALL GETFLD2(1,PHIS,NC4TO8("GRID"),0,NC4TO8("PHIS"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('DELHATO',-1)
      WRITE(6,6025) IBUF
      NWDS=IBUF(5)*IBUF(6)
      NPACK=MIN(2,IBUF(8))
      DO 110 I=1,8
  110 KBUF(I)=IBUF(I) 
C 
C     * READ PHI ONE LEVEL AT A TIME. 
C 
      NR=0
  150 CALL GETFLD2(2,X,NC4TO8("GRID"),-1,NC4TO8(" PHI"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('DELHATO',-2)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('DELHATO',0) 
        ENDIF 
      ENDIF 
      IF(NR.LT.20) WRITE(6,6025) IBUF 
C 
      CALL CMPLBL(0,IBUF,0,KBUF,OK) 
      IF(.NOT.OK)THEN 
        CALL                                       XIT('DELHATO',-3)
        WRITE(6,6025) IBUF,KBUF 
      ENDIF 
      IF (NR.EQ.0) THEN
        NPACK=MIN(NPACK,IBUF(8))
      ENDIF
C 
C     * COMPUTE DELTA-HAT FOR EACH POINT ON THIS LEVEL
C 
      DO 210 I=1,NWDS 
      PHI=X(I)
      IF(PHI.GE.PHIS(I))THEN
        X(I)=1.E0 
      ELSE
        X(I)=0.E0 
      ENDIF 
  210 CONTINUE
C 
C     * PUT THIS LEVEL OF DELTA-HAT ON FILE 3 (PACKED 2 TO 1).
C 
      IBUF(3)=NC4TO8("DELO")
      IBUF(8)=NPACK
      CALL PUTFLD2(3,X,IBUF,MAXX)
      IF(NR.LT.20) WRITE(6,6025) IBUF 
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------- 
 6010 FORMAT(' ',I6,'  RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END

      PROGRAM MKWGHT
C     PROGRAM MKWGHT (W,      MASK,      INPUT,      OUTPUT,            )       D2
C    1          TAPE1=W,TAPE2=MASK,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------------                    D2
C                                                                               D2
C     MAR 02/09 - S.KHARIN (ADD INORM AND ICYCL PARAMETERS.                     D2
C                           IMPROVE SUPPORT FOR P-S GRIDS.                      D2
C                           REMOVE CHECK FOR ODD/EVEN NLATG.)                   D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     MAR 09/99 - S.KHARIN.
C                                                                               D2
CMKWGHT  - CALCULATE AREA WEIGHTS FOR A SUBAREA OF A GLOBAL GRID.       1  1 C  D1
C                                                                               D3
CAUTHOR  - S.KHARIN                                                             D3
C                                                                               D3
CPURPOSE - CALCULATE AREA WEIGHTS FOR A SUBAREA OF A GLOBAL GRID.               D3
C          GRID TYPE CAN BE GAUSSIAN, LAT/LON OR STANDARD POLAR STEREOGR.       D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C     MASK      (OPTIONAL) IF SPECIFIED, PROVIDES MASK FOR CALCULATION          D3
C               OF AREA WEIGHTS.                                                D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      W   =    GRID OF AREA WEIGHTS.                                           D3
C
CINPUT PARAMETERS...
C                                                                               D5
C(I5) NLATG     NUMBER OF LATITUDES IN THE GLOBAL GRID.                         D5
C               THIS PARAMETER IS IGNORED, IF GRID IS POLAR STEREOGRAPHIC.      D5
C(2I5)I1,I2     THE FIRST AND LAST SUBAREA'S GRID X-COORDINATES.                D5
C               I2 MUST BE GREATER THAN, OR EQUAL TO I1.                        D5
C               TIP: IF THE SUBAREA OF A GAUSSIAN OR LAT/LON GRID CONTAINS      D5
C               THE GREENWICH MERIDIAN, I.E. THE LAST LONGITUDE IN THE SUBAREA  D5
C               IS SMALLER THAN THE FIRST ONE, USE I1=1 AND                     D5
C               I2=(NUMBER OF LONGITUDES IN THE SUBAREA).                       D5
C(2I5)J1,J2     THE FIRST AND LAST SUBAREA'S GRID Y-COORDINATES.                D5
C               J2 MUST BE GREATER THAN, OR EQUAL TO J1.                        D5
C(I1) INORM = 1 NORMALIZE WEIGHTS SO THAT THEIR AVERAGE IS EQUAL TO ONE,        D5
C               OTHERWISE, THE WEIGHTS ARE AREA FRACTIONS.                      D5
C(I2) ICYCL = 0 NO CYCLIC LONGITUDE,                                            D5
C           = 1 ADD A CYCLIC LONGITUDE.                                         D5
C(I2) ISQRT = 0 WEIGHTS ARE GRID BOX AREA FRACTIONS I.E. W(IJ)=A(IJ)/ATOT       D5
C               WHERE A(IJ) IS AREA OF A GRID BOX IJ AND ATOT IS TOTAL AREA.    D5
C               SUM[W(IJ)]=1 AND SUM[W(IJ)*X(IJ)]=AREA AVERAGED X.              D5
C               IF MASK IS SPECIFIED, SUMMATION IS DONE ONLY FOR 'VALID'        D5
C               GRID BOXES.                                                     D5
C           = 1 TAKE THE SQUARE ROOT OF THE AREA WEIGHTS DEFINED AS ABOVE.      D5
C               (USEFUL FOR PREPARING FIELDS FOR MKEOF, CANCOR OR SVDANA        D5
C               OR OTHER 2ND ORDER STATISTICS PROGRAMS).                        D5
C(I5) ITYPE =   GRID TYPE:                                                      D5
C           = 0 GAUSSIAN GRID,                                                  D5
C           = 1 LAT-LON GRID SPANNING THE POLES,                                D5
C           = 2 LAT-LON GRID EXCLUDING THE POLES,                               D5
C           = 3 STANDARD 51X55 POLAR STEREOGRAPHIC. NLATG IS IGNORED.           D5
C           = 9 CUSTOM POLAR STEREOGRAPHIC.                                     D5
C               THE PARAMETERS XP,YP,D60,LC,DGRW,NHEM ARE READ FROM CARD2.      D5
C                                                                               D5
C               THE FOLLOWING PARAMETERS ARE IGNORED UNLESS MASK IS SPECIFIED.  D5
C(3X)                                                                           D5
C(A2) LOP    =  2 CHARACTER LOGICAL OPERATOR (EQ,NE,LT,LE,GT,GE).               D5
C(E10)VALUE  =  COMPARISON VALUE FOR THE LOGICAL OPERATOR LOP.                  D5
C(E10)SPVAL  =  SPECIAL VALUE TO INDICATE MISSING DATA.                         D5
C               IF LOGICAL EXPRESSION (MASK(IJ).LOP.VALUE) IS TRUE,             D5
C               THE AREA WEIGHT IS CALCULATED FOR THIS GRID BOX.                D5
C               OTHERWISE, THE GRID BOX VALUE IS SET TO SPECIAL VALUE SPVAL.    D5
C                                                                               D5
C      CARD 2-                                                                  D5
C      ------                                                                   D5
C              THIS CARD IS READ ONLY WHEN ITYPE=9 (CUSTOM P-S SUB-AREA).       D5
C                                                                               D5
C      XP,YP = GRID COORDINATES OF THE POLE (FLOATING POINT NUMBERS), OR        D5
C              LON AND LAT COORDINATES OF THE GRID CENTER (DEGREES).            D5
C      D60   = GRID LENGTH AT 60 DEGREES (METERS)                               D5
C      LC    = 1, XP AND YP ARE INTERPRETED AS THE LON AND LAT COORDINATES      D5
C              OF THE GRID CENTRE.                                              D5
C              OTHERWISE, XP AND YP ARE THE GRID COORDINATES OF THE POLE.       D5
C      DGRW  = ANGLE OF GREENWHICH MERIDIAN OFF HORIZONTAL (LC=1 ONLY)          D5
C      NHEM  =0,1 PLOT NORTH HEMISPHERE,                                        D5
C            = 2  PLOT SOUTH HEMISPHERE (LC=1 ONLY)                             D5
C                                                                               D5
C       NOTE1 - IF MASK IS SPECIFIED, ITS DIMENSIONS MUST BE CONSISTENT WITH    D5
C               I1,I2,J1,J2.                                                    D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C*MKWGHT     48    1   96    1   48  1                                          D5
C                                                                               D5
C FOR THE ABOVE EXAMPLE, CALCULATE GRID BOX AREA FRACTIONS FOR THE GLOBAL 97X48 D5
C GAUSSIAN GRID WITH AN EXTRA CYCLIC LONGITUDE.                                 D5
C                                                                               D5
C*MKWGHT     48    1   96    1   48  0 1    0   GT    273.16    1.E+38          D5
C                                                                               D5
C FOR THE ABOVE EXAMPLE, PROVIDED MASK FILE IS SPECIFIED,                       D5
C CALCULATE AREA WEIGHTS (SQUARE ROOTS OF AREA FRACTIONS) FOR THE GLOBAL 96X48  D5
C GAUSSIAN GRID WITHOUT A CYCLIC LONGITUDE AT GRID POINTS WHERE MASK IS GREATER D5
C THAN 273.16 AND SET VALUE 1.E+38 AT ALL OTHER GRID POINTS.                    D5
C                                                                               D5
C*MKWGHT           1  125    1   951 0 1    9                                   D5
C*MKWGHT     38.6162  105.1807    5.00E4    1                                   D5
C(CANADA 125X95 POLAR STEREOGRAPHIC)                                            D5

C-------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      LOGICAL OK, MSKTST, COND
      CHARACTER*2 LOP
C
C     * LENMAX IS MAX LENGTH OF GRIDS
C     * NLATMAX IS MAX NUMBER OF LATITUDES
C
      PARAMETER (LENMAX=SIZES_BLONP1xBLAT,
     & LENMAX2=SIZES_BLONP1xBLATxNWORDIO,NLATMAX=1024)
C
C     * W ARE AREA WEIGHTS
C     * FMSK IS MASK

      REAL W(LENMAX),FMSK(LENMAX)
      COMMON/ICOM/IBUF(8),IDAT(LENMAX2)
C
C     * INTERNAL ARRAYS FOR CALCULATING GAUSSIAN LATITUDES AND WEIGHTS
C
      REAL  F(NLATMAX),WT(NLATMAX),SIA(NLATMAX)
     1     ,RAD(NLATMAX),WOCS(NLATMAX)
C
      DATA PI/3.14159265358979E0/,AE/6.37122E+6/
      DATA MAXX/LENMAX2/
C---------------------------------------------------------------------
C
C     * ASSIGN FILES TO FORTRAN UNITS
C
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
C
C     * CHECK IF MSK FILE IS SPECIFIED AND SET LOGICAL SWITCH ACCORDINGLY.
C
      IF(NFF.GT.3)THEN
        MSKTST=.TRUE.
        REWIND 2
        WRITE(6,'(A)') ' MASK FIELD IS SPECIFIED.'
      ELSE
        MSKTST=.FALSE.
      ENDIF
      REWIND 1
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,END=900)NLATG,I1,I2,J1,J2,INORM,ICYCL,ISQRT,ITYPE,            D4
     1     LOP,VALUE,SPVAL                                                      D4
C
C     * PRINT INPUT PARAMETERS TO STANDARD OUTPUT
C
      WRITE(6,6010)NLATG,I1,I2,J1,J2,INORM,ICYCL,ISQRT,ITYPE
      IF (MSKTST) WRITE(6,6020)LOP,VALUE,SPVAL
C
C     * CHECK THE INPUT PARAMETERS
C
      IF (ITYPE.NE.0.AND.ITYPE.NE.1.AND.ITYPE.NE.2.AND.ITYPE.NE.3.AND.
     1    ITYPE.NE.9) THEN
         WRITE(6,'(A,I5)') '0*** ERROR: UNKNOWN GRID TYPE ITYPE=',ITYPE
         CALL                                      XIT('MKWGHT',-1)
      ENDIF

      NHEM=0
      IF (ITYPE.EQ.9) THEN
        READ (5,5015,END=901) XP,YP,D60,LC,DGRW,NHEM                            D4
        WRITE(6,6015) XP,YP,D60,LC
        IF(NHEM.EQ.0)NHEM=1
        IF(LC.EQ.1)WRITE(6,6016) DGRW,NHEM
      ENDIF

      IF (NLATG.LE.0.AND.ITYPE.NE.3.AND.ITYPE.NE.9) THEN
         WRITE(6,'(A)') '0*** ERROR: NON POSITIVE NLATG'
         CALL                                      XIT('MKWGHT',-2)
      ENDIF

      IF (ITYPE.EQ.0.AND.MOD(NLATG,2).EQ.1) THEN
         WRITE(6,'(A)') '0*** ERROR: GRID IS GAUSSIAN BUT NLATG IS ODD.'
         CALL                                      XIT('MKWGHT',-3)
      ENDIF

      IF (I1.GT.I2) THEN
         WRITE(6,'(A)') '0*** ERROR: I1 > I2!'
         CALL                                      XIT('MKWGHT',-4)
      ENDIF

      IF (J1.GT.J2) THEN
         WRITE(6,'(A)') '0*** ERROR: J1 > J2!'
         CALL                                      XIT('MKWGHT',-5)
      ENDIF

      IF (ISQRT.EQ.0) THEN
         WRITE(6,'(A)') ' CALCULATE GRID BOX AREA FRACTIONS.'
      ELSEIF (ISQRT.EQ.1) THEN
         WRITE(6,'(A)') ' CALCULATE SQRT OF GRID BOX AREA FRACTIONS.'
      ELSE
         WRITE(6,'(A,I5)') '0*** ERROR: ILLEGAL ISQRT=',ISQRT
         CALL                                      XIT('MKWGHT',-6)
      ENDIF

      IF (ICYCL.EQ.0) THEN
         WRITE(6,'(A)') ' DO NOT ADD CYCLIC LONGITUDE.'
      ELSE IF (ICYCL.EQ.1) THEN
         WRITE(6,'(A)') ' ADD CYCLIC LONGITUDE.'
      ELSE
         WRITE(6,'(A,I5)') '0*** ERROR: ILLEGAL ICYCL=',ICYCL
         CALL                                      XIT('MKWGHT',-7)
      ENDIF

      NLON=I2-I1+1+ICYCL
      NLAT=J2-J1+1
      NWDS=NLON*NLAT
C
C     * READ MASK FIELD (IF SPECIFIED)
C
      IF (MSKTST) THEN
         CALL GETFLD2(2,FMSK,-1,-1,-1,-1,IBUF,MAXX,OK)
         IF (.NOT.OK) CALL                         XIT('MKWGHT',-8)
         CALL PRTLAB(IBUF)
C
C       * MAKE SURE THAT THE MASK IS CONSISTENT WITH I1,I2,J1,J2
C
         IF (IBUF(5).NE.NLON .OR. IBUF(6).NE.NLAT) THEN
            WRITE(6,'(2A)')
     1           ' *** ERROR: MASK DIMENSIONS ARE NOT CONSISTENT',
     2           ' WITH I1,I2,J1,J2. ***'
            CALL                                   XIT('MKWGHT',-9)
         ENDIF
C
C     * SET FMSK TO 1 IF (FMSK.LOP.VALUE)=TRUE AND 0 OTHERWISE.
C
         DO IJ=1,NWDS
            IF ( LOP.EQ.'LT'.AND.FMSK(IJ).LT.VALUE.OR.
     1           LOP.EQ.'GT'.AND.FMSK(IJ).GT.VALUE.OR.
     2           LOP.EQ.'EQ'.AND.FMSK(IJ).EQ.VALUE.OR.
     3           LOP.EQ.'LE'.AND.FMSK(IJ).LE.VALUE.OR.
     4           LOP.EQ.'GE'.AND.FMSK(IJ).GE.VALUE.OR.
     5           LOP.EQ.'NE'.AND.FMSK(IJ).NE.VALUE) THEN
               FMSK(IJ)=1.E0
            ELSE
               FMSK(IJ)=0.E0
            ENDIF
         ENDDO
      ELSE
         DO IJ=1,NWDS
            FMSK(IJ)=1.E0
         ENDDO
      ENDIF

      IF (ITYPE.EQ.0) THEN
C
C     * COMPUTE GAUSSIAN WEIGHTS
C
         WRITE(6,'(A)')
     1        ' CALCULATE AREA WEIGHTS FOR GAUSSIAN GRID.'
         NLATG2=NLATG/2
         CALL GAUSSG(NLATG2,F,WT,SIA,RAD,WOCS)
         DO J=NLATG2+1,NLATG
            WT(J)=WT(NLATG-J+1)
         ENDDO
C
C        * FILL GRID WITH GAUSSIAN AREA WEIGHTS
C
         IJ=0
         DO J=J1,J2
            DO I=I1,I2+ICYCL
               IJ=IJ+1
               W(IJ)=WT(J)
            ENDDO
         ENDDO

      ELSE IF (ITYPE.EQ.1 .OR. ITYPE.EQ.2) THEN
C
C     * COMPUTE WEIGHTS FOR REGULAR LAT/LON GRID
C
         IF (ITYPE.EQ.1) THEN
            WRITE(6,'(A)')
     1   ' CALCULATE AREA WEIGHTS FOR LON/LAT GRID SPANNING THE POLES.'
            DELPHI=PI/FLOAT(NLATG-1)
            PHI=-0.5E0*(PI-DELPHI)
            NLATG2=NLATG/2+1
         ELSE
            WRITE(6,'(A)')
     1   ' CALCULATE AREA WEIGHTS FOR LON/LAT GRID EXCLUDING THE POLES.'
            DELPHI=PI/FLOAT(NLATG)
            PHI=-0.5E0*PI+DELPHI
            NLATG2=(NLATG+1)/2
         ENDIF

         SIN1=-1.E0
         SIN2=SIN(PHI)
         WT(1)=SIN2-SIN1
         DO J=2,NLATG2
            SIN1=SIN2
            PHI=PHI+DELPHI
            SIN2=SIN(PHI)
            WT(J)=SIN2-SIN1
         ENDDO
         DO J=NLATG2+1,NLATG
            WT(J)=WT(NLATG-J+1)
         ENDDO
C
C        * FILL GRID WITH LAT/LON AREA WEIGHTS
C
         IJ=0
         DO J=J1,J2
            DO I=I1,I2+ICYCL
               IJ=IJ+1
               W(IJ)=WT(J)
            ENDDO
         ENDDO

      ELSE IF (ITYPE.GE.3) THEN
         IF(ITYPE.EQ.3)THEN
           WRITE(6,'(A)')
     1          ' CALCULATE AREA WEIGHTS FOR 51X55 P.-S. GRID.'
           NLON=51
           NLAT=55
           XP=26.E0
           YP=28.E0
           D60=3.81E5
         ELSE
           WRITE(6,'(A)')
     1          ' CALCULATE AREA WEIGHTS FOR CUSTOM P.-S. GRID.'
           IF(LC.EQ.1)THEN
             CALL XYFLL(XC,YC,YP,-XP,D60,DGRW,NHEM)
             XP=FLOAT(NLON+1)/2.E0-XC
             YP=FLOAT(NLAT+1)/2.E0-YC
             WRITE(6,'(A,2F13.4)')' XP,YP=',XP,YP
           ENDIF
         ENDIF
         RE=(1.E0+SIN(PI/3.E0))*AE/D60
         RE2=RE**2 
         IJ=0
         DO J=J1,J2
            DO I=I1,I2
               IJ=IJ+1
               X2=(FLOAT(I)-XP)**2
               Y2=(FLOAT(J)-YP)**2
               W(IJ)=RE2/((RE2+X2+Y2)**2)
            ENDDO
         ENDDO
      ENDIF
C
C     * CALCULATE SUM OF ALL AREA WEIGHTS
C     * (EXCLUDING CYCLIC LONGITUDE)
C
      SW=0.E0
      SF=0.E0
      DO J=1,NLAT
         DO I=1,NLON-ICYCL
            IJ=(J-1)*NLON+I
            IF (FMSK(IJ).GT.0.5E0) THEN
               SW=SW+W(IJ)
               SF=SF+1.E0
            ENDIF
         ENDDO
      ENDDO
      
      IF (SW .EQ. 0.E0) THEN
        WRITE(6,'(A)') ' *** ERROR: NO VALID GRID BOXES. ***'
        CALL                                      XIT('MKWGHT',-10)
      ENDIF
C
C     * NORMALIZE WEIGHTS
C
      IF (INORM.EQ.1) THEN
        SW=SF/SW
      ELSE
        SW=1.E0/SW
      ENDIF
      DO IJ=1,NWDS
        IF (FMSK(IJ).GT.0.5E0) THEN
          W(IJ)=W(IJ)*SW
        ELSE
          W(IJ)=SPVAL
        ENDIF
      ENDDO
C
C     * TAKE SQUARE ROOT, IF NEEDED
C
      IF(ISQRT .EQ. 1) THEN
        DO IJ=1,NWDS
          IF (FMSK(IJ).GT.0.5E0) W(IJ)=SQRT(W(IJ))
        ENDDO
      ENDIF
C
C     * SAVE
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("WGHT"),0,
     1     NLON,NLAT,NHEM,1)
      CALL PUTFLD2(1,W,IBUF,MAXX)
      CALL PRTLAB(IBUF)
      CALL                                         XIT('MKWGHT',0)
 900  CALL                                         XIT('MKWGHT',-11)
 901  CALL                                         XIT('MKWGHT',-12)
C-------------------------------------------------------------------------
 5010 FORMAT (10X,5I5,I1,2I2,I5,3X,A2,2E10.0,I5)                                D4
 5015 FORMAT (10X,3E10.0,I5,E10.0,I5)                                           D4
 6010 FORMAT (' INPUT PARAMETERS:'/' NLATG=',I5,' I1,I2,J1,J2=',4I5,
     1     ' INORM=',I1,' ICYCL=',I2,' ISQRT=',I2,' ITYPE=',I5)
 6015 FORMAT (' INPUT PARAMETERS:'/' XP,YP,D60,LC=',3F13.4,I5)
 6016 FORMAT (' INPUT PARAMETERS:'/' DGRW,NHEM=',1F13.4,I5)
 6020 FORMAT (' LOP=',A2,' VALUE=',E16.5,' SPVAL=',1P1E12.5)
      END

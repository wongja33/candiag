      PROGRAM LOWRRES 
C     PROGRAM LOWRRES (LL,       HFLL,       INPUT,       OUTPUT,       )       D2
C    1           TAPE1=LL, TAPE2=HFLL, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------------------               D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     OCT 05/95 - F.MAJAESS (FIX MYSTERIOUS TYPO ERROR FOR DISPLAY PURPOSES)    
C     JUL 10/92 - E. CHAN  (REPLACE CALLS TO CVMGT WITH CONDITIONAL BLOCKS)     
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     JUL 03/90 - F.MAJAESS                                                     
C                                                                               D2
CLOWRRES - TRANSFORMS DATA ON A GRID TO HALF/THIRD THE ORIGINAL                 D1
C                                                GRID RESOLUTION.       1  1 C GD1
C                                                                               D3
CAUTHOR  - F.MAJAESS                                                            D3
C                                                                               D3
CPURPOSE - TRANSFORMS DATA ON A GRID TO A GRID WITH HALF/THIRD THE ORIGINAL     D3
C          GRID RESOLUTION (IN "LL" FILE ) BY APPLYING WEIGHTED AREA            D3
C          AVERAGING.                                                           D3
C          NOTE : USER MUST ENTER A VALID VALUE FOR "IUPOL" (-1 OR 1)           D3
C          ----   AGAINST WHICH THE PROGRAM CHECKS "IPOL" COMPUTED VALUE        D3
C                 BASED ON THE NUMBER OF LATITUDINAL GRID POINTS OF THE         D3
C                 FIELD READ.                                                   D3
C                 THE PROGRAM DOES NOT HANDLE DATA ON/OFF THE POLES WITH AN     D3
C                 EVEN/ODD NUMBER OF LATITUDINAL POINTS.                        D3
C                 (I.E. IF THE NUMBER OF LATITUDINAL POINTS IS ODD, THEN        D3
C                         THE DATA ON THE BOUNDARIES MUST SPAN THE POLES,       D3
C                       OTHERWISE                                               D3
C                         THE DATA ON THE BOUNDARIES MUST BE OFF THE POLES      D3
C                         AT A DISTANCE EQUAL TO HALF THE SPACING BETWEEN       D3
C                         2 INTERNAL LATITUDINAL GRID POINTS).                  D3
C                                                                               D3
C                  THE TOTAL NUMBER OF LATITUDINAL SEPARATIONS MUST BE A        D3
C                  MULTIPLE OF 4/6 AND THE TOTAL NUMBER OF LONGITUDINAL         D3
C                  SEPARATIONS MUST BE A MULTIPLE OF 2/6 IF THE EXPECTED        D3
C                  RESULTING NUMBER OF SEPARATIONS IS HALF/THIRD THE            D3
C                  ORIGINAL.                                                    D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      LL = STANDARD CCRN GRID FILE.                                            D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      HFLL = TRANSFORMED GRID VALUES ON A GRID WITH HALF/THIRD THE ORIGINAL    D3
C             GRID RESOLUTION.                                                  D3
C 
CINPUT PARAMETER... 
C                                                                               D5
C      IHFTHRD = 2, DATA AVERAGED TO HALF  THE ORIGINAL GRID RESOLUTION,(DEF)   D5
C              = 3, DATA AVERAGED TO THIRD THE ORIGINAL GRID RESOLUTION,        D5
C                   (ORIGINAL SEPARATION IS TRIPLED).                           D5
C      IUPOL   =-1, LATITUDINAL DATA SPANS  THE POLES                           D5
C              = 1, LATITUDINAL DATA IS OFF THE POLES                           D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* LOWRRES    3   -1                                                            D5
C                                                                               D5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/
     & H((INT((SIZES_BLONP1+1)/2.E0)-1)*INT((SIZES_BLAT+1)/2.E0)),
     & F(SIZES_BLONP1xBLAT)
      REAL HALAT(INT((SIZES_BLAT+1)/2.E0)),
     & HAREA(INT((SIZES_BLAT+1)/2.E0)),
     & FALAT(SIZES_BLAT),FAREA(SIZES_BLAT)
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
  
C     * READ IN THE AVERAGING SWITCH (DOUBLE OR TRIPLE THE ORIGINAL SPACING), 
C     *     AND THE POLES     SWITCH (DATA ON/OFF THE POLES)
C 
      READ(5,5010,END=900) IHFTHRD,IUPOL                                        D4 
      IF((IHFTHRD.LT.2).OR.(IHFTHRD.GT.3)) IHFTHRD=2
      WRITE(6,6000) IHFTHRD, IUPOL
      IF(ABS(IUPOL).NE.1) CALL                     XIT('LOWRRES',-1)
  
C     * SET THE REQUIRED NUMBER OF WHICH THE NUMBER OF SPACING MUST BE
C     * A MULTIPLE. 
  
      IF(IHFTHRD.EQ.3)THEN
        AMLTPLJ=6.0E0 
        AMLTPLI=6.0E0 
      ELSE
        AMLTPLJ=4.0E0 
        AMLTPLI=2.0E0 
      ENDIF 
  
C     * INITIALIZE VARIABLES... 
  
      NR=0
      IFLON=-1
      JFLAT=-1
  
C     * GET THE REQUESTED FIELD FROM FILE "LL" (UNIT #1). 
  
   10 CALL GETFLD2(1,F,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK)  THEN
          IF (NR.EQ.0)  CALL                       XIT('LOWRRES',-2)
          WRITE(6,6020) NR
          CALL                                     XIT('LOWRRES',0) 
      ENDIF 
      IF(NR.EQ.0)  WRITE(6,6030) IBUF 
C 
C     * SKIP AREAS COMPUTATION BY LATITUDINAL SECTORS IF DIMENSIONS 
C     * FOR THE RECORD JUST READ IN ARE UNCHANGED FROM THOSE OF THE 
C     * PREVIOUSLY PROCESSED RECORD.
C 
      IF((IBUF(5).EQ.IFLON).AND.(IBUF(6).EQ.JFLAT)) GO TO 50
  
C 
C     * FOR THE ORIGINAL GRID * 
C 
C     * COMPUTE DIMENSIONS... 
C 
      IFLON=IBUF(5) 
      JFLAT=IBUF(6) 
      NFWDS=IFLON*JFLAT 
C 
C     * SET THE BOUNDARY DATA FLAG (ON/OFF THE POLE(S)) 
C     * IPOL SET EQUAL TO -1/1 IF DATA IS ON/OFF THE POLES WITH AN ODD/EVEN 
C     * NUMBER OF GRID LATITUDINAL POINTS.
C 
      IF(MOD(FLOAT(JFLAT),2.E0).EQ.0.E0) THEN
        IPOL=1
      ELSE
        IPOL=-1 
      ENDIF 
C 
      IF(IUPOL.NE.IPOL) CALL                       XIT('LOWRRES',-3)
C 
C     * ABORT IF THE NUMBER OF LATITUDINAL GRID SPACING IS NOT A MULTIPLE 
C     * OF "AMLTPLJ". 
C 
      IF(((IPOL.EQ.1).AND.
     1    ((MOD(FLOAT(JFLAT  ),AMLTPLJ).NE.0.E0).OR. 
     2     (FLOAT(JFLAT  ).LT.AMLTPLJ)))
     3  .OR.
     4   ((IPOL.EQ.-1).AND. 
     5    ((MOD(FLOAT(JFLAT-1),AMLTPLJ).NE.0.E0).OR. 
     6     (FLOAT(JFLAT-1).LT.AMLTPLJ)))) 
     7   CALL                                      XIT('LOWRRES',-4)
C 
C     * ABORT IF THE NUMBER OF LONGITUDINAL GRID SPACING IS NOT A MULTIPLE
C     * OF "AMLTPLI". 
C 
      IF((MOD(FLOAT(IFLON-1),AMLTPLI).NE.0.E0).OR. 
     1   (FLOAT(IFLON-1).LT.AMLTPLI)) CALL         XIT('LOWRRES',-5)
C 
C     * COMPUTE NORMALIZED AREAS BY LATITUDINAL SECTORS.
C 
      CALL CMPAREA(FAREA,FALAT,JFLAT,IPOL,OK) 
      IF(.NOT.OK) CALL                             XIT('LOWRRES',-6)
C 
C     * COMPUTE NORMALIZED GRID AREAS BY (LAT,LON) FROM AREAS BY
C     * LATITUDINAL SECTORS.
C 
      DO 25 J=1,JFLAT 
        FAREA(J)=FAREA(J)/FLOAT(IFLON-1)
  25  CONTINUE
C 
C     * FOR THE RESULTANT GRID *
C 
C     * COMPUTE NEW DIMENSIONS
C 
      IHLON=((IFLON-1)/IHFTHRD)+1 
      JHLAT=((JFLAT-1)/IHFTHRD)+1 
      NHWDS=IHLON*JHLAT 
  
      IF(NR.EQ.0) WRITE(6,6050) IFLON,JFLAT,NFWDS,IHLON,JHLAT,NHWDS 
C 
C     * COMPUTE NORMALIZED AREAS BY LATITUDINAL SECTORS.
C 
      CALL CMPAREA(HAREA,HALAT,JHLAT,IPOL,OK) 
      IF(.NOT.OK) CALL                             XIT('LOWRRES',-7)
C 
C     * COMPUTE NORMALIZED GRID AREAS BY (LAT,LON) FROM AREAS BY
C     * LATITUDINAL SECTORS.
C 
      DO 40 J=1,JHLAT 
        HAREA(J)=HAREA(J)/FLOAT(IHLON-1)
  40  CONTINUE
C 
C     * COMPUTE GLOBAL SUM FROM ORIGINAL GRID VALUES NORMALIZED BY THE
C     * REPRESENTED AREA. 
C 
  50  SUMF=0.0E0
      DO 70 J=1,JFLAT 
        JB=(J-1)*IFLON
        DO 60 I=1,IFLON-1 
          SUMF=SUMF+FAREA(J)*F(JB+I)
  60    CONTINUE
  70  CONTINUE
C 
C     * DO THE AREA AVERAGING BY LONGITUDE WITHIN LATITUDE... 
C 
  
      DO 400 JHF=1,JHLAT
        JFL=(JHF-1)*IHFTHRD+1 
        IF((IPOL.EQ.1).AND.(IHFTHRD.EQ.3)) THEN 
          IF (JFL.GE.(JFLAT-2)) THEN
            JFLM1=JFLAT
          ELSE
            JFLM1=JFL+2
          ENDIF
        ELSE
          IF (JFL.LE.IHFTHRD) THEN
            JFLM1=1
          ELSE 
            JFLM1=JFL-1
          ENDIF 
        ENDIF 
        IF (JFL.GE.(JFLAT-1)) THEN
          JFLP1=JFLAT
        ELSE 
          JFLP1=JFL+1
        ENDIF 
C 
        AJFL  =FAREA(JFL) 
        AJFLM1=FAREA(JFLM1) 
        AJFLP1=FAREA(JFLP1) 
C 
        JHBASE=(JHF  -1)*IHLON
        JFLB  =(JFL  -1)*IFLON
        JFLM1B=(JFLM1-1)*IFLON
        JFLP1B=(JFLP1-1)*IFLON
C 
        ASUM=0.0E0
  
C 
C       * DO THE AREAS' AVERAGING BY LONGITUDE... 
C 
        DO 300 IHF=1,IHLON
          IFL=(IHF-1)*IHFTHRD+1 
          IF (IFL.EQ.1) THEN
            IFLM1=IFLON-1
          ELSE
            IFLM1=IFL-1
          ENDIF
          IF (IFL.EQ.IFLON) THEN
            IFLP1=2
          ELSE
            IFLP1=IFL+1
          ENDIF
C 
C         * DO THE AREAS' AVERAGING FOR A PARTICULAR DATA POINT 
C 
          IF((.NOT.((IPOL.EQ.1).AND.(IHFTHRD.EQ.3))).AND. 
     1       ((JHF.EQ.1).OR.(JHF.EQ.JHLAT).OR.
     2        ((IPOL.EQ.1).AND.(IHFTHRD.EQ.2)))) THEN 
C 
C         * PERFORM SIX POINTS WEIGHTED AREA AVERAGING. 
C 
            IF((JHF.EQ.1).OR.((IPOL.EQ.1).AND.(IHFTHRD.EQ.2))) THEN 
              IF(IHFTHRD.EQ.3) THEN 
                H(JHBASE+IHF)=
     1   (AJFL  *(F(JFLB  +IFLM1)+F(JFLB  +IFL)+F(JFLB  +IFLP1))+ 
     2    AJFLP1*(F(JFLP1B+IFLM1)+F(JFLP1B+IFL)+F(JFLP1B+IFLP1)))/
     3           (3.0E0*(AJFL+AJFLP1))
              ELSE
                IF(IPOL.EQ.1)THEN 
                  H(JHBASE+IHF)=0.5E0*
     1 (AJFL  *(F(JFLB  +IFLM1)+2.E0*F(JFLB  +IFL)+F(JFLB  +IFLP1))+
     2  AJFLP1*(F(JFLP1B+IFLM1)+2.E0*F(JFLP1B+IFL)+F(JFLP1B+IFLP1)))/ 
     3         (2.0E0*(AJFL+AJFLP1))
                ELSE
                  H(JHBASE+IHF)=0.5E0*
     &              (AJFL  *(F(JFLB  +IFLM1)+
     &              2.E0*F(JFLB  +IFL)+F(JFLB  +IFLP1))+ 
     &              0.5E0*AJFLP1*(F(JFLP1B+IFLM1)+
     &              2.E0*F(JFLP1B+IFL)+F(JFLP1B+IFLP1)))/
     &              (2.0E0*AJFL+AJFLP1)
                ENDIF 
              ENDIF 
            ELSE
              IF(IHFTHRD.EQ.3) THEN 
                H(JHBASE+IHF)=
     1   (AJFL  *(F(JFLB  +IFLM1)+F(JFLB  +IFL)+F(JFLB  +IFLP1))+ 
     2    AJFLM1*(F(JFLM1B+IFLM1)+F(JFLM1B+IFL)+F(JFLM1B+IFLP1)))/
     3           (3.0E0*(AJFL+AJFLM1))
              ELSE
                IF(IPOL.EQ.1)THEN 
                  H(JHBASE+IHF)=0.5E0*
     1 (AJFL  *(F(JFLB  +IFLM1)+2.E0*F(JFLB  +IFL)+F(JFLB  +IFLP1))+
     2  AJFLM1*(F(JFLM1B+IFLM1)+2.E0*F(JFLM1B+IFL)+F(JFLM1B+IFLP1)))/ 
     3         (2.0E0*(AJFL+AJFLM1))
                ELSE
                  H(JHBASE+IHF)=0.5E0*
     &              (AJFL  *(F(JFLB  +IFLM1)+
     &              2.E0*F(JFLB  +IFL)+F(JFLB  +IFLP1))+ 
     &              0.5E0*AJFLM1*(F(JFLM1B+IFLM1)+
     &              2.E0*F(JFLM1B+IFL)+F(JFLM1B+IFLP1)))/
     &              (2.0E0*AJFL+AJFLM1)
                ENDIF 
              ENDIF 
            ENDIF 
            ASUM=ASUM+H(JHBASE+IHF) 
          ELSE
C 
C           * PERFORM NINE POINTS WEIGHTED AREA AVERAGING.
C 
            IF(IHFTHRD.EQ.3) THEN 
              H(JHBASE+IHF)=
     1 (AJFLM1*(F(JFLM1B+IFLM1)+F(JFLM1B+IFL)+F(JFLM1B+IFLP1))
     2 +AJFL  *(F(JFLB  +IFLM1)+F(JFLB  +IFL)+F(JFLB  +IFLP1))
     3 +AJFLP1*(F(JFLP1B+IFLM1)+F(JFLP1B+IFL)+F(JFLP1B+IFLP1))) 
     4                /(3.E0*(AJFLM1+AJFL+AJFLP1))
            ELSE
              H(JHBASE+IHF)=
     &          (0.25E0*AJFLM1*(F(JFLM1B+IFLM1)+
     &          2.E0*F(JFLM1B+IFL)+F(JFLM1B+IFLP1))
     &          +0.5E0 *AJFL  *(F(JFLB  +IFLM1)+
     &          2.E0*F(JFLB  +IFL)+F(JFLB  +IFLP1))
     &          +0.25E0*AJFLP1*(F(JFLP1B+IFLM1)+
     &          2.E0*F(JFLP1B+IFL)+F(JFLP1B+IFLP1))) 
     &           /(AJFLM1+2.0E0*AJFL+AJFLP1) 
            ENDIF 
          ENDIF 
 300    CONTINUE
        IF((IPOL.EQ.-1).AND.((JHF.EQ.1).OR.(JHF.EQ.JHLAT)))THEN 
          ASUM=ASUM-H(JHBASE+IHLON) 
          AVG=ASUM/(IHLON-1.E0) 
          DO 350 IHF=1,IHLON
            H(JHBASE+IHF)=AVG 
 350      CONTINUE
        ENDIF 
 400  CONTINUE
  
C 
C     * COMPUTE GLOBAL SUM FROM RESULTANT GRID VALUES NORMALIZED BY THE 
C     * REPRESENTED AREA. 
C 
      SUMH=0.0E0
      DO 570 J=1,JHLAT
        JB=(J-1)*IHLON
        DO 560 I=1,IHLON-1
          SUMH=SUMH+HAREA(J)*H(JB+I)
 560    CONTINUE
 570  CONTINUE
C 
C     * COMPARE GLOBAL NORMALIZED SUMS... 
C 
      DSUMFH=SUMF-SUMH
      ARATIO=100.0E0*(SUMH/SUMF)
  
C     * SAVE THE SMOOTHED FIELD AND WRITE OUT GLOBAL NORMALIZED SUMS... 
  
      IBUF(5)=IHLON 
      IBUF(6)=JHLAT 
      CALL PUTFLD2(2,H,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6030) IBUF
      WRITE(6,6060) SUMF,SUMH,DSUMFH,ARATIO 
      NR=NR+1 
      GO TO 10
  
C     * E.O.F. ON INPUT.
  
  900 CALL                                         XIT('LOWRRES',-8)
C---------------------------------------------------------------------
 5010 FORMAT(10X,2I5)                                                           D4
 6000 FORMAT('0CARD READ - IHFTHRD= ',I5,', IUPOL= ',I5)
 6020 FORMAT('0PROGRAM PROCESSED ',I5,' RECORDS')
 6030 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6050 FORMAT(' IFLON,JFLAT,NFWDS,IHLON,JHLAT,NHWDS = ',7I7)
 6060 FORMAT(' SUM BEFORE AVERAGING = ',E16.8,', AFTER = ',E18.6,
     1       ', DIFFERENCE OF (B-A) = ',E16.8,', RATIO = ',F8.4,'%')
      END

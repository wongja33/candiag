      PROGRAM LLSSTA
C     PROGRAM LLSSTA (LLPHYS,       INPUT,       OUTPUT,                )       D2
C    1          TAPE1=LLPHYS, TAPE5=INPUT, TAPE6=OUTPUT)
C     --------------------------------------------------                        D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS(ALLOW READING PACKING DENSITY FROM INPUT CARD)      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 15/83 - R.LAPRISE.                                                    
C                                                                               D2
CLLSSTA  - GENERATES A LAT-LON FIELD OF SEA-SURFACE TEMP. ANOMALIES     0  1 C  D1
C                                                                               D3
CAUTHOR  - R.LAPRISE                                                            D3
C                                                                               D3
CPURPOSE - GENERATES A LAT-LON FIELD OF SEA-SURFACE TEMPERATURE ANOMALIES       D3
C          TO BE ADDED TO CLIMATOLOGICAL SST ON STANDARD LLPHYS FILE.           D3
C          THE ANOMALY IS OF THE FORM AMP*SIN2(X-XO/2DX)*SIN2(Y-YO/*2DY)        D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      LLPHYS = THE GENERATED LAT-LON FIELD OF SEA-SURFACE TEMPERATURE          D3
C               ANOMALIES                                                       D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      AMP   = AMPLITUDE OF BUMP                                                D5
C      RX,RY = HALF WAVELENGTH  (DEG.)                                          D5
C      XO,YO = MAXIMUM LOCATED AT LON,LAT                                       D5
C      NPACK = PACKING DENSITY TO USE (0 DEFAULT TO 4)                          D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* LLSSTA     "AMP"    "DELX"    "DELY"      "X0"      "Y0"                     D5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON /BUF/ IBUF(8),DATA(SIZES_LONP1xLATxNWORDIO) 
C 
      COMMON/BLANCK/ GR(73,46)
C 
      DATA LON1/73/, LAT1/46/, 
     & MAXX/SIZES_LONP1xLATxNWORDIO/, PI/3.1415926E0/ 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
C 
C     * READ IN SPECIFICATIONS FOR THIS EXPERIMENT. 
C 
      READ(5,5000,END=901)AMP,RX,RY,XO,YO,NPACK                                 D4
      IF(NPACK.EQ.0) NPACK=4
      WRITE(6,6020)AMP,RX,RY,XO,YO
      YO=YO+90.E0 
C 
C     * NUMBER OF LONG,LAT INTERVALS. 
C     * BOTH SOUTH AND NORTH POLES ROWS ARE PRESENT IN GR.
C 
      LON=LON1-1
      LAT=LAT1-1
      DLON=360.E0/LON 
      DLON=1.E-10*ANINT(1.E10*DLON) 
      DLAT=180.E0/LAT 
      DLAT=1.E-10*ANINT(1.E10*DLAT) 
C 
C     * REGION COVERED BY ONE WAVELENGTH ON LON*LAT GRID. 
C     * ROUND UP/DOWN ON LEFT/RIGHT SIDE. 
C 
      IW=INT((XO-RX)/DLON)+2
      IE=INT((XO+RX)/DLON)+1
      JS=INT((YO-RY)/DLAT)+2
      JN=INT((YO+RY)/DLAT)+1
C 
C     * INITIALIZE GRID TO 0. 
C 
      DO 100 I=1,LON1 
      DO 100 J=1,LAT1 
  100 GR(I,J)=0.E0
C 
C     * INSERT THE BUMP.
C     * SP AND NP ROWS LEFT UNALTERED.
C 
      DO 400 J=JS,JN
      IF(J.LT.2 .OR. J.GT.LAT) GO TO 400
      COSY=COS(   PI*((J-1.E0)*DLAT-YO    )/(2.E0*RY)) **2
C 
      DO 300 II=IW,IE 
      I=II
      IF(I.GT.LON) I=I-LON
      IF(I.LT.0)   I=I+LON
      IF(I.LT.1 .OR. I.GT.LON) GO TO 300
      GR(I,J)=AMP*COSY*COS(   PI*((I-1.E0)*DLON-XO)/(2.E0*RX)) **2
  300 CONTINUE
C 
C     * ENSURE E-W PERIODICITY. 
C 
      GR(LON1,J)=GR(1,J)
  400 CONTINUE
C 
C     * WRITE OUT THE FIELD.
C 
      CALL SETLAB(IBUF,NC4TO8("GRID"),0,NC4TO8("SSTA"),1,
     +                                 LON1,LAT1,0,NPACK)
      WRITE(6,6010) IBUF
      CALL PUTFLD2(1,GR,IBUF,MAXX) 
C 
      CALL                                         XIT('LLSSTA',0)
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('LLSSTA',-1) 
C---------------------------------------------------------------- 
 5000 FORMAT(10X,5E10.0,I5)                                                     D4
 6010 FORMAT(1X,A4,I10,2X,A4,I10,4I6)
 6020 FORMAT(' AMPLITUDE OF BUMP=',F10.3,/,
     1       ' HALF WAVELENGTH  (DEG.)=',2F10.3,/,
     2       ' MAXIMUM LOCATED AT LON,LAT=',2F10.3)
      END

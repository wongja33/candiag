      PROGRAM LLAFR 
C     PROGRAM LLAFR (LL,       FC,       INPUT,       OUTPUT,           )       D2
C    1         TAPE1=LL, TAPE2=FC, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------                   D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 12/83 - R.LAPRISE.                                                    
C     FEB 20/80 - J.D.HENDERSON 
C                                                                               D2
CLLAFR   - FOURIER ANALYSIS OF GRID FILE                                1  1 C  D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - CONVERTS A FILE OF GAUSSIAN OR LAT-LONG GRIDS TO FOURIER             D3
C          COEFFICIENTS AT EACH LATITUDE.                                       D3
C          NOTE - GRID ROW LENGTH SHOULD BE (2**N) OR 3*(2**N).                 D3
C                 ANALYSIS IS LATITUDE-BY-LATITUDE TO A RESOLUTION              D3
C                 SPECIFIED ON A CARD.                                          D3
C                 MAXIMUM GRID OR FOURIER ARRAY SIZE IS "IJ" WORDS.             D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      LL = GAUSSIAN OR LAT-LONG GRIDS.                                         D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      FC = LATITUDINAL FOURIER ANALYSIS OF THE GRIDS IN LL.                    D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      MAXF = MAXIMUM WAVE NUMBER USED IN THE FOURIER ANALYSIS.                 D5
C             EACH LATITUDE IN THE OUTPUT FILE WILL HAVE MAXF+1                 D5
C             COMPLEX NUMBERS.                                                  D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C*   LLAFR   20                                                                 D5
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      COMMON/BLANCK/GG(SIZES_BLONP1xBLAT),
     & FC(2*(SIZES_LMTP1+1)*SIZES_LAT) 
C 
      LOGICAL OK
      REAL WRKS(3*(SIZES_BLONP1+3))
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
C 
      DATA MAXX,MAXG/SIZES_BLONP1xBLATxNWORDIO,SIZES_BLONP1xBLAT/
C-------------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE MAXIMUM WAVENUMBER.
C 
      READ(5,5010,END=905) MAXF                                                 D4
      WRITE(6,6005) MAXF
C 
C     * GET THE NEXT GRID FROM FILE 1.
C 
      NR=0
  140 CALL GETFLD2(1,GG,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('LLAFR',-1)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('LLAFR',0) 
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
      NLG1=IBUF(5)
      NLAT=IBUF(6)
      NLG=NLG1-1
C 
      IF(MAXF.GT.NLG/2) CALL                       XIT('LLAFR',-2)
      IF(NLG1*NLAT.GT.MAXG) CALL                   XIT('LLAFR',-3)
      IF(2*MAXF*NLAT.GT.MAXG) CALL                 XIT('LLAFR',-4)
C 
C     * CALCULATE FOURIER COEFF FOR EACH LATITUDE IN THE GRID.
C 
      CALL FFWFG2(FC,MAXF+1,GG,NLG1,MAXF,NLG,WRKS,NLAT) 
C 
C     * SAVE ARRAY OF FOURIER COEFF ON FILE 2.
C 
      LFR=(MAXF+1)
      CALL SETLAB(IBUF,NC4TO8("FOUR"),-1,-1,-1,LFR,NLAT,-1,1)
      CALL PUTFLD2(2,FC,IBUF,MAXX) 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 140 
C 
C     * E.O.F. ON INPUT.
C 
  905 CALL                                         XIT('LLAFR',-5)
C-------------------------------------------------------------------- 
 5010 FORMAT(10X,I5)                                                            D4
 6005 FORMAT('0FOURIER ANALYSIS TO WAVENUMBER',I5)
 6007 FORMAT('0NLG1,NLAT,NWDS=',3I6)
 6010 FORMAT('0',I6,' GRIDS CONVERTED TO FOURIER COEFF')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END

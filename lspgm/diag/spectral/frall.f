      PROGRAM FRALL
C     PROGRAM FRALL (FC,       LL,       INPUT,       OUTPUT,           )       E2
C    1         TAPE1=FC, TAPE2=LL, TAPE5=INPUT, TAPE6=OUTPUT)
C     -------------------------------------------------------                   E2
C                                                                               E2
C     MAR 17/04 - S.KHARIN (USE FFGFW AS IN COFAGG INSTEAD OF FFGFW2)           E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     MAY 11/83 - R.LAPRISE.
C     MAR 25/80 - J.D.HENDERSON
C                                                                               E2
CFRALL   - CONVERT FOURIER FILE TO GRID FILE                            1  1 C  E1
C                                                                               E3
CAUTHOR  - J.D.HENDERSON                                                        E3
C                                                                               E3
CPURPOSE - CONVERTS A FILE OF LATITUDINAL FOURIER COEFFICIENTS                  E3
C          TO LAT-LONG GRIDS.                                                   E3
C                                                                               E3
CINPUT FILES...                                                                 E3
C                                                                               E3
C      FC = INPUT FILE OF LATITUDINAL FOURIER COEFICIENTS                       E3
C           (I.E. ONE FOURIER ANALYSIS FOR EACH LATITUDE FOR                    E3
C                 EACH FIELD IN THE FILE)                                       E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      LL = OUTPUT FILE OF LAT-LONG GRIDS                                       E3
C
CINPUT PARAMETERS...
C                                                                               E5
C      NLG = NUMBER OF GRID POINTS IN A LATITUDE CIRCLE                         E5
C            OF THE LAT-LONG GRID                                               E5
C                                                                               E5
CEXAMPLE OF INPUT CARD...                                                       E5
C                                                                               E5
C*   FRALL   96                                                                 E5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LONP2,
     &                       SIZES_MAXLONP1LAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GG(SIZES_LONP1xLAT),FC(SIZES_LONP1xLAT)
C
      LOGICAL OK
      INTEGER IFAX(10)
      REAL WRKS(64*SIZES_LONP2),WRKL(SIZES_MAXLONP1LAT),
     & TRIGS(SIZES_MAXLONP1LAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C
      DATA MAXX,MAXG/SIZES_LONP1xLATxNWORDIO,SIZES_LONP1xLAT/
C--------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ THE NUMBER OF LONGITUDE POINTS.
C
      READ(5,5010,END=905) NLG                                                  E4
      WRITE(6,6005) NLG
      IF(MOD(NLG,2).NE.0)CALL                      XIT('FRALL',-1)

      CALL FTSETUP(TRIGS,IFAX,NLG)
C
C     * GET THE NEXT FOURIER COEFF.
C
      NR=0
  140 CALL GETFLD2(1,FC,NC4TO8("FOUR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0)THEN
          CALL                                     XIT('FRALL',-2)
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('FRALL',0)
        ENDIF
      ENDIF
      IF(NR.EQ.0) THEN
        CALL PRTLAB(IBUF)
        NPACK=MIN(4,IBUF(8))
      ENDIF
C
      NLAT=IBUF(6)
      NFR2=IBUF(5)*2
      MAXF=IBUF(5)-1
      NLG1=NLG+1
      NLG2=NLG/2
C
C     * NLG MUST BE BIG ENOUGH TO RESOLVE ALL HARMONICS
C
      IF(MAXF.GT.NLG2) CALL                        XIT('FRALL',-3)
C
C     * THE DIMENSIONS MUST NOT EXCEED THE DECLARED ARRAY DIMENSIONS
C
      IF(NLG1*NLAT.GT.MAXG) CALL                   XIT('FRALL',-4)
C
C     * COPY WAVES FROM FC TO WRKL
C
      DO J=1,NLAT
        JR=(J-1)*NFR2
        DO I=1,NFR2
          WRKL(I)=FC(JR+I)
        ENDDO
        DO I=NFR2+1,NLG+2
          WRKL(I)=0.
        ENDDO
C
C     * CALCULATE GRID VALUES FOR EACH LATITUDE.
C
        CALL FFGFW(WRKL,NLG+2,WRKL,NLG2+1,MAXF,NLG,WRKS,1,IFAX,TRIGS) 
C
C     * COPY WRKL TO GG AND ADD CYCLIC MERIDIAN
C
        JR=(J-1)*NLG1
        DO I=1,NLG
          GG(JR+I)=WRKL(I)
        ENDDO
        GG(JR+NLG1)=GG(JR+1)
      ENDDO
C
C     * SAVE THE GRID ON FILE 2.
C
      CALL SETLAB(IBUF,NC4TO8("GRID"),-1,-1,-1,NLG1,NLAT,-1,NPACK)
      CALL PUTFLD2(2,GG,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
      NR=NR+1
      GO TO 140
C
C     * E.O.F. ON INPUT.
C
  905 CALL                                         XIT('FRALL',-5)
C--------------------------------------------------------------------
 5010 FORMAT(10X,I5)                                                            E4
 6005 FORMAT('0LONGITUDES=',I5)
 6010 FORMAT('0',I6,' FOURIER COEFF CONVERTED TO GRIDS')
      END

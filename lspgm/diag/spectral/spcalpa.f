      PROGRAM SPCALPA 
C     PROGRAM SPCALPA (IN,       OUT,       OUTPUT,                     )       E2
C    1           TAPE1=IN, TAPE2=OUT, TAPE6=OUTPUT) 
C     ---------------------------------------------                             E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C     JAN 08/81 - J.D.HENSERSON 
C     JUN 01/79 - TED SHEPHERD. 
C                                                                               E2
CSPCALPA - MAKES FILE OF CMPLX(N*(N+1),0.)                              1  1   GE1
C                                                                               E3
CAUTHOR  - J.D.HENDERSON, T.SHEPHERD                                            E3
C                                                                               E3
CPURPOSE - PRODUCES A COMPLEX FILE (OUT) OF THE FACTOR C-ALPHA=N(N+1);          E3
C          (CMPLX(N*(N+1),0.)), WHERE N IS THE LEGENDRE POLYNOMIAL              E3
C          COEFFICIENT.                                                         E3
C          NOTE - ONE RECORD IS PRODUCED FOR EACH RECORD IN THE INPUT           E3
C                 FILE (IN).                                                    E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      IN = FILE OF COMPLEX COEFFICIENTS.                                       E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      OUT = COMPLEX FILE CONTAINING THE REQUIRED VALUES FOR EACH               E3
C            RECORD OF THE INPUT FILE (IN).                                     E3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXC = MAX((SIZES_LMTP1+1)*SIZES_LAT,(SIZES_LA+SIZES_LMTP1))
      integer, parameter ::
     & MAXX = 2*MAXC*SIZES_NWORDIO ! defined as a max buffer size for I/O 

      COMMON/BLANCK/G(2*MAXC)
C 
      LOGICAL OK
      INTEGER LSR(2,SIZES_LMTP1+1)
      COMMON/ICOM/IBUF(8),F(MAXX)
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ RECORDS FROM INPUT FILE TO GIVE FILE OUT THE SAME DIMENSION
C 
      NR=0
  100 CALL RECGET(1,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('SPCALPA',-1)
        WRITE(6,6020) NR
        CALL                                       XIT('SPCALPA',0) 
      ENDIF 
C 
C     * COMPUTE THE ARRAY C-ALPHA(N,M) - ONLY IF NR=0.
C 
      IF(NR.NE.0) GO TO 225 
      LRLMT=IBUF(7) 
      IF(LRLMT.LT.100) CALL FXLRLMT (LRLMT,IBUF(5),IBUF(6),0)
      CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
      CALL CALPHA(G,LSR,LM,LA)
C 
C     * SAVE ON FILE OUT. 
C 
  225 IBUF(3)=NC4TO8("CALP")
      CALL PUTFLD2(2,G,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB (IBUF)
      NR=NR+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6020 FORMAT('0SPCALPA PRODUCED',I6,' RECORDS')
      END

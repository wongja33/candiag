      PROGRAM LPPRINT
C     PROGRAM LPPRINT (INLP,       INPUT,       OUTPUT,                 )       A2
C    1           TAPE1=INLP, TAPE5=INPUT, TAPE6=OUTPUT)
C     -------------------------------------------------                         A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JUL 12/93 - C. WILLISTON (ADD OPTION OF PRINTING AN OFFSET LAT-LON GRID)  
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8 AND        
C                           CONVERT LABEL TO CHARACTER DATA)                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     APR 25/85 - M.LAZARE.
C     JUN 22/84 - M.LAZARE.
C                                                                               A2
CLPPRINT - PRINTS VALUES OF ANY SIZE (MAX 361X180) GRID                 1  0 C  A1
C                                                                               A3
CAUTHOR  - M.LAZARE                                                             A3
C                                                                               A3
CPURPOSE - PRINTS VALUES FROM INLP FILE, ONE RECORD AT A TIME.                  A3
C          THE PRINTOUT IS CLASSIFIED BY LATITUDE (AND POSSIBLY                 A3
C          LONGITUDE), FOR EASY READING.                                        A3
C          NOTE - THE PROGRAM DOES NOT CHECK FOR NAME, LEVEL OR                 A3
C                 TIMESTEP NUMBER.                                              A3
C                 THE MAXIMUM SIZE ALLOWABLE CORRESPONDS TO GRID OF             A3
C                 1X1 DEGREE RESOLUTION.                                        A3
C                 THE PROGRAM WILL NOT ABORT; IT WILL TERMINATE WITH            A3
C                 A MESSAGE IF:                                                 A3
C                   1) - THERE IS A PROBLEM IN OBTAINING THE RECORD.            A3
C                   2) - AN ATTEMPT IS MADE TO OPERATE ON A SPECTRAL            A3
C                        FILE.                                                  A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      INLP = LAT-LONG, GAUSSIAN GRID, ZONAL FIELDS WITH EQUALLY-SPACED         A3
C             LATITUDES, OR ZONAL FIELDS WITH GAUSSIAN LATITUDES FILE.          A3
C
CINPUT PARAMETERS...
C                                                                               A5
C      NTYP  = DETERMINES THE TYPE OF GRID. 0 DEFAULTS TO A GAUSSIAN            A5
C              GRID, 1 TO A LAT-LONG GRID, AND 2 TO A SHIFTED LAT-LONG GRID.    A5
C      SCALE = REAL NUMBER BY WHICH THE VALUES ARE TO BE MULTIPLIED             A5
C              BEFORE BEING PRINTED IN A F9.2 FORMAT. (DEFAULTED TO 1.).        A5
C      LABEL = 80 CHARACTER LABEL PRINTED BELOW LISTING OF DATA.                A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*LPPRINT.    0       1.0                                                       A5
C* LLPHYS=IPHS1LP.  GROUND TEMPERATURE FOR DECEMBER. UNITS DEC C.               A5
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      CHARACTER*80 LABEL
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT),RAD(SIZES_BLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/FIELD/A(SIZES_BLONP1xBLAT)
      COMMON/GRIDS/RLONG(SIZES_BLONP1),RLAT(SIZES_BLAT)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C-------------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
      NR=0
C
C     * READ THE INPUT CARDS.
C
  150 READ(5,5010,END=900) NTYP,SCALE                                           A4
      IF(SCALE.EQ.0.E0) SCALE=1.E0
      WRITE(6,6005) NTYP,SCALE
      READ(5,5020) LABEL                                                        A4
C
C     * FIND THE FILE LABEL FROM IBUF.
C     * TERMINATE IF RECORD NOT FOUND OR IF FIELD READ IN IS SPECTRAL.
C
      CALL GETFLD2(1,A,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6000)
        CALL                                       XIT('LPPRINT',-101)
      ENDIF
      IF(IBUF(1).EQ.NC4TO8("SPEC"))THEN
        WRITE(6,6010)
        CALL                                       XIT('LPPRINT',-102)
      ENDIF
      IF(IBUF(1).EQ.NC4TO8("GRID")) ILAT=IBUF(6)
      IF(IBUF(1).EQ.NC4TO8("ZONL")) ILAT=IBUF(5)
C
C     * SCALE THE FIELD.
C
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS
  210 A(I)=A(I)*SCALE
C
C     * DEFINE THE LONGITUDES (IF GRID FIELD).
C
      IF(IBUF(1).EQ.NC4TO8("GRID")) THEN
        ILONG=IBUF(5)
        IF(NTYP.NE.2) THEN
C
C         * EQUALLY-SPACED LONGITUDES CASE.
C
          NLONG=ILONG-2
          RX=360.E0/(ILONG-1)
          RLONG(1)=0.0E0
          RLONG(ILONG)=0.0E0
        ELSE
C    
C         * OFF-SET GRID CASE.
C
          NLONG=ILONG-1
          RX=360.E0/ILONG
          RLONG(1)=RX*0.5E0
        ENDIF
C
        DO 1 I=1,NLONG
          RLONG(I+1)=RLONG(I)+RX
    1   CONTINUE
      ENDIF
      IF(NTYP.NE.0) THEN
C
C     * NO GAUSSIAN LATITUDES.
C     * CALCULATE EQUALLY-SPACED LATITUDES.
C     * THESE MAYBE OFFSET FROM SOUTH-POLE OR NOT.
C
        NLAT=ILAT-1
        IF(NTYP.EQ.2) THEN
          RY=180.E0/ILAT
          RLAT(1)=-90.0E0+RY*0.5E0
        ELSE
          RY=180.E0/NLAT
          RLAT(1)=-90.0E0
        ENDIF
        DO 2 J=1,NLAT
          RLAT(J+1)=RLAT(J)+RY
    2   CONTINUE
      ELSE
C
C     * CALCULATE GAUSSIAN LATITUDES.
C
         ILATH=ILAT/2
         CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)
         CALL TRIGL (ILATH,SL,WL,CL,RAD,WOSSL)
         DO 220 I=1,ILAT
  220    RLAT(I)=RAD(I)*180.E0/3.14159E0
      ENDIF
C
C     * PRINT OUT THE VALUES OF A.
C     * GRID CASE.
C
      IF(IBUF(1).EQ.NC4TO8("GRID")) THEN
         N=12
         DO 3 I=1,ILONG,12
           J=I+11
           IF(J.GT.ILONG) THEN
              J=ILONG
              N=ILONG-I+1
           ENDIF
           WRITE(6,6500) (RLONG(M),M=I,J)
           WRITE(6,6510)
           DO 4 JJ=1,ILAT
             LJ=ILAT+1-JJ
             K=(LJ-1)*ILONG+(I-1)
             WRITE(6,6520) RLAT(LJ),(A(K+M),M=1,N)
    4      CONTINUE
    3    CONTINUE
      ENDIF
C
C     * ZONL FIELD.
C
      IF(IBUF(1).EQ.NC4TO8("ZONL")) THEN
         WRITE(6,6600)
         DO 5 JJ=1,ILAT
           LJ=ILAT+1-JJ
           WRITE(6,6610) RLAT(LJ),A(LJ)
    5    CONTINUE
      ENDIF
C
      NR=NR+1
      WRITE(6,6040) LABEL
      WRITE(6,6025) IBUF
      GO TO 150
C
C     * E.O.F. ON INPUT.
C
  900 WRITE(6,6050) NR
      CALL                                         XIT('LPPRINT',0)
C------------------------------------------------------------------------
 5010 FORMAT(10X,I5,E10.0)                                                      A4
 5020 FORMAT(A80)                                                               A4
 6000 FORMAT('0FIRST RECORD NOT FOUND...')
 6005 FORMAT('0NTYP,SCALE=   ',I5,1PE12.4)
 6010 FORMAT('0YOU ARE TRYING TO OPERATE ON A SPECTRAL FILE...')
 6025 FORMAT('0',A4,I10,2X,A4,I10,4I6)
 6040 FORMAT('0',A80)
 6050 FORMAT('0',I6,' RECORDS PROCESSED')
 6500 FORMAT('1VALUES OF FIELD: ','LONG =',12F9.2)
 6510 FORMAT('0')
 6520 FORMAT('0','LAT =',F6.2,6X,'VALUE=',12F9.2)
 6600 FORMAT('1VALUES OF FIELD: ')
 6610 FORMAT('0','LAT =',F6.2,6X,'VALUE=',F9.2)
      END

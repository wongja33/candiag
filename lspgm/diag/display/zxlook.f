      PROGRAM ZXLOOK
C     PROGRAM ZXLOOK (INZX,       INPUT,       OUTPUT,                  )       A2
C    1          TAPE1=INZX, TAPE5=INPUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 15/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     NOV 24/83 - B.DUGAS.                                                      
C     MAR 31/82 - J.D.HENDERSON 
C                                                                               A2
CZXLOOK  - PRINTS AND MAPS A CROSS-SECTION.                             1  0 C GA1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - PRINTS AND MAPS PRESSURE LEVEL CROSS-SECTIONS IN A FILE AS           A3
C          REQUESTED ON CARDS.                                                  A3
C          NOTE - CROSS-SECTIONS CAN BE ZONAL (N TO S) OR MERIDIONAL (E TO W).  A3
C                 INTERPOLATION TO THE MAP CAN BE LINEAR OR CUBIC.              A3
C                 FOUR OR MORE LEVELS ALLOWS OPTION OF LINEAR OR CUBIC          A3
C                 INTERPOLATION.                                                A3
C                 TWO OR THREE LEVEL CROSS-SECTIONS ALWAYS USE LINEAR           A3
C                 INTERPOLATION.                                                A3
C                 ONE LEVEL CROSS-SECTIONS ARE SIMPLY GRAPHED.                  A3
C                 VERTICAL COORDINATE CAN BE PRESSURE OR LOG(PRESSURE).         A3
C                 MAXIMUM LEVELS IS $L$ AND MAXIMUM LATITUDES IS $BJ$.          A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      INZX = FILE CONTAINING CROSS-SECTIONS TO BE MAPPED.                      A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C                                                                               A5
C      NAME  = NAME OF VARIABLE TO BE MAPPED                                    A5
C              4HNEXT  MAPS THE NEXT CROSS-SECTION ON THE FILE                  A5
C              4HSKIP SKIPS THE NEXT CROSS-SECTION ON THE FILE                  A5
C      ISW   = SHOULD BE ZERO.                                                  A5
C              IF NOT ZERO ONLY THE FIRST CROSS-SECTION IS MAPPED               A5
C              (THIS IS INCLUDED FOR COMPATIBILITY WITH LAST VERSION)           A5
C      CINT  = CONTOUR INTERVAL                                                 A5
C      SCAL  = SCALE FACTOR BEFORE MAPPING                                      A5
C               (IF THE CROSS-SECTION HAS ONLY ONE LEVEL THESE TWO              A5
C                NUMBERS ARE USED AS THE GRAPH LIMITS GMIN, GMAX                A5
C                FOR THE GRAPH THAT IS DRAWN)                                   A5
C      MS    = MAP SIZE CONTROL (USE 22, 0 MEANS NO MAP).                       A5
C      LX    = DISPLAY NUMBER OF LEVELS (USE 33)                                A5
C      KAX   = 0 FOR VERTICAL AXIS OF PRESSURE                                  A5
C            = 1 FOR VERTICAL AXIS OF LOG(PRESSURE)                             A5
C      KIND  = VERTICAL INTERPOLATION CONTROL FROM DATA TO DISPLAY              A5
C              (1 GIVES LINEAR, OTHERWISE CUBIC).                               A5
C      KZM   = (0,1) FOR (S-N, E-W) CROSS SECTION.                              A5
C      LABEL = 80 CHARACTER LABEL PRINTED AFTER THE MAP                         A5
C              NOTE - NO LABEL CARD IS READ IF NAME IS 4HSKIP                   A5
C                                                                               A5
CEXAMPLE OF INPUT CARD(S)...                                                    A5
C                                                                               A5
C*  ZXLOOK TEMP    0      1.E1      1.E0   22   33    1    1    0               A5
C* TIME AND ZONALLY AVERAGED TEMPERATURE.                                       A5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO,
     &                       SIZES_BLONP1xBLAT,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     Load diag size values
      integer, parameter :: MAXLX = MAX(33,SIZES_MAXLEV)
      DATA MAXX/SIZES_BLATxNWORDIO/, MAXL/SIZES_MAXLEV/, 
     & MAXJ/SIZES_BLAT/

      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV),IPR(SIZES_MAXLEV) 
      CHARACTER*8 LABEL(10)
      REAL DEN(4,SIZES_MAXLEV-3),ANG(SIZES_BLAT) 
      REAL PR(SIZES_MAXLEV),Y(SIZES_MAXLEV),F(SIZES_BLAT)
      REAL PRX(MAXLX),YX(MAXLX) 
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT), RAD(SIZES_BLAT)
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLATxNWORDIO) 
C 
      COMMON/BLANCK/GX(SIZES_BLAT,SIZES_MAXLEV),
     & XS(SIZES_MAXLEV,SIZES_BLAT),FXS(SIZES_BLONP1xBLAT)
C 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
C 
C     * READ THE CONTROL CARDS. 
C     * LX = NUMBER OF LEVELS IN THE DISPLAY GRID (4 TO MAXLX). 
C     * KAX = KIND OF VERTICAL AXIS(0=PRESSURE, 1=LN(PRESSURE)).
C     * KIND=1 FOR LINEAR INTERPOLATION TO MAP, OTHERWISE CUBIC.
C     * KZM=(0,1) FOR (ZONAL,MERIDIONAL) CROSS-SECTION. 
C     * LABEL IS SIMPLY PRINTED AFTER THE MAP.
C     * NO LABEL CARD IS READ IF WE ARE SKIPPING (NAME=4HSKIP). 
C 
  150 READ(5,5010,END=900) NAME,ISW,CINT,SCAL,MS,LX,KAX,KIND,KZM                A4
      IF(NAME.NE.NC4TO8("SKIP")) READ(5,5012,END=902) LABEL                     A4
      IF(LX.LT.4.OR.LX.GT.MAXLX) LX=MAXLX 
      IF(KIND.NE.1) KIND=3
      IF(KZM.NE.0) KZM=1
C 
C     * GET THE NEXT REQUESTED CROSS-SECTION. 
C     * OMIT THE NAME CHECK IF ISW.GT.0. THIS IS INCLUDED 
C     * FOR COMPATIBILITY WITH A PREVIOUS VERSION.
C 
      NAM=NAME
      IF(NAME.EQ.NC4TO8("SKIP")) NAM=NC4TO8("NEXT")
      IF(ISW.GT.0) NAM=NC4TO8("NEXT")
      CALL FIND(1,NC4TO8("ZONL"),-1,NAM,-1,OK)
      IF(.NOT.OK) THEN
                  WRITE(6,6010) NAME
                  CALL                             XIT('ZXLOOK',-101) 
      ENDIF 
      CALL GETZX2(1,GX,MAXJ,LEV,NLEV,IBUF,MAXX,OK) 
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) THEN
                  CALL                             XIT('ZXLOOK',-102) 
      ENDIF 
      IF(NAME.EQ.NC4TO8("SKIP")) GO TO 150
C 
C     * PUT GX(NLAT,NLEV) INTO XS(NLEV,NLAT) WITH LEVELS REVERSED,
C     * AND ROWS REVERSED IF MERIDIONAL.
C     * NLAT IS (LATITUDE,LONGITUDE) AS KZM IS (0,1). 
C     * GX(1,1) CONTAINS (SOUTH,TOP) OR (WEST,TOP). 
C     * XS(1,1) CONTAINS (BOTTOM,SOUTH) OR (BOTTOM,EAST). 
C 
      NLAT=IBUF(5)
      CALL LVDCODE(PR,LEV,NLEV)
C
      DO 210 L=1,NLEV 
      K=NLEV+1-L
      IPR(K)=LEV(L) 
      IF(KAX.EQ.1) PR(L)=LOG(PR(L))
      DO 210 J=1,NLAT 
      JJ=J
      IF(KZM.EQ.1) JJ=NLAT+1-J
  210 XS(K,J)=GX(JJ,L)
C 
C     * NLEV=1 DRAWS A GRAPH FROM N TO S AND STOPS. 
C     * CINT,SCAL ARE USED AS GMIN,GMAX IN SPLAT. 
C 
      IF(NLEV.GT.1) GO TO 310 
      DO 230 J=1,NLAT 
      K=NLAT+1-J
  230 F(J)=XS(1,K)
      CALL SPLAT(F,NLAT,1,NLAT,1,CINT,SCAL) 
      WRITE(6,6040) LABEL 
      WRITE(6,6025) IBUF
      GO TO 150 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * COMPUTE LATITUDES FOR ZONAL CROSS-SECTION (KZM=0).
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C     * NLAT MUST BE GLOBAL AND A MULTIPLE OF 2.
C 
  310 IF(KZM.NE.0) GO TO 322
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
      DO 320 J=1,NLAT 
  320  ANG(J)= RAD(J)*180.E0/3.14159E0
      GO TO 326 
C 
C     * COMPUTE LONGITUDES FOR MERIDIONAL CROSS-SECTION (KZM=1).
C 
  322 DO 324 I=1,NLAT 
      N=NLAT+1-I
  324 ANG(N)=FLOAT(I-1)*360.E0/FLOAT(NLAT-1)
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * PRINT OUT THE CROSS-SECTION.
C     * N TO S IF KZM=0, E TO W IF KZM=1. 
C     * MAX LEVELS PRINTED PER PAGE IS 10.
C 
  326 LL=1
  328 LR=LL+9 
C 
      IF(LR.GT.NLEV) LR=NLEV
      IF(KZM.EQ.0) WRITE(6,6033) (IPR(L),L=LL,LR) 
      IF(KZM.EQ.1) WRITE(6,6034) (IPR(L),L=LL,LR) 
      DO 330 M=1,NLAT 
      J=NLAT+1-M
      K=M 
      IF(KZM.EQ.0) K=NLAT+1-M 
  330 WRITE(6,6035) K, ANG(J),(XS(L,J),L=LL,LR) 
      LL=LR+1 
      IF(LL.LE.NLEV) GO TO 328
      WRITE(6,6040) LABEL 
      WRITE(6,6025) IBUF
C 
C     * EXPAND THE GRID IN THE VERTICAL IF REQUESTED. 
C     * COMPUTE THE EQUALLY SPACED PRESSURE LEVELS OF THE DISPLAY GRID. 
C     * PRECOMPUTE THE LAGRANGIAN DENOMINATORS IN DEN IF INTERP IS CUBIC
C 
      IF(MS.EQ.0) GO TO 150 
      IF(NLEV.EQ.1) GO TO 150 
      IF(NLEV.LE.3) KIND=1
      DP=(PR(NLEV)-PR(1))/FLOAT(LX-1) 
      DO 390 L=1,LX 
  390 PRX(L)=(L-1)*DP+PR(1) 
      IF(KIND.EQ.3) CALL LGRDC(DEN,PR,NLEV) 
C 
C     * INTERPOLATE THE DISPLAY LEVELS ONE COLUMN AT A TIME.
C 
      DO 450 J=1,NLAT 
      DO 410 L=1,NLEV 
      K=NLEV+1-L
  410 Y(L)=XS(K,J)
      IF(KIND.EQ.1) CALL LINIL(YX,PRX,LX,Y,PR,NLEV,0.E0,0.E0) 
      IF(KIND.EQ.3) CALL LGRIC(YX,PRX,LX,Y,PR,DEN,NLEV,0.E0,0.E0) 
      JX=(J-1)*LX 
      DO 420 L=1,LX 
      K=LX+1-L
  420 FXS(JX+L)=YX(K) 
  450 CONTINUE
C 
C     * MAP THE CROSS-SECTION. PRES DECREASES TO THE RIGHT ON THE PAGE. 
C     * (NORTH,EAST) IS AT THE TOP FOR (ZONAL,MERIDIONAL) CASES.
C 
      CALL FCONW2(FXS,CINT,SCAL,LX,NLAT,1,1,LX,NLAT,MS) 
      IF(KAX.EQ.0) WRITE(6,6042) LABEL
      IF(KAX.EQ.1) WRITE(6,6044) LABEL
      IF(KIND.EQ.1) WRITE(6,6051) 
      IF(KIND.EQ.3) WRITE(6,6053) 
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('ZXLOOK',0)
  902 CALL                                         XIT('ZXLOOK',-103) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,1X,A4,I5,2E10.0,5I5)                                           A4
 5012 FORMAT(10A8)                                                              A4
 6010 FORMAT('0..ZXLOOK EOF LOOKING FOR',2X,A4)
 6025 FORMAT('0',A4,I10,1X,A4,I10,4I6)
 6033 FORMAT('1ROW   LAT',10I12)
 6034 FORMAT('1ROW   LON',10I12)
 6035 FORMAT(' ',I3,F6.1,1P10E12.4)
 6040 FORMAT('0 CROSS-SECTION...',10A8)
 6042 FORMAT('0     PR-LAT CROSS-SECTION...',10A8)
 6044 FORMAT('0LOG(PR)-LAT CROSS-SECTION...',10A8)
 6051 FORMAT('0PRESSURE INTERP TO MAP IS LINEAR')
 6053 FORMAT('0PRESSURE INTERP TO MAP IS CUBIC ')
      END

      SUBROUTINE PATTERN (XWRK,YWRK,NWRK,IAREA,IGRP,NGRPS) 

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C This routine does non-color shading.
      IMPLICIT NONE

      REAL XWRK(*), YWRK(*)
      INTEGER NWRK
      INTEGER IAREA(*), IGRP(*)
      INTEGER NGRPS

C For passing colour index array into colour FILL routines through
C Common Block.

      INTEGER NIPAT, NPAT, MAXPAT, MAXCLRS
      PARAMETER(NIPAT = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS

      INTEGER NDSTWK,NINDWK
      REAL DSTWK(16384)
      INTEGER INDWK(32768)

      INTEGER IDCONT, FILPA
      INTEGER I


C If the area is defined by 2 or fewer points, return to ARSCAM
      IF (NWRK .LE. 3) RETURN

      NDSTWK=3*NWRK
      NINDWK=5*NWRK

      IDCONT=-1
      DO 10, I=1,NGRPS
        IF (IGRP(I).EQ.3) IDCONT=IAREA(I)
 10   CONTINUE
C Check if the area is over the map
      IF (IDCONT .GT. 0) THEN
         CALL DFNCLR(IPAT(IDCONT),FILPA)
         CALL SFSGFA(XWRK,YWRK,NWRK,
     1        DSTWK,NDSTWK,
     2        INDWK,NINDWK,
     3        FILPA)
      ENDIF

      RETURN

      END

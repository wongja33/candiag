      SUBROUTINE AGPWRT (XPOS, YPOS, CHRS, NCHS, ISIZ, IORI, ICEN)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)

      CHARACTER*(*) CHRS
C
C This routine just passes its arguments along to the character-drawing
C routine PWRIT, in the system plot package.  By substituting his/her
C own version of AGPWRT, the user can cause a fancier character-drawer
C to be used.
C
C      CALL GSTXFP(-2,0)
      CALL PCGETR ('CS - CONSTANT SPACING FLAG', CSFL) 
      IF (ICEN.NE.0) THEN
        CALL PCSETR ('CS - CONSTANT SPACING FLAG', 1.25)
      ELSE
        CALL PCSETR ('CS - CONSTANT SPACING FLAG', 0.0 )
      ENDIF

      CALL PCHIQU (XPOS, YPOS, CHRS(1:NCHS),
     +             .9*REAL(ISIZ), REAL(IORI), REAL(ICEN))
      CALL PCSETR ('CS - CONSTANT SPACING FLAG', CSFL)

      RETURN
      END

      SUBROUTINE GGP_CONLS(FLOWN, FHIGHN, FINC)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
C CONtour Level Selection routine. This routine makes GGP_CONDEF
C simulate the contour line selection of CONREC. This routine sets
C contour levels and dash patterns before drawing contour lines.

      REAL FLOWN, FHIGHN, FINC

      INTEGER NCL, I, NCIT
      PARAMETER (NCIT = 10)
      REAL FLVAL,TOL, FLABVAL
      CHARACTER*8 CLABVAL
      PARAMETER (TOL = 1D-8)
C     ARBITRARY CONTOUR LINES INFO
      INTEGER NIPAT, NPAT, MAXPAT, MAXCLRS, NIPATS
      PARAMETER (NIPAT = 28, NIPATS = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT,ZLEV,NPAT,MAXPAT,MAXCLRS
C     ARBITRARY CONTOUR LINES INFO
      INTEGER NPATS, IARBI
      REAL ZLEVS(NIPATS),CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI, CWM
C
      CALL CPSETI('NOF - NUMERIC OMMISSION FLAG',3)
      CALL CPSETI('NLZ - NUMERIC LEADING ZERO',1)
C
C     VERIFY IF ARBITRARY CONTOURS ARE NEEDED
C
      IF(IARBI.GT.0) THEN
         CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',0)
C         CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
C         CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
C     
         NCL = NPATS
         CALL CPSETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
         DO I = 1, NCL
            FLVAL=ZLEVS(I)
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPSETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
         ENDDO
      ELSEIF (FINC .GE. TOL) THEN
         IF (FLOWN .LT. FHIGHN) THEN
            CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',0)
            CALL CPSETR('CIS - CONTOUR INTERVAL SPECIFIER', FINC)
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)

            NCL = INT((FHIGHN - FLOWN) / FINC) + 1
            CALL CPSETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
            DO I = 1, NCL
              FLVAL=FLOWN + (I - 1) * FINC
              CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
              CALL CPSETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
C     Moved some decisions to ggp_colcon.f
            ENDDO
         ELSE
            CALL CPSETR('CIS - CONTOUR INTERVAL SPECIFIER', FINC)
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
C Conpack will choose contour levels at 'nice' intervals.
            CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',10)

            CALL CPSETI('PAI',1)
            CALL CPSETR('CIT',1.0)
            CALL CPSETI('PAI',2)
            CALL CPSETR('CIT',2.0)
            CALL CPSETI('PAI',3)
            CALL CPSETR('CIT',5.0)
            DO I = 1,3
               CALL CPSETI('PAI',I)
               CALL CPSETI('LIT',2)
            ENDDO
            DO I = 4,NCIT
               CALL CPSETI('PAI',I)
               CALL CPSETR('CIT',0.0)
               CALL CPSETI('LIT',0)
            ENDDO
         ENDIF

         CALL CPSETR('CIU - CONTOUR INTERVAL USED', FINC)
      ELSE IF (ABS(FINC) .LT. TOL) THEN
         IF (FLOWN .LT. FHIGHN) THEN
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
         ENDIF
C Conpack will choose 10 to 20 contour levels at 'nice' intervals.
         CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',10)
         CALL CPSETR('CIS - CONTOUR INTERVAL SPECIFIER', FINC)

         CALL CPSETI('PAI',1)
         CALL CPSETR('CIT',1.0)
         CALL CPSETI('PAI',2)
         CALL CPSETR('CIT',2.0)
         CALL CPSETI('PAI',3)
         CALL CPSETR('CIT',5.0)
         DO I = 1,3
            CALL CPSETI('PAI',I)
            CALL CPSETI('LIT',2)
         ENDDO
         DO I = 4,NCIT
            CALL CPSETI('PAI',I)
            CALL CPSETR('CIT',0.0)
            CALL CPSETI('LIT',0)
         ENDDO
      ELSE
         WRITE(6,*) '#########################################'
         WRITE(6,*) '#### SHOULD NOT BE HERE - FINC .LT. 0 ###'
         WRITE(6,*) '#########################################'

         IF (FLOWN .LT. FHIGHN) THEN
            CALL CPSETR('CMN - MINIMUM CONTOUR LEVEL', FLOWN)
            CALL CPSETR('CMX - MAXIMUM CONTOUR LEVEL', FHIGHN)
         ENDIF
C Conpack will choose 10 to 20 contour levels at 'nice' intervals.
         CALL CPSETI('CLS - CONTOUR LEVEL SELECTION FLAG',10)
         CALL CPSETR('CIS - CONTOUR INTERVAL SPECIFIER', FINC)

         CALL CPSETI('PAI',1)
         CALL CPSETR('CIT',1.0)
         CALL CPSETI('PAI',2)
         CALL CPSETR('CIT',2.0)
         CALL CPSETI('PAI',3)
         CALL CPSETR('CIT',5.0)
         DO I = 1,3
            CALL CPSETI('PAI',I)
            CALL CPSETI('LIT',2)
         ENDDO
         DO I = 4,NCIT
            CALL CPSETI('PAI',I)
            CALL CPSETR('CIT',0.0)
            CALL CPSETI('LIT',0)
         ENDDO
      ENDIF
C
 7000 format(F4.1)

      RETURN
      END

      SUBROUTINE LBFILL(IFTP,XCRA,YCRA,NCRA,INDX)

C     * MAY 13/2004 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C     This routine is internal in NCAR v3.2. When non-color shading
C     patterns is used, LBFILL sets non-color shading patterns for
C     legend boxes.
      IMPLICIT NONE

      INTEGER IFTP,NCRA,INDX
      REAL XCRA,YCRA
      DIMENSION XCRA(*), YCRA(*)
      
      CHARACTER*4 ATYPE, CSPPL, CGGPL, CHOVP, CZXPL
      INTEGER*4 ITYPE, ISPPL, IGGPL, IHOVP, IZXPL
C ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE

      REAL RWRK
      INTEGER IWRK
      DIMENSION RWRK(32),IWRK(32)
      INTEGER FILPA
      INTEGER ARG
      COMMON /PAT/ ARG
C For passing colour index array into colour FILL routines through
C Common Block.
      INTEGER NIPAT, NPAT, MAXPAT,MAXCLRS
      PARAMETER(NIPAT = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS

      INTEGER IERR
      REAL LWIDTHORIG
      EQUIVALENCE (CSPPL,ISPPL)
      EQUIVALENCE (CGGPL,IGGPL)
      EQUIVALENCE (CHOVP,IHOVP)
      EQUIVALENCE (CZXPL,IZXPL)

      CSPPL='SPPL'
      CGGPL='GGPL'
      CHOVP='HOVP'
      CZXPL='ZXPL'


      CALL GQLWSC(IERR,LWIDTHORIG)
      CALL GSLWSC(1.0)

C     IF ((ITYPE .EQ. 'GGPL') .OR. (ITYPE .EQ. 'HOVP') 
C    +     .OR. (ITYPE .EQ. 'SPPL').OR. (ITYPE .EQ.'ZXPL')) THEN
      IF ((ITYPE .EQ. IGGPL) .OR. (ITYPE .EQ. IHOVP) 
     +     .OR. (ITYPE .EQ. ISPPL).OR. (ITYPE .EQ.IZXPL)) THEN
        ARG=ARG+1
        CALL DFNCLR(IPAT(ARG),FILPA)
        CALL SFSGFA (XCRA,YCRA,NCRA-1,RWRK,32,IWRK,32,INDX)
      ELSE
        CALL GSFACI(INDX)
        CALL GFA(NCRA-1,XCRA,YCRA)
      ENDIF

      IF(IERR.EQ.0) CALL GSLWSC(LWIDTHORIG)

      RETURN
      END

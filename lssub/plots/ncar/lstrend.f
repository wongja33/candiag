      INTEGER FUNCTION LSTREND(STR)
      IMPLICIT NONE
      CHARACTER*(*) STR

      INTEGER IEND

      IEND=LEN(STR)
      DO WHILE(IEND.GT.1.AND.STR(IEND:IEND).EQ.' ')
         IEND=IEND-1
      ENDDO

      LSTREND=IEND
      END

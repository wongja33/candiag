      SUBROUTINE BFCRDF(WHITFG)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
C Background/Foreground ColouR DeFinition routine.

      INTEGER WHITFG

C Set color fill to solid
      CALL GSFAIS (1)

      IF (WHITFG .EQ. 0) THEN
C        Background colour is white
         CALL GSCR(1,0,1.,1.,1.)
C        First foreground color is black
         CALL GSCR(1,1,0.,0.,0.)
      ELSE
C        Background colour is black
         CALL GSCR(1,0,0.,0.,0.)
C        First foreground color is white
         CALL GSCR(1,1,1.,1.,1.)
      ENDIF


      RETURN
      END

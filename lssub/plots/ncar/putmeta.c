/*
 *   Append metacode or postscript from GMETA to output file AAASPUT
 *
 * DEC 12/2007 - F.MAJAESS (ADJUST for '(atend)' format change in '%%Pages:' 
 *                                from '(at end)' for PS case)
 * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 0

#define METATAILSIZE   1440
#define IOBUFSIZE   4096

#define GMETA "gmeta"
#define AAASPUT "aaasput"

#define HEADERPAGES "%%Pages:"
#define HEADERPAGESATEND HEADERPAGES" (at end)"
#define HEADERPAGESATEND2 HEADERPAGES" (atend)"
#define HEADERPAGECOUNT "%%Page:"
#define HEADEREOF "%%EOF"
#define HEADERTRAILER "%%Trailer"


static int prepoutput(FILE *fp);

void
putmeta_(idummy)
int *idummy;

{
   char   buf[IOBUFSIZE+1],*p,*otp;
   FILE   *file1, *file2;
   int    o, n, m, pagecount;
   unsigned long   bytecount=0;

   if(!(file1 = fopen(GMETA, "r" ))) {
     fprintf(stderr,"Could not open '%s' file\n",GMETA);
     exit(-1);
   }
   if(!(file2 = fopen(AAASPUT, "r+"))) {
     if(!(file2 = fopen(AAASPUT, "w"))) {
       fprintf(stderr,"Could not open '%s' file\n",AAASPUT);
       exit(-1);
     }
   }

   if(!(otp=getenv("NCAROUTPUT")) || (strcmp(otp,"PS"))) {
     /* CGM FORMAT */
     fseek(file2,0,SEEK_SET);
     n = fread(buf, sizeof(char), METATAILSIZE, file2);
#if DEBUG
     fprintf(stderr,"non-ps, read %d\n",n);
#endif

     if (n < METATAILSIZE) {
#if DEBUG
       fprintf(stderr,"seek end\n");
#endif
       fseek(file2,0,SEEK_END);
     }
     else {
#if DEBUG
     fprintf(stderr,"seek -%d\n",METATAILSIZE);
#endif
       m = fseek(file2, -METATAILSIZE, SEEK_END);
       if (m != 0) fprintf(stderr,"Should abort - fseek error on %s.\n",GMETA);
       /* Discard meta tail */
       n = fread(buf, sizeof(char), METATAILSIZE, file1);
     }

     n = fread(buf, sizeof(char), IOBUFSIZE, file1);
     m=0;
     while (n > 0) {
       o=fwrite(buf,sizeof(char),n,file2);
#if DEBUG
       fprintf(stderr,"r%d/w%d ",n,o);
#endif
       m+=n;
       n = fread(buf, sizeof(char), IOBUFSIZE, file1);
     }
#if DEBUG
     fprintf(stderr,"%d bytes copied\n",m);
#endif
   }
   else {
     /* PS FORMAT */
#if DEBUG
     fprintf(stderr,"ps\n");
#endif
     pagecount=prepoutput(file2);
#if DEBUG
      fprintf(stderr,"pagecount %d\n",pagecount);
#endif

     while(p=fgets(buf,IOBUFSIZE,file1)) {
       /* skip over "Pages: (at end)" header */
       /* if(strncmp(p,HEADERPAGESATEND,strlen(HEADERPAGESATEND))) { */
       if((strncmp(p,HEADERPAGESATEND,strlen(HEADERPAGESATEND)) != 0 ) &&
          (strncmp(p,HEADERPAGESATEND2,strlen(HEADERPAGESATEND2)) != 0 )
         ) {
	 /* break out when end headers found */
	 if(!strncmp(p,HEADERPAGES,strlen(HEADERPAGES))
	    ||!strncmp(p,HEADEREOF,strlen(HEADEREOF))) {
#if DEBUG
	   fprintf(stderr,"Found '%s' in %s\n",p,GMETA);
#endif
	   break;
	 }
       }
       /* Print corrected pagecount */
       if(!strncmp(p,HEADERPAGECOUNT,strlen(HEADERPAGECOUNT))) {
	 pagecount++;
#if DEBUG
	 fprintf(stderr,"%s %d %d\n",HEADERPAGECOUNT,pagecount,pagecount);
#endif
	 fprintf(file2,"%s %d %d\n",HEADERPAGECOUNT,pagecount,pagecount);
       }
       else {
	 bytecount += strlen(buf);
	 /* skip over "Trailer" header */
	 if(!strncmp(p,HEADERTRAILER,strlen(HEADERTRAILER))) {
#if DEBUG
	   fprintf(stderr,"Found Trailer\n");
#endif
	   continue;
	 }
	 fputs(buf,file2);
       }
     }

#if DEBUG
     fprintf(stderr,"%s %d\n",HEADERPAGES,pagecount);
#endif
     fprintf(file2,"%s %d\n",HEADERPAGES,pagecount);
#if 0
#if DEBUG
     fprintf(stderr,"%s\n",HEADERTRAILER);
#endif
     fprintf(file2,"%s\n",HEADERTRAILER);
#endif
#if DEBUG
     fprintf(stderr,"%s\n",HEADEREOF);
#endif
     fprintf(file2,"%s\n",HEADEREOF);
#if DEBUG
     fprintf(stderr,"%lu bytes copied\n",bytecount);
#endif
   }

   fclose(file1);
   fclose(file2);
}

static int
prepoutput(FILE *fp)
{
  char linebuf[IOBUFSIZE+1];
  char *p;
  int pagecount=0,pos=0;

  while(p=fgets(linebuf,IOBUFSIZE,fp)) {
#if 0
#if DEBUG
    fprintf(stderr,"pos: %d %d\n",pos,pagecount);
#endif
#endif
    /* skip over "Trailer" header */
    if(!strncmp(p,HEADERTRAILER,strlen(HEADERTRAILER))) {
#if DEBUG
      fprintf(stderr,"Found Trailer\n");
#endif
      continue;
    }
    /* skip over "Pages: (at end)" header */
    /* if(!strncmp(p,HEADERPAGESATEND,strlen(HEADERPAGESATEND))) { */
    if((strncmp(p,HEADERPAGESATEND,strlen(HEADERPAGESATEND)) == 0 ) ||
       (strncmp(p,HEADERPAGESATEND2,strlen(HEADERPAGESATEND2)) == 0 )
      ) {
#if DEBUG
      fprintf(stderr,"Found atend\n");
#endif
      continue;
    }

    /* break out when end headers found */
    if(!strncmp(p,HEADERPAGES,strlen(HEADERPAGES))
       ||!strncmp(p,HEADEREOF,strlen(HEADEREOF))) {
#if DEBUG
      fprintf(stderr,"Found '%s' in %s\n\tSeek to %d\n",p,AAASPUT,pos);
#endif
      fseek(fp,pos,SEEK_SET);
      break;
    }

    /* keep count of pages so far */
    if(!strncmp(p,HEADERPAGECOUNT,strlen(HEADERPAGECOUNT)))
      pagecount++;

    pos=ftell(fp);
  }
  return(pagecount);
}

      SUBROUTINE MAPUSR(IPRT)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
C
C This Ezmap subroutine is called just before and just after each portion
C of the map is drawn. The default version of MAPUSR does nothing, but you
C can modify it to control how Ezmap draws lines.
C
      INTEGER IPRT
      LOGICAL PUB, DOTITLE, DOINFOLABEL, SHAD1
      COMMON /PPPP/ PUB, SHAD1, DOTITLE, DOINFOLABEL


      IF (IPRT .EQ. 1) CALL GSLWSC(3.)
      IF (IPRT .EQ. 2) CALL GSLWSC(2.) 
      IF (IPRT .EQ. 3) CALL GSLWSC(3.)
      IF ((IPRT .EQ. 5).AND.(PUB)) CALL GSLWSC(4.)
      IF ((IPRT .EQ. 5).AND.(.NOT.PUB)) CALL GSLWSC(2.)
      IF ((IPRT .EQ. 5).AND.(.NOT.PUB).AND.(.NOT.SHAD1))
     1   CALL GSLWSC(1.)
  
      RETURN
      END 

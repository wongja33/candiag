      SUBROUTINE CPCHIL(IFLG)

C     * MAY 13/2004 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE
      
C     NAME
C          CPCHIL - Called by Conpack routines as the informational
C          label is drawn.  The default version does nothing.  A user-
C          written version may be supplied to provide control as the
C          label is drawn.

      INTEGER IFLG
      CHARACTER*4 ATYPE, CGGPL
      INTEGER*4 ITYPE, IGGPL
C ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE

C Variables to temporarily hold integer and real values.
      INTEGER ITEMP
      REAL RTEMP
C Common block for SToring variables accessed by the CPCHIL routine.
      COMMON /GGP_CHILST/ ITEMP, RTEMP
      EQUIVALENCE (CGGPL,IGGPL)

      CGGPL='GGPL'

C     IF (ITYPE .NE. 'GGPL') RETURN
      IF (ITYPE .NE. IGGPL) RETURN
      IF (IFLG .EQ. 3) THEN
C Use Plotchar Fontcap database 1, saving current database number.
         CALL PCGETI('FN - FONT NUMBER OR NAME', ITEMP)
         CALL PCSETI('FN - FONT NUMBER OR NAME', 1)
C Use the default line width of Plotchar characters, saving current
C line width.
         CALL PCGETR('CL - PRINCIPAL LINE WIDTH', RTEMP)
         CALL PCSETR('CL - PRINCIPAL LINE WIDTH', 1.0)
      ENDIF

      IF (IFLG .EQ. -3) THEN
C Restore the Plotchar utility font and character line width.
         CALL PCSETI('FN - FONT NUMBER OR NAME', ITEMP)
         CALL PCSETR('CL - PRINCIPAL LINE WIDTH', RTEMP)
      ENDIF


      RETURN
      END

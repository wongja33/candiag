      SUBROUTINE GGP_U_DATPRP(LX,LY,LXY,U,GG2,FLIP,VLO,VI, 
     1           VSCAL,SPVAL,INCX,INCY,LYR,LXR) 

C     * SEP 18/2006 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C     This routine prepare data for U vector field.
      IMPLICIT NONE

      INTEGER LX,LY,LXY
      REAL U(*),GG2(*)
      LOGICAL FLIP
      REAL VLO,VI,VSCAL,SPVAL,SPVALT
C     Interval for vectors - along X/Y axis
      INTEGER INCX, INCY
      INTEGER LYR,LXR

      INTEGER ILGH,I,J,JR,NL,NR,NN,JJ,N,IPN

      PRINT*, "U_DATPRP"

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      IF (FLIP) THEN
        IF ( INT(LX/2.)*2 .EQ. LX ) THEN
          ILGH= LX/2
          DO 235 J=1,LY
            JR=(J-1)*LX
            DO 230 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              U(NL)=GG2(NR)
              U(NR)=GG2(NL)
  230       CONTINUE
  235     CONTINUE
        ELSE
          ILGH=(LX-1)/2
          DO 245 J=1,LY
            JR=(J-1)*LX
            DO 240 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              U(NL)=GG2(NR)
              U(NR)=GG2(NL)
  240       CONTINUE
            U(JR+LX)=U(JR+1)
  245     CONTINUE
        ENDIF
C       CALL MOVLEV(U,GG2,LXY)
        CALL SCOPY(LXY,U,1,GG2,1)
      ENDIF

C     * ZERO-OUT POINTS AND SCALE THE U FIELD.

      IF (INCX.LT.1 .OR. INCY.LT.1)   THEN
        LXR=LX
        LYR=LY
        GOTO 380
      ELSE
        LXR=(LX-1)/INCX+1
        LYR=(LY-1)/INCY+1
      ENDIF

      IF (INCX.GT.1 .OR. INCY.GT.1)    THEN
        DO 360 I=1,LXR*LY
           IF(ABS(U(I)-SPVAL).GT.SPVALT) THEN
              U(I)=0.
           ENDIF
  360   CONTINUE
      ENDIF

      DO 370 J=1,LYR 
        JJ=(J-1)*INCY+1
        DO 370 I=1,LXR
          N = I           + (JJ-1)*LXR
          NN=(I-1)*INCX+1 + (JJ-1)*LX
          IF(ABS(U(N)-SPVAL)   .GT.SPVALT .AND.
     1       ABS(GG2(NN)-SPVAL).GT.SPVALT) THEN
             U(N)=GG2(NN)*VSCAL
          ELSE
             U(N)=SPVAL
          ENDIF
  370 CONTINUE

C     * ZERO-OUT THE POLE VALUES.

  380 IPN=(LY-1)*LXR

      END

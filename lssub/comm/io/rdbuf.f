      SUBROUTINE RDBUF(NF,IBUF,LENGTH,OK)
C 
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * NOV 02/92 - J.SNYDER,E.CHAN   
C 
C     * READS DATA OF "LENGTH" ELEMENTS INTO IBUF FROM FILE NF.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      DIMENSION IBUF(LENGTH)
C---------------------------------------------------------------------
      OK=.FALSE.
      READ(NF,END=910,ERR=900) IBUF
      OK=.TRUE.
  900 RETURN
C
  910 LENGTH=0
      RETURN
C---------------------------------------------------------------------
      END 

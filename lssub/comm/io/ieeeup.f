      SUBROUTINE IEEEUP(NWDS,LCM,IEEEF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * DEC 06/00 - F. MAJAESS - ENSURE CONVERTED VALUES TO 32-BIT IEEE MODE
C                                ARE WITHIN THE VALID RANGE.
C     * OCT 25/96 - F. MAJAESS - ENSURE 64-BIT IEEE "NEGATIVE ZERO" (IF ANY) 
C                                CONVERTED INTO "POSITIVE ZERO" SINCE WITHOUT
C                                THIS FIX CONVERSION ROUTINES MAY PRODUCE WRONG
C                                VALUES (-2.0 IN THE CASE OF 32-BIT IEEE MODE).
C     * APR 15/92 - J. STACEY -
C     *
C     * PURPOSE: THIS ROUTINE CONVERTS FROM IEEE 64-BIT REAL*8 TO EITHER 
C     *          IBM 32-BIT, IBM 64-BIT CRAY 64-BIT, IEEE 32-BIT OR IEEE 64-BIT 
C     *          DATA.  
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LCM(1),IEEEF(1)
C
C     * INTEGER REPRESENTATIONS FOR THE HIGH ORDER 32-BITS OF SQRT(2)
C     * ARE DIFFERENT ON IBM, CRAY AND IEEE MACHINES.  WE USE THIS TO TEST
C     * FOR THE EXACT TYPE OF MACHINE BEFORE CONVERTING TO THE STANDARD.
C
      PARAMETER  ( IAMIBM=1092001950)
      PARAMETER  (IAMCRAY=1073853700)
      PARAMETER  (IAMIEEE=1073127582)
      REAL*8     A,B
      INTEGER    IA(2),JB
      EQUIVALENCE (A,IA),(B,JB)
      LOGICAL    IBM,CRAY,IEEE,IM64BIT,INTEST2
      INTEGER    IZERO
      DIMENSION  ZERO(1)
      EQUIVALENCE (ZERO,IZERO)
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DATA       ZERO/0.0E0/
C--------------------------------------------------------------------------
C
C     * USE THE STANDARD TRICK TO SEE IF WE HAVE 32- OR 64-BIT INTEGERS.
C
      IA(2) = 0
      B = 0.D0
      A          = DSQRT(2.0D0)
      JB         = IA(1)
      IF (A .EQ. B) THEN
        IM64BIT  = .TRUE.
        MSHFM32 = -32
        JB = ISHFT(JB,MSHFM32)
      ELSE
        IM64BIT  = .FALSE.
      END IF
C
C     * TEST FOR IBM, CRAY OR IEEE MACHINE.
C
      IBM        = JB .EQ. IAMIBM
      CRAY       = JB .EQ. IAMCRAY
      IEEE       = JB .EQ. IAMIEEE
C     TEST FOR IEEE ON LITTLE-ENDIAN 32-BIT INTEGER MACHINES
      IF(.NOT.IEEE.AND..NOT.IM64BIT) IEEE = IA(2) .EQ. IAMIEEE
      INTEST2    = INTSIZE .EQ. 2
C
C     * ENSURE "NEGATIVE ZERO" (IF ANY) CONVERTED
C     * INTO   "POSITIVE ZERO".
C
      IF(IBM .OR. CRAY .OR. IEEE ) THEN
        IF (IM64BIT) THEN
          MBSE0 = 0
          MBSE63 = 63
          MASKX = IBSET(MBSE0,MBSE63)
          DO 10 I=1,NWDS
            MSHF1 = 1
            IF(IEEEF(I).EQ.MASKX) IEEEF(I)=ISHFT(MASKX,MSHF1)
  10      CONTINUE
        ELSE
          MBSE0 = 0
          MBSE31 = 31
          MASKX = IBSET(MBSE0,MBSE31)
          DO 15 I=1,2*NWDS-1,2
            MSHF1 = 1
            IF(IEEEF(I).EQ.MASKX .AND. IEEEF(I+1).EQ.0 ) 
     1                            IEEEF(I)=ISHFT(MASKX,MSHF1)
  15      CONTINUE
        ENDIF
      ENDIF


C
C     * NOW BRANCH ON TYPE OF MACHINE.
C
      IF (IBM  .AND. IM64BIT) THEN
        CALL BF1I64B(IEEEF,LCM,NWDS)
        RETURN
      END IF

      IF (IBM .AND. .NOT.IM64BIT) THEN
        IF (.NOT.INTEST2) THEN !32' INTEGERS/32' REALS
          CALL BF2I64B(IEEEF,LCM,NWDS)
        ELSE                   !32' INTEGERS/64' REALS
          CALL BF1I64B(IEEEF,LCM,NWDS)
        END IF
        RETURN
      END IF

      IF (CRAY) THEN
        CALL CF1I64C(IEEEF,LCM,NWDS)
        RETURN
      END IF

      IF (IEEE .AND. IM64BIT) THEN
        DO 20 I=1,NWDS
          LCM(I) = IEEEF(I)
   20   CONTINUE
        RETURN
      END IF

      IF (IEEE .AND. .NOT.IM64BIT) THEN
        MBSE0 = 0
        MBSE31 = 31
        MASK1 = IBSET(MBSE0,MBSE31) !1 BIT MASK FOR SIGN
        MASK2 = IBITS(-1,0,8)     !8 BIT MASK FOR IEEE 32' EXPONENT.
        MASK3 = IBITS(-1,0,20)    !20 BIT MASK FOR IEEE 32' MANTISSA.
        MASK4 = IBITS(-1,0,3)     !3 BIT MASK FOR 3' OF IEEE MANTISSA.
        MASK5 = IBITS(-1,0,11)    !11 BIT MASK FOR IEEE 64' EXPONENT.
        IF (.NOT.INTEST2) THEN !INTEGERS ARE 32', REALS ARE 32'.
          K = 1
          DO 30 I=1,2*NWDS-1,2
            IF (IEEEF(I).NE.0 .OR. IEEEF(I+1).NE.0) THEN
             IEXP = IBITS(IEEEF(I),20,11)-1023+127
             IF ( IEXP .GT. MASK2) THEN
              WRITE(6,6050)
 6050         FORMAT('0 *** ERROR *** IEEEUP: IEEE 32-BIT OVERFLOW.')
              CALL ABORT
             END IF
             IF ( IEXP .LT. 0) THEN
              LCM(K) = IZERO 
             ELSE
              MSHF23 = 23
              MSHF3 = 3
              MSHFM29 = -29
              LCM(K) = IOR(IOR(IOR(
     1                 IAND(IEEEF(I),MASK1),
     2        ISHFT(IAND(IBITS(IEEEF(I),20,11)-1023+127,MASK2),
     3        MSHF23)
     3                          ),
     4        ISHFT(IAND(IEEEF(I),MASK3),MSHF3)
     5                      ),
     6        IAND(ISHFT(IEEEF(I+1),MSHFM29),MASK4)
     7                    )
              MSHF1 = 1
              IF(LCM(K).EQ.MASK1) LCM(K)=ISHFT(MASK1,MSHF1)
             END IF
            ELSE
             LCM(K) = IZERO
            END IF
            K = K + 1
   30     CONTINUE
        ELSE        !INTEGERS ARE 32', REALS ARE 64'.
          DO 40 I=1,2*NWDS
            LCM(I) = IEEEF(I)
   40     CONTINUE
        END IF
        RETURN
      END IF

C     * IF WE GET HERE THEN WE HAVE A FATAL ERROR (WE DON'T KNOW WHAT MACHINE
C     * WE'RE ON.

      WRITE(6,6100)
 6100 FORMAT('0 *** ERROR *** IEEEUP: UNKNOWN MACHINE FORMAT.')
      CALL ABORT
      END

      SUBROUTINE GETFLD2(NF,G,KIND,NSTEP,NAME,LEVEL,IBUF,MAXPK,OK)
C 
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * APR 21/92 - J.STACEY - REVISED TO IMPLEMENT PORTABILITY ACROSS
C     *                        BOTH 32- AND 64-BIT MACHINES.  
C     * JAN 15/80 - J.D.HENDERSON.
C
C     * SCANS FILE NF FOR A RECORD WHOSE FIRST FOUR LABEL WORDS ARE...
C     *  1) KIND = 4 CHARACTER FIELD TYPE (4HSPEC,4HGRID,ETC).
C     *  2) NSTEP = TIMESTEP NUMBER.
C     *  3) NAME = 4 CHARACTER NAME.
C     *  4) LEVEL = LEVEL NUMBER. 
C 
C     * THEY ARE NOT CHECKED IF THEY ARE SET TO -1. 
C     *  ORDER OF PRIORITY IN THAT CASE IS KIND,NAME,NSTEP,LEVEL. 
C 
C     * UPON RETURNING IBUF CONTAINS THE 8 WORD LABEL 
C     * FOLLOWED BY THE DATA (WHICH MAY BE PACKED).
C     * G CONTAINS THE UNPACKED FIELD.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DIMENSION G(1) 
C 
      logical :: ok
      integer, dimension(9) :: ibuf
! ---------------------------------------------------------------------
! 
!     * First get the record from file NF.
      call recget (nf, kind, nstep, name, level, ibuf, maxpk, ok)
      if (.not.ok) return
! 
!     * Unpack the data into array G. 
      call recup2 (g, ibuf) 
! 
      return
      end

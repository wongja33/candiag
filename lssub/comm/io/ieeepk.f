      SUBROUTINE IEEEPK(NWDS,LCM,IEEEF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * DEC 06/00 - F. MAJAESS - FIX A TYPO ERROR.
C     * OCT 25/96 - F. MAJAESS - ENSURE IEEE "NEGATIVE ZERO" (IF ANY)
C                                CONVERTED INTO IEEE 64-BIT 
C                                "POSITIVE ZERO" SINCE WITHOUT THIS FIX
C                                THE CONVERSION ROUTINES MAY RECOVER 
C                                INCORRECT RESULTS (SUCH AS -2.0 WHEN 
C                                RECONVERTING INTO 32-BIT IEEE).
C
C     * APR 15/92 - J. STACEY -
C     *
C     * PURPOSE: THIS ROUTINE CONVERTS EITHER FROM IBM 32-BIT, IBM 64-BIT
C     *          CRAY 64-BIT, IEEE 32-BIT OR IEEE 64-BIT DATA INTO IEEE 64-BIT
C     *          DATA.  
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION LCM(1),IEEEF(1)
C
C     * INTEGER REPRESENTATIONS FOR THE HIGH ORDER 32-BITS OF SQRT(2)
C     * ARE DIFFERENT ON IBM, CRAY AND IEEE MACHINES.  WE USE THIS TO TEST
C     * FOR THE EXACT TYPE OF MACHINE BEFORE CONVERTING TO THE STANDARD.
C
      PARAMETER  ( IAMIBM=1092001950)
      PARAMETER  (IAMCRAY=1073853700)
      PARAMETER  (IAMIEEE=1073127582)
      REAL*8     A,B
      INTEGER    IA(2),JB
      EQUIVALENCE (A,IA),(B,JB)
      LOGICAL    IBM,CRAY,IEEE,IM64BIT,INTEST2
      INTEGER    IZERO
      DIMENSION  ZERO(1)
      EQUIVALENCE (ZERO,IZERO)
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DATA       ZERO/0.0E0/
C-------------------------------------------------------------------------------
C     * USE THE STANDARD TRICK TO SEE IF WE HAVE 32- OR 64-BIT INTEGERS.
C
      IA(2) = 0
      B = 0.D0
      A          = DSQRT(2.0D0)
      JB         = IA(1)
      IF (A .EQ. B) THEN
        IM64BIT  = .TRUE.
        MSHFM32 = -32
        JB = ISHFT(JB,MSHFM32)
      ELSE
        IM64BIT  = .FALSE.
      END IF
C
C     * TEST FOR IBM, CRAY OR IEEE MACHINE.
C
      IBM        = JB .EQ. IAMIBM
      CRAY       = JB .EQ. IAMCRAY
      IEEE       = JB .EQ. IAMIEEE
C     TEST FOR IEEE ON LITTLE-ENDIAN 32-BIT INTEGER MACHINES
      IF(.NOT.IEEE.AND..NOT.IM64BIT) IEEE = IA(2) .EQ. IAMIEEE
      INTEST2   = INTSIZE .EQ. 2
C
C     * NOW BRANCH ON TYPE OF MACHINE.
C
      IF (IBM  .AND. IM64BIT) THEN
        CALL BF1BI64(LCM,IEEEF,NWDS)
        RETURN
      END IF

      IF (IBM .AND. .NOT.IM64BIT) THEN
        IF (.NOT.INTEST2) THEN !32' INTEGERS/32' REALS.
          CALL BF2BI64(LCM,IEEEF,NWDS)
        ELSE                   !32' INTEGERS/64' REALS.
          CALL BF1BI64(LCM,IEEEF,NWDS)
        END IF
        RETURN
      END IF

      IF (CRAY) THEN
        CALL CF1CI64(LCM,IEEEF,NWDS)
        RETURN
      END IF

      IF (IEEE .AND. IM64BIT) THEN
        MBSE0 = 0
        MBSE63 = 63
        MASKX = IBSET(MBSE0,MBSE63)
        DO 20 I=1,NWDS
          IEEEF(I) = LCM(I)
          MSHF1 = 1
          IF(IEEEF(I).EQ.MASKX) IEEEF(I)=ISHFT(MASKX,MSHF1)
   20   CONTINUE
        RETURN
      END IF

      IF (IEEE .AND. .NOT.IM64BIT) THEN
        K = 1
        MBSE0 = 0
        MBSE31 = 31
        MASK1 = IBSET(MBSE0,MBSE31)
        MASK2 = IBITS(-1,0,8)    !8 BIT MASK FOR IEEE 32' EXPONENT.
        MASK3 = IBITS(-1,0,20)   !20 BIT MASK FOR IEEE 32' MANTISSA.
        MASK4 = IBITS(-1,0,3)    !3 BIT MASK FOR 3' OF IEEE MANTISSA.
        MASK5 = IBITS(-1,0,11)   !11 BIT MASK FOR IEEE 64' EXPONENT.
        IF (.NOT.INTEST2) THEN !INTEGERS ARE 32', REALS ARE 32'.
          DO 30 I=1,2*NWDS-1,2
            IF (LCM(K) .NE. IZERO .AND. LCM(K) .NE. MASK1) THEN
              MSHF20 = 20
              MSHFM3 = -3
              IEEEF(I) = IOR(IOR(
     1                   IAND(LCM(K),MASK1),
     2        ISHFT(IAND(IBITS(LCM(K),23,8)-127+1023,MASK5),MSHF20)
     3                        ),
     4                   IAND(ISHFT(LCM(K),MSHFM3),MASK3)
     5                    )
              MSHF29 = 29
              IEEEF(I+1) = ISHFT(IAND(LCM(K),MASK4),MSHF29)
            ELSE
              IEEEF(I)     = 0
              IEEEF(I+1)   = 0
            END IF
            K = K + 1
   30     CONTINUE
        ELSE        !INTEGERS ARE 32', REALS ARE 64'.
          DO 40 I=1,2*NWDS-1,2
            IEEEF(I) = LCM(I)
            IEEEF(I+1) = LCM(I+1)
            MSHF1 = 1
            IF(IEEEF(I).EQ.MASK1 .AND. IEEEF(I+1).EQ.0 ) 
     1                             IEEEF(I)=ISHFT(MASK1,MSHF1)
   40     CONTINUE
        END IF
        RETURN
      END IF

C     * IF WE GET HERE THEN WE HAVE A FATAL ERROR (WE DON'T KNOW WHAT MACHINE
C     * WE'RE ON.

      WRITE(6,6100)
 6100 FORMAT('0 *** ERROR *** IEEEPK: UNKNOWN MACHINE FORMAT.')
      CALL ABORT
      END

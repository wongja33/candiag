      SUBROUTINE SIGLAB (LSG,LSH,SG,SH,NLEV)
C 
C     * DEC 03/87 - R.LAPRISE.
C     * COMPUTES INTEGER LABEL VALUES OF SIGMA LEVELS 
C     * FOR GENERAL STAGGERED OR NOT VERSION OF GCM.
C     * SG  = SIGMA VALUES AT MID POINT OF MOMENTUM LAYERS. 
C     * SH  = SIGMA VALUES AT MID POINT OF THERMODYNAMIC LAYERS.
C     * LSG = 1000*SG.
C     * LSH = 1000*SH.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL     SG(NLEV), SH(NLEV) 
      INTEGER LSG(NLEV),LSH(NLEV) 
C-----------------------------------------------------------------------
      DO 210 L=1,NLEV 
      LSG(L)=INT(1000.E0*SG(L) + 0.5E0)
      LSH(L)=INT(1000.E0*SH(L) + 0.5E0)
  210 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END

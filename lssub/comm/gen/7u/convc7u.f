      SUBROUTINE CONVC7U (TH,QH,PCP,PRESSG, 
     1                   PBLT,SHLT,TCV,DEL,SHJ,SHTJ,SHXKJ,
     2                   ILEV,ILEVM,LEV,ILG,IL1,IL2,MSG,NUPS,NSUPS, 
     3                   RGOCP,SH,DELT,IFIZPHS,TWODTI,IBETTS, 
     4                   TCROW,QCROW,MILEV,MILEVM,MILEVP, 
     5                   TMOIS,QMOIS,P,ST,ALFA, 
     6                   PCPDH,ADFCTR,BSENG,SBSENG,SUMDEL,
     7                   SUMQ,CAPE,DEEPSUM,SUMDQ,FLXM,
     8                   SUMDT,GBLFAC,DPTOP,SUM1,SUM2,
     9                   CORFAC,DTCOR,QAD,BETA,TLIFT, 
     A                   R1DEEP,R2DEEP,TREF,GADEEP,GBDEEP,
     B                   GBRATIO,DPRES,GAL,GBL,GAMM,
     C                   QGRAD,UGRAD,VGRAD,SMU,CRH, 
     D                   DSPCRT,ETA,H,TBASE,DSPBP,
     E                   DMSE,DTMP,DSPH,H0,CLSTFAC, 
     F                   HTHRESH,EST,QST,HTL,ELIFT, 
     G                   QLIFT,TAD,T1,T2,QSHAL, 
     H                   GAMC,GAMS,DQLBT,PLIFT,IDEEP, 
     I                   ISHL,JTOP,JBASE,ICON)
  
C     * MAY 09/91 - M.LAZARE. A. TOP-OF-CONVECTION FIELD ("TCV")
C     *                          ADDED AND CALCULATED.
C     *                       B. PCP,TCV FIELDS INITIALIZED NOW ONLY IF 
C     *                          USING BETTS-MILLER (IBETTS.NE.0).
C     *                       C. NO SHALLOW MIXING PERMITTED IN 
C     *                          LOWEST MODEL LEVEL.
C     *                       D. "SUMEN" RESTRICTION ON SHALLOW 
C     *                          MIXING REMOVED.
C     *                       E. LESS RESTRICTIVE "ETA1" CRITERION USED.
C     *                       F. RE-ORGANIZED SO THAT CAN BE USED IN
C     *                          CLOUD DIAGNOSTIC (SINCE CALLED BEFORE
C     *                          CLOUDS IN PHYSICS NOW).
C     *                       G. CHANGED DEFINITION OF GAMCR BASED ON 
C     *                          "CLOUD" COVER (IN CONSISTENT MANNER
C     *                          AS CLOUDS DEFINITION). 
C     *                       H. TOP-OF-SHALLOW MIXING ARRAY PASSED OUT 
C     *                          FOR USE IN CLOUDS SUBROUTINE.
  
C     * AUG 14/90 - M.LAZARE. MOIST CONVECTIVE ADJUSTMENT REMOVED 
C     *                       AND REPLACED BY SHALLOW AND DEEP
C     *                       CONVECTION(BETTS-MILLER) PARAMETRIZATIONS.
C     *                       HOLE-FILLING MOVED TO PHYSICS.
  
C     *    PERFORMS SHALLOW MIXING AND DEEP CONVECTIVE ADJUSTMENT.
  
C     **************** INDEX OF VARIABLES *********************** 
C     *  I    => INPUT ARRAYS.
C     * I/O   => INPUT/OUTPUT ARRAYS. 
C     *  W    => WORK ARRAYS. 
C     * IC    => INPUT DATA CONSTANTS.
C     *  C    => DATA CONSTANTS PERTAINING TO SUBROUTINE ITSELF.
  
C  I  * DEL      LOCAL SIGMA HALF-LEVEL THICKNESS (I.E. DSHJ).
C  IC * DELT     LENGTH OF MODEL TIME-STEP IN SECONDS.
C  W  * GAMS     SATURATED ADIABATIC LAPSE RATE.
C  W  * HTHRESH  THRESHOLD RELATIVE HUMIDITY FOR PRECIPITATION ONSET. 
C  IC * IBETTS   OPTION TO DO BETTS-MILLER DEEP SCHEME (=1) OR NOT (=0).
C  W  * ICON     HOLDS POSITION OF GATHERED POINTS VS LONGITUDE INDEX.
C  W  * IDEEP    INDICATES WHETHER LEVEL IS UNDERGOING DEEP CONVECTION. 
C IC  * ILEV     NUMBER OF MODEL LEVELS.
C IC  * ILEVM    ILEV-1.
C IC  * ILG      LON+2 = SIZE OF GRID SLICE.
C IC  * IL1,IL2  START AND END LONGITUDE INDICES FOR LATITUDE CIRCLE. 
C  W  * ISHL     INDICATES WHETHER LEVEL IS UNDERGOING SHALLOW MIXING.
C  W  * JBASE    BASE LEVEL INDEX OF DEEP CUMULUS CONVECTION. 
C  W  * JTOP     TOP  LEVEL INDEX OF DEEP CUMULUS CONVECTION. 
C IC  * LEV      ILEV+1.
C IC  * MSG      NUMBER OF MISSING MOISTURE LEVELS AT THE TOP OF MODEL. 
C  C  * NSUPS    NUMBER OF POINTS UNDERGOING SHALLOW MIXING.
C  C  * NUPS     NUMBER OF POINTS UNDERGOING DEEP CONVECTION. 
C  W  * P        GRID SLICE OF AMBIENT PRESSURE IN MBS. 
C I/O * PCP      ROW OF PRECIPITATION RATE. 
C  W  * PCPDH    SCALED SURFACE PRESSURE. 
C  I  * PRESSG   ROW OF SURFACE PRESSURE IN PA. 
C I/O * QH       GRID SLICE OF SPECIFIC HUMIDITY. 
C IC  * RGOCP    RGAS/CP = "KAPA".
C  I  * SH       USER-DEFINED ETA MID-LAYER THERMODYNAMIC VALUES. 
C  I  * SHJ      GRID SLICE OF LOCAL HALF-LEVEL SIGMA VALUES. 
C  O  * SHLT     ROW OF TOP-OF-SHALLOW MIXING INDICES.
C  I  * SHTJ     GRID SLICE OF LOCAL HALF-LEVEL SIGMA INTERFACE VALUES. 
C  I  * SHXKJ    GRID SLICE OF LOCAL SH**KAPA.
C  W  * ST       STABILITY MATRICES.
C  O  * TCV      ROW OF TOP-OF-CONVECTION INDICES.
C I/O * TH       GRID SLICE OF TEMPERATURE. 
  
C     *********************************************************** 
  
C     * MULTI-LEVEL I/O FIELDS: 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL TH(ILG,MILEV),  QH(ILG,MILEV), 
     1    DEL(ILG,MILEV), SHJ(ILG,MILEV), SHTJ(ILG,MILEVP), 
     2  SHXKJ(ILG,MILEV),  SH(MILEV)
  
C     * MULTI-LEVEL "FIZ" FIELDS (IF IFIZPHS.NE.0): 
  
      REAL TCROW(ILG,MILEV),  QCROW(ILG,MILEV)
  
C     * SINGLE-LEVEL I/O FIELDS:  
  
      REAL     PCP(ILG),  PRESSG(ILG),    PBLT(ILG),    SHLT(ILG),
     1         TCV(ILG) 
  
C     * WORK FIELDS:  
  
      REAL   TMOIS(ILG,MILEV),   QMOIS(ILG,MILEV),     P(ILG,MILEV) 
      REAL     TAD(ILG,MILEV),      T1(ILG,MILEV),    T2(ILG,MILEV) 
      REAL   QSHAL(ILG,MILEV),    GAMC(ILG,MILEV),  GAMS(ILG,MILEV) 
      REAL   DQLBT(ILG,MILEV),   PLIFT(ILG,MILEV) 
      REAL      ST(ILG,MILEVM,6)
  
      REAL    ALFA(ILG),   PCPDH(ILG),  ADFCTR(ILG),   BSENG(ILG) 
      REAL  SBSENG(ILG),  SUMDEL(ILG),    SUMQ(ILG),    CAPE(ILG) 
      REAL DEEPSUM(ILG),   SUMDQ(ILG),    FLXM(ILG),   SUMDT(ILG) 
      REAL  GBLFAC(ILG),   DPTOP(ILG),    SUM1(ILG),    SUM2(ILG) 
      REAL  CORFAC(ILG),   DTCOR(ILG),     QAD(ILG),    BETA(ILG) 
      REAL   TLIFT(ILG),  R1DEEP(ILG),  R2DEEP(ILG),    TREF(ILG) 
      REAL  GADEEP(ILG),  GBDEEP(ILG), GBRATIO(ILG),   DPRES(ILG) 
      REAL     GAL(ILG),     GBL(ILG),    GAMM(ILG),   QGRAD(ILG) 
      REAL   UGRAD(ILG),   VGRAD(ILG),     SMU(ILG),     CRH(ILG) 
      REAL  DSPCRT(ILG),     ETA(ILG),       H(ILG),   TBASE(ILG) 
      REAL   DSPBP(ILG),    DMSE(ILG),    DTMP(ILG),    DSPH(ILG) 
      REAL      H0(ILG), CLSTFAC(ILG), HTHRESH(ILG),     EST(ILG) 
      REAL     QST(ILG),     HTL(ILG),   ELIFT(ILG),   QLIFT(ILG) 
  
  
      INTEGER IDEEP(ILG),    ISHL(ILG),    JTOP(ILG),   JBASE(ILG)
      INTEGER  ICON(ILG)
  
      COMMON/ADJPCP/ HC,HF,HM,AA,DEPTH,LHEAT,MOIADJ,MOIFLX
      COMMON/EPS   / A, B, EPS1, EPS2 
      COMMON/EPSICE/ AICE,BICE,TICE,QMIN
      COMMON/GAMS  / EPSS,CAPA
      COMMON/HTCP  / T1S,T2S,AI,BI,AW,BW,SLP
  
C     * STATEMENT FUNCTIONS DEFENITIONS.
  
C     * A) CRITICAL SATURATION RELATIVE HUMIDITY. 
  
      CR(HM,HHH,AA)     =     MERGE( AA*(HHH-HM)**3,
     &  AA*(2.E0-HM-HHH)**3+HHH-1.E0,
     &  HHH.LE.1.E0) 
      CRIRLH(HM,HHH,AA) = HHH -
     &  MERGE( CR(HM,HHH,AA), HHH-1.E0, HM+HHH.LE.2)
  
C     * B) COMPUTES THE RATIO OF LATENT HEAT OF VAPORIZATION OF 
C     *    WATER OR ICE TO THE SPECIFIC HEAT OF AIR AT CONSTANT 
C     *    PRESSURE CP. 
  
      TW(TTT)     = AW-BW*TTT 
      TI(TTT)     = AI-BI*TTT 
      HTVOCP(TTT) = MERGE( TW(TTT), 
     1              MERGE( TI(TTT), 
     2                     SLP*((TTT-T2S)*TW(TTT)+(T1S-TTT)*TI(TTT)), 
     3                     TTT.LE.T2S), 
     4                     TTT.GE.T1S)
  
C     * C) COMPUTES THE SATURATION VAPOUR PRESSURE OVER WATER OR ICE. 
  
      ESW(TTT)    = EXP(A-B/TTT)
      ESI(TTT)    = EXP(AICE-BICE/TTT)
      ESTEFF(TTT) = MERGE( ESW(TTT),
     1              MERGE( ESI(TTT),
     2                     SLP*((TTT-T2S)*ESW(TTT)+(T1S-TTT)*ESI(TTT)), 
     3                     TTT.LE.T2S), 
     4                     TTT.GE.T1S)
  
      DATA TSCALDP /7200.E0/
      DATA H0LOW /0.70E0/, H0HI/0.90E0/ 
C-----------------------------------------------------------------------
C     * CALCULATE ROW OF STABILITY MATRICES.
C 
      CALL STAWCL3 (ST,SHTJ(1,2),SHTJ,SHJ,
     1              ILEVM,ILEV,ILEV+1,IL1,IL2,ILG,RGOCP,
     2              MILEVM,MILEV) 
C 
C     * INITIALIZE NECESSARY ARRAYS.
C 
      DO 50 IL=IL1,IL2
         ICON(IL)  = IL 
         IDEEP(IL) = -1 
         ISHL(IL)  = -1 
         IF(IBETTS.NE.0)           PCP(IL) = 0.E0 
         SUMQ(IL)  = 0.E0 
         PCPDH(IL) = PRESSG(IL)*DEPTH 
         IF(IBETTS.NE.0)           TCV(IL) = FLOAT(ILEV)
   50 CONTINUE
C 
      DO 60 L=1,ILEV
      DO 60 IL=IL1,IL2
         P(IL,L)   = SHJ(IL,L) * PRESSG(IL) * 0.01E0
   60 CONTINUE
C 
C     ******************************************************************
C     * SHALLOW MIXING *************************************************
C     ******************************************************************
C 
C     * INITIALIZE NECESSARY VALUES.
C 
      DO 70 IL=IL1,IL2
         LEVSM=NINT(PBLT(IL)) 
         SHLT(IL)=FLOAT(LEVSM)
         IF(NINT(PBLT(IL)).EQ.ILEV)                        ISHL(IL)=-99 
         DSPCRT(IL)=1.0E0 
         ESBASE=EXP(A-B/TH(IL,LEVSM)) 
         SHBASE=EPS1*ESBASE/(P(IL,LEVSM)-EPS2*ESBASE) 
         HTL(IL)=AW-BW*TH(IL,LEVSM) 
         GA=HTL(IL)*SHBASE/(CAPA*TH(IL,LEVSM))
         GB=GA*EPSS*HTL(IL)/TH(IL,LEVSM)
         TBASE(IL)=TH(IL,LEVSM)-HTL(IL)*(SHBASE-QH(IL,LEVSM))/(1.E0+GB) 
   70 CONTINUE
C 
C     * MAJOR CALCULATION AND ADJUSTMENT. 
C 
C     * THE ARRAY "ISHL" CONTROLS WHETHER SHALLOW MIXING IS PERFORMED 
C     * OR NOT. IT IS INITIALIZED TO -1 AND RE-SET TO 1 (I.E. SHALLOW 
C     * MIXING WILL OCCUR) ONLY AT THE TOP OF THE PBL. IT IS THEN SET 
C     * TO ZERO AT THE LEVEL WHERE A "NEGATIVE BUOYANCY" CONDITION
C     * EXISTS AND NO FURTHER SHALLOW MIXING IS PERMITTED IN THE COLUMN.
C     * NO SHALLOW MIXING IS PERMITTED UNDER STABLE SURFACE CONDITIONS
C     * (PBLT=ILEV), SINCE ISHL IS SET TO -99 (SEE ABOVE LOOP). 
C 
      DO 90 L=ILEVM,MSG+2,-1
         LM=L-1 
         DO 80 IL=IL1,IL2 
            IF(L.LE.NINT(PBLT(IL)) .AND. ISHL(IL).EQ.-1)    ISHL(IL) = 1
            R1=ST(IL,LM,1)+ST(IL,LM,3)
            R2=ST(IL,LM,3)-ST(IL,LM,2)
            GAMM(IL)=R1*TH(IL,LM)-R2*TH(IL,L) 
            QGRAD(IL)=QH(IL,L)-QH(IL,LM)
            SMU(IL)=DEL(IL,L)*DEL(IL,LM)/(DEL(IL,L)+DEL(IL,LM)) 
            ALFA(IL)=DEL(IL,L)*DEL(IL,LM)/(R1*DEL(IL,L)+R2*DEL(IL,LM))
            IF(GAMM(IL).LT.0.E0.AND.ISHL(IL).EQ.1) THEN
               TH(IL,L)  = TH(IL,L) +ALFA(IL)*GAMM(IL)/DEL(IL,L)
               TH(IL,LM) = TH(IL,LM)-ALFA(IL)*GAMM(IL)/DEL(IL,LM) 
               GAMM(IL)  = 0.0E0
C              QH(IL,L)  = QH(IL,L) -SMU(IL)*QGRAD(IL)/DEL(IL,L)
C              QH(IL,LM) = QH(IL,LM)+SMU(IL)*QGRAD(IL)/DEL(IL,LM) 
C              QGRAD(IL) = 0.0
            ENDIF 
            DENOM=DEL(IL,L)+DEL(IL,LM)
            TT=(TH(IL,L)*DEL(IL,L)+TH(IL,LM)*DEL(IL,LM))/DENOM
            QQ=(QH(IL,L)*DEL(IL,L)+QH(IL,LM)*DEL(IL,LM))/DENOM
            EST(IL)=EXP(A-B/TT) 
            PP=P(IL,L)*ST(IL,LM,6)
            QST(IL)=EPS1*EST(IL)/(PP-EPS2*EST(IL))
            HTL(IL)=AW-BW*TT
            GA=HTL(IL)*QST(IL)/(CAPA*TT)
            GB=GA*EPSS*HTL(IL)/TT 
            GAMS(IL,L)=TT*(GB-GA)/(1.E0+GB) 
            RATIO=(1.E0+0.1E0*GB+0.9E0*GA)/(1.E0+GB)
            R1TBASE=ST(IL,LM,1)*RATIO+ST(IL,LM,3) 
            R2TBASE=ST(IL,LM,3)-RATIO*ST(IL,LM,2) 
            TBASE(IL)=R2TBASE*TBASE(IL)/R1TBASE 
            DTEXC=TBASE(IL)-TH(IL,LM) 
C           GAMCR=0.1*(1.+GB)*GAMS(IL,L)
C           GAMCR=(1.+GB)*(GAMS(IL,L)-MIN(0.9*GAMS(IL,L),GAMM(IL))) 
C 
            H(IL)=MAX(QH(IL,L),QMIN)/QST(IL)
            IF(L.LE.NINT(PBLT(IL)))                                 THEN
              HTHRESH(IL)=HM
              H0(IL)=HM 
            ELSE
              HTHRESH(IL)=0.99E0
              H0(IL)=0.99E0 
            ENDIF 
            IF(H(IL).GT.HTHRESH(IL))                                THEN
               CRH(IL)   = CRIRLH(HTHRESH(IL),H(IL),AA) 
            ELSE
               CRH(IL)   = MIN(H(IL),1.E0)
            ENDIF 
            CLSTFAC(IL)=(MAX(GAMM(IL)-GAMS(IL,L),0.E0)/GAMS(IL,L))**2 
            CCLD=(MAX(CRH(IL)-H0(IL),0.E0))/(1.E0-H0(IL)) 
            CVEC=CCLD*(1.E0+CLSTFAC(IL))/(1.E0+CCLD*CLSTFAC(IL))
            GMCRIT=CVEC     *0.9E0*GAMS(IL,L) 
     1            +(1.E0-CVEC)*MIN(GAMM(IL),0.9E0*GAMS(IL,L)) 
            GAMCR=(1.E0+GB)*(GAMS(IL,L)-GMCRIT) 
C 
            DENOM1=CAPA*LOG(P(IL,L)/P(IL,LM))
            GAMFAC=GB/GA
            ANUM=CAPA*QGRAD(IL)/DENOM1+QQ*GAMFAC*GAMM(IL)/TT
            DENOM2=QQ*(GAMFAC-1.E0) 
            DSPBP(IL)=ANUM/DENOM2 
            DMSE(IL)=HTL(IL)*QGRAD(IL)/DENOM1-DSPBP(IL)*GAMCR -GAMM(IL) 
            DSPCRT(IL)=MAX(GAMM(IL)/GAMS(IL,L),1.0E0) 
            IF(DSPBP(IL).GT.DSPCRT(IL).AND.DMSE(IL).GT.0.E0 
     1         .AND.QGRAD(IL).GT.0.E0   .AND.ISHL(IL).EQ.1) THEN
               FLXM(IL)  = (2.E0*DELT/TSCALDP)*(DSPBP(IL)-DSPCRT(IL)) 
     1                     /DSPBP(IL) 
               DTMP(IL)  = ALFA(IL)*GAMM(IL)*FLXM(IL) 
               DSPH(IL)  = SMU(IL)*QGRAD(IL)*FLXM(IL) 
               TH(IL,L)  = TH(IL,L) +DTMP(IL)/DEL(IL,L) 
               TH(IL,LM) = TH(IL,LM)-DTMP(IL)/DEL(IL,LM)
               QH(IL,L)  = QH(IL,L) -DSPH(IL)/DEL(IL,L) 
               QH(IL,LM) = QH(IL,LM)+DSPH(IL)/DEL(IL,LM)
               SHLT(IL)=FLOAT(LM) 
            ELSE IF(ISHL(IL).EQ.1)                                  THEN
               ISHL(IL)=0 
            ENDIF 
   80    CONTINUE 
   90 CONTINUE
  
      IF(IFIZPHS.NE.0)                                              THEN
         DO 95 L=1,ILEV 
         DO 95 IL=IL1,IL2 
            TCROW(IL,L) = TH(IL,L)
            QCROW(IL,L) = QH(IL,L)
   95    CONTINUE 
      ENDIF 
C 
      IF(IBETTS.EQ.0)                                      RETURN 
C     ******************************************************************
C       MODIFIED BETTS-MILLER DEEP MOIST ADJUSTMENT 
C     ******************************************************************
C 
C     * DETERMINE LAUNCHING LEVEL(JBASE) QUANTITIES AND INITIALIZE SUMS.
C     * JBASE IS SET TO THE TOP OF THE PBL (AS DETERMINED IN VRTDFS_).
C 
      DO 100 L=ILEV,MSG+1,-1
      DO 100 IL=IL1,IL2 
        IF(L .GE. MIN( ILEV-1, NINT(PBLT(IL)) ))                THEN 
          JTOP(IL)=L
          JBASE(IL)=L 
          HTL(IL)=AW-BW*TH(IL,L)
          EST(IL)=EXP(A-B/TH(IL,L)) 
          QST(IL)=EPS1*EST(IL)/(P(IL,L)-EPS2*EST(IL)) 
          GADEEP(IL)=HTL(IL)*QST(IL)/(CAPA*TH(IL,L))
          GBDEEP(IL)=GADEEP(IL)*EPSS*HTL(IL)/TH(IL,L) 
          TREF(IL)=TH(IL,L)+HTL(IL)*(QH(IL,L)-QST(IL))/(1.E0+GBDEEP(IL))
          ETA(IL)=MAX( (273.E0-TREF(IL))/10.E0 , 0.E0 ) 
          DPRES(IL)=60.E0-30.E0*(ETA(IL)**2)/(1.E0+ETA(IL)**2)
          PLIFT(IL,L)=P(IL,L)-DPRES(IL) 
          TLIFT(IL)=TREF(IL)*(PLIFT(IL,L)/P(IL,L))**CAPA
          ELIFT(IL)=EXP(A-B/TLIFT(IL))
          QLIFT(IL)=EPS1*ELIFT(IL)/(PLIFT(IL,L)-EPS2*ELIFT(IL)) 
          BSENG(IL)=HTL(IL)*MAX(QH(IL,L)-(QH(IL,L)-QST(IL))/
     1                           (1.E0+GBDEEP(IL)) - QLIFT(IL), 0.E0 )
          SBSENG(IL)=BSENG(IL)*DEL(IL,L)/(1.E0+GBDEEP(IL))
          GAL(IL)=HTL(IL)*QLIFT(IL)/(CAPA*TLIFT(IL))
          GBL(IL)=GAL(IL)*EPSS*HTL(IL)/TLIFT(IL)
          GBLFAC(IL)=(1.E0+GBL(IL)) 
          SUMDEL(IL)=DEL(IL,L)*GBLFAC(IL)/(1.E0+GBDEEP(IL)) 
          DQLBT(IL,L)=GBL(IL) 
          HTL(IL)=AW-BW*TREF(IL)
          EST(IL)=EXP(A-B/TREF(IL)) 
          QST(IL)=EPS1*EST(IL)/(P(IL,L)-EPS2*EST(IL)) 
          GADEEP(IL)=HTL(IL)*QST(IL)/(CAPA*TREF(IL))
          GBDEEP(IL)=GADEEP(IL)*EPSS*HTL(IL)/TREF(IL) 
          GAMS(IL,L)=TREF(IL)*(GBDEEP(IL)-GADEEP(IL))/(1.E0+GBDEEP(IL)) 
          GAMC(IL,L)=0.90E0*GAMS(IL,L)
          CAPE(IL)=DEL(IL,L)*(TREF(IL)-TH(IL,L))
          QSHAL(IL,L)=QLIFT(IL) 
          DEEPSUM(IL)=DEL(IL,L)*(TREF(IL)-TH(IL,L)) 
          SUMDT(IL)=DEEPSUM(IL) 
          SUMDQ(IL)=HTL(IL)*DEL(IL,L)*(QH(IL,L)-QLIFT(IL))
          TBASE(IL)=TREF(IL)
          T1(IL,L)=TREF(IL) 
          T2(IL,L)=TREF(IL) 
          TAD(IL,L)=TREF(IL)
        ENDIF 
  100 CONTINUE
C 
C     * CALCULATE DEPTH OF DEEP CONVECTION (JTOP), FIRST GUESS HUMIDITY 
C     * PROFILE AND SET UP SATURATION PRESSURE PROFILE. 
C 
      DO 150 L=ILEV-1,MSG+1,-1
      DO 150 IL=IL1,IL2 
        IF(L.LT.JBASE(IL) .AND. IDEEP(IL).EQ.-1)    IDEEP(IL) = 1 
        R1DEEP(IL)=ST(IL,L,1)+ST(IL,L,3)
        R2DEEP(IL)=ST(IL,L,3)-ST(IL,L,2)
        T1(IL,L)=(R2DEEP(IL)*T1(IL,L+1)+GAMS(IL,L+1))/R1DEEP(IL)
        IF( (T1(IL,L).GE.TH(IL,L).OR.DEEPSUM(IL).LE.0.E0) .AND. 
     1      IDEEP(IL).EQ.1) THEN
          DEEPSUM(IL)=DEEPSUM(IL)+DEL(IL,L)*(T1(IL,L)-TH(IL,L))*
     1                P(IL,JBASE(IL))/P(IL,L) 
          T2(IL,L)=(R2DEEP(IL)*T2(IL,L+1)+GAMC(IL,L+1))/R1DEEP(IL)
          ETA(IL)=MAX((273.E0-T1(IL,L))/10.E0,0.E0) 
          DTMP(IL)=MAX(T1(IL,L)-T2(IL,L),0.0E0) 
C ****    TREF(IL)=T2(IL,L)+DTMP(IL)*(ETA(IL)**2)/(1.+ETA(IL)**2) 
          TREF(IL)=T2(IL,L) 
          DTMP(IL)=TREF(IL)-TH(IL,L)
          CAPE(IL)=CAPE(IL)+DTMP(IL)*DEL(IL,L)*P(IL,JBASE(IL))/P(IL,L)
          TAD(IL,L)=TREF(IL)
          JTOP(IL)=L
          SUMDT(IL)=SUMDT(IL)+DEL(IL,L)*DTMP(IL)
          HTL(IL)=AW-BW*TREF(IL)
          EST(IL)=EXP(A-B/T1(IL,L)) 
          QST(IL)=EPS1*EST(IL)/(P(IL,L)-EPS2*EST(IL)) 
          GADEEP(IL)=HTL(IL)*QST(IL)/(CAPA*T1(IL,L))
          GBDEEP(IL)=GADEEP(IL)*EPSS*HTL(IL)/T1(IL,L) 
          GBRATIO(IL)=P(IL,JBASE(IL))/(P(IL,L)*(1.E0+GBDEEP(IL))) 
          SBSENG(IL)=SBSENG(IL)+ BSENG(IL)*DEL(IL,L)*GBRATIO(IL)* 
     1                          T1(IL,L)/TBASE(IL)
          SUMDEL(IL)=SUMDEL(IL)+GBLFAC(IL)*DEL(IL,L)*GBRATIO(IL)* 
     1                          T1(IL,L)/TBASE(IL)
          GAMS(IL,L)=T1(IL,L)*(GBDEEP(IL)-GADEEP(IL))/(1.E0+GBDEEP(IL)) 
          GAMC(IL,L)=0.90E0*GAMS(IL,L)*T2(IL,L)/T1(IL,L)
          ETA(IL)=MAX( (273.E0-T1(IL,L))/10.E0 , 0.E0 ) 
          DPRES(IL)=60.E0-30.E0*(ETA(IL)**2)/(1.E0+ETA(IL)**2)
          PLIFT(IL,L)=P(IL,L)-MIN(DPRES(IL),P(IL,L)-P(IL,1))
          TLIFT(IL)=TREF(IL)*((PLIFT(IL,L)/P(IL,L))**CAPA)
          ELIFT(IL)=EXP(A-B/TLIFT(IL))
          QLIFT(IL)=EPS1*ELIFT(IL)/(PLIFT(IL,L)-EPS2*ELIFT(IL)) 
          GAL(IL)=HTL(IL)*QLIFT(IL)/(CAPA*TLIFT(IL))
          GBL(IL)=GAL(IL)*EPSS*HTL(IL)/TLIFT(IL)
          DQLBT(IL,L)=GBL(IL) 
          QSHAL(IL,L)=QLIFT(IL) 
          SUMDQ(IL)=SUMDQ(IL)+HTL(IL)*DEL(IL,L)*(QH(IL,L)-QLIFT(IL))
        ELSE
          IDEEP(IL)=MIN(IDEEP(IL),0) 
        ENDIF 
  150 CONTINUE
C 
C     * TEST CONDITIONS TO SEE IF DEEP WILL BE DONE (IDEEP=1).
C 
      DO 175 IL=IL1,IL2 
         IDEEP(IL)=1
         IF(JTOP(IL).GT.JBASE(IL)-2)                         IDEEP(IL)=0
         DPTOP(IL)=P(IL,JBASE(IL))-P(IL,JTOP(IL)) 
         CAPE(IL)=CAPE(IL)+SBSENG(IL) 
C        IF(DEEPSUM(IL).LE.1.46.OR.CAPE(IL).LE.0.0)          IDEEP(IL)=0
         IF(SUMDT(IL).LE.0.E0.OR.SUMDQ(IL).LE.0.E0) IDEEP(IL)=0
         SUM1(IL)=0.0E0 
         SUM2(IL)=0.0E0 
         IF(IDEEP(IL).EQ.1)                                  NUPS=NUPS+1
         IF(IDEEP(IL).EQ.1)                     TCV(IL)=FLOAT(JTOP(IL))
  175 CONTINUE
C 
C     * FIRST ITERATION OF TEMPERATURE CORRECTION PROFILE.
C 
      DO 200 L=MSG+1,ILEV 
      DO 200 IL=IL1,IL2 
        IF(L.GE.JTOP(IL) .AND. L.LE.JBASE(IL) .AND. IDEEP(IL).EQ.1) THEN
          ETA(IL)=(P(IL,L)-P(IL,JTOP(IL)))/DPTOP(IL)
          T1(IL,L)=ETA(IL)*(2.E0-ETA(IL))*
     &      (P(IL,L)/P(IL,JBASE(IL)))**CAPA 
          T2(IL,L)=ETA(IL)*((1.E0-ETA(IL))**2)*(P(IL,L)/P(IL,JBASE(IL)))
     1             **CAPA 
          SUM1(IL)=SUM1(IL)+DEL(IL,L)*T1(IL,L)*P(IL,JBASE(IL))/P(IL,L)
          SUM2(IL)=SUM2(IL)+DEL(IL,L)*T2(IL,L)*P(IL,JBASE(IL))/P(IL,L)
        ENDIF 
  200 CONTINUE
C 
C     * CALCULATE CORRECTION FACTOR AND RE-INITIALIZE SUMS. 
C 
      DO 225 IL=IL1,IL2 
        IF(IDEEP(IL).EQ.1)                            THEN
          CORFAC(IL)=(SUMDEL(IL)-SUM1(IL))/SUM2(IL) 
          SUM1(IL)=0.E0 
          SUM2(IL)=0.E0 
        ENDIF 
  225 CONTINUE
C 
C     * SECOND ITERATION OF TEMPERATURE CORRECTION PROFILE. 
C 
      DO 250 L=MSG+1,ILEV 
      DO 250 IL=IL1,IL2 
        IF(L.GE.JTOP(IL) .AND. L.LE.JBASE(IL) .AND. IDEEP(IL).EQ.1) THEN
          ETA(IL)=(P(IL,L)-P(IL,JTOP(IL)))/DPTOP(IL)
          T1(IL,L)=ETA(IL)*(2.E0-ETA(IL))*
     &      (P(IL,L)/P(IL,JBASE(IL)))**CAPA 
          T2(IL,L)=CORFAC(IL)*ETA(IL)*((1.E0-ETA(IL))**2)*(P(IL,L)/ 
     1             P(IL,JBASE(IL)))**CAPA 
          TMOIS(IL,L)=(T1(IL,L)+T2(IL,L)) 
          SUM1(IL)=SUM1(IL)+DEL(IL,L)*(1.E0+DQLBT(IL,L))*TMOIS(IL,L)
          SUM2(IL)=SUM2(IL)+DEL(IL,L)*TMOIS(IL,L) 
        ENDIF 
 250  CONTINUE
C 
C     * RE-INITIALIZE CORRECTION AND SUM2.
C 
      DO 275 IL=IL1,IL2 
        IF(IDEEP(IL).EQ.1)                            THEN
          DTCOR(IL)=(SUMDQ(IL)-SUMDT(IL))/SUM1(IL)
          SUM2(IL)=SUM2(IL)*DTCOR(IL) 
        ENDIF 
  275 CONTINUE
C 
C     * FINAL CORRECTON OF MOISTURE PROFILE.
C 
      DO 300 L=MSG+1,ILEV 
      DO 300 IL=IL1,IL2 
        IF(L.GE.JTOP(IL) .AND. L.LE.JBASE(IL) .AND. IDEEP(IL).EQ.1) THEN
          HTL(IL)=AW-BW*TAD(IL,L) 
          TMOIS(IL,L)=DTCOR(IL)*TMOIS(IL,L)+TAD(IL,L) 
          TLIFT(IL)=TMOIS(IL,L)*(PLIFT(IL,L)/P(IL,L))**CAPA 
          ELIFT(IL)=EXP(A-B/TLIFT(IL))
          QLIFT(IL)=EPS1*ELIFT(IL)/(PLIFT(IL,L)-EPS2*ELIFT(IL)) 
          QMOIS(IL,L)=QLIFT(IL) 
          SUM2(IL)=SUM2(IL)+DEL(IL,L)*HTL(IL)*(QLIFT(IL)-QSHAL(IL,L)) 
        ENDIF 
  300 CONTINUE
C 
C     * CALCULATE BETA AND ADFCTR REQUIRED FOR ADJUSTMENT.
C 
      DO 325 IL=IL1,IL2 
        IF(IDEEP(IL).EQ.1)                            THEN
          BETA(IL)=DTCOR(IL)*SUM1(IL)/SUM2(IL)
          ADFCTR(IL)=2.E0*DELT/TSCALDP
        ENDIF 
  325 CONTINUE
C 
C     * FINAL ADJUSTMENT AND PRECIPITATION. 
C 
      DO 350 L=MSG+1,ILEV 
      DO 350 IL=IL1,IL2 
        IF(L.GE.JTOP(IL) .AND. L.LE.JBASE(IL) .AND. IDEEP(IL).EQ.1) THEN
          QAD(IL)=QSHAL(IL,L)+BETA(IL)*(QMOIS(IL,L)-QSHAL(IL,L))
          DTMP(IL)=ADFCTR(IL)*(TAD(IL,L)+BETA(IL)*(TMOIS(IL,L)- 
     1             TAD(IL,L))-TH(IL,L)) 
          DSPH(IL)=ADFCTR(IL)*(QAD(IL)-QH(IL,L))
          TH(IL,L)=TH(IL,L)+DTMP(IL)
          QH(IL,L)=QH(IL,L)+DSPH(IL)
          SUMQ(IL)=SUMQ(IL)-DEL(IL,L)*DSPH(IL)
        ENDIF 
  350 CONTINUE
C 
C     * OBTAIN FINAL PRECIPITATION RATE.
C 
      DO 400 IL=IL1,IL2 
          PCP(IL) = PCP(IL) + PCPDH(IL) * MAX(SUMQ(IL),0.E0)
  400 CONTINUE
  
      RETURN
      END 

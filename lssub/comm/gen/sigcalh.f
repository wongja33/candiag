      SUBROUTINE SIGCALH
     1                  (DSGJ,DSHJ,DLNSGJ,
     2                   D1SGJ,A1SGJ,B1SGJ, 
     3                   D2SGJ,A2SGJ,B2SGJ, 
     4                   NI2,NI,NK, 
     5                   PTOIT, 
     6                   PSJ,AG,BG,AH,BH,ALFMOD)

C     * DEC 15/92 - M.LAZARE. REMOVE CK,DBK OUT OF INNER 1002 LOOP
C     *                       TO ENSURE VECTORIZATION ON LONGITUDES. 
C     * MAR 15/88 - R.LAPRISE, CLAUDE GIRARD
C
C     * CALCUL DES PARAMETRES DE LA DISCRETISATION VERTICALE  DU
C     * MODELE SPECTRAL, SEMI-IMPLICITE, ELEMENTS FINIS CONSTANTS 
C     * A VARIABLES INTERCALEES (OU NON). 
C     * COUCHES D'EPAISSEURS VARIABLES, 
C     * TANT DANS L'HORIZ. (COORD. HYB.) QUE DANS LA VERT.
C     * UTILISATION DANS DYNCAL.
C     * 
C     * ON CALCULE
C 
C     * DSGJ(I,K)    : EPAISSEURS SIGMA DES COUCHES DU VENT.
C     * DSHJ(I,K)    : EPAISSEURS SIGMA DES COUCHES DE TEMPERATURE. 
C     * DLNSGJ(I,K)  : EPAISSEURS LOGJ(SIGMA) DES COUCHES DE TEMPERATURE. 
C     *                POUR CONVERSION DIAGNOSTIQUE T A PHI CONSISTANTE.
C     * D1SGJ(I,K)   : EPAISSEURS SIGMA ENTRE HAUT DE COUCHE DU VENT
C     *                ET BAS DE COUCHE DE TEMPERATURE. 
C     * A1SGJ(I,K)   : POIDS DE LA S-COUCHE D1SGJ POUR LE CALCUL DU 
C     *                GEOPOTENTIEL 
C     * B1SGJ(I,K)   : POIDS DE LA S-COUCHE D1SGJ POUR LE CALCUL DU 
C     *                GRADIENT DE PRESSION 
C     * D2SGJ(I,K)   : EPAISSEURS SIGMA ENTRE BAS DE COUCHE DE
C     *                TEMPERATURE ET BAS DE COUCHE DU VENT.
C     * A2SGJ(I,K)   : POIDS DE LA S-COUCHE D2SGJ POUR LE CALCUL DU 
C     *                GEOPOTENTIEL 
C     * B2SGJ(I,K)   : POIDS DE LA S-COUCHE D2SGJ POUR LE CALCUL DU 
C     *                GRADIENT DE PRESSION 
C     * 
C     * DE DIMENSION
C     * 
C     * NI2       : NOMBRE DE POSITIONS DE MEMOIRE PAR COUCHES. 
C     * NI        : NOMBRE DE LONGITUDES. 
C     * NK        : NOMBRE DE COUCHES.
C 
C     * A PARTIR DES PARAMETRES D'ENTREE: 
C     * 
C     * PSJ(I)      : PRESSION DE SURFACE 
C     * AG(K)       : PARAMETRE A DE LA COORDONNEE ETA POUR LE VENT.
C     * BG(K)       : PARAMETRE B DE LA COORDONNEE ETA POUR LE VENT.
C     * AH(K)       : PARAMETRE A DE LA COORDONNEE ETA POUR LA TEMP.
C     * BH(K)       : PARAMETRE B DE LA COORDONNEE ETA POUR LA TEMP.
C     * 
C     * N.B. LORSQUE LES COUCHES DE VENT ET TEMP. COINCIDENT
C     *      DSHJ=DSGJ ET D2SGJ=A2SGJ=B2SGJ=0.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL  DSGJ(NI2,NK),  DSHJ(NI2,NK),DLNSGJ(NI2,NK)
      REAL D1SGJ(NI2,NK), A1SGJ(NI2,NK), B1SGJ(NI2,NK)
      REAL D2SGJ(NI2,NK), A2SGJ(NI2,NK), B2SGJ(NI2,NK)
C 
      REAL PSJ(NI2) 
C 
      REAL AG(NK),BG(NK)
      REAL AH(NK),BH(NK)
C-------------------------------------------------------------------- 
      C1=-PTOIT*BG(1) 
      DB1=BG(1) 
C 
C     * CALCUL DES PARAMETRES.
C 
      DO 1001 I=1,NI
          STOIT      =PTOIT/PSJ(I)
          SGB1       =AG(1)/PSJ(I)+BG(1)
          SHB1       =AH(1)/PSJ(I)+BH(1)
C 
          DSGJ(I,1)  =SGB1-STOIT
          DSHJ(I,1)  =SHB1-STOIT
          DLNSGJ(I,1)=LOG(SHB1/STOIT)
C 
          D1SGJ(I,1) =SGB1-STOIT
          A1SGJ(I,1) =ALFMOD*D1SGJ(I,1) 
     1                          +SGB1*LOG(SHB1/SGB1)-STOIT*DLNSGJ(I,1) 
          B1SGJ(I,1) =(D1SGJ(I,1)*DB1+LOG(SGB1/STOIT)*C1/PSJ(I)) 
     1                                                 /DSGJ(I,1) 
C 
          D2SGJ(I,1) =0.0E0 
          A2SGJ(I,1) =0.E0
          B2SGJ(I,1) =0.E0
 1001 CONTINUE
C 
      DO 1002 K=2,NK
        CK         =AG(K)*BG(K-1) -AG(K-1)*BG(K)
        DBK        =BG(K)-BG(K-1) 
        DO 1002 I=1,NI
          SGBK       =AG(K)  /PSJ(I)+BG(K)
          SGBKM      =AG(K-1)/PSJ(I)+BG(K-1)
          SHBK       =AH(K)  /PSJ(I)+BH(K)
          SHBKM      =AH(K-1)/PSJ(I)+BH(K-1)
C 
          DSGJ(I,K)  =SGBK-SGBKM
          DSHJ(I,K)  =SHBK-SHBKM
          DLNSGJ(I,K)=LOG(SHBK/SHBKM)
C 
          D1SGJ(I,K)=SGBK-SHBKM 
          A1SGJ(I,K)=D1SGJ(I,K) +SGBK*LOG(SHBK/SGBK)-SGBKM*DLNSGJ(I,K) 
          B1SGJ(I,K)=(D1SGJ(I,K)*DBK +LOG(SGBK/SHBKM)*CK/PSJ(I)) 
     1                                                   /DSGJ(I,K) 
C 
          D2SGJ(I,K)=SHBKM-SGBKM
          A2SGJ(I,K)= D2SGJ(I,K) -SGBKM*LOG(SHBKM/SGBKM) 
          B2SGJ(I,K)=(D2SGJ(I,K)*DBK   +LOG(SHBKM/SGBKM)*CK/PSJ(I))
     1                                                   /DSGJ(I,K) 
 1002 CONTINUE
C 
      RETURN
C-----------------------------------------------------------------------
      END

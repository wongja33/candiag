      SUBROUTINE BETAINF(NLON,NLAT,ZHP,ZHM,ZB,BETA)
C
C     * JUN 08/01 - B. MIVILLE
C      
C     * CALCULATE THE 3-D SHAVED BETA BASED ON DEPTH OF BOTTOM OF OCEAN
C
C     * INPUT PARAMETERS...
C
C     * NLON = NUMBER OF LONGITUDES
C     * NLAT = NUMBER OF LATITUDES
C     * ZHP  = CURRENT DEPTH
C     * ZHM  = PREVIOUS DEPTH
C     * ZB   = DEPTH OF THE OCEAN BOTTOM
C
C     * OUTPUT PARAMETERS...
C
C     * BETA = SHAVED GRID CELL HEAVISIDE FUNCTION
C
C----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NLON, NLAT
      REAL    ZHP,  ZHM, ZB(NLON,NLAT), BETA(NLON,NLAT)
C-----------------------------------------------------------------------
C
C     * BETA,  INCLUDING SHAVED BOTTOM CELL IN THE VERTICAL
C     *
C     *
C     *    -------------------- ZH(K-1)=ZHM
C     *
C     *    ____________________ ZB(I,J) -> BOTTOM DEPTH
C     *    ////////////////////
C     *   ////////////////////
C     *    -------------------- ZH(K)=ZHP
C     *
C
      DO 150 J = 1,NLAT
         DO 140 I = 1,NLON
            IF ((ZB(I,J).EQ.0.0E0).OR.(ZB(I,J).LT.ZHM)) THEN
               BETA(I,J)=0.0E0
            ELSE
               IF(ZB(I,J).LT.ZHP) THEN
                  BETA(I,J) = (ZB(I,J)-ZHM) / (ZHP-ZHM)
               ELSE
                  BETA(I,J) = 1.0E0
               ENDIF
            ENDIF
 140     CONTINUE
 150  CONTINUE
C
C---------------------------------------------------------------------
C
      RETURN
      END 

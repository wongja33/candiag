      SUBROUTINE READOGI(NF,IM,JM,IJM,KM,IBUF,MAXX,DSOURCE,GTYPE,
     1                   RADIUS,LAT,LATH,LON,LONH,Z,ZH,ZB,BASN,IBASIN,
     2                   NLON,NLAT,NLEV)
C
C     * SEP 27 2005 - DYR - ERROR IN REWINDING INPUT FILE CORRECTED
C     * JUL 09/2002 - BM  - MODIFIED TO ADD AN OCEANOBS  DATA SOURCE
C     * APR 04/2002 - BM  - MODIFIED FOR NEW DATA DESCRIPTION FORMAT
C     * OCT 19/2001 - BM  - MODIFIED TO ADD LON AND LONH
C     * OCT 12/2001 - WGL - CODE RE-WRITTEN TO SEPARATE OUT SECTION THAT
C                           READS IN THE OCEAN GRIDINFO FILE
C    
C     * READ OCEAN GRID INFO FILE
C
C---------------------------------------------------------------------
C
C     * INPUT PARAMETERS...
C
C     * NF     = UNIT NUMBER OF FILE TO BE READ
C     * IM     = NUMBER OF X GRID POINTS
C     * JM     = NUMBER OF Y GRID POINTS
C     * IJM    = IM*JM
C     * KM     = MAXIMUM NUMBER OF LEVEL
C     * IBUF   = INPUT/OUTPUT SCRATCH ARRAY
C     * MAXX   = SIZE OF INPUT/OUTPUT SCRATCH SPACE
C
C     * OUTPUT PARAMETERS...
C
C     * DSOURCE = DATA SOURCE (CURRENTLY ONLY OGCM1,2,3 AND OCEANOBS)
C     * GTYPE   = GRID TYPE (CURRENTLY ONLY LL FOR LAT/LON GRID)
C     * RADIUS  = RADIUS OF THE EARTH (DEPENDS ON MODEL)
C     * LAT     = LATITUDES OF FULL INDEX GRID CELL
C     * LATH    = LATITUDES OF HALF INDEX GRID CELL
C     * LON     = LONGITUDES OF FULL INDEX GRID CELL
C     * LONH    = LONGITUDES OF HALF INDEX GRID CELL
C     * Z       = CENTER DEPTH LEVEL OF FULL INDEX GRID CELL
C     * ZH      = CENTER DEPTH LEVEL OF HALF INDEX GRID CELL
C     * GTYPE   = GRID TYPE (CURRENTLY ONLY LL IS VALID)
C     * ZB      = DEPTH LEVEL OF BOTTOM OF THE OCEAN
C     * BASN    = ORIGINAL BASIN NUMBER
C     * IBASIN  = BASIN NUMBER ASSIGNED TO EACH OCEAN GRID CELL
C     * NLON    = NUMBER OF LONGITUDES (INCLUDING CYCLIC ONE)
C     * NLAT    = NUMBER OF LATITUDES
C     * NLEV    = NUMBER OF DEPTH LEVELS
C
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
C
      REAL FRAD(10), BASN(IJM)
      REAL LAT(IJM), LATH(IJM), Z(KM), ZH(KM), ZB(IJM)
      REAL LON(IJM), LONH(IJM)
      INTEGER IBASIN(IJM)
      CHARACTER*80 FMOD(10), GTP(10)
      CHARACTER*8 DSOURCE, GTYPE, OGCM1, OGCM2, OGCM3, LL, OCEANOBS
C
      INTEGER IBUF(8), MAXX
C--------------------------------------------------------------------
C
C     * VALID MODEL/DATA SOURCES
C
      OGCM1 = 'OGCM1   '
      OGCM2 = 'OGCM2   '
      OGCM3 = 'OGCM3   '
      OCEANOBS ='OCEANOBS'
C
C     * VALID GRID TYPE
C
      LL = 'LL      '
C
C     * TO FORCE A REWIND OF THE INPUT FILE BEFORE EACH 
C     * GETFLD2 (FOR THIS SUBROUTINE ONLY)
C
      NUNIT = -ABS(NF)
C
C     * READ GRID INFO FILE GRIDI
C
C     * MODEL TYPE
C
      CALL GETFLD2 (NUNIT,FMOD,NC4TO8("CHAR"),-1,NC4TO8("DSRC"),-1,
     1                                                IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-1)
      ENDIF
C
      WRITE(DSOURCE,7000) FMOD(1)
C
C     * VERIFY THAT THE MODEL/DATA SOURCE IS VALID
C
      IF((DSOURCE.NE.OGCM1).AND.(DSOURCE.NE.OGCM2).AND.
     1   (DSOURCE.NE.OGCM3).AND.(DSOURCE.NE.OCEANOBS)) THEN
         WRITE(6,6000) DSOURCE
         CALL                                      XIT('READOGI',-2)
      ENDIF
C
C     * RADIUS OF THE EARTH
C
      CALL GETFLD2 (NUNIT,FRAD,NC4TO8("GRID"),-1,NC4TO8("RADO"),-1,
     1                                                IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-3)
      ENDIF
C
C
      RADIUS = FRAD(1)
C
C     * LATITUDE OF FULL INDEX GRID VALUES (TRACER LOCATION)
C
      CALL GETFLD2 (NUNIT,LAT,NC4TO8("GRID"),-1,NC4TO8(" LAT"),-1,
     1                                               IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-4)
      ENDIF
C
      NLON = IBUF(5)
      NLAT = IBUF(6)
C
C     * VERIFY NLON AND NLAT ARE WITHIN IM AND JM LIMITS
C
      IF((NLON.GT.IM).OR.(NLAT.GT.JM)) CALL      XIT('READOGI',-5)
C
C     * LATITUDE OF HALF-INDEX GRID VALUES (U,V LOCATION)
C
      CALL GETFLD2 (NUNIT,LATH,NC4TO8("GRID"),-1,NC4TO8("LATH"),-1,
     1                                                IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-6)
      ENDIF
C
C     * VERIFY THAT THE DIMENSION OF LATH ARE THE SAME AS LAT
C
      IF((NLON.NE.IBUF(5)).OR.(NLAT.NE.IBUF(6))) 
     1   CALL                                      XIT('READOGI',-7)
C
C     * LONGITUDE OF FULL-INDEX GRID VALUES (TRACER LOCATION)
C
      CALL GETFLD2 (NUNIT,LON,NC4TO8("GRID"),-1,NC4TO8(" LON"),-1,
     1                                               IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-8)
      ENDIF
C
C     * VERIFY THAT THE DIMENSION OF LON ARE THE SAME AS LAT OR LATH
C
      IF((NLON.NE.IBUF(5)).OR.(NLAT.NE.IBUF(6))) 
     1   CALL                                      XIT('READOGI',-9)
C
C     * LONGITUDE OF HALF-INDEX GRID VALUES (U,V LOCATION)
C
      CALL GETFLD2 (NUNIT,LONH,NC4TO8("GRID"),-1,NC4TO8("LONH"),-1,
     1                                                IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-10)
      ENDIF
C
C     * VERIFY THAT THE DIMENSION OF LONH ARE THE SAME AS LAT OR LATH
C
      IF((NLON.NE.IBUF(5)).OR.(NLAT.NE.IBUF(6))) 
     1   CALL                                      XIT('READOGI',-11)
C
C     * FULL-LEVEL DEPTHS (TRACER, U,V LOCATIONS) AND NUMBER 0F LEVELS
C
      CALL GETFLD2 (NUNIT,Z,NC4TO8("GRID"),-1,NC4TO8("   Z"),-1,
     1                                             IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-12)
      ENDIF
C
      NLEV = IBUF(5)
C
C     * VERIFY NLEV IS WITHIN KM LIMIT
C
      IF(NLEV.GT.KM) CALL                          XIT('READOGI',-13)
C
C     * HALF-LEVEL DEPTHS (W LOCATIONS)
C
      CALL GETFLD2 (NUNIT,ZH,NC4TO8("GRID"),-1,NC4TO8("  ZH"),-1,
     1                                              IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-14)
      ENDIF
C
C     * GRID TYPE
C
      CALL GETFLD2 (NUNIT,GTP,NC4TO8("CHAR"),-1,NC4TO8("GTYP"),-1,
     1                                               IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-15)
      ENDIF
C
      WRITE(GTYPE,7000) GTP(1)
C
      IF(GTYPE.NE.LL) THEN
         WRITE(6,6002) GTYPE
         CALL                                      XIT('READOGI',-16)
      ENDIF
C
C     * DEPTH OF BOTTOM OF OCEAN
C
      CALL GETFLD2 (NUNIT,ZB,NC4TO8("GRID"),-1,NC4TO8("ZBOT"),-1,
     1                                              IBUF,MAXX,OK)
      IF (.NOT.OK)THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-17)
      ENDIF
C
C     * CROSS CHECK TO MAKE SURE THEY ARE SAME DIMENSION
C     * AS LAT AND LON
C
      IF((NLON.NE.IBUF(5)).OR.(NLAT.NE.IBUF(6)))
     1  CALL                                       XIT('READOGI',-18)
C
C     * BASIN FOR EACH OCEAN GRID
C
      CALL GETFLD2 (NUNIT,BASN,NC4TO8("GRID"),-1,NC4TO8("BASN"),-1,
     1                                                IBUF,MAXX,OK)
      IF (.NOT.OK)THEN
         CALL PRTLAB(IBUF)
         CALL                                      XIT('READOGI',-19)
      ENDIF
C
C     * CROSS CHECK TO MAKE SURE THEY HAVE THE SAME DIMENSION
C     * AS LAT AND LON
C
      IF((NLAT.NE.IBUF(6)).OR.(NLON.NE.IBUF(5))) 
     1  CALL                                       XIT('READOGI',-20)
C
C     * CONVERT BASIN MASK TO INTEGER
C
      NWRDS=NLON*NLAT
C
      DO 200 IJ = 1,NWRDS
         IBASIN(IJ) = INT(BASN(IJ)+0.5E0)
 200  CONTINUE
C
      RETURN
 6000 FORMAT('NOT A VALID OCEAN MODEL/DATA SOURCE: ',A8)
 6002 FORMAT('NOT A VALID GRID TYPE: ',A8)
 7000 FORMAT(A8)
      END

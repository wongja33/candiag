      SUBROUTINE EAPLT (TEMPS,PHIS,RGASMS,LNSPS,LA,NSL,NSL1,PR,NPL,     
     1                  RLUP,RLDN,A,B,TEMPPR,PHIPR,
     2                  TEMPC,PHIC,PRESS,RMEAN,GAMMA,CHI)

C     * OCT  5/94 - J.KOSHYK (INTRODUCE COLUMN ARRAYS TEMPC/PHIC TO CORRECT    
C                   CALCULATIONS OF TEMPPR/PHIPPR).              
C     * JUL 27/94 - J.KOSHYK                                                     

C     * INTERPOLATES MULTI-LEVEL SETS OF TEMP AND PHI FROM ETA       
C     * LEVELS TO PRESSURE LEVELS.                                      
C                                                                                 
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LA POINTS).                      
C     * TEMPS    = INPUT TEMPERATURE GRIDS ON ETA LEVELS.                  
C     * PHIS     = INPUT GEOPOTENTIAL HEIGHT GRIDS ON ETA LEVELS.            
C     * RGASMS   = INPUT MOIST GAS CONSTANT GRIDS ON ETA LEVELS.             
C     * LNSPS    = INPUT GRID OF LN(SURFACE PRESSURE IN MB).                    
C     * NSL      = NUMBER OF ETA LEVELS.                                 
C     * PR(NPL)  = VALUES OF INPUT PRESSURE LEVELS (MB);
C     *            (MUST BE MONOTONIC AND INCREASING).
C     * RLUP     = -DT/DZ USED TO EXTRAPOLATE ABOVE TOP ETA.                
C     * RLDN     = -DT/DZ USED TO EXTRAPOLATE BELOW BOTTOM ETA.
C     * A, B     = PARAMETERS OF ETA VERTICAL DISCRETIZATION.
C     * TEMPPR   = OUTPUT GRIDS OF TEMPERATURE ON PRESSURE LEVELS.
C     * PHIPR    = OUTPUT GRIDS OF GEOPOTENTIAL HEIGHT ON PRESSURE LEVELS.
C                                                                                 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL TEMPS (LA,NSL),PHIS (LA,NSL),RGASMS (LA,NSL),LNSPS     (LA)  
      REAL PR       (NPL),A       (NSL),B         (NSL) 
      REAL TEMPPR(LA,NPL),PHIPR(LA,NPL)
                                                                        
C     * WORK SPACE.                                                               
                                                                        
      REAL TEMPC(NSL),PHIC(NSL)
      REAL PRESS(NPL)
      REAL RMEAN(NSL1),GAMMA(NSL1),CHI(NSL1)

      PARAMETER(G=9.81E0)

C---------------------------------------------------------------------------------
C     * LOOP OVER ALL HORIZONTAL POINTS.                                          

      DO 500 I=1,LA                                                     
                                                                        
C     * COMPUTE PRESSURE ON ETA LEVELS.

      DO 150 L=1,NSL
  150 PRESS(L) = A(L)+B(L)*(100.E0*EXP(LNSPS(I)))

C

      DO 160 L=1,NSL
        TEMPC(L) = TEMPS(I,L)
        PHIC (L) = PHIS (I,L)
  160 CONTINUE

C     * COMPUTE THE LAPSE RATE, GAMMA = -DT/D PHI OVER ALL INPUT INTERVALS,
C     * AND CALCULATE RELATED QUANTITIES FOR LATER USE.

      DO 180 L=1,NSL-1                                                  
        RMEAN(L+1) =  (RGASMS(I,L+1)+RGASMS(I,L))/2.E0
        GAMMA(L+1) = -(TEMPC(L+1)-TEMPC(L))/
     1                (PHIC (L+1)-PHIC (L))
        CHI  (L+1) =  RMEAN(L+1)*GAMMA(L+1)
  180 CONTINUE                                                          
                                                                        
C     * ASSIGN VALUES OF QUANTITIES ABOVE HIGHEST AND BELOW LOWEST
C     * ETA LEVEL.

      GAMMA(1)     = RLUP/G
      GAMMA(NSL1)  = RLDN/G                                             

C     * ASSUME VALUE OF RGASM ABOVE HIGHEST LEVEL = VALUE WITHIN HIGHEST LAYER,
C     *    AND VALUE OF RGASM BELOW LOWEST  LEVEL = VALUE WITHIN LOWEST  LAYER.

      RMEAN(1)     = RMEAN(2)
      RMEAN(NSL1)  = RMEAN(NSL)
      CHI  (1)     = RMEAN(1)*GAMMA(1)
      CHI  (NSL1)  = RMEAN(NSL1)*GAMMA(NSL1)
                                                                        
C     * LOOP OVER PRESSURE LEVELS TO BE INTERPOLATED.                             
                                                                        
      K=1                                                               
      DO 350 N=1,NPL                                                    
                                                                        
C     * FIND WHICH SIGMA INTERVAL WE ARE IN.                                      
                                                                        
      DO 310 L=K,NSL                                                    
      INTVL=L                                                           
  310 IF(PR(N).LT.PRESS(L)) GO TO 320                                   
      INTVL=NSL+1                                                       
  320 K=INTVL-1                                                         
      IF(K.EQ.0) K=1                                                    
                                                                        
C     * NOW INTERPOLATE AT THIS POINT.                                            

      TEMPPR(I,N) = TEMPC(K)*(PR(N)/PRESS(K))**CHI(INTVL)
      
      IF (GAMMA(INTVL) .NE. 0) THEN
        PHIPR (I,N) = PHIC(K)-(TEMPPR(I,N)-TEMPC(K))/GAMMA(INTVL)
      ELSE

C       * COMPUTE PHI FROM HYDROSTATIC EQUATION, ASSUMING ISOTHERMAL LAYER.

        PHIPR (I,N) = PHIC(K)-RMEAN(INTVL)*TEMPPR(I,N)
     1                          *LOG(PR(N)/PRESS(K))
      ENDIF
      
  350 CONTINUE
  500 CONTINUE                                                          
                                                                        
      RETURN                                                            
      END  

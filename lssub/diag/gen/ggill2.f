      SUBROUTINE GGILL2(GLL,NLG1,NLAT,GG,ILG1,ILAT,SLAT,INTERP) 
C 
C     * AUG 09/96 - F.MAJAESS (CORRECT COMPUTATIONS FOR THE POLES)
C     * MAY  1/80 - J.D.HENDERSON 
C     * INTERPOLATES GLOBAL LAT-LONG GRID GLL(NLG1,NLAT)
C     * FROM GLOBAL GAUSSIAN GRID GG(ILG1,ILAT).
C     * SLAT = LAT (DEG) OF GAUSSIAN GRID ROWS FROM THE S POLE. 
C     * INTERP = (1,3) FOR (LINEAR,CUBIC) INTERPOLATION.
C     *          (OTHERWISE THE GRID GG IS SET TO ZERO).
C 
C     * LEVEL 2,GLL,GG
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GLL(NLG1,NLAT),GG(ILG1,ILAT) 
      REAL SLAT(ILAT) 
C-----------------------------------------------------------------------
      IF ( NLAT.LT.3 ) CALL XIT ('GGILL2',-1)
      NLG=NLG1-1
      DX=360.E0/FLOAT(NLG)
      DY=180.E0/FLOAT(NLAT-1) 
C 
C!!!  DO 320 J=1,NLAT 
      DO 320 J=2,NLAT-1 
      DLAT=FLOAT(J-1)*DY
      DO 310 I=1,NLG
      DLON=FLOAT(I-1)*DX
      VAL=0.E0
      IF(INTERP.EQ.1) CALL GGPNL2(VAL,GG,ILG1,ILAT,DLAT,DLON,SLAT)
      IF(INTERP.EQ.3) CALL GGPNT2(VAL,GG,ILG1,ILAT,DLAT,DLON,SLAT)
  310 GLL(I,J)=VAL
  320 GLL(NLG1,J)=GLL(1,J)
C 
C     * SET N POLE TO MEAN OF TOP ROW.
C     * SET S POLE TO MEAN OF BOTTOM ROW. 
C 
C!!!  CALL FMEAN2(GLL(1,NLAT),NLG,1,AVG,0)
      CALL FMEAN2(GG(1,ILAT),ILG1-1,1,AVG,0)
      DO 410 I=1,NLG1 
  410 GLL(I,NLAT)=AVG 
C 
C!!!  CALL FMEAN2(GLL(1,1),NLG,1,AVG,0) 
      CALL FMEAN2(GG(1,1),ILG1-1,1,AVG,0) 
      DO 510 I=1,NLG1 
  510 GLL(I,1)=AVG
C 
      RETURN
      END 

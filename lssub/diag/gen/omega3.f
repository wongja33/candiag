      SUBROUTINE OMEGA3 ( OMEGAG, DJ, UJ, VJ, PSDLJ, PSDPJ, PRESSJ, 
     A                    SHJ, DSGJ, DSHJ, DLNSGJ,
     B                    A1SGJ, B1SGJ, A2SGJ, B2SGJ, 
     C                    DB, VGRPSJ, SSDRJK, NI2, NI, NK)
  
C     * JAN 28/91- M.LAZARE, F.MAJAESS (BASED ON CODES EXTRACTED FROM DYNCAL3)
C 
C     * THIS SUBROUTINE COMPUTES VERTICAL MOTION FOR ILG POINTS ABOUT A 
C     * LATITUDE CIRCLE THE SAME WAY AS IT IS DONE IN DYNCAL3.
C     * THEN SCALE BY SF.PRES. AND "SHJ" TO GET UNITS OF (NEWTONS/M**2/SEC).
C 
  
C     * NI2        : VECTOR LENGTH. 
C     * NI         : NUMBER OF LONGITUDES.
C     * NK         : NUMBER OF LAYERS.
  
  
C     * OUTPUT: 
C     * ------- 
C     * 
C     * OMEGAG(I,K) : VERTICAL MOTION (N/M**2/SEC) AT THERMODYNAMICS
C     *               MID-LAYER POSITIONS.
  
C     * INPUT PARAMETERS: 
C     * ----------------- 
  
C     * DJ(I,K)    : DIVERGENCE AT LATITUDE J.
C     * UJ(I,K)    : ZONAL      REAL WIND AT LATITUDE J.
C     * VJ(I,K)    : MERIDIONAL REAL WIND AT LATITUDE J.
C     * PSDLJ(I)   : LONGITUDINAL DERIVATIVE AT LATITUDE J OF LOG OF SF. PRES.
C     * PSDPJ(I)   : LATITUDINAL  DERIVATIVE AT LATITUDE J OF LOG OF SF. PRES.
C     * PRESSJ(I)  : SURFACE PRESSURE AT LATITUDE J.
  
C     * VERTICAL DISCRETIZATION PARAMETERS: 
C     * ----------------------------------- 
  
C     * SHJ(I,K)   : TEMP MID-LAYER VALUES. 
C     * DSGJ(I,K)  : WIND SIGMA LAYER THICKNESS.
C     * DSHJ(I,K)  : TEMP SIGMA LAYER THICKNESS.
C     * DLNSGJ(I,K): THERMODYNAMIC LAYERS D LN(SIGMA).
C     * A1SGJ(I,K) : WEIGHT OF S-LAYER OF SIGMA THICKNESS BETWEEN UPPER WIND
C     *              LAYER AND LOWER TEMPERATURE LAYER USED IN THE CALCULATION
C     *              OF GEOPOTENTIAL. 
C     * B1SGJ(I,K) : WEIGHT OF S-LAYER OF SIGMA THICKNESS BETWEEN UPPER WIND
C     *              LAYER AND LOWER TEMPERATURE LAYER USED IN THE CALCULATION
C     *              OF PRESSURE GRADIENT.
C     * A1SGJ(I,K) : WEIGHT OF S-LAYER OF SIGMA THICKNESS BETWEEN LOWER WIND
C     *              LAYER AND LOWER TEMPERATURE LAYER USED IN THE CALCULATION
C     *              OF GEOPOTENTIAL. 
C     * B2SGJ(I,K) : WEIGHT OF S-LAYER OF SIGMA THICKNESS BETWEEN LOWER WIND
C     *              LAYER AND LOWER TEMPERATURE LAYER USED IN THE CALCULATION
C     *              OF PRESSURE GRADIENT.
C     * DB(K)      : DB PARAMETER OF THE VERTICAL DISCRETIZATION
  
  
C     * WORKING ARRAYS: 
C     * --------------- 
  
C     * VGRPSJ(I,K): V.GRAD(LPS) .
C     * SSDRJK(I)  : SUM OF LAYERS 1 TO K OF "D ROND" * DSIGMA. 
C 
C     ------------------------------------------------------------------------
C 
C     * INPUT:  
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL  DJ   (NI2,NK), UJ   (NI2,NK), VJ     (NI2,NK) 
      REAL  PSDLJ(NI2   ), PSDPJ(NI2   ), PRESSJ (NI2   ) 
  
C     * VERTICAL DISCRETIZATION PARAMETERS: 
  
      REAL  SHJ   (NI2,NK), DSGJ  (NI2,NK), DSHJ  (NI2,NK)
      REAL  DLNSGJ(NI2,NK)
      REAL  A1SGJ (NI2,NK), B1SGJ (NI2,NK)
      REAL  A2SGJ (NI2,NK), B2SGJ (NI2,NK)
      REAL  DB (NK) 
  
C     * WORKING ARRAYS: 
  
      REAL  VGRPSJ(NI2,NK), SSDRJK(NI2) 
  
C     * OUTPUT ARRAY: 
  
      REAL  OMEGAG(NI2,NK)
C-----------------------------------------------------------------------
C     * INITIALIZATION
C 
  
      DO 1000 I = 1,NI
         SSDRJK (I)= 0.E0 
 1000 CONTINUE
  
C     ----------------------------------------------------------------- 
C     * DO THE INTERMEDIATE CALCULATION (RPS) 
C 
  
      DO 2010 K = 1,NK
        DO 2005 I = 1,NI
            VGRPSJ(I,K) =  UJ(I,K)*PSDLJ(I) + VJ(I,K)*PSDPJ(I)
 2005   CONTINUE
 2010 CONTINUE
  
C     ----------------------------------------------------------------- 
C     * DO THE CALCULATION OF OMEGA OVER P. 
C 
  
      DO 3010 K=1,NK-1
        DO 3005 I = 1,NI
  
          DRJKI       =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K)
          DRJKPI      =  DJ(I,K+1) + VGRPSJ(I,K+1)*DB(K+1)/DSGJ(I,K+1)
  
          OMEGAG(I,K) = ( - SSDRJK(I)*DLNSGJ(I,K) 
     1                   - DRJKI*A1SGJ(I,K) - DRJKPI*A2SGJ(I,K+1) 
     2                   + VGRPSJ(I,K)*B1SGJ(I,K) 
     3                   + VGRPSJ(I,K+1)*B2SGJ(I,K+1) )/DSHJ(I,K) 
  
          SSDRJK(I)   = SSDRJK(I) +DRJKI*DSGJ(I,K)
  
 3005   CONTINUE
 3010 CONTINUE
  
C     ----------------------------------------------------------------- 
  
      K=NK
  
      DO 3015 I = 1,NI
  
        DRJKI       =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K)
  
        OMEGAG(I,K) = ( - SSDRJK(I)*DLNSGJ(I,K) - DRJKI*A1SGJ(I,K)
     1                     + VGRPSJ(I,K)*B1SGJ(I,K) )/DSHJ(I,K) 
  
 3015 CONTINUE
  
C     ----------------------------------------------------------------- 
C     * CALCULATE OMEGA (N/M**2/SEC)
C 
  
      DO 4010 K=1,NK
        DO 4005 I = 1,NI
  
C         VGRPSJ(I,K) = OMEGAG(I,K) 
          OMEGAG(I,K) = OMEGAG(I,K) * PRESSJ(I) * SHJ(I,K)
  
 4005   CONTINUE
 4010 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 

      SUBROUTINE DIGITS(ID,N,NUM) 
C 
C     * JUN 20/78 - J.D.HENDERSON 
C     * RETURNS IN ID THE N LOWEST ORDER DIGITS OF NUM. 
C     * ALL DIGITS RETURNED ARE POSITIVE. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER ID(N) 
C-------------------------------------------------------------------- 
C 
      NA=ABS(NUM)
C 
      DO 210 K=1,N
      ID(K)=NA-NA/10*10 
      NA=NA/10
  210 CONTINUE
C 
      RETURN
      END 

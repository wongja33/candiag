      SUBROUTINE FMEAN2(G,LI,LJ,AVRG,N) 
C 
C     *****   JAN 1975  -  JOHN D. HENDERSON  ****
C     * CALCULATES MEAN OF G(LI,LJ) OMITTING N BORDER ROWS. 
C 
C     * LEVEL 2,G 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION G(LI,LJ)
      DOUBLE PRECISION SUM
C-----------------------------------------------------------------------
C 
      IL=1+N
      JL=1+N
      IH=LI-N 
      JH=LJ-N 
      PTS=(IH-IL+1)*(JH-JL+1) 
      SUM=0.E0
C 
      DO 20 J=JL,JH 
      DO 20 I=IL,IH 
   20 SUM=SUM+G(I,J)
      AVRG=SUM/PTS
C 
      RETURN
      END 

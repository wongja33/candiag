       SUBROUTINE GGILL(DAT,G,I1,I2,J1,J2,LX,LY)
C 
C     * AUG 10/87 - M.SUTCLIFFE 
C     * EXTRACT SEGMENTS OF ARRAY G AND PUT THEM INTO ARRAY DAT.
C     * I1,I2 AND J1,J2 ARE THE LEFT,RIGHT AND LOWER,UPPER GRID 
C     * INDICES OF THE WINDOW.
C     * LX IN IS THE NUMBER OF INPUT GRID POINTS IN THE X DIRECTION.
C     * LX,LY OUT ARE THE NUMBERS OF GRID POINTS IN THE WINDOW. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL G(LX)
      REAL DAT(LX)
C---------------------------------------------------------------------
C 
C     * EXTRACT WINDOW. 
C 
      IWINDO=0
      DO 330 J=J1,J2
         IF (I1.GE.I2) THEN 
           DO 300 I=I1,LX-1 
              IWINDO=IWINDO+1 
              IFULLG=(J-1)*LX+I 
              DAT(IWINDO)=G(IFULLG) 
  300      CONTINUE 
           DO 310 I=1,I2
              IWINDO=IWINDO+1 
              IFULLG=(J-1)*LX+I 
              DAT(IWINDO)=G(IFULLG) 
  310      CONTINUE 
         ELSE 
           DO 320 I=I1,I2 
              IWINDO=IWINDO+1 
              IFULLG=(J-1)*LX+I 
              DAT(IWINDO)=G(IFULLG) 
  320      CONTINUE 
         ENDIF
  330 CONTINUE
C 
      LY=J2-J1+1
      IF (I1.GE.I2) THEN
        LX=I2-I1+LX 
      ELSE
        LX=I2-I1+1
      ENDIF 
C 
      RETURN
      END 

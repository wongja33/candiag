      SUBROUTINE SPECTRA(F,LM,LR,KTR,LA,LSR,SUM,KD) 
C
C     * JUN 29/93 - J.KOSHYK (ADD OPTION FOR OUTPUT OF 2D ARRAY, CONSTANT IN
C     *             ONE SPECTRAL INDEX)
C     * AUG 08/90 - G.J.BOER, F.MAJAESS(ADD LR, KTR AND RESTRUCTURE DO LOOPS) 
C     * FEB XX/80 - S. LAMBERT
C     * 
C     * SUBROUTINE READS IN A COMPLEX-VALUED SPECTRAL FILE F(LA) AND RETURNS
C     *   THE SUM OVER N/M AS A FUNCTION OF M/N IF (KD .EQ./.NE. 2).
C     *   NOTE:  KTR=0/2 INDICATES RHOMBOIDAL/TRIANGULAR TRUNCATION.
C     * 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX F(LA) 
      INTEGER LSR(2,1)
      COMPLEX SUM(1)
C-----------------------------------------------------------------------
      IF ((LR.LT.2).OR.(LM.LT.2)) CALL              XIT('SPECTRA',-1) 
      IF (LR.LT.LM) CALL                            XIT('SPECTRA',-2) 
C 
C     * CHECK IF THE SUM IS OVER M OR N.
C 
      IF(ABS(KD).EQ.2) THEN
  
C       * SUM OVER N IS REQUESTED.
  
        DO 200 M=1,LM 
          SUM(M)=0.0E0
          NF=M
C***      NL=CVMGT((LR+M-1),LR,KTR.EQ.0)
          IF(KTR.EQ.0)           THEN
            NL=LR+M-1
          ELSE
            NL=LR
          ENDIF   
C
          DO 100 N=NF,NL
            K=LSR(1,M)+(N-M)
            SUM(M)=SUM(M)+F(K)
 100      CONTINUE

C       * FILL OUT A 2D ARRAY WHOSE ENTRIES FOR A FIXED M = SUM(M)
C       * FOR EVERY N.

          IF (KD.LT.0) THEN
            DO 150 N=NF,NL
              K=LSR(1,M)+(N-M)
              F(K)=SUM(M)
 150        CONTINUE
          END IF

 200    CONTINUE
  
      ELSE
  
C       * SUM OVER M IS REQUESTED.
  
C***    NMAX=CVMGT((LM+LR-1),LR,KTR.EQ.0) 
        IF(KTR.EQ.0)           THEN
          NMAX=LM+LR-1
        ELSE
          NMAX=LR
        ENDIF
  
        DO 400 N=1,NMAX 
          SUM(N)=0.0E0
C***      MF=CVMGT(MAX(1,(N-LR+1)),1,KTR.EQ.0) 
          IF(KTR.EQ.0)           THEN
            MF=MAX(1,(N-LR+1))
          ELSE
            MF=1
          ENDIF
          ML=MIN(N,LM) 
          DO 300 M=MF,ML
            K=LSR(1,M)+(N-M)
            SUM(N)=SUM(N)+F(K)
 300      CONTINUE

C       * FILL OUT A 2D ARRAY WHOSE ENTRIES FOR A FIXED N = SUM(N)
C       * FOR EVERY M.

          IF (KD.LT.0) THEN
            DO 350 M=MF,ML
              K=LSR(1,M)+(N-M)
              F(K) = SUM(N)
 350        CONTINUE
          END IF

 400    CONTINUE
  
      ENDIF 
  
      RETURN
      END 

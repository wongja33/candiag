      SUBROUTINE ZXAST (SC,LSR,LA, ZX,ILAT,ILEV,SL,WL,
     1                  ALP,EPSI) 
C 
C     * JUL 14/92 - E. CHAN   (ADD REAL*8 DECLARATIONS)
C     * DEC 03/90 - F.MAJAESS (ELIMINATE DOUBLING THE SPECTRAL COEFICIENTS) 
C     * AVR 10/85 - B.DUGAS.
C     * PRODUCES GLOBAL SPECTRAL COEFF SET SC(LA,ILEV)
C     * FROM GLOBAL ZONAL CROSS-SECTIONS SET ZX(ILAT,ILEV). 
C     * NOTE THAT ONLY THE M=0 PART OF SC HAS ANY INFORMATION.
C     * LSR CONTAINS ROW LENGTH INFO FOR ALP,EPSI,SC. 
C     * EPSI = PRECOMPUTED CONSTANTS. 
C     * SL = SIN(LATITUDE) OF GAUSSIAN ROWS FROM S.POLE TO N.POLE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SC(2,LA,ILEV),ZX(ILAT,ILEV)
      REAL*8 SL(1),WL(1)
      REAL*8 ALP(1),EPSI(1) 
      INTEGER LSR(2,1)
C-------------------------------------------------------------------- 
      LSR1=LSR(1,2)-1 
      DO 110 L=1,ILEV 
         DO 110 N=1,LSR1
            SC(1,N,L)=0.E0
            SC(2,N,L)=0.E0
  110 CONTINUE
  
      DO 230 J=1,ILAT 
  
         CALL ALPST2 (ALP,LSR ,1,SL(J),EPSI)
         WLJ=WL(J)
         DO 215 N=1,LSR1
            ALP(N)=ALP(N)*WLJ 
  215    CONTINUE 
         IF (ILEV.GT.LSR1)   THEN 
  
             DO 220 N=1,LSR1
                WP=ALP(N) 
                DO 220 L=1,ILEV 
                   SC(1,N,L)=SC(1,N,L)+WP*ZX(J,L) 
  220        CONTINUE 
  
         ELSE 
  
             DO 225 L=1,ILEV
                ZXJL=ZX(J,L)
                DO 225 N=1,LSR1 
                   SC(1,N,L)=SC(1,N,L)+ALP(N)*ZXJL
  225        CONTINUE 
  
         ENDIF
  230 CONTINUE
  
      RETURN
      END 

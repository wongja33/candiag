      SUBROUTINE NORMAR (VALEUR,NOMBRE) 
  
C     * DECEMBRE 16/85 - B. DUGAS 
  
C     * CETTE ROUTINE CALCULE LES PROBABILITES NORMALE (0,1) ASSO-
C     * CIEES AUX NOMBRE DE VALEURS QUI  LUI SONT PASSEES.  VALEUR
C     * DOIT ETRE EN ORDRE CROISSANT A L'ENTREE.
  
C     * UNE APPROXIMATION RATIONNELLE EST UTILISEE. (VOIR  " HAND-
C     * BOOK OF MATHEMATICAL FUNCTIONS ",  ABRAMOWITZ  ET  STEGUN,
C     * NATIONAL BUREAU OF STANDARTS, 1964, PP. 931-933.)  LA PRE-
C     * CISION DU CALCUL EST DE L'ORDRE DE 7.5E-8.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL VALEUR(NOMBRE), M1 
  
      DATA P  /  0.2316419E0   /, OSQ2PI / 0.3989422804E0 /,
     &  DEMI /  0.5E0  /,
     &  B1 /  0.319381530E0 /, B2 / -0.356563782E0 /,
     &  B3 / 1.781477937E0 /,
     &  B4 / -1.821255978E0 /, B5 /  1.330274429E0 /,
     &  UN / 1.0E0 /, ZERO /  0.0E0 /, M1 / -1.0E0  / 
C-------------------------------------------------------------------- 
  
C     * SI ABS(VALEUR) > 15., LES VALEURS SONT DEFINIES PAR ZERO OU UN. 
  
      NI = 0
      NF = NOMBRE+1 
  
  050 NI =NI+1
      IF (VALEUR(NI).GE.-15.0E0)                                 THEN 
          GOTO 100
      ELSE IF (NI.LT.NOMBRE)                                   THEN 
          GOTO 050
      ELSE
          DO 060 I=1,NOMBRE 
              VALEUR(I) = ZERO
  060     CONTINUE
      END IF
  
  100 NF = NF-1 
      IF (VALEUR(NF).LE.+15.0E0)                                 THEN 
          GOTO 200
      ELSE IF (NF.GT.1)                                        THEN 
          GOTO 100
      ELSE
          DO 160 I=1,NOMBRE 
              VALEUR(I) = UN
  160     CONTINUE
      END IF
  
  200 DO 300 I=NI,NF
  
          U  = ABS(VALEUR(I)) 
  
          ZU = OSQ2PI * EXP (M1 * U*U * DEMI) 
  
          T1 = UN / (UN + P*U)
          T2 = T1*T1
          T3 = T2*T1
          T4 = T3*T1
          T5 = T4*T1
  
          QU = ZU * (B1*T1 + B2*T2 + B3*T3 + B4*T4 + B5*T5) 
  
          VALEUR(I) = MERGE( UN - QU, QU, VALEUR(I).GE.ZERO)
  
  300 CONTINUE
  
C     * DEFINIR LES VALEURS EXTREMES. 
  
      IF (NI.GT.1)                                             THEN 
          DO 400 I=1,NI-1 
              VALEUR(I) = ZERO
  400     CONTINUE
      ELSE IF (NF.LT.NOMBRE)                                   THEN 
          DO 500 I=NF+1,NOMBRE
              VALEUR(I) = UN
  500     CONTINUE
      END IF
  
C---------------------------------------------------------------------- 
      RETURN
      END 

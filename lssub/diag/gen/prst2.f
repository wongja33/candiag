      SUBROUTINE PRST2(ST,LSR,LM,NPR,MPR) 
C 
C     * JUL  2/79 - J.D.HENDERSON 
C     * ST IS A GENERALLY TRUNCATED SPECTRAL ARRAY (ROW INFO IN LSR). 
C     * PRINT ALL COEFFICIENTS INSIDE BOX (NPR,MPR) CORNERED ON (0,0).
C 
C     * LEVEL 2,ST
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX ST(1) 
      INTEGER LSR(2,1)
C-------------------------------------------------------------------- 
      WRITE(6,6010) LSR(2,1),LM,NPR,MPR 
      MLIM=MPR
      IF(MLIM.GT.LM) MLIM=LM
C 
      DO 230 M=1,MLIM 
      MS=M-1
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      KPR=KL+NPR-1
      IF(KPR.GT.KR) KPR=KR
C 
      WRITE(6,6020) MS
      WRITE(6,6030) (ST(K),K=KL,KPR)
  230 CONTINUE
C 
      RETURN
C-------------------------------------------------------------------- 
 6010 FORMAT('1 SPECTRAL ARRAY',2I4,4X,'PRINT',2I4)
 6020 FORMAT(' M=',I2)
 6030 FORMAT((' ',4(4X,1PE13.6,',',1PE13.6))) 
      END 

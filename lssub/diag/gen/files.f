      SUBROUTINE FILES (TYPE,NUMBER)

C     * FEB 15/02 - F.MAJAESS (ENSURE AN UPPER LIMIT ON THE NUMBER 
C     *                        OF UNITS SPECIFIED IN "NUMBER",
C     *                        CORRECT THE UNIT NUMBER IN THE
C     *                        RELEASE CASE, RE-WRITE THE NESTED 
C     *                        IF BLOCKS BY DO LOOPS)
C     *                        
C     * APR 25/95 - F.MAJAESS (ADD "FORM='UNFORMATTED'" TO
C     *                        OPEN STATEMENTS.)
C     * NOV 28/91 - M.LAZARE. (CALLS TO ASSIGN/RELEASE REPLACED BY  
C     *                        OPEN/CLOSE STATEMENTS FOR NON-CRAY 
C     *                        MACHINES.)
C     * JUILLET 18/85 - B.DUGAS.
  
C     * ASSIGN OR RELEASE UP TO "NUMBER" SCRATCH FILE UNIT PAIRS.
C     * TYPE= 1; ASSIGN
C     *     =-1; RELEASE
C     *     = ANYTHING ELSE ; ABORT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER TYPE, NUMBER, MAXUN,IOFFST
      CHARACTER CFNM1*4,CFNM2*4
      LOGICAL LOCAL 

C     * SET IN "MAXUN" THE MAXIMUM NUMBER OF FILE UNIT PAIRS WHICH 
C     * CAN BE HANDLED. AND, IN "IOFFST" AN OFFSET FOR THE UNIT 
C     * VALUES.

      DATA MAXUN,IOFFST /40,10/
C-------------------------------------------------------------------- 
      IF (NUMBER.LE.1) RETURN 
      IF (NUMBER.GT.MAXUN) THEN
        WRITE(6,6000) NUMBER,MAXUN
        CALL                                       XIT('FILES',-1)
      ENDIF
  
C     * CREATION DES FICHIERS XX00-XXYY.
  
      IF (TYPE.EQ.1)                                           THEN 
  
C      * VERIFIER QUE CETTE SERIE DE FICHIERS N'EXISTE PAS DEJA.
  
       INQUIRE (FILE='XX01',EXIST=LOCAL)
       IF (LOCAL) CALL                             XIT('FILES',-2)
  
       DO I=1,NUMBER
         I1=IOFFST+I
         CFNM1(1:2)='XX'
         WRITE(CFNM1(3:4),1000) I
         I2=I1+MAXUN
         CFNM2(1:2)='XX'
         WRITE(CFNM2(3:4),1000) I+MAXUN
         WRITE(6,6010) I,I1,CFNM1,I2,CFNM2
 6010    FORMAT(' I=',I3,', I1=',I3,', CFNM1=',A4,
     1                   ', I2=',I3,', CFNM2=',A4)
         OPEN(I1,FILE=CFNM1, FORM='UNFORMATTED')
         OPEN(I2,FILE=CFNM2, FORM='UNFORMATTED')
         
       ENDDO
  
      ELSE IF (TYPE.EQ.-1)                                     THEN 
  
C      * ON RETOURNE LES FICHIERS DE TRAVAIL.
C      * VERIFIER QUE CETTE SERIE DE FICHIERS EXISTE. 
  
       INQUIRE (FILE='XX01',EXIST=LOCAL)
       IF (.NOT.LOCAL) CALL                        XIT('FILES',-3) 
  
       DO I=1,NUMBER
         I1=IOFFST+I
         I2=I1+MAXUN
         WRITE(6,6020) I,I1,I2
 6020    FORMAT(' I=',I3,', I1=',I3,', I2=',I3)
         CLOSE (I1,STATUS='DELETE')
         CLOSE (I2,STATUS='DELETE')
       ENDDO
      ELSE
  
       CALL                                        XIT('FILES',-4)
  
      ENDIF 
  
      RETURN

C-----------------------------------------------------------------
 1000 FORMAT(I2.2)
 6000 FORMAT('0 FILES - SPECIFIED NUMBER OF UNITS PAIR OF ',I3,
     1       ', EXCEEDS THE SUPPORTED MAXIMUM OF ',I3,'.')

      END 

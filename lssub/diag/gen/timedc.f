      SUBROUTINE TIMEDC(NSEC,ITIME) 
C 
C     * 80-1-29, J.R. GILLESPIE.
C 
C     * CONVERTS FGGE TIME CODE (I.E. YYMMDDHH FORMAT) TO THE NUMBER
C     * OF SECONDS AFTER A REFERENCE TIME (THE BEGINNING OF 1950).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER ID(8), INY(12,2)
      DATA INY /0,31,59,90,120,151,181,212,243,273,304,334, 
     A          0,31,60,91,121,152,182,213,244,274,305,335/ 
C-----------------------------------------------------------------------
C 
C     * UNPACK THE YEAR, MONTH, DAY AND HOUR GROUPS FROM THE TIME CODE. 
C 
      CALL DIGITS(ID,8,ITIME) 
      IYR = 10*ID(8)+ID(7)+1900 
      IMO = 10*ID(6)+ID(5)
      IDY = 10*ID(4)+ID(3)
      IHR = 10*ID(2)+ID(1)
C 
C     * COMPUTE THE NUMBER OF DAYS SINCE THE BEGINNING OF 1 B.C.
C     * (ALGORITHM BY S.J. LAMBERT.)
C 
C     * FIRST GET THE NUMBER OF DAYS FROM 1 B.C. UP TO THE START OF THE 
C     * CURRENT YEAR. 
C 
      NCURYR = 365*(IYR-1)+(IYR-1)/4-(IYR-1)/100+(IYR-1)/400
C 
C     * DECIDE WHETHER THE CURRENT YEAR IS A LEAP YEAR. 
C 
      ILEAP = 0 
      IF ((MOD(IYR,4).EQ.0 .AND. MOD(IYR,100).NE.0) 
     A    .OR. MOD(IYR,400).EQ.0)                     ILEAP = 1 
C 
C     * ADD ON THE NUMBER OF DAYS FROM THE START OF THE CURRENT YEAR. 
C 
      NCOUNT = NCURYR+INY(IMO,ILEAP+1)+IDY-1
C 
C     * NOW SUBTRACT THE NUMBER OF DAYS FROM 1 B.C. TO 1950 A.D.
C 
      NDAYS = NCOUNT-711857 
C 
C     * CONVERT DAYS TO HOURS AND ADD THE EXTRA HOURS FROM THE TIME CODE
C 
      NHOUR = 24*NDAYS+IHR
C 
C     * CONVERT TO SECONDS. 
C 
      NSEC = 3600*NHOUR 
      RETURN
C-----------------------------------------------------------------------
      END 

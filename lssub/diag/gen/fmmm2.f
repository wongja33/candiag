      SUBROUTINE FMMM2(FMIN,FMAX,FMEAN,F,LA)
C 
C     * SEP 27/79 - J.D.HENDERSON 
C     * COMPUTES MIN,MAX,MEAN OF FIELD F(LA). 
C 
C     * LEVEL 2,F 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL F(LA)
C-------------------------------------------------------------------- 
C 
      FSUM=0.E0 
      FMIN=F(1) 
      FMAX=F(1) 
C 
      DO 210 I=1,LA 
      IF(F(I).LT.FMIN) FMIN=F(I)
      IF(F(I).GT.FMAX) FMAX=F(I)
  210 FSUM=FSUM+F(I)
      FMEAN=FSUM/FLOAT(LA)
C 
      RETURN
      END 

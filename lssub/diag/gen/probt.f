      SUBROUTINE PROBT(T,NDF,NWDS,P,WK1,WK2,WK3,WK4)
C 
C     * F. ZWIERS  DEC 6,1984 
C     * 
C     * COMPUTE PROB(T>T-OBSERVED)
C     * 
C-----------------------------------------------------------------------------
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION T(1),P(1),WK1(1),WK2(1),WK3(1),WK4(1) 
C 
C 
      DO 10 I=1,NWDS
         WK1(I)=T(I)*T(I) 
   10 CONTINUE
      CALL PROBF(WK1,1,NDF,NWDS,P,WK2,WK3,WK4)
      DO 20 I=1,NWDS
        P(I)=MERGE(P(I)/2.0E0,1-P(I)/2.0E0,T(I).GE.0.E0) 
   20 CONTINUE
C 
C-----------------------------------------------------------------------------
C 
      RETURN
      END 

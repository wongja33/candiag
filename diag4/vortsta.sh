#!/bin/sh
#                  vortsta             gjb,ml,dl. jan 24/00 - sk.
#   ---------------------------------- calculate vorticity statistics,
#                                      covariances with u, v and w
#                                      and zonal statistics.
#   sk: re-write for new timavg/xsave/rzonavg. phase out xtrans.
#
      access x ${flabel}_gpvr
      access u ${flabel}_gpu
      access v ${flabel}_gpv

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF       VORT $t1 $t2 $delt" | ccc dif x dxdt
      else
	echo "DIFF       VORT $t1 $t2 $delt       1.0" | ccc dif x dxdt
      fi
      timavg x  tx xp  txp2
      timavg u  tu up
      timavg v  tv vp
      timcov xp up txpup
      timcov xp vp txpvp

echo "XSAVE.        VORT
NEWNAM.
XSAVE.        DVORT/DT
NEWNAM.      P.
XSAVE.        VORT\"VORT\"
NEWNAM.    P\"P\"
XSAVE.        VORT\"U\"
NEWNAM.    P\"U\"
XSAVE.        VORT\"V\"
NEWNAM.    P\"V\"" > ic.xsave
      vars="tx dxdt txp2 txpup txpvp"

      if [ "$wxstats" = on ]; then
#   ---------------------------------- do calculations for w
         access w  ${flabel}_gpw
         timavg w  tw wp
         timcov xp wp txpwp
echo "XSAVE.        VORT\"W\"
NEWNAM.    P\"W\"" >> ic.xsave
         vars="$vars txpwp"
      fi

#   ---------------------------------- xsave
      xsave new_gp $vars new.
      mv new. new_gp

#   ---------------------------------- zonal averages.
      access oldgp ${flabel}gp
      echo "XFIND         $d" | ccc xfind oldgp beta
#                                      [x]r, [dx/dt]r,[x"x"]r, and [x*x*]r.
      rzonavg tx    beta rztx  xs
      rzonavg dxdt  beta rzdxdt
      square  xs    xs2
      rzonavg xs2   beta rzxs2
      rzonavg txp2  beta rztxp2
#                                      [x'v']r and [x*v*]r.
      rzonavg tv    beta rztv  vs
      mlt     vs    xs   xsvs
      rzonavg xsvs  beta rzxsvs
      rzonavg txpvp beta rztxpvp

echo "XSAVE.        (VORT)R
NEWNAM.    VORT
XSAVE.        (DVORT/DT)R
NEWNAM.    P/DT
XSAVE.        (VORT*VORT*)R
NEWNAM.    P*P*
XSAVE.        (VORT\"VORT\")R
NEWNAM.    P\"P\"
XSAVE.        (VORT*V*)R
NEWNAM.    P*V*
XSAVE.        (VORT\"V\")R
NEWNAM.    P\"V\"" > ic.xsave
      vars="rztx rzdxdt rzxs2 rztxp2 rzxsvs rztxpvp"

      if [ "$wxstats" = on ]; then
#   ---------------------------------- do calculations for w
#                                      [x'w']r and [x*w*]r.
        rzonavg tw    beta rztw  ws
        mlt     ws    xs   xsws
        rzonavg xsws  beta rzxsws
        rzonavg txpwp beta rztxpwp
echo "XSAVE.        (VORT*W*)R
NEWNAM.    P*W*
XSAVE.        (VORT\"W\")R
NEWNAM.    P\"W\"" >> ic.xsave
        vars="$vars rzxsws rztxpwp"
      fi

#   ---------------------------------- xsave
      xsave new_xp $vars new.
      mv new. new_xp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi

#deck sfctest
jobname=sfct ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   remove condef parameter "pooled" and join gb file to gp file.
#                   sfctest             m.lazare. jun 22/90 - ec.
#   ----------------------------------- computes the standard t-test or f-test
#                                       for significance of the deviation of a
#                                       statistic from its pooled control
#                                       state  and plots out the result for the
#                                       transformed significance levels
#                                        k-values . the ratio of hypothesis
#                                       test rejection is also calculated.
#                                       this test is done for statistics at the
#                                       surface.
# 
#                                       model1  u1  v1  and letter x refer to
#                                       pooled control run  while model2  u2 
#                                       v2 and letter y refer to the experim-
#                                       ental run.
#
    access gpfile ${model1}gp
    access gbfile ${model1}gb na
    joinup file1 gpfile gbfile 
    rm gpfile gbfile
# 
    access gpfile ${model2}gp
    access gbfile ${model2}gb na
    joinup file2 gpfile gbfile 
    rm gpfile gbfile
# 
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- make initial null file
    xfind file1 alb 
    xlin alb null 
    rm alb
#   ----------------------------------- mean sea level pressure pmsl .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
#   ----------------------------------- precipitation rate pcp .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
#   ----------------------------------- surface moisture flux qfs .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
#   ----------------------------------- surface heat flux hfs .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
    if [ "$nobeg" != on ] ; then
#     ----------------------------------- surface energy balance beg .
.     ttestgp.cdk
      ggplot mdif _ _ kvalu 
      rm mdif kvalu
    fi
#   ----------------------------------- surface wind stress ufs vfs .
.   ht2test.cdk
    ggplot amp du dv kvalu 
    rm amp du dv kvalu
#   ----------------------------------- model level wind lu lv .
.   ht2test.cdk
    ggplot amp du dv kvalu 
    rm amp du dv kvalu
#   ----------------------------------- lowest model level temperature lt .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
#   ----------------------------------- lowest model level specific humidity lq .
.   ttestgp.cdk
    ggplot mdif _ _ kvalu 
    rm mdif kvalu
    rm file1 file2
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 SFCTEST -------------------------------------------------------------- SFCTEST
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0      0.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PMSL) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(PMSL) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -40.E0     40.E0      2.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PMSL. UNITS MBS.
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR PMSL. 
              3    0   15    9
                1.E0      2.E0      1.E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(PCP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(PCP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E5    -20.E0     20.E0    25.E-13${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PCP. UNITS 1.E-5*KG/(M2*SEC). 
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR PCP.
              3    0   15    9
                1.E0      2.E0      1.E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(QFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(QFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E6    -40.E0     40.E0     10.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR QFS. UNITS 1.E-6*KG/(M2*SEC). 
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR QFS.
              3    0   15    9
                1.E0      2.E0      1.E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(HFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(HFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR HFS. UNITS W/M2.
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR HFS.
              3    0   15    9
                1.E0      2.E0      1.E2
if [ "$nobeg" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BEG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(BEG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BEG. UNITS W/M2.
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR BEG.
              3    0   15    9
                1.E0      2.E0      1.E2
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(UFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(VFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS-VFS-
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(UFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(VFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS-VFS-
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
T2TEST.      $alpha$xyr$yyr    1 
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP    1$map      1.E2    -80.E0     80.E0      4.E04${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR SFC WIND STRESS. UNITS 1.E-2*N/M2.
GGPLOT.           -1    K    1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR SFC.
              3    0   15    9
                1.E0      2.E0      1.E2
                1.E2        0.       10.$ncx$ncy
                      VECPLOT OF DIFFERENCE IN (TAUX,TAUY)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LU) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LV) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU-LV-
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LU) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LV) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU-LV-
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
T2TEST.      $alpha$xyr$yyr    1 
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP   -1$map      1.E0      0.E0     30.E0      2.E04${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL WIND. UNITS M/SEC
GGPLOT.           -1    K   -1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR LOWEST MODEL LEVEL WIND.
              3    0   15    9
                1.E0      2.E0      1.E2
                1.E0        0.        5.$ncx$ncy
                      VECPLOT OF DIFFERENCE IN (U,V). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(LT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E0    -40.E0     40.E0      2.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL T. UNITS DEGK. 
GGPLOT.           -1    K   -1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR LOWEST MODEL LEVEL T. 
              3    0   15    9
                1.E0      2.E0      1.E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(LQ) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.    1   S(LQ) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
TTESTE.      $alpha$xyr$yyr    0    1
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
EXTRAC.   $lnl$lnr$ltb$ltt    1    0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E3    -40.E0     40.E0      2.E03${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL Q. UNITS G/KG. 
GGPLOT.           -1    K   -1$map      1.E0     -4.E0      1.E2      1.E00-1+8
                      K-VALUE           FOR LOWEST MODEL LEVEL Q. 
              3    0   15    9
                1.E0      2.E0      1.E2
. tailfram.cdk

end_of_data

. endjcl.cdk



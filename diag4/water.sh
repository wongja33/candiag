#!/bin/sh
#                  water              r. harvey - Jun. 10/98
#   ---------------------------------- compute water-related fields (including clouds)
#                                      for model version gcm12.
#   new deck for gcm12-> with fields now saved in model.
# 

.     ggfiles.cdk
#   ---------------------------------- select fields and time average.
#   ---------------------------------- first, multi-level fields.
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS    1 1000 NAME  CLD   RH TACN" | ccc select npakgg cld rh tac
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS    1 1000 NAME  CLW  CIC" | ccc select npakgg clw cic

#   ---------------------------------- cloud amount.
      timavg  cld    tcld
      zonavg  tcld   rztcld
#   ---------------------------------- relative humidity.
      timavg  rh     trhum
      zonavg  trhum  ztrhum
#   ---------------------------------- optical depth.
      timavg  tac    ttac
      zonavg  ttac   zttac
#   ---------------------------------- cloud water content.
      timavg  clw    tclw
      zonavg  tclw   ztclw
#   ---------------------------------- cloud ice content.
      timavg  cic    tcic
      zonavg  tcic   ztcic

echo "XSAVE.        (SCLD)R
NEWNAM.    SCLD
XSAVE.        (RHC)R
NEWNAM.     RHC
XSAVE.        (TACN)R
NEWNAM.    TACN
XSAVE.        (CLW)R
NEWNAM.     CLW
XSAVE.        (CIC)R
NEWNAM.     CIC" | ccc xsave new_xp rztcld ztrhum zttac ztclw ztcic new. ; mv new. new_xp

echo "XSAVE.        SCLD
NEWNAM.    SCLD
XSAVE.        RHC
NEWNAM.     RHC
XSAVE.        TACN
NEWNAM.    TACN
XSAVE.        CLW
NEWNAM.     CLW
XSAVE.        CIC
NEWNAM.     CIC" | ccc xave new_gp tcld trhum ttac tclw tcic new. ; mv new. new_gp

      rm rztcld tcld cld ztrhum rhum trhum zttac  tac  ttac ztclw  clw  tclw ztcic  cic  tcic

#   ---------------------------------- now, single-level fields.
echo "SELECT.   STEPS $t1 $t2 $t3 LEVS    1 1000 NAME CLDT CLWT CICT PWAT" | ccc   select npakgg cldt clwt cict pwat
      rm npakgg
#   ---------------------------------- total overlapped cloud amount.
      timavg  cldt   tcldt
#   ---------------------------------- precipitable water.
      timavg pwat tpwat
#   ---------------------------------- total liquid water path.
      timavg  clwt   tclwt
#   ---------------------------------- total ice path.
      timavg  cict   tcict

echo "XSAVE.        CLDT
NEWNAM.    CLDT
XSAVE.        PWAT
NEWNAM.    PWAT
XSAVE.        CLWT
NEWNAM.    CLWT
XSAVE.        CICT
NEWNAM.    CICT" | ccc xave new_gp tcldt tpwat tclwt tcict new. ; mv new. new_gp
      rm cldt tcldt pwat tpwat clwt tclwt cict tcict

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi

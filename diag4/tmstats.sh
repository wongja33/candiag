#!/bin/sh
#                  tmstats             sk - Feb 20/17
#   ---------------------------------- compute statistics on all fields
#                                      in tm file

#  ---------------------------------- access tm files

      splitfiles_tm="on"
      spltdir=${splitdir:-"$RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaktm"}
      wrkdir=`pwd`
.     tmfiles.cdk

#  ---------------------------------- Do for each variables in splitdir
      cd $spltdir
      vars=`cat .VARTABLE | cut -c1-4`

#  ----------------------------------- compute and save standard statistics.

      rcm="on" # to avoid calculating zonal averages
      statsav $vars $wrkdir/old_dat $wrkdir/dummy "off" ; rcm="off" # set rcm back to off
      cd $wrkdir

#   ---------------------------------- save results
      save old_dat ${flabel}tp

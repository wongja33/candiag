#comdeck engjcl
#                  engjcl              ml nes. sept 27/84 - ml. --------------
#   ----------------------------------- computes vertically integrated total
#                                       energy stream and potential functions.
#   ----------------------------------- prepare fields.
#                                       first find basic fields.
    xfind gp bet 
    xfind gp temp 
    xfind gp u 
    xfind gp v 
    xfind gp phi 
#   ----------------------------------- multiply temp terms by cp.
    xlin temp tempe 
    rm temp
    xlin u unit 
#   ----------------------------------- construct mean energy flux.
    add tempe phi x 
    rm tempe phi
#   ----------------------------------- first the rotational part.
    mlt x bet bx 
    mlt u bet bu 
    mlt v bet bv 
    mlt u bx bxu 
    vpint bxu wibxu 
    rm bxu
    mlt v bx bxv 
    vpint bxv wibxv 
    rm bxv
    newnam wibxu ibxu 
    rm wibxu
    newnam wibxv ibxv 
    rm wibxv
    gwtqd ibxu ibxv cibxv y 
    rm ibxu ibxv y
#   ----------------------------------- next the divergent part.
#                                       vertical mean term.
    vpint bu ibu 
    vpint bv ibv 
    vpint bx ibx 
    rm bu bv bx
    vpint bet ibet 
    div ibx ibet ixr 
    rm ibx
    ggacof ixr isxr 
    gwtqd ibu ibv ibq ibd 
    cwinds ibq ibd isbu isbv 
    rm ibq ibd
    spvdgx isbu isbv isxr bvdgx 
    rm isxr isbu isbv
#   ----------------------------------- vertical deviation term.
    div ibu ibet iur 
    rm ibu
    div ibv ibet ivr 
    rm ibv ibet
    psmlt iur unit mlur 
    rm iur
    psmlt ivr unit mlvr 
    rm ivr
    psmlt ixr unit mlxr 
    rm ixr unit
    sub u mlur ju 
    sub v mlvr jv 
    sub x mlxr jx 
    rm u v x mlur mlvr mlxr
    mlt ju jx ujxj 
    mlt jv jx vjxj 
    mlt ujxj bet bujxj 
    mlt vjxj bet bvjxj 
    rm jx ujxj vjxj
    vpint bujxj wibujxj 
    vpint bvjxj wibvjxj 
    newnam wibujxj ibujxj 
    newnam wibvjxj ibvjxj 
    rm wibujxj wibvjxj ju jv bujxj bvjxj
    gwtqd ibujxj ibvjxj y dvibxv 
    rm ibujxj ibvjxj y
#   ----------------------------------- total vertical mean flux.
    add bvdgx dvibxv wdibxv 
    newnam wdibxv dibxv 
    rm bvdgx dvibxv wdibxv
    xlin cibxv zeroq 
    xlin dibxv zerod 
    cwinds cibxv zerod mru mrv 
    cwinds zeroq dibxv mdu mdv 
    splinv cibxv npsim 
    xlin npsim psim 
    rm cibxv npsim zerod
    splinv dibxv nchim 
    xlin nchim chim 
    rm dibxv nchim
#   ----------------------------------- now find covariance terms.
    xfind gp tpup 
    xfind gp tpvp 
    xfind gp zpup 
    xfind gp zpvp 
    xlin tpup tpupe 
    xlin tpvp tpvpe 
    rm tpup tpvp
#   ----------------------------------- construct eddy energy flux.
    add tpupe zpup xpup 
    add tpvpe zpvp xpvp 
    rm tpupe zpup tpvpe zpvp
    mlt xpup bet bxpup 
    mlt xpvp bet bxpvp 
    rm xpup xpvp bet
    vpint bxpup wibxpup 
    vpint bxpvp wibxpvp 
    newnam wibxpup ibxpup 
    newnam wibxpvp ibxpvp 
    rm bxpup bxpvp wibxpup wibxpvp
#   ----------------------------------- relabel files constructed from pooled
#                                       data  before using gwtqd and cwinds  or
#                                         elsethese programs may fail due to
#                                       mismatched time step numbers.
    if [ "$pooled" = on ] ; then
      relabl ibxpup udum 
      relabl ibxpvp vdum 
      relabl zeroq qdum 
      rm ibxpup ibxpvp zeroq
      cp udum ibxpup 
      cp vdum ibxpvp 
      cp qdum zeroq 
      rm udum vdum qdum
    fi
    gwtqd ibxpup ibxpvp cibxpvp dibxpvp 
    rm ibxpup ibxpvp
    xlin dibxpvp zerod 
    cwinds cibxpvp zerod eru erv 
    add mru eru wtru 
    add mrv erv wtrv 
    rm mru mrv eru erv
    newnam wtru tru 
    newnam wtrv trv 
    rm wtru wtrv zerod
    cwinds zeroq dibxpvp edu edv 
    add mdu edu wtdu 
    add mdv edv wtdv 
    newnam wtdu tdu 
    newnam wtdv tdv 
    rm mdu mdv edu edv wtdu wtdv zeroq
    splinv cibxpvp npsie 
    xlin npsie psie 
    add psim psie psit 
    splinv dibxpvp nchie 
    xlin nchie chie 
    add chim chie chit 
    rm npsie nchie psim psie chim chie cibxpvp dibxpvp
#   ----------------------------------- g.g. projection of velocity potential.
    cofagg chit gchit 
#   ----------------------------------- p.s. projection of streamfunction.
    cofaps psit gpsitn 
    cofaps psit gpsits 
#   ----------------------------------- vectors to grid representation.
    cofagg tru gtru 
    cofagg trv gtrv 
    cofagg tdu gtdu 
    cofagg tdv gtdv 
    rm tru trv tdu tdv psit chit
#   ----------------------------------- grid vectors to p.s. coordinates.
    ggvecps gtru gtrv ngtrup ngtrvp sgtrup sgtrvp 
#   ----------------------------------- grid vectors to p.s. projection.
    ggaps ngtrup gtrupn 
    ggaps ngtrvp gtrvpn 
    ggaps sgtrup gtrups 
    ggaps sgtrvp gtrvps 
    rm gtru gtrv ngtrup ngtrvp sgtrup sgtrvp
#   .......................................................................engjcl

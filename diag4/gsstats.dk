#deck gsstats
jobname=gsstats ; time=$gptime ; memory=$memory3
. comjcl.cdk

cat > Execute_Script <<'end_of_script'

# sk: gsstats.dk is a merge of mom_stat.dk, eng_stat4.dk, wat_stat.dk,
#     sfc_stat.dk, misc_stat.dk, and cloud8.dk.
#                  gsstats             sk - Nov 17/08 - sk
#   ---------------------------------- compute statistics on fields 
#                                      in gs file.
#
#  Output variables:
#
#  ----------------------------------- from mom_stat.dk
#
#   UFS : zonal wind stress
#  OUFS : zonal wind stress without gravity wave drag effects
#   VFS : meridional wind stress
#  OVFS : meridional wind stress without gravity wave drag effects
#  TAUA : wind stress amplitude
# OTAUA : wind stress amplitude without gravity wave drag effects
#    SU : zonal anemometer (10m) wind
#    SV : meridional anemometer (10m) wind
#  SWMX : mean 10m wind amplitude (NOT wind maximum)
#
#  ----------------------------------- from wat_stat.dk
#
#   PCP : total precipitation rate
#  PCPN : snowfall rate
#  PCPC : convective precipitation rate
#  PWAT : precipitable water
#   QFS : moisture flux
#   BWG : fresh water flux (at ice-atmosphere interface above sea-ice)
#  OBWG : fresh water flux (at ice-ocean interface below sea-ice)
#    SQ : screen specific humidity
#
#  ----------------------------------- from cloud8.dk
#
#  SCLD : cloud amount
#   RHC : relative humidity
#  TACN : optical depth
#   CLW : cloud water content
#   CIC : cloud ice content
#  CLDT : total overlapped cloud amount
#  CLDO : total overlapped cloud amount with optical depth cutoff (for gcm16+)
#  CLWT : integrated cloud water path
#  CICT : integrated cloud ice path
#
#  ----------------------------------- from misc_stat.dk
#
#  PBLH : height of planetary boundary layer
#   TCV : top of convection
#    DR : surface drag coefficients
#
#  ----------------------------------- from eng4_stat.dk
#
# 1) Energy flux terms:
#
#  FSGV : Net shortwave flux into vegetation
#  FLGV : Net longwave flux into vegetation
#  HMFV : Energy used in melting/ freezing of ice/water on vegetation
#  HFCV : Heat transfer to vegetation via precipitation interception
#  HFSV : Sensible heat flux from vegetation
#  HFLV : Latent heat flux from vegetation
#  FSGN : Net shortwave flux across snow surface
#  FLGN : Net longwave flux across snow surface
#  HMFN : Energy used in melting/ freezing of ice/water in snow pack
#  HFCN : Heat transfer through snow via percolation and conduction
#  HFSN : Sensible heat flux from snow
#  HFLN : Latent heat flux from snow
#  FSGG : Net shortwave flux across soil surface
#  FLGG : Net longwave flux across soil surface
#  HFSG : Sensible heat flux from soil surface
#  HFLG : Latent heat flux from soil surface
#  HMFG : Energy used in melting/ freezing of ice/water in soil layers
#  HFCG : Heat transfer through soil layers via percolation and conduction
#
# 2) Moisture flux terms:
#
#  PIVF : Frozen water input to vegetation
#  PIVL : Liquid water input to vegetation
#  QFVF : Sublimation rate of frozen water from vegetation
#  QFVL : Evaporation rate of liquid water from vegetation
#  ROFV : Total water flow from bottom of vegetation canopy
#   QFN : Sublimation rate from snow surface
#   PIN : Total water input (regardless of source) falling on snow
#  ROFN : Water flow from bottom of snow pack
#  ROVG : Water flow from bottom of vegetation canopy onto bare soil
#   PIG : Total water input (regardless of source) reaching soil surface
#   QFG : Evaporation rate from soil surface
#  ROFO : Overland runoff
#   ROF : Total (surface outflow and bottom drainage) runoff
#  QFVG : Water extraction from soil layers due to transpiration
#
# 3) Energy storage terms:
#
#    TV : Canopy temperature
#    TN : Snow temperature
#    GT : Ground temperature
#  TBAS : Bedrock temperature
#   FVG : Fraction of canopy over bare ground
#    FN : Snow fraction
#   FVN : Fraction of canopy over snow
#  SKYN : Sky view factor for vegetation over snow
#  SKYG : Sky view factor for vegetation over ground
#    TG : Soil layer temperature
#
# 4) Moisture storage terms:
#
#   WVF : Frozen water on vegetation
#   WVL : Liquid water on canopy
#   SNO : Snow depth (water equivalent)
#   WGF : Soil layer frozen water content
#   WGL : Soil layer liquid water content
#
# 5) Misc terms:
#
#    AN : Snow albedo
#    ZN : Snow depth
#  RHON : Snow density
#  SMLT : Snow melting rate
#    MV : Instantaneous vegetation canopy mass
#    TT : Growth index for trees
#    GC : Ground cover
#   SIC : Sea-ice mass
#  SICN : Sea-ice concentration
#  WTRN : Gains/losses to snow pack because of freezing of surface water or because of disappearance of vegetation canopy
#  WTRG : Gains/losses to surface soil water because of freezing of surface water or because of disappearance of vegetation canopy
#  WTRV : Intercepted water transferred from vegetation canopy to soil or snow because of canopy disappearance
#
#  ----------------------------------- from eng4_stat.dk
#
#  1) Energy fluxes:
#
#      a) Atmospheric energy fluxes
#
#   FSO : solar flux incident on top of atmosphere
#   FSA : solar flux absorbed by atmosphere
#   FLA : net lw absorbed/emitted by atmosphere
#  FSLO : solar/lw overlap (gcm15+)
#
#      b) Surface energy fluxes
#
#   FSS : flux received at the surface
#   FSG : solar heat absorbed by ground
#   FLG : net lw at the ground
#   FDL : downward atmospheric lw at sfc
#   FSV : visible flux received at the surface
#   FSD : direct flux received at the surface
#   HFS : surface sensible heat flux
#   HFL : surface latent heat flux
#
#      c) Earth-atmosphere fluxes
#
# FSAG : heat absorbed by earth-atmosphere system,
#        i.e. net solar flux at top of the atmosphere
# FLAG : net lw emitted by earth-atmosphere system,
#        i.e. net lw at the top of atmosphere
#
#   2) Net fluxes
#
# BALG : radiation budget at the ground
# BALT : radiation budget at top of atmosphere
#        i.e. at the highest full sigma level
#  FSR : reflected solar flux at TOA
# FSRC : clear-sky reflected solar flux at TOA
#  OLR : outgoing l/w radiation
# OLRC : clear-sky outgoing l/w radiation
#  BEG : net surface energy flux (at ice-atmosphere interface above sea-ice)
# OBEG : net surface energy flux (at ice-ocean interface below sea-ice)
#  RES : energy residual (used for non-interactive ocean runs)
# CBGO : energy removed at sea-ice/ocean interface to ensure no heat flow into
#        ice covered ocean when sea-ice mass is specifed
#        (used for non-interactive ocean runs)
#
#   3) Near-surface quantities
#
#   ST : screen level (2m) temperature
#  STMX: mean screen level daily maximum temperature
#  STMN: mean screen level daily minimum temperature
#
#   4) Misc
#
#      a) Albedoes - calculated from monthly mean fluxes, by default, unless
#         albedo_monthly=off in which case they are computed from daily fluxes.
#
#  LPA : planetary albedo
# SALB : surface albedo
#
#      b) Clear-sky fluxes, cloud forcings and albedoes
#
# FSTC : net clear-sky solar flux at toa.
# FSGC : net clear-sky solar flux at ground.
# CFST : solar cloud forcing at toa.
# CFSB : solar cloud forcing at ground.
# FLTC : net clear-sky lw flux at toa.
# FLGC : net clear-sky lw flux at ground.
# CFLT : lw cloud forcing at toa.
# CFLB : lw cloud forcing at ground.
#  CFT : total cloud forcing at toa.
#  CFB : total cloud forcing at ground.
# LPAC : clear-sky planetary albedo

#  ---------------------------------- access gs files
 
.     ggfiles.cdk

#  ---------------------------------- Select variables

#  ---------------------------------- from mom_stat.dk + wat_stat.dk
 
echo "SELECT    STEPS $g1 $g2 $g3 LEVS    1    1 NAME  UFS OUFS  VFS OVFS
            BWG OBWG" | ccc select npakgg  UFS OUFS  VFS OVFS BWG OBWG

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME  PCP PCPC PCPN PWAT
            QFS   SQ SWMX   SU   SV" | ccc select npakgg  PCP PCPC PCPN PWAT \
             QFS   SQ SWMX   SU   SV

#  ---------------------------------- from cloud8.dk + misc_stat.dk

echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME  CLD TACN  CLW  CIC
             RH CLDT CLWT CICT PBLH  TCV   DR" | ccc select npakgg SCLD TACN  CLW  CIC \
              RHC CLDT CLWT CICT PBLH  TCV  DR

#                                      try to select CLDO (gcm16+)

echo "SELECT          $t1 $t2 $t3         1    1      CLDO" | ccc select npakgg CLDO || true

#                                      test for CLDO

    CLDO="CLDO"
    if [ ! -s CLDO ] ; then
      CLDO=""
    fi

#  ---------------------------------- from sfc_stat.dk
#                                     energy flux terms.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME FSGV FLGV HMFV HFCV
           HFSV HFLV FSGN FLGN HMFN HFCN HFSN HFLN FSGG FLGG HFSG HFLG" | ccc select npakgg FSGV FLGV HMFV HFCV \
            HFSV HFLV FSGN FLGN HMFN HFCN HFSN HFLN FSGG FLGG HFSG HFLG

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    3 NAME HMFG HFCG" | ccc select npakgg HMFG HFCG

#                                     moisture flux terms.
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PIVF PIVL QFVF QFVL
           ROFV  QFN  PIN ROFN ROVG  PIG  QFG ROFO  ROF" | ccc select npakgg PIVF PIVL QFVF QFVL \
            ROFV  QFN  PIN ROFN ROVG  PIG  QFG ROFO ROF

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    3 NAME QFVG" | ccc select npakgg QFVG

#                                     energy storage terms.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME   TV   TN   GT TBAS
            FVG   FN  FVN SKYG SKYN" | ccc select npakgg   TV   TN   GT TBAS  FVG  FN  FVN SKYG SKYN

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    3 NAME   TG" | ccc select npakgg   TG

#                                     moisture storage terms.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME  WVF  WVL  SNO" | ccc select npakgg  WVF  WVL  SNO
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    3 NAME  WGF  WGL" | ccc select npakgg  WGF  WGL

#                                     misc terms.

echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME   AN   ZN RHON SMLT
             MV   TT  SIC SICN   GC WTRN WTRG WTRV" | ccc select npakgg   AN   ZN RHON SMLT \
                    MV   TT  SIC SICN  GC WTRN WTRG WTRV

#  ---------------------------------- from eng_stat4.dk

echo "SELECT          $r1 $r2 $r3         1    1       FSO  FSA  FLA  FSS
SELECT      FSG  FLG  FDL  FSV  FSD  HFS  HFL FSTC FSGC FLTC FLGC" | ccc select npakgg  FSO  FSA  FLA FSS  \
                   FSG  FLG  FDL FSV  FSD  HFS  HFL FSTC FSGC FLTC FLGC

echo "SELECT          $g1 $g2 $g3         1    1       BEG OBEG" | ccc select npakgg  BEG OBEG
echo "SELECT          $t1 $t2 $t3         1    1        ST STMX STMN" | ccc select npakgg   ST STMX STMN

#                                      try to select FSLO used with gcm15 onward
#                                      in calculation of planetary albedo.
echo "SELECT          $r1 $r2 $r3         1    1      FSLO" | ccc select npakgg FSLO || true

#                                      create a zero field if FSLO does not exist.

    if [ ! -s FSLO ] ; then
      echo ' XLIN             0.        0.' > .xlin_input_card
      xlin FSO FSLO input=.xlin_input_card
      rm .xlin_input_card
    fi

#                                      try to select RES and CBGO
#                                      (for non-interactive ocean coupled runs only)
echo "SELECT          $g1 $g2 $g3         1    1       RES CBGO" | ccc select npakgg RES CBGO || true

#                                      test for RES

    RES="RES CBGO"
    if [ ! -s RES ] ; then
      RES=""
    fi

    rm -f npakgg .npakgg_Link

#  ----------------------------------- Derivative variables computation
#                                      (mainly based on eng_stat4.dk)

#  ----------------------------------- earth-atmosphere fluxes

    add FSA FSG FSAG
    add FLA FLG FLAG

#  ----------------------------------- energy budgets

    add FSAG FLAG BALT
    add FSG  FLG  BALG

#   ---------------------------------- reflected solar flux at TOA.
#                                      limit FSR to non-negative values to
#                                      avoid numerical differences between
#                                      fsag calculated from model data  i.e.
#                                      fsa derived from vertical integration
#                                      of heating rates  and "precise"
#                                      calculation of fso.

    echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card

    sub    FSO  FSLO fsia
    sub    fsia FSAG fsri
    fmask  fsri msk  input=.fmask_input_card
    mlt    fsri msk  FSR
    rm     fsri msk  fsia

#   ---------------------------------- local planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSR .tFSR
      timavg FSO .tFSO
      div  .tFSR .tFSO LPA
    else
      div    FSR   FSO LPA
    fi

#   ---------------------------------- reflected clear-sky solar flux at TOA.
#                                      limit FSRC to non-negative values to
#                                      avoid numerical differences between
#                                      fsag calculated from model data  i.e.
#                                      fsa derived from vertical integration
#                                      of heating rates  and "precise"
#                                      calculation of fso.

    sub    FSO  FSLO fsia
    sub    fsia FSTC fsri
    fmask  fsri msk  input=.fmask_input_card
    mlt    fsri msk  FSRC
    rm     fsri msk  fsia

#   ---------------------------------- local clear-sky planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSRC .tFSRC
      div  .tFSRC .tFSO LPAC
    else
      div    FSRC   FSO LPAC
    fi

#   ---------------------------------- ground albedo. limit fsrg to
#                                      non-negative values to avoid problems
#                                      with small numerical differences
#                                      when fss is near zero.
    sub    FSS   FSG  fsrgi
    fmask  fsrgi msk  input=.fmask_input_card
    mlt    fsrgi msk  fsrg
    if [ "$albedo_monthly" != "off" ] ; then
      timavg fsrg tfsrg
      timavg FSS  .tFSS
      div   tfsrg .tFSS SALB
    else
      div    fsrg  FSS  SALB
    fi
    rm fsrgi msk  fsrg

#   ----------------------------------  OLR (all-sky and clear-sky).

    sub    FSLO  FLAG OLR
    sub    FSLO  FLTC OLRC

#   ---------------------------------- cloud forcings

    sub FSAG FSTC CFST
    sub FSG  FSGC CFSB
    sub FLAG FLTC CFLT
    sub FLG  FLGC CFLB
    add CFST CFLT CFT
    add CFSB CFLB CFB

#   ---------------------------------- surface wind stresses

    mag2d  UFS  VFS  TAUA
    mag2d OUFS OVFS OTAUA
#                                      convert K->C
#   ---------------------------------- screen temperature and daily maximum/
#                                      minimum screen temperature.

    echo ' XLIN          1.0E0 -2.7316E2' > .xlin_input_card
    for x in ST STMX STMN GT ; do
      xlin $x .$x input=.xlin_input_card
      mv .$x $x
    done


#  ----------------------------------- compute and save statistics.

    statsav UFS  OUFS VFS  OVFS TAUA OTAUA SU SV SWMX \
            PCP  PCPC PCPN PWAT QFS  BWG  OBWG SQ \
            SCLD RHC  TACN CLW  CIC  CLDT $CLDO CLWT CICT \
            PBLH TCV  DR \
            FSGV FLGV HMFV HFCV HFSV HFLV FSGN FLGN HMFN HFCN \
            HFSN HFLN FSGG FLGG HFSG HFLG HMFG HFCG \
            PIVF PIVL QFVF QFVL ROFV QFN  PIN  ROFN ROVG PIG \
            QFG  ROFO ROF  QFVG \
            TV   TN   GT   TBAS FVG  FN   FVN  SKYN SKYG TG \
            WVF  WVL  SNO  WGF  WGL \
            AN   ZN   RHON SMLT MV   TT   GC   SIC  SICN \
            WTRN WTRG WTRV \
            FSO  FSA  FLA  FSS  FSG  FLG  FDL  FSV  FSD  HFS  HFL \
            FSAG FLAG BALT BALG BEG  OBEG \
            ST   STMX STMN FSR  FSRC OLR  OLRC \
            FSTC FSGC FLTC FLGC CFST CFSB CFLT CFLB CFT  CFB $RES \
            new_gp new_xp $stat2nd

    statsav SWMX PCP STMX new_gp new_xp timmax
    statsav STMN          new_gp new_xp timmin

#  ----------------------------------- compute albedos only where FSO > 5 W/m2
#                                      (~1% of max FSO) to avoid areas where
#                                      albedos are unreliable.

    echo ' FMASK            -1   -1   GT        5.' > .fmask_input_card_alb

    if [ "$albedo_monthly" != "off" ] ; then
      fmask .tFSO  MFSO input=.fmask_input_card_alb
      fmask .tFSS  MFSS input=.fmask_input_card_alb
      statsav mask=MFSO LPA LPAC new_gp new_xp off
      statsav mask=MFSS SALB     new_gp new_xp off
    else
      fmask   FSO  MFSO input=.fmask_input_card_alb
      fmask   FSS  MFSS input=.fmask_input_card_alb
      statsav mask=MFSO LPA LPAC new_gp new_xp $stat2nd
      statsav mask=MFSS SALB     new_gp new_xp $stat2nd
    fi

#   ---------------------------------- save results.

      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
        access oldxp ${flabel}xp
        xjoin oldxp new_xp newxp
        save newxp ${flabel}xp
        delete oldxp
      fi

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         T1        = $t1
         T2        = $t2
         T3        =      $t3
0 GSSTATS ------------------------------------------------------------ GSSTATS
end_of_data

. endjcl.cdk

#!/bin/sh
#
#
#                  pla                 KS - JAN 25/2013 -------------pla
#   ---------------------------------- Compute diagnostic results for
#                                      PLA calculations.
#
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
   isdiag=22
   isdnum=30
#
# select input fields.
#
#   varl3d='SSLD RESS VESS DSLD REDS VEDS BCLD REBC VEBC OCLD REOC VEOC AMLD REAM
#           VEAM BCIC EFFR EFFV CORN CORM RSN1 RSN2 RSN3 RSM1 RSM2 RSM3 PM25 PM10
#           DM25 DM10'
   varl3d='PM25 PM10 DM25 DM10'
   varl3dx=''
   varl2d=''
   varl2dx=''
#
#   varl2ddiag='SI'
   varl2ddiag=''
#   varl3ddnum=''
   varl3ddnum=''
#
   for VL in $varl3d ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000      $VL" | ccc select npakgg $VL
   done
   for VL in $varl3dx ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000       $VL" | ccc select npakgg $VL
   done
   for VL in $varl2d ; do
     echo "SELECT          $r1 $r2 $r3         1    1      $VL" | ccc select npakgg $VL
   done
   for VL in $varl2dx ; do
     echo "SELECT          $r1 $r2 $r3         1    1       $VL" | ccc select npakgg $VL
   done
   for VL in $varl2ddiag ; do
      i2=1
      while [ $i2 -le $isdiag ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "SELECT          $r1 $r2 $r3         1    1      ${VL}${i2}" | ccc select npakgg ${VL}${i2}
        i2=`expr $i2 + 1`
      done
   done
   for VL in $varl3ddnum ; do
      i2=1
      while [ $i2 -le $isdnum ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "SELECT          $r1 $r2 $r3     -9001 1000      ${VL}${i2}" | ccc select npakgg ${VL}${i2}
        i2=`expr $i2 + 1`
      done
   done
#
# repack fields.
#
   for VL in $varl3d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl3dx ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl2d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl2dx ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl2ddiag ; do
      i2=1
      while [ $i2 -le $isdiag ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "REPACK.       2" | ccc repack ${VL}${i2} ${VL}${i2}0
        i2=`expr $i2 + 1`
      done
   done
   for VL in $varl3ddnum ; do
      i2=1
      while [ $i2 -le $isdnum ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "REPACK.       2" | ccc repack ${VL}${i2} ${VL}${i2}0
        i2=`expr $i2 + 1`
      done
   done

#
#  ----------------------------------- MASK OUT REGIONS WITH MISSING VALUES
#
#
   echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card
   for VL in $varl2ddiag ; do
      i2=1
      while [ $i2 -le $isdiag ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        fmask ${VL}${i2}0 ${VL}${i2}M   input=.fmask_input_card
        i2=`expr $i2 + 1`
      done
   done
#
# time average results.
#
   for VL in $varl3d ; do
     ccc timavg ${VL}0 ${VL}T
   done
   for VL in $varl3dx ; do
     ccc timavg ${VL}0 ${VL}T
   done
   for VL in $varl2d ; do
     ccc timavg ${VL}0 ${VL}T
   done
   for VL in $varl2dx ; do
     ccc timavg ${VL}0 ${VL}T
   done
   for VL in $varl2ddiag ; do
      i2=1
      while [ $i2 -le $isdiag ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        ccc timavg ${VL}${i2}0 ${VL}${i2}T
        ccc timavg ${VL}${i2}M ${VL}${i2}MT
        i2=`expr $i2 + 1`
      done
   done
   for VL in $varl3ddnum ; do
      i2=1
      while [ $i2 -le $isdnum ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        ccc timavg ${VL}${i2}0 ${VL}${i2}T
        i2=`expr $i2 + 1`
      done
   done
#   
# save results
#
   for VL in $varl3d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl3dx ; do
     echo "XSAVE.        ${VL}
NEWNAM.     ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl2d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl2dx ; do
     echo "XSAVE.        ${VL}
NEWNAM.     ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl2ddiag ; do
      i2=1
      while [ $i2 -le $isdiag ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "XSAVE.        ${VL}${i2}
NEWNAM.    ${VL}${i2}" | ccc xsaveup new_gp ${VL}${i2}T
        echo "XSAVE.        MS${i2}
NEWNAM.    MS${i2}" | ccc xsaveup new_gp ${VL}${i2}MT
        i2=`expr $i2 + 1`
      done
   done
   for VL in $varl3ddnum ; do
      i2=1
      while [ $i2 -le $isdnum ] ; do
        i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
        echo "XSAVE.        ${VL}${i2}
NEWNAM.    ${VL}${i2}" | ccc xsaveup new_gp ${VL}${i2}T
        i2=`expr $i2 + 1`
      done
   done
#
#  ----------------------------------- save results.
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp
#    cp new_gp newgp
#    save newgp ${flabel}xt
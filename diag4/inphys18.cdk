#comdeck inphys18
#
#
#                  inphys18             ML. - Mar 03/15 - ML ------ inphys18
#   ----------------------------------- Preparation of grid fields for GCM18
#                                       startup. Based on earlier Unix script
#                                       inphys16 except:
#                                       - Calls new lpcrev2 to correct
#                                         scaling at beginning.
#                                         Also calls new initg13 to remove
#                                         FARE calculation and writing
#                                         out, since it is now done in
#                                         the init12 common deck.
#                                       - Accesses new soil colour file
#                                         and proccesses it in a revised
#                                         initg15 to be read in by the model.
#                                       - Calls new lpcrev2 (replaces lpcrrev)
#                                         to output glacier ice percentage
#                                         ("LICN") and new GLC2000 lookup
#                                         tables.
#                                       - Calls new icntrld (replaces icntrlc)
#                                         to read from cards (new common
#                                         deck icntrldat2, replacing icntrldat)
#                                         {ntld,ntlk,ntwt,iflm} and write
#                                         out to control file (to be read
#                                         by new initg13).
#                                       - Calls new initg13 (replaces initg12)
#                                         to take high-res glacier ice
#                                         percentage (from lpcrev2 above) and
#                                         outputs it (LICN) on model resolution.
#                                       - The LICN field is now used to
#                                         define the glacier mask (SAND=-4.)
#                                         at model resolution. This is
#                                         what differs from earlier development
#                                         versions.
#                                       - Also, the new initg14 uses
#                                         the high-res land mask to produce
#                                         an in-land lake fraction (FLAK).     
#                                       - In addition, new initg13 also
#                                         outputs land fraction (FLND)
#                                         and land tile fraction (FARE).
#                                       - We are now working with Ed's
#                                         new 1/12x1/12 GLC2000 dataset,
#                                         so the BOXL2HR card is revised
#                                         from "4" to "12".

# Restrict use of "inphys18" to  GCM15H+ or later model versions.

if [ -n "$modver" ] ; then
 tmp_modver=`echo $modver | sed -e 's/ *//g'`
 case "$tmp_modver" in
   gcm6u|gcm15[bcdefg]|gcm13[de]) touch haltit ;
     echo "" ; echo " INPHYS18: Invalid modver (=$modver) setting!" ; echo "" ;
    (echo "" ; echo " INPHYS18: Invalid modver (=$modver) setting!" ; echo "") >> haltit ;
    exit 1 ;;
                              *) : ;;
 esac
else
  echo "" ; echo " INPHYS18: Sorry, 'modver' must have a valid setting!" ; echo ""
  touch haltit
 (echo "" ; echo " INPHYS18: Sorry, 'modver' must have a valid setting!" ; echo "") >> haltit
 exit 2
fi

# Ensure proper "ioztyp" setting and setup accordingly in "INOZM" 
# the proper program name to call further down.

if [ -n "$ioztyp" ] ; then
 tmp_ioztyp=`echo $ioztyp | sed -e 's/ *//g'`
 case "$tmp_ioztyp" in 
  1) INOZM='inoz6m' ;;
  2) INOZM='inoz7m' ;;
  3) INOZM='inoz8m' ;;
  4) INOZM='inoz9m' ;;
  5) INOZM='inozam' ;;
  *) echo "" ; echo " INPHYS18: Sorry, invalid ioztyp (=$ioztyp) setting!" ; echo "" ;
     touch haltit ;
     (echo "" ; echo " INPHYS18: Sorry, invalid ioztyp (=$ioztyp) setting!" ; echo "") >> haltit ;
     exit 3 ;;
 esac
else
 echo "" ; echo " INPHYS18: Sorry, 'ioztyp' must have a valid setting!" ; echo ""
 touch haltit
 (echo "" ; echo " INPHYS18: Sorry, 'ioztyp' must have a valid setting!" ; echo "")  >> haltit
 exit 4
fi

access veg $veg
access lp_lr $llphys
access ssti_lr $ssti
access chm $llchem
access oxi $oxi
access oz $oz
access llo3 $oz_chem
#
access gp $gpinit
access spphis $mtnfile
access mask $maskfile
access sub $subfle
access soci_in $soci
HOSTIDf6=`echo $HOSTID | sed -e 's/eccc.*-ppp/cs/g' -e 's/^ppp/cs/g' | cut -c 1-6` ; if [ "$HOSTIDf6" = 'xc1mom' -o "$HOSTIDf6" = 'xc2mom' -o "$HOSTIDf6" = 'xc3mom' -o "$HOSTIDf6" = 'xc4mom' ] ; then XCPRFX='aprun -n 1 -cc none ' ; fi
# Run look-up table program on high-res veg data.

$XCPRFX lpcrev2 veg fcan lnz0 avw aiw lamax lamin cmass zroot gc licn

# Extrapolate low-res (1x1) to high-res.

echo '  BOXL2HR.   12' | ccc $XCPRFX boxl2hr ssti_lr ssti
rm ssti_lr

echo '  BOXL2HR.   12' | ccc $XCPRFX boxl2hr lp_lr lpx
rm lp_lr  

# ll initialization.

cat lpx ssti fcan lnz0 avw aiw lamax lamin cmass zroot gc licn >> ground_in
rm  lpx ssti fcan lnz0 avw aiw lamax lamin cmass zroot gc licn
#
$XCPRFX icntrld gp spphis control input=icntrl.dat   
$XCPRFX initg13 control mask sub soci_in ground_in ground 

# oxidants (can be straight interpolated since no gc sensitivity).
$XCPRFX initoxi control gp oxi ggoxi
if [ "$lonsl" = '  192' -a "$nlat" = '   96' ] ; then 
 \mv ggoxi gsoxi
else
 echo "  EXTFIL. $lonsl$nlat    3" | ccc $XCPRFX extfil ggoxi gsoxi
fi
echo "  LLAGG.  $lonsl$nlat    1    1" | ccc $XCPRFX llagg llo3 o3 
echo "  NEWNAM.   O3C" | ccc $XCPRFX newnam o3 o3c 

# ozone.

$XCPRFX ${INOZM} control oz ozone

# joinup all to create final AN.

$XCPRFX joinup AN ground chm ozone gsoxi o3c
rm lp ground chm ozone ggoxi gsoxi o3c o3

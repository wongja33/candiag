#!/bin/sh
#          merged_diag_deck_parallel   SK - SEP 20/2019
#  ----------------------------------- Run merged_diag_deck.sh for all months in parallel

# This script is normally executed at the end of a year.

# last year/month of the multi-month chunk (current year/month)
year2=$year
mon2=$mon

# first year/month of the multi-month chunk
nmon=${chunk_size:-12}
mon1m=`echo $mon $nmon | awk '{printf "%02d",$1-$2+1}'`
if [ $mon1m -lt 1 ] ; then
  year1=`echo $year | awk '{printf "%04d",$1-1}'`
  mon1=`echo $mon1m | awk '{printf "%02d",$1+12}'`
else
  year1=$year
  mon1=$mon1m
fi

echo year1=$year1 mon1=$mon1
echo year2=$year2 mon2=$mon2

wrkdir=`pwd`
echo wrkddir=$wrkdir

# run the merged diagnostic shell script for all months in parallel
for m in `seq $mon1m 1 $mon2` ; do
  if [ $m -lt 1 ] ; then
    mon=`echo $m | awk '{printf "%02d",$1+12}'`
    year=$year1
  else
    mon=`echo $m | awk '{printf "%02d",$1}'`
    year=$year2
  fi
  echo year=$year mon=$mon

  tmpdir=$wrkdir/tmp.dc_${runid}_${year}${mon}.$$
  mkdir -p $tmpdir
  cd $tmpdir
  model1="${uxxx}_${runid}_${year}_m${mon}_";
  flabel="${diag_uxxx}_${runid}_${year}_m${mon}_"
  { merged_diag_deck.sh && status=$? || status=$? ; echo $status > $wrkdir/exit_status_diag_${year}${mon} ; } &
done
wait
cd $wrkdir
# check that the exit status is 0 for all processes
for f in exit_status_diag_* ; do
  echo $f=`cat $f`
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in merged_diag_deck_parallel.dk: $f = `cat $f`"
    exit 1
  fi
done

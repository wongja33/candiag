#!/bin/sh
# -----------------------------------------------------------------
#  Gravity wave drag diagnostics for CMIP6 (CanESM5 only orographic GWs)
#  Based on deck for QBOi (cmamdyn_gwdiag.dk by C. Mcl.)
#  Extracts instantaneous orographic (tendencies and momentum fluxes) 
#  from gs file, interpolates to pressure levels, computes daily and monthly means; 
#  saves various quantities in gp,xp and dx files
#                                          -- MS (6 Feb 2019)
#  Some parmsubs and flags:
#      nsavesgw_per_day = number of times per day that GW fields 
#                         are saved in gs file (=4 for 6hr data).
#      dxsave = basic flag to save daily zonal mean data
#
#  Modifications:
#     (1) change superlabels (C.McL 12 Nov 2015)
#     (2) based on qboi_gwdiag.dk but used program GSAPL_LNSP 
#         instead of GSAPL_LNSP_TST99; north & south comps of 
#         nonoro GW fluxes not done; flags xp24save_gwdiag & 
#         gp24save_gwdiag added (C.McL. 8 Apr 2016)
#     (3) based on cmamdyn_gwdiag.dk, but removed flags and NOGWD   
#     (4) Save data in zp instead of xp files (to avoid having to convert all xp variables to timeseries)  
# -----------------------------------------------------------------

# access surface pressure
      access gslnsp ${flabel}_gslnsp

# OLD:access beta for representative zonal avg 
# Note: moved away from representative zonal means
# as cmip6/dynvarmip requests either the regular zonal mean
# or a representative zonal mean that is inconsistent
# with rzonavg and timeavg (timeavg can't deal with missing values
# that would be the result of a proper rzonavg)
#      access beta ${flabel}_gpbeta

      if [ "$datatype" = "specsig" ] ; then

#                                      gsapl input card
      echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

#   ---------------------------------- select variables
.     ggfiles.cdk

echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME FTOX FTOY TDOX TDOY" | ccc select npakgg FTOX FTOY TDOX TDOY
#                                      do for each variable
      for v in FTOX FTOY TDOX TDOY; do
        gsapl_lnsp ${v} gslnsp ${v}_P input=.gsapl_input_card
        timavg ${v}_P  ${v}_PT
        zonavg ${v}_PT ${v}_PTZ
      done
      zonavg TDOX_P TDOX_PZ
      rm *_P

#   ---------------------------------- compute & save daily means in dx file
      if [ "$dxsave" = "on" ] ; then
        echo "BINSML.   $nsavesgw_per_day    0    0    0" > ic.binsml
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model
        binsml TDOX_PZ TDOX_PZ.d input=ic.binsml
        tstep TDOX_PZ.d TDOX_PZ.t input=ic.tstep_model   
        access olddx ${flabel}dx
        echo "XSAVE.        (TDOX)R
NEWNAM.    TDOX" | ccc xsave olddx TDOX_PZ.t newdx
        save newdx ${flabel}dx
        delete olddx
      fi

#   ---------------------------------- save gp file
      access oldgp ${flabel}gp
      echo "XSAVE.        FTOX_PT
NEWNAM.    FTOX
XSAVE.        FTOY_PT
NEWNAM.    FTOY
XSAVE.        TDOX_PT
NEWNAM.    TDOX
XSAVE.        TDOY_PT
NEWNAM.    TDOY" | ccc xsave oldgp FTOX_PT FTOY_PT TDOX_PT TDOY_PT newgp
      save newgp ${flabel}gp
      delete oldgp

#   ---------------------------------- save zp file
      access oldzp ${flabel}zp
      echo "XSAVE.        (FTOX_PTZ)R
NEWNAM.    FTOX
XSAVE.        (FTOY_PTZ)R
NEWNAM.    FTOY
XSAVE.        (TDOX_PTZ)R
NEWNAM.    TDOX
XSAVE.        (TDOY_PTZ)R
NEWNAM.    TDOY" | ccc xsave oldzp FTOX_PTZ FTOY_PTZ TDOX_PTZ TDOY_PTZ newzp
      save newzp ${flabel}zp
      delete oldzp
     
      fi # specsig endif

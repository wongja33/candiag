#!/bin/bash
# Dump and/or delete 'cmorized' netcdf data
WRK_DIR=$(pwd)

#=====================
# Define bail function
#=====================
  # bail is a simple error exit routine
  bail_prefix="NETCDF DUMPDEL"
  bail(){
    echo $(date)" $this_host $runid --- ${bail_prefix}: $*"
    exit 1
  }

#===================================================
# TO BE ALTERED WITH NEW INFRASTRUCTURE/SEQUENCER
#   - Specifically once we no longer rely on cccjob and we can
#     use more logical variables
#===================================================
chunk_start_year=$previous_year
chunk_start_month=$previous_month
chunk_stop_year=$current_year
chunk_stop_month=$current_month

#=============
# Archive Data
#=============
nc_dir=${RUNPATH}/nc_output
nc_file_pat='*_'${chunk_start_year}'*'-${chunk_stop_year}'*'
fx_pat='.*_fx_.*\|.*_Ofx_.*\|.*_Efx_.*\|.*_AERfx_.*\|.*_IfxAnt_.*\|.*_IfxGre_.*'
TO_DEPRECATE_3hr_pat="*_"${chunk_start_year}'*'-$((chunk_stop_year+1))'01*'
log_tar_file=${RUNPATH}/nc_output/conv_logs/${chunk_start_year}${chunk_start_month}_${chunk_stop_year}${chunk_stop_month}.tar.gz
if (( with_dump_nc == 1 )) ; then

    # Create directory to be dumped by copying in the desired netcdf files
    #---------------------------------------------------------------------
    temp_dir=$(pwd)/output
    mkdir $temp_dir

    # navigate to netcdf storage dir, so we don't get the full path of the files in the archives
    cd $nc_dir

    # files with dates matching the given pattern
    find . -name "$nc_file_pat" -exec cp --parents {} $temp_dir \;

    # fixed files without dates
    find . -regex "$fx_pat" -exec cp --parents {} $temp_dir \;

    # EDGE CASE EXCEPTION - to be deprecated if fixed in agcm code
    # 3hr files that have had end dates that land just beyond the chunk end dates
    find . -name "$TO_DEPRECATE_3hr_pat" -exec cp --parents {} $temp_dir \;

    # navigate back after copying
    cd -

    files2arch=$(basename $temp_dir)
    # check if logs exists and archive them if they do
    if [[ -f $log_tar_file ]] ; then
        archd_logs=conv_logs_$(basename $log_tar_file)
        cp -r $log_tar_file $archd_logs
        files2arch="${files2arch} ${archd_logs}"
    fi

    # check for minimum archive size
    sizemin=501 # minimum archive size in MB
    size=`du -smc $files2arch | tail -1 | cut -f1 -d$'\t' | cut -f1 -d$' '`
    echo size=${size}MB
    if [ $size -lt $sizemin ] ; then
      # add a dummy file to meet the min size
      sizeadd=`echo $size $sizemin | awk '{printf "%d",($2-$1+1)}'`
      dd if=/dev/zero of=dummy count=$sizeadd bs=1048576
      files2arch="${files2arch} dummy"
    fi

    # Archive
    #---------
    # Archive directory name
    now=$(date -u +%Y%j%H%M)
    this_archive=${ncdf_uxxx}_${runid}_${chunk_start_year}${chunk_start_month}_${chunk_stop_year}${chunk_stop_month}_${now}

    # set hpcarchive flags
    if [ "$canesm_dump_nc_short_term" = "on" ] ; then
      hpcarchive_project=crd_short_term
    else
      hpcarchive_project=crd_cccma
    fi

    if [ "$hpcarchive_checksum" = "on" ] ; then
      hcparchive_checksum_arg="-k"
    else
      hcparchive_checksum_arg=""
    fi

    # try archiving several times
    ntrymax=10
    ntry=1
    while [ $ntry -le $ntrymax ] ; do
      status=""
      hpcarchive ${hcparchive_checksum_arg} -v -g -p ${hpcarchive_project} -a ${files2arch} -c ${this_archive} || status="failed"
      echo status=$status
      if [ "$status" = "" ] ; then
        break
      fi
      # unsuccessful archival - sleep a bit and try again.
      sleep 60
      ntry=$(( ntry + 1 ))
    done
    if [ "$status" != "" ] ; then
      exit 1
    fi
fi

#==========================================
# Delete data, potentially keeping a subset
#==========================================
if (( with_del_nc == 1 )) ; then 
    
    # check if the user wants to keep SOME on disk
    if [[ -n $keep_vartab_pairs ]] ; then
        # create regex pattern to match the given var/table pairs
        #--------------------------------------------------------
        for vartab in $keep_vartab_pairs; do
            file_prefix=${vartab/:/_}
            PATTERN="${PATTERN} .*${file_prefix}.*"
        done

        # remove leading whitespace
        PATTERN=$(echo $PATTERN | sed 's/^ *//')
        
        # replace spaces between pattern list with 'or' syntax '\|'
        PATTERN=${PATTERN/\* \./\*\\|\.}

        # delete files for the given year that don't match the given pattern
        #-------------------------------------------------------------------
        # files matching the chunk pattern
        find $nc_dir -name "$nc_file_pat" -not -regex "$PATTERN" -delete

        # fixed files (which get reproduced each year)
        find $nc_dir -regex "$fx_pat" -not -regex "$PATTERN" -delete

        # EDGE CASE EXCEPTION - to be deprecated if fixed in agcm code
        # 3hr files that have had end dates that land just beyond the chunk end dates
        find $nc_dir -name "$TO_DEPRECATE_3hr_pat" -not -regex "$PATTERN" -delete
    else
        # delete files for the given year
        #--------------------------------
        # files matching the chunk pattern
        find $nc_dir -name "$nc_file_pat" -delete
        # fixed files (which get reproduced each year)
        find $nc_dir -regex "$fx_pat" -delete
        # EDGE CASE EXCEPTION - to be deprecated if fixed in agcm code
        # 3hr files that have had end dates that land just beyond the chunk end dates
        find $nc_dir -name "$TO_DEPRECATE_3hr_pat" -delete
    fi

    # if log files exist, delete
    if [[ -f $log_tar_file ]] ; then
        rm -f $log_tar_file 
    fi

    # delete left over empty directories
    find $nc_dir -type d -empty -delete
fi

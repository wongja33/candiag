#deck cloud5
jobname=cloud ; time=$gptime ; memory=$memory3
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   remove use of condef parameters ggsave and zxsave;
#     getmtmj.cdk->gsqt.cdk
#     getmtmd.cdk->gsqtd.cdk
#     remove use of zxlook
#   add ground cover in call to cldprg3.
#                  cloud5              e.chan., ec. oct 3/96 - dl.
#   ---------------------------------- compute cloud fields for model ver gcm7.
#                                      based on cloud4 except that cldt is rename
#                                      to clds total sampled cloud and all
#                                      interpolations to pressure surfaces are
#                                      removed.

.     spfiles.cdk
.     ggfiles.cdk
#   ---------------------------------- select fields; do vertical interpolation.
.     gsqt.cdk
ccc   select  npakgg pbt tcv gc
      cldprg3 gstemp gsshum gslnsp pbt tcv gc \
              vcld scld lwp sem stac sztac swc r h cx tx ex
      rm gstemp gsshum gslnsp pbt tcv gc

#   ---------------------------------- cloud amount sampled.
      timavg  scld   tscld
      zonavg  tscld  rztscld
      xsaveup new_xp rztscld
      xsaveup new_gp tscld
      rm rztscld tscld scld
#   ---------------------------------- relative humidity.
      timavg  h      trhum
      zonavg  trhum  ztrhum
      xsaveup new_xp ztrhum
      xsaveup new_gp trhum
      rm ztrhum rhum trhum
#   ---------------------------------- total overlapped cloud amount.
      timavg  vcld   tstcld
      xsaveup new_gp tstcld
      rm vcld tstcld
#   ---------------------------------- total liquid water path.
      timavg  lwp    tlwp
      xsaveup new_gp tlwp
      rm lwp tlwp
#   ---------------------------------- additional saved 3-d fields.
      if [ "$xtracld" = on ] ; then
#   ---------------------------------- cloud * emissivity.
         timavg  sem    temi
         zonavg  temi   rztemi
         xsaveup new_xp rztemi
         xsaveup new_gp temi
         rm sem emi temi rztemi
#   ---------------------------------- cloud * optical depth.
         timavg  stac   ttac
         zonavg  ttac   rzttac
         xsaveup new_xp rzttac
         xsaveup new_gp ttac
         rm stac tac ttac rzttac
#   ---------------------------------- cloud optical depth per 100 mb.
         timavg  sztac  tztac
         zonavg  tztac  rztztac
         xsaveup new_xp rztztac
         xsaveup new_gp tztac
         rm sztac tztac rztztac g2 z2
#   ---------------------------------- log10 of water content.
         timavg  swc    tlwcd
         zonavg  tlwcd  rztlwcd
         xsaveup new_xp rztlwcd
         xsaveup new_gp tlwcd
         rm swc lwcd tlwcd rztlwcd
#   ---------------------------------- equivalent radius microns.
         timavg  r      trade
         zonavg  trade  rztrade
         xsaveup new_xp rztrade
         xsaveup new_gp trade
         rm r trade rztrade
      fi
      if [ "$blckcld" = on ] ; then
#   ---------------------------------- extra gaussian fields of cloud blocks
#                                      coupled layers of cloud amount
#                                      cloud amount * emissivity and
#                                      cloud amount * optical depth.
         timavg cx tgscldx
         timavg ex tgsemix
         timavg tx tgstacx
         xsaveup new_gp tgscldx
         xsaveup new_gp tgsemix
         xsaveup new_gp tgstacx
         rm cx ex tx tgscldx tgsemix tgstacx
      fi

#   ---------------------------------- save results.
      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      access oldxp ${flabel}xp
      xjoin oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         T1        = $t1
         T2        = $t2
         T3        =      $t3
0 CLOUD5 ---------------------------------------------------------------- CLOUD5
. gsqtd.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT.   STEPS $t1 $t2 $t3 LEVS    1 1000 NAME PBLT  TCV   GC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CLDPRG3.  $lay$coord$plid$delt$nbl
TOP       $lt1$lt2$lt3$lt4$lt5
BASE      $lb1$lb2$lb3$lb4$lb5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (SCLD)R
NEWNAM.    SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        SCLD
NEWNAM.    SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (RHC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        RHC
NEWNAM.    IRHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        CLDS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        TLWC
if [ "$xtracld" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (EMI)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (TAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (ZTAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (LWCD)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        LWCD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        (RADE)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        RADE
fi
if [ "$blckcld" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        BLOCK CLOUDINESS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        BLOCK EMISSIVITY
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        BLOCK OPTICAL DEPTH
fi
end_of_data

. endjcl.cdk

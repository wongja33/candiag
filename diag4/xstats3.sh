#!/bin/sh
#                  xstats3             SK - Jan 21/2009 - ML -----------XSTAT3
#
#   sk: use gpxstat and statsav for statistics computation,
#       use revised gstracx that can operate on spectral files directly,
#       fix 'gridsig' case
#   ml: support hybrid tracers and (most recent) add xtvi,xsrf.
#   sk: rewrite for multiple tracers; use input=.input_card for input cards
#
#  ----------------------------------- calculate tracer statistics
#                                      and covariances with u, v and w.
#                                      tracer names are specified in variable $trac.
#

#   ---------------------------------- access beta, u and v
      lzon="    0"
      if [ "$rcm" != "on" ] ; then
        lzon="    1"
#                                      try first to access beta directly
	access beta ${flabel}_gptbeta na
	if [ ! -s beta ] ; then
#                                      if not available, get it from gp file
           access oldgp ${flabel}gp
	   echo "XFIND.        $d" | ccc xfind oldgp beta
	fi
      fi
      access u    ${flabel}_gpu
      access v    ${flabel}_gpv
#                                      access w and perform calculations for w
      lw="    0"
      if [ "$wxstats" = "on" ]; then
        access w ${flabel}_gpw
        lw="    1"
      fi

#   ---------------------------------- common input cards
      echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

      echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card

      xtstat_getdata="y"
#   ---------------------------------- process all tracers
      gpx="" # initialize tracer list
      vix="" # initialize tracer diagnostics list
      for x in $trac ; do
        echo "\n==> For $x in 'trac':\n"
        NAME=`echo "    $x" | tail -5c` # right-aligned 4-character name
        echo "NAME=$NAME"
        NUM=`echo $x | sed -e 's/^X//'`
        echo "NUM=$NUM"
        eval "NUMBER=\${it_${NUM}}"
        echo "NUMBER=$NUMBER"
#
        eval  "xref=\${xref_${NUM}}"
        if [ -n "$xref" ] ; then
          echo "xref(=xref_${NUM})=$xref"
	else
          echo "\nNote: **** Variable >xref_${NUM}< is undefined! ****\n"
	  exit -1
        fi
#
        eval  "xpow=\${xpow_${NUM}}"
        if [ -n "$xpow" ] ; then
          echo "xpow(=xpow_${NUM})=$xpow"
	else
          echo "\nNote: **** Variable >xpow_${NUM}< is undefined! ****\n"
	  exit -1
        fi

#   ---------------------------------- try to access the tracer
        access ${x} ${flabel}_gp${x} na

        if [ ! -f $x ] ; then

#   ---------------------------------- get the tracer

          if [ "$xtstat_getdata" = "y" ] ; then
#   ---------------------------------- get model files
#                                      spectral sigma case
            if [ "$datatype" = "specsig" ] ; then
.             spfiles.cdk
.             ggfiles.cdk
#                                      try to access gslnsp
              access gslnsp ${flabel}_gslnsp na
              if [ ! -f gslnsp ] ; then
#                                      if gslnsp not found, get it from npaksp
                echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" > .select_input_card_lnsp
ccc             select npaksp sslnsp input=.select_input_card_lnsp
                cofagg sslnsp gslnsp input=.cofagg_input_card
              fi
#                                      grid sigma case
            elif [ "$datatype" = "gridsig" ] ; then
.             ggfiles.cdk
#                                      try to access gslnsp
              access gslnsp ${flabel}_gslnsp na
              if [ ! -f gslnsp ] ; then
#                                      if gslnsp not found, get it from npakgg
                echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" > .select_input_card_lnsp
ccc             select npakgg sslnsp input=.select_input_card_lnsp
              fi
#                                      spectral pressure case
            elif [ "$datatype" = "specpr" ] ; then
.             spfiles.cdk
#                                      grid pressure case
            elif [ "$datatype" = "gridpr" ] ; then
.             ggfiles.cdk
            fi
            xtstat_getdata="n"
          fi

#   ---------------------------------- process tracers and tracer diagnostics.

#                                      spectral-sigma case.
          if [ "$datatype" = "specsig" ] ; then
#   ---------------------------------- single-level tracer diagnostics.
#                                      for now, only vertical integral
#                                      (XTVI) and lowest level (XSRF).
            echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME VI$NUMBER XL$NUMBER" > .select_input_card
ccc         select npakgg VI$NUMBER XL$NUMBER input=.select_input_card
	    vix="$vix VI$NUMBER XL$NUMBER"

#   ---------------------------------- interpolate tracer to pressure levels.
            if [ "$itrvar" = "SL3D" -o "$itrvar" = "SLQB" ] ; then
#                                      tracer on grid
              echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card
ccc           select npakgg gstrac input=.select_input_card
              if [ "$itrvar" = "SL3D" ] ; then
                mv gstrac gsx
                gsapl gsx gslnsp $x input=.gsapl_input_card
                rm gsx
              else
                echo " GSTRACX. $coord $itrvar$xref$xpow    1" > .gstracx_input_card
                cat .gsapl_input_card >> .gstracx_input_card
                gstracx gstrac gsx input=.gstracx_input_card
                rm gstrac
              fi
            else
#                                      spectral tracer
              echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card
ccc           select npaksp sstrac input=.select_input_card
              if [ "$itrvar" = "   Q" ] ; then
                cofagg sstrac gsx input=.cofagg_input_card
                rm sstrac
                gsapl gsx gslnsp $x input=.gsapl_input_card
                rm gsx
              else
                echo " GSTRACX. $coord $itrvar$xref$xpow    1" > .gstracx_input_card
                cat .cofagg_input_card >> .gstracx_input_card
                cat .gsapl_input_card >> .gstracx_input_card
                gstracx sstrac $x gslnsp input=.gstracx_input_card
                rm sstrac
              fi
            fi

#                                      grid-sigma case.

          elif [ "$datatype" = "gridsig" ] ; then
#   ---------------------------------- single-level tracer diagnostics.
#                                      for now, only vertical integral
#                                      (XTVI) and lowest level (XSRF).
	    NUM5=`echo "$NUMBER     " | cut -c1-5`
            echo "SELECT    STEPS $t1 $t2 $t3 LEVS$NUM5$NUM5 NAME   VI   XL" > .select_input_card
ccc         select npakgg VI$NUMBER XL$NUMBER input=.select_input_card
	    vix="$vix VI$NUMBER XL$NUMBER"

#   ---------------------------------- interpolate tracer to pressure levels.
	    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" > .select_input_card
ccc         select npakgg gsx input=.select_input_card
            gsapl gsx gslnsp $x input=.gsapl_input_card
            rm gsx

#                                      spectral-pressure case.

          elif [ "$datatype" = "specpr" ] ; then
            echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME $NAME" > .select_input_card
ccc         select npaksp spx input=.select_input_card
            cofagg spx $x input=.cofagg_input_card
            rm spx
          fi

#                                      grid-pressure case.

          if [ "$datatype" = "gridpr" ] ; then
            echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME $NAME" > .select_input_card
ccc         select npakgg $x input=.select_input_card
          fi
        fi
        gpx="$gpx $x"
      done
      echo gpx="$gpx"
      echo vix="$vix"

#   ---------------------------------- tracer statistics
      luvw="    0"
      lbeta="    1"
      if [ "$stat2nd" != "off" ] ; then
        lstat="    1"
        ldt="    1"
      else
        lstat="    0"
        ldt="    0"
      fi

      echo "          $luvw$lw$ldt$lzon$lbeta$lstat$delt" > .ic_gpxstat
      gpx=`echo "$gpx" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      nfile=`echo $gpx | wc -w`
      while [ -n "$gpx" ] ; do
	# process maximum 80 tracers at a time
	gpx1=`echo "$gpx" | cut -f1-80 -d' '`
	gpxstat beta u v w $gpx1 new_gp new_xp input=.ic_gpxstat
	gpx=`echo "$gpx" | cut -f81- -d' '`
      done
#                                      standard statistics for tracer diagnostics
      statsav $vix new_gp new_xp $stat2nd

#   ---------------------------------- save results.

      if [ ! -s oldgp ] ; then
#                                      access only if not already accessed above
      access oldgp ${flabel}gp
      fi
      xjoin  oldgp new_gp newgp
      save   newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
       access oldxp ${flabel}xp
       xjoin  oldxp new_xp newxp
       save   newxp ${flabel}xp
       delete oldxp
      fi

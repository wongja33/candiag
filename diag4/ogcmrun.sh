#!/bin/bash

# Get useful CanESM shell functions
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh

# ---------------------------------- Perform code checks

event_year=$(echo $year | awk '{printf "%04d",$1}')
event_mon=$(echo $mon | awk '{printf "%02d",$1}')
(( production == 1 )) && flgs="-tx" || flgs="-tdx"   # use 'development' mode if production is off
strict_check $flgs $runid model-run-${event_year}m${event_mon}

#  ----------------------------------- Setup to run in parallel mode.

source gcm_parallel_setup.sh

#  ----------------------------------- Determine job stop date and initial chunk dates
chunk_start_year=$(strip_leading_zeros $year)
chunk_start_month=$(strip_leading_zeros $mon)
chunk_start_date=$( printf "%04d-%02d-%02d" $chunk_start_year $chunk_start_month 1 )
chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $months_run )
job_stop_date=$( calc_chunk_stop_date $chunk_start_date $months )

# get chunk stop year/month in integer values (no leading zeros)
chunk_stop_date_array=( ${chunk_stop_date//-/ } )
chunk_stop_year=$( strip_leading_zeros ${chunk_stop_date_array[0]} )
chunk_stop_month=$( strip_leading_zeros ${chunk_stop_date_array[1]} )
chunk_stop_day=$( strip_leading_zeros ${chunk_stop_date_array[2]} )

#  ----------------------------------- Loop over month chunks (within the job)
months_run=${months_run:-1}
number_of_internal_chunks=$(( ( months - 1 )/months_run + 1 ))
months_sofar=$months_run

run_prfx=$(echo ${model} | sed -n -e 's/\(.*_\)[0-9]*_m[0-9][0-9][ _]*$/\1/p')
tail_suffix=$(echo ${model} | sed -n -e 's/^.*_$/_/p')
for chunk_counter in $(seq $number_of_internal_chunks -1 1) ; do # count DOWN
  if (( chunk_counter == 1 )); then
    final_internal_chunk=1
  fi

  echo chunk_start_year=$chunk_start_year chunk_start_month=$chunk_start_month
  echo chunk_stop_year=$chunk_stop_year chunk_stop_month=$chunk_stop_month

  # model/restart names
  model=$( printf "%s%04d_m%02d%s" $run_prfx $chunk_start_year $chunk_start_month $tail_suffix )
  model_rs=$( printf "%s%04d_m%02d%s" $run_prfx $chunk_stop_year $chunk_stop_month $tail_suffix )
  echo model=$model
  echo model_rs=$model_rs

  #  ----------------------------------- NEMO setup
  if [[ $use_nemo == on ]]; then
    . ${CANESM_SRC_ROOT}/CanNEMO/lib/nemo_prelude
    update_nemo_counters start_date=$chunk_start_date \
                         stop_date=$chunk_stop_date \
                         nemo_timestep=$nemo_rn_rdt \
                         namelist_file=namelist
  fi

  #  ------------------------------- Run the model.
  cat prunsc
  ./prunsc && model_status=$? || model_status=$?
  echo model_status=$model_status

  #  ------------------------------- Exit if model is aborted.
  if (( model_status != 0 )) ; then
    bail "Model exited with non-zero exit status!"
  fi

  #  ----------------------------------- CanNEMO postlude
  if [[ $use_nemo == on ]]; then
    . ${CANESM_SRC_ROOT}/CanNEMO/lib/nemo_postlude
  fi

  # ------------------------------------ Determine start/stop times next internal chunk (if needed)
  if (( final_internal_chunk != 1 )); then
      # Determine start time
      if (( chunk_stop_month < 12 )); then
          chunk_start_year=$chunk_stop_year
          chunk_start_month=$(( chunk_stop_month + 1 ))
      else
          # at a year boundary, roll over
          chunk_start_year=$(( chunk_stop_year + 1 ))
          chunk_start_month=1
      fi
      chunk_start_date=$( printf "%04d-%02d-%02d" $chunk_start_year $chunk_start_month 1 )

      # Determine stop time - make sure we don't exceed job total (defined by months)
      if (( months_sofar + months_run > months )); then
          # running for months_run would exceed the total number of months we want for this job
          #  so we instead only go the remainder
          last_chunk=$(( months_sofar + months_run - months ))
          chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $last_chunk )
          months_sofar=$(( months_sofar + last_chunk ))
      else
          # we can do another clean chunk
          chunk_stop_date=$( calc_chunk_stop_date $chunk_start_date $months_run )
          months_sofar=$(( months_sofar + months_run ))
      fi
      chunk_stop_date_array=( ${chunk_stop_date//-/ } )
      chunk_stop_year=$( strip_leading_zeros ${chunk_stop_date_array[0]} )
      chunk_stop_month=$( strip_leading_zeros ${chunk_stop_date_array[1]} )
      chunk_stop_day=$( strip_leading_zeros ${chunk_stop_date_array[2]} )
  fi

  #  --------------------------------- End of loop.
done

# save environment and jobstring
cp $HOME/.queue/.crawork/${crawork}_string ${model_rs}jobstring
env > ${model_rs}env
save ${model_rs}jobstring ${model_rs}jobstring
save ${model_rs}env ${model_rs}env

##############################################################
# create and submit diagnostic job to frontend every 12 months,
# or at the end of the run
if (( with_post_processing == 1 )) ; then
  job_stop_date_array=( ${job_stop_date//-/ } )
  job_stop_year=${job_stop_date_array[0]}
  job_stop_month=${job_stop_date_array[1]}

  int_run_start_month=$( strip_leading_zeros $run_start_month )
  if (( int_run_start_month == 1 )) ; then
    diag_end_month=12
  else
    diag_end_month=$(echo $int_run_start_month | awk '{printf "%02d",$1-1}')
  fi

  echo job_stop_month=$job_stop_month diag_end_month=$diag_end_month
  echo current_YYYYMM=$job_stop_year$job_stop_month run_stop_YYYYMM=$run_stop_year$run_stop_month

  if [[ $job_stop_month == $diag_end_month ]] || [[ $job_stop_year$job_stop_month == $run_stop_year$run_stop_month ]] ; then

    diag_start_year=$job_stop_year
    if [ $diag_start_year$run_start_month -gt $job_stop_year$job_stop_month ] ; then
      diag_start_year=`expr $job_stop_year - 1`
    fi

    echo "make_diag_job_${runid}_${diag_start_year}m${run_start_month}_${job_stop_year}m${job_stop_month}"

    cd $WRK_DIR

    finp=make_diag_job_${runid}
    fout=make_diag_job_${runid}_${diag_start_year}m${run_start_month}_${job_stop_year}m${job_stop_month}
    diagjob_sfx=diagjob_`echo ${CMCFEDEST} | cut -f2 -d'-'`_${DIAGJOB_SFX}

    # modify some parameters in make_diag_job_${runid}
    rm -f $fout
    cat $finp | sed \
     -e "s/^start\=[0-9:]\+/start\=$diag_start_year:$run_start_month/" \
     -e "s/^stop\=[0-9:]\+/stop\=$job_stop_year:$job_stop_month/" \
     -e "s/crawork\=.\+/crawork\=${runid}_${diagjob_sfx}/" \
    > $fout

    # create diag string to be run on front end
    chmod +x $fout
    ./$fout

    # the resulting diag job is saved in
    fdiag="${runid}_${diag_start_year}m${run_start_month}_${job_stop_year}m${job_stop_month}_diag_job"

    flist="$fout"
    if [ -s $fdiag ] ; then
      # diagnostic string is not empty - submit or append to the existing string in .crawork

      # checks if the job is being updated
      ntrymax=10
      for n in `seq 1 1 $ntrymax` ; do
        if [ -f "$HOME/.queue/.crawork/.${runid}_${diagjob_sfx}_string_updtng" ] ; then
          sleep 5
        else
          break
        fi
        if [ $n -eq $ntrymax ] ; then
          abort='abort'
          exit 1
        fi
      done
      if [ -s "$HOME/.queue/.crawork/${runid}_${diagjob_sfx}_string" ] ; then
        # append job to the existing string
        cat $fdiag >> $HOME/.queue/.crawork/${runid}_${diagjob_sfx}_string
      else
        # submit diag job to FE
        dest=`echo $CMCFEDEST | cut -f2 -d'-'`
        ssh $dest "cd $WRK_DIR ; . env_setup_file ; rsub mdest=$dest $fdiag" && status=$? || status=$?
        echo status=$status
        if [ $status -ne 0 ] ; then
          # failed to submit. Copy to crawork
          cp $fdiag $HOME/.queue/.crawork/${runid}_${diagjob_sfx}_string
        fi
      fi
      flist="$flist $fdiag"
    fi # -s $fdiag

    # move jobs to post_jobs subdirectory
    mv $flist $CCRNSRC/post_jobs/. && status=$? || status=$?
    echo status=$status
    if [ $status -ne 0 ] ; then
      # failed to move
      echo "Warning: hostname=`hostname`. Failed to move $fout $fdiag to $CCRNSRC/post_jobs/." > $HOME/.queue/${fout}_warning_failed_to_move
      ls -ld $CCRNSRC/post_jobs/. >> $HOME/.queue/${fout}_warning_failed_to_move 2>> $HOME/.queue/${fout}_warning_failed_to_move || :
    fi

  fi # $job_stop_month -eq $diag_end_month ...

fi # $with_post_processing -eq 1

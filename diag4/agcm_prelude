#!/bin/bash
# Setup and bring in AGCM input files

#  --------------------------------- Create initial conditions files
#                                    conditional on proper setting of
#                                    start/model/initsp env. variables.
if [[ $initsp == on ]] ; then
. gcminit.cdk
fi

# Copy in AGCM exec
! is_defined $agcm_exec && bail "agcm_exec must be defined!"
cp $CCRNSRC/executables/$agcm_exec .

access PAR ${start}par nocp=off

# get agcm namelist files for this run
for f in $agcm_namelists; do
    cp ${WRK_DIR}/config/namelists/${f} .
done

#  --------------------------------- Access AGCM restart bundle and pull out RS/TS files
access ${start}${agcm_restart_identifier} ${start}${agcm_restart_identifier} nocp=off
mv ${start}${agcm_restart_identifier}/${agcm_restart_identifier}_OLDRS OLDRS
mv ${start}${agcm_restart_identifier}/${agcm_restart_identifier}_OLDTS OLDTS

#  ----------------------------------- Access multi-year boundary condition
#                                      files through the coupler.

# individual netcdf files for GT/SICN/SIC
if [ "${coupler_specified_bc_file_gt}" != "" ] ; then
  access coupler_specified_bc_file_gt.nc ${coupler_specified_bc_file_gt}.nc
elif [ "${coupler_specified_bc_file}" != "" ] ; then
  access coupler_specified_bc_file_gt.nc ${coupler_specified_bc_file}_gt.nc
fi
if [ "${coupler_specified_bc_file_sicn}" != "" ] ; then
  access coupler_specified_bc_file_sicn.nc ${coupler_specified_bc_file_sicn}.nc
elif [ "${coupler_specified_bc_file}" != "" ] ; then
  access coupler_specified_bc_file_sicn.nc ${coupler_specified_bc_file}_sicn.nc
fi
if [ "${coupler_specified_bc_file_sic}" != "" ] ; then
  access coupler_specified_bc_file_sic.nc ${coupler_specified_bc_file_sic}.nc
elif [ "${coupler_specified_bc_file}" != "" ] ; then
  access coupler_specified_bc_file_sic.nc ${coupler_specified_bc_file}_sic.nc
fi
# GT/SICN/SIC climatologies in one file
if [ "${coupler_specified_bc_file_clim}" != "" ] ; then
  access coupler_specified_bc_file.nc ${coupler_specified_bc_file_clim}.nc
fi
# additional GT anomaly
if [ "${coupler_specified_bc_file_gtano}" != "" ] ; then
  access coupler_specified_bc_file_gtano.nc ${coupler_specified_bc_file_gtano}.nc
fi

if [ ! -s LAKES  ] ; then
  access LAKES  $lakes  na
fi
if [ ! -s TARGET ] ; then
  access TARGET $target na
fi

#  ----------------------------------- Access extra GCM input files.

. gcm_access_extra.cdk
. cgcm_access_extra.cdk

if [ -n "$spfilt" -a ! -f "SPFILT" ] ; then
  access SPFILT $spfilt
fi

# Access files required when gas-phase chemistry is switched on.
if [ -n "$ssadtran" -a ! -f "SSADF" ] ; then
  access SSADF $ssadtran
fi
if [ -n "$initso4" -a ! -f "SULPHATE" ] ; then
  access SULPHATE $initso4
fi
if [ -n "$jtab1" -a ! -f "JTAB1" ] ; then
  access JTAB1 $jtab1
fi
if [ -n "$jtab2" -a ! -f "JTAB2" ] ; then
  access JTAB2 $jtab2
fi
if [ -n "$emisfile" -a ! -f "SFCEMIS" ] ; then
  access SFCEMIS $emisfile
fi
if [ -n "$acftemfl" -a ! -f "ACFTEMIS" ] ; then
  access ACFTEMIS $acftemfl
fi

#  ----------------------------------- AGCM nudging

if [[ $relax == on ]] ; then
  . agcm_nudging.cdk
fi # $relax = on

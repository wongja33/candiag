#deck plotzrx
jobname=plotz ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script


#   Change ZXPLOT PSI's lower bound limit from -20 to -30.
#                  plotzrx             gjb,ec,dl. - Mar 22/05 - ml.
#   ---------------------------------- rep. zonal avg. cross-section plots.
#
      libncar plunit=$plunit
.     plotid.cdk
      access xpfile ${flabel}xp
#   ---------------------------------- beta term.
      xfind xpfile zdel
#   ---------------------------------- u stats.
      xfind xpfile ur
      xfind xpfile us2r
      xfind xpfile up2r
      sqroot us2r sdus
      sqroot up2r sdup
      zxplot ur
      zxplot sdus
      zxplot sdup
#   ---------------------------------- v stats.
      xfind xpfile vr
      xfind xpfile vs2r
      xfind xpfile vp2r
      sqroot vs2r sdvs
      sqroot vp2r sdvp
      mlt vr zdel dv
      zxpsi dv psi
      zxplot vr
      zxplot psi
      zxplot sdvs
      zxplot sdvp
#   ---------------------------------- kinetic energies.
      ke ur vr kznew
      add us2r up2r a
      rm up2r us2r
      add vp2r vs2r b
      rm vs2r vp2r
      add a b ke2
      xlin ke2 kenew
      zxplot kznew
      zxplot kenew
      rm ur sdus sdup dv dvp2 dvs2 vr sdvs
      rm sdvp psi kenew kznew a b ke2
#   ---------------------------------- t stats.
      xfind xpfile tr
      xfind xpfile ts2r
      xfind xpfile tp2r
      sqroot ts2r sdts
      sqroot tp2r sdtp
      rm ts2r tp2r
      xlin tr tc
      zxplot tc
      zxplot sdts
      zxplot sdtp
      rm tc sdts sdtp
#   ---------------------------------- z stats.
      xfind xpfile gzr
      xfind xpfile gzs2r
      xfind xpfile gzp2r
      sqroot gzs2r sdgzs
      sqroot gzp2r sdgzp
      rm gzs2r gzp2r
      zxplot gzr
      zxplot sdgzs
      zxplot sdgzp
      rm gzr sdgzs sdgzp
#   ---------------------------------- q stats.
      xfind xpfile  qr
      xfind xpfile  qs2r
      xfind xpfile  qp2r
      sqroot qs2r sdqs
      sqroot qp2r sdqp
      rm qs2r qp2r
      zxplot qr
      zxplot sdqs
      zxplot sdqp
      rm qr sdqs sdqp

      if [ "$wxstats" = on ] ; then
#   ---------------------------------- find psi from w.
         xfind xpfile wr
         mlt wr zdel dw
         globavg zdel gdel
         globavg dw gdw
         div gdw gdel rgw
         sub wr rgw wa
         mlt wa zdel wb
         newnam wb wc
         zxpsi wc psi2
         zxplot psi2
      fi

#   ---------------------------------- relative humidity.
      xfind xpfile rhr
      zxplot rhr
      rm rhr
#   ---------------------------------- vdgz term.
      access gpfile ${flabel}gp
      xfind gpfile del
      xfind gpfile vdgz
      rzonavg vdgz del rztvdgz
      rm gpfile del vdgz
      zxplot rztvdgz
      rm rztvdgz
#   ---------------------------------- plot the radiative heating terms.
      if [ "$datatype" = "specsig" ] ; then
         set +e
         xfind xpfile hrs
         xfind xpfile hrl
         add hrs hrl hr
         zxplot hrs
         zxplot hrl
         zxplot hr
        set -e
      fi

.     plotid.cdk
.     plot.cdk

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
0 PLOTZRX--------------------------------------------------------------- PLOTZRX
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        ($d)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U*U*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U"U")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.       U    0    0$lxp        1.     -150.      150.        5.$kax$kin
  RUN $run.  DAYS $days.  ZONAL VELOCITY (U)R.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U*U*    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((U*U*)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U"U"    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((U+U+)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V*V*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V"V")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.       V    0    0$lxp       10.      -45.       45.        5.$kax$kin
  RUN $run.  DAYS $days.  MERIDIONAL VELOCITY (V)R.  UNITS .1*M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     PSI    0    0$lxp    1.E-10    -3.0E1     3.0E1     1.0E0    0$kin
  RUN $run.  DAYS $days.  MASS STREAM FUNCTION PSI.  UNITS 1.E10*KG/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    V*V*    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((V*V*)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    V"V"    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((V+V+)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          0.5E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    1.0E-1       10.      500.       10.$kax$kin
  RUN $run.  DAYS $days.  ZONAL KINETIC ENERGY (KZ)R.  UNITS  10*(M/SEC)2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    1.0E-1        5.      500.       5.0$kax$kin
  RUN $run.  DAYS $days.   EDDY KINETIC ENERGY (KE)R.  UNITS 10*(M/SEC)2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T*T*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T"T")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.             1.   -273.16
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp        1.     -140.      100.       10.$kax$kin
  RUN $run.  DAYS $days.  TEMPERATURE (T)R.  UNITS DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    T*T*    0    0$lxp        1.        0.      500.        1.$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((T*T*)R.)**0.5)  UNITS DEGC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    T"T"    0    0$lxp        1.        0.      500.        1.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((T+T+)R.)**0.5)  UNITS DEGC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         (GZ)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (GZ*GZ*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (GZ"GZ")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp   1.02E-2      200.      4.E3      200.$kax$kin
  RUN $run  DAYS $days.  GEOPOTENTIAL HEIGHT Z.  UNITS DECAMETERS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp   1.02E-2        0.      500.        5.$kax$kin
  RUN $run  DAYS $days.  STANDING EDDY SD  ((Z*Z*)R.)**0.5) UNITS DECAMETERS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp   1.02E-2        0.      500.        5.$kax$kin
  RUN $run  DAYS $days.  TRANSIENT EDDY SD  ((Z+Z+)R.)**0.5) UNITS DECAMETERS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (Q)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (Q*Q*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (Q"Q")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp      1.E3      -10.       30.        1.    0$kin
  RUN $run.  DAYS $days.  SPECIFIC HUMIDITY (Q)R.  UNITS GM/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    Q*Q*    0    0$lxp      1.E3        0.       40.       0.5    0$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((Q*Q*)R.)**0.5)  UNITS GM/KG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    Q"Q"    0    0$lxp      1.E3        0.       40.       0.5    0$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((Q+Q+)R.)**0.5)  UNITS GM/KG.
if [ "$wxstats" = on ]; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (W)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM        W
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     PSI    0    0$lxp    1.E-10    -3.0E1     3.0E1     1.0E0    0$kin
  RUN $run.  DAYS $days.  MASS STREAM FUNCTION PSI2. UNITS 1.E10*KG/SEC.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (RH)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp     1.0E2     -200.      100.     1.0E1    0$kin
  RUN $run.  DAYS $days.  RELATIVE HUMIDITY (RH)R.  UNITS PERCENTAGE.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        $d
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V".DELGZ"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp     1.0E4     -100.      100.        2.$kax$kin
  RUN $run.  DAYS $days.  V+.DEL(GZ+).  UNITS 1.0E-4*M2/SEC3.
if [ "$datatype" = "specsig" ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         (HRS)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         (HRL)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    8.64E5     -100.      100.        5.$kax$kin
 RUN $run. DAYS $days.  SHORT-WAVE RAD HEATING.  UNITS .1*DEG/DAY
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    8.64E5     -100.      100.        5.$kax$kin
 RUN $run. DAYS $days.   LONG-WAVE RAD HEATING.  UNITS .1*DEG/DAY
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    8.64E5     -100.      100.        5.$kax$kin
 RUN $run. DAYS $days.       TOTAL RAD HEATING.  UNITS .1*DEG/DAY
fi
. tailfram.cdk
end_of_data

. endjcl.cdk

#!/bin/sh
#                  tndstat             ml. oct 3/96 - dl.
#   ---------------------------------- physics tendencies on pressure levels.
#   remove use of condef parameters ggsave and zxsave;
#      remove use of zxlook;
#      support up to 100 pressure levels
#
.     ggfiles.cdk
.     spfiles.cdk

    if [ "$datatype" = "gridsig" ] ; then
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" | ccc   select npakgg gsphis gslnsp
    else
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" | ccc   select npaksp ssphis sslnsp
echo "COFAGG    $lon$lat     $npg" | ccc cofagg ssphis gsphis
echo "COFAGG    $lon$lat     $npg" | ccc cofagg sslnsp gslnsp
    fi
    expone gslnsp sfprx
    if [ "$rcm" = "on" ] ; then
#   ----------------------------------- convert pressure to mb
echo "XLIN            0.01" | ccc      xlin sfprx sfprx1
      mv sfprx1 sfprx
    fi
echo "NEWNAM       PS" | ccc newnam sfprx sfpr1
    rm sfprx
#
echo "DELHAT    $plv
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc    delhat sfpr1 del1
echo "BETA      $plv
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc    beta   sfpr1 beta1
#
    if [ "$d" = "D" ] ; then
      timavg del1  del
    else
      timavg beta1 del
    fi
    if [ "$rcm" != "on" ] ; then
      zonavg del zdel
    fi

#   ---------------------------------- select model data and interp to pressure.
echo "SELECT    STEPS $r1 $r2 $r3 LEVS-9001 1000 NAME  TTP  QTP  UTP  VTP" | ccc   select npakgg gsttp gsqtp gsutp gsvtp
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc      gsapl gsttp gslnsp ttp
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc      gsapl gsqtp gslnsp qtp
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc      gsapl gsutp gslnsp utp
echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc      gsapl gsvtp gslnsp vtp
      rm npaksp npakgg sslnsp gslnsp sfpr tsfpr
      rm gsttp gsqtp gsutp gsvtp
#   ---------------------------------- time and zonally average.
      timavg ttp tttp
      timavg qtp tqtp
      timavg utp tutp
      timavg vtp tvtp
      rm ttp qtp utp vtp
      rzonavg tttp  del  rztttp
      rzonavg tqtp  del  rztqtp
      rzonavg tutp  del  rztutp
      rzonavg tvtp  del  rztvtp
      rm del

#   ---------------------------------- xsave fields.

echo "XSAVE         TTP
NEWNAM.     TTP
XSAVE         QTP
NEWNAM.     QTP
XSAVE         UTP
NEWNAM.     UTP
XSAVE         VTP
NEWNAM.     VTP" | ccc xsave new_gp tttp   tqtp   tutp   tvtp new.
      mv new. new_gp

echo "XSAVE         (TTP)R
NEWNAM.     TTP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (QTP)R
NEWNAM.     QTP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (UTP)R
NEWNAM.     UTP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (VTP)R
NEWNAM.     VTP" | ccc xsave new_xp rztttp rztqtp rztutp rztvtp new.
      mv new. new_xp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi

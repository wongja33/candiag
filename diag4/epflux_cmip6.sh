#!/bin/sh
# ---------------------------------------------------------------------------
# EP flux diagnostics for CMIP6. Based on deck for QBOi (cmamdyn_epflux2.dk) 
# Formulation follows AHL87. 
# Deck outputs monthly and daily mean zonal wind tendency due 
# to EP flux divergence, meridional and vertical components of EP flux, 
# meridional residual velocity (v*), vertical residual velocity (w* in m/s), 
# residual streamfunction computed from v* and meridional heat flux. 
# Daily means saved if dxsave=on. Log pressure coordinates used.  
# Options to: (1) decompose all fluxes into different zonal wavenumber bands
# (used only if nbands > 0); (2) plot monthly mean results.
#                                                       C. McL (2 Nov 2015)
#  Some parmsubs and flags:
#      dxsave = flag to save daily zonal (mean) data
#
# Modifications:
#    (1) change superlabels (C.McL. 12 Nov 2015)
#    (2) based on qboi_epflux.dk but program EPFLUX used instead of EPFLUX99; 
#        daily mean heat flux computed & saved; flags xp24save_epdiag and 
#        xp24save_extra added (C.McL. 8 Apr 2016)
#    (3) replace equal signs in superlabels with blanks since "=" may cause a problem 
#        accessing timeseries files when dumping to CFS (C.McL. 14 Dec 2016)
#    (4) compute/save daily & monthly mean advection terms in TEM eqn (C.McL. Nov/18) 
#    (5) Simplified flags, changed file names (xp24-->dx), and renamed (cmamdyn_epflux2.dk --> epflux_cmip6.dk) (MS, 5 Feb 2019)
#    (6) Simplified daily binning code, and corrected time labelling,  
#	 Shortened superlabels for U tendency due to TEM, renamed fortran program to epflux_cmip6 (MS, 13 Feb 2019)
#    (7) Save monthly data in zp instead of xp files, added representative zonal means of u,v,w,t and z 
#        (to avoid having to convert all xp variables to timeseries) (MS, 7 Mar 2019)
# ---------------------------------------------------------------------------
#                                    - C.McLandress (Jan 9/2007)
#				     - adapted by M.Sigmond (Jun 6/2007)
#                                    - bug in relab corrected M.Sigmond (Jul 19/2013)            
#                                    - use program epflux99 which permits increased
#                                      resolution between 10 and 100 hPa (CM May/15)
#                                    - compute mass stremfunction (CM Oct/15)
#                                    - xp24save added (CM Oct/15)
#                                    - remove plotting (SK Oct/19)

     access u ${flabel}_gpu
     access v ${flabel}_gpv
     access w ${flabel}_gpw
     access t ${flabel}_gpt
     access phi ${flabel}_gpz
     
# Calculate zonal means

     zonavg u uz udev
     zonavg v vz vdev
     zonavg w wz wdev
     zonavg t tz tdev
     zonavg phi phiz phidev

# Calculate monthly zonal means
     for v in u v w t phi; do
	timavg ${v}z ${v}zt
     done
     rm u v w t phi

# OLD: Calculate representative zonal means 
# Note: moved away from representative zonal means
# as cmip6/dynvarmip requests either the regular zonal mean
# or a representative zonal mean that is inconsistent
# with rzonavg and timeavg (timeavg can't deal with missing values
# that would be the result of a proper rzonavg)
#
#     access beta ${flabel}_gptbeta na
#     if [ ! -s beta ] ; then
#                                      if not available, get it from gp file
#        access oldgp ${flabel}gp
#        echo "XFIND.        $d" | ccc xfind oldgp beta
#     fi
#     for v in u v w t phi; do
#	timavg ${v} ${v}_t
#	rzonavg ${v}_t beta ${v}_tz
#     done
#     rm u v w t phi


# ****************************************************************
# EP flux diags for all zonal wavenumbers 
# ****************************************************************
# SK: pure I/O should be avoided
#      cp udev up
#      cp vdev vp
#      cp wdev wp
#      cp tdev tp
     mlt   udev vdev upvp
     mlt   udev wdev upwp
     mlt   vdev tdev vptp
     zonavg upvp upvpz
     zonavg upwp upwpz
     zonavg vptp vptpz
     rm upvp upwp vptp
     epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy epfz epfd vres wres utendvtem utendwtem
echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux

# ****************************************************************
# Compute mass streamfunction from residual meridional velocity
# ****************************************************************

     mstrfcn vres mpsi

# Compute daily means and save in dx file
 
    if [ "$dxsave" = "on" ] ; then
       vars="epfd epfy epfz vres wres utendvtem utendwtem mpsi htflux"	
       echo "C*BINSML  $bins_isgg         1" > ic.binsml.isgg
       echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model

       for v in $vars ; do 
#  	                            bin daily  
	  binsml $v $v.d input=ic.binsml.isgg
#                                      relabel time step
          tstep $v.d $v.t input=ic.tstep_model
       done
#  	                            save in dx file  
       access olddx ${flabel}dx
echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX
        x
              VERTICAL COMPONENT EP FLUX
        x
              EP FLUX DIVERGENCE
        x
              RESIDUAL MERIDIONAL VELOCITY
        x
              RESIDUAL VERTICAL VELOCITY
        x
              U TNDCY DUE TO V TEM ADV AND CORIOLIS TERM 
        x
              U TNDCY DUE TO W TEM ADV
        x
              RESIDUAL STREAMFUNCTION
        x
              MERIDIONAL HEAT FLUX
        x" | ccc xsave olddx epfy.t epfz.t epfd.t vres.t wres.t utendvtem.t utendwtem.t mpsi.t htflux.t newdx
       #save daily variables in dx
       save newdx ${flabel}dx
       delete olddx
     fi


# Time averages

     timavg epfy epfyt
     timavg epfz epfzt
     timavg epfd epfdt
     timavg vres vrest
     timavg wres wrest
     timavg utendvtem utendvtemt
     timavg utendwtem utendwtemt
     timavg mpsi mpsit
     timavg htflux htfluxt

echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX
        x
              VERTICAL COMPONENT EP FLUX
        x
              EP FLUX DIVERGENCE
        x
              RESIDUAL MERIDIONAL VELOCITY
        x
              RESIDUAL VERTICAL VELOCITY
        x
              RESIDUAL STREAMFUNCTION
        x
              MERIDIONAL HEAT FLUX
        x
              U TNDCY DUE TO V TEM ADV AND CORIOLIS TERM 
        x
              U TNDCY DUE TO W TEM ADV
        x
              U
        x
              V
        x
              W
        x
              T
        x
              Z
        x" | ccc xsave epfile epfyt epfzt epfdt vrest wrest mpsit htfluxt utendvtemt utendwtemt uzt vzt wzt tzt phizt new
     mv new epfile

# ****************************************************************
# EP flux diags for zonal wavenumber bands 
# ****************************************************************

     if [ "$nbands" -ge 1 ] ; then

# SK: pure I/O should be avoided
#        cp udev up
#        cp vdev vp
#        cp wdev wp
#        cp tdev tp
       echo "  LLAFRB.  $maxf $mfirst_band1 $mlast_band1" | ccc llafrb udev um
       echo "  FRALL. $lon" | ccc frall  um up     
       echo "  LLAFRB.  $maxf $mfirst_band1 $mlast_band1" | ccc llafrb vdev vm
       echo "  FRALL. $lon" | ccc frall  vm vp
       echo "  LLAFRB.  $maxf $mfirst_band1 $mlast_band1" | ccc llafrb wdev wm
       echo "  FRALL. $lon" | ccc frall  wm wp
       echo "  LLAFRB.  $maxf $mfirst_band1 $mlast_band1" | ccc llafrb tdev tm
       echo "  FRALL. $lon" | ccc frall  tm tp
       rm um vm wm tm
       mlt up vp upvp
       mlt up wp upwp
       mlt vp tp vptp
       zonavg upvp upvpz
       zonavg upwp upwpz
       zonavg vptp vptpz
       rm upvp upwp vptp
       epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy1 epfz1 epfd1 vdum wdum vdum2 wdum2
       echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux1

       timavg epfy1 epfy1t
       timavg epfz1 epfz1t
       timavg epfd1 epfd1t
       timavg htflux1 htflux1t

       echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX FOR M $mfirst_band1 TO$mlast_band1
        x
              VERTICAL COMPONENT EP FLUX FOR M $mfirst_band1 TO$mlast_band1
        x
              EP FLUX DIVERGENCE FOR M $mfirst_band1 TO$mlast_band1
        x
              MERIDIONAL HEAT FLUX FOR M $mfirst_band1 TO$mlast_band1
        x" | ccc xsave epfile epfy1t epfz1t epfd1t htflux1t new
       mv new epfile

       if [ "$nbands" -ge 2 ] ; then

# SK: pure I/O should be avoided
#          cp udev up
#          cp vdev vp
#          cp wdev wp
#          cp tdev tp
         echo "  LLAFRB.  $maxf $mfirst_band2 $mlast_band2" | ccc llafrb udev um
         echo "  FRALL. $lon" | ccc frall  um up     
         echo "  LLAFRB.  $maxf $mfirst_band2 $mlast_band2" | ccc llafrb vdev vm
         echo "  FRALL. $lon" | ccc frall  vm vp
         echo "  LLAFRB.  $maxf $mfirst_band2 $mlast_band2" | ccc llafrb wdev wm
         echo "  FRALL. $lon" | ccc frall  wm wp
         echo "  LLAFRB.  $maxf $mfirst_band2 $mlast_band2" | ccc llafrb tdev tm
         echo "  FRALL. $lon" | ccc frall  tm tp
         rm um vm wm tm
         mlt up vp upvp
         mlt up wp upwp
         mlt vp tp vptp
         zonavg upvp upvpz
         zonavg upwp upwpz
         zonavg vptp vptpz
         rm upvp upwp vptp
         epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy2 epfz2 epfd2 vdum wdum vdum2 wdum2
         echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux2

         timavg epfy2 epfy2t
         timavg epfz2 epfz2t
         timavg epfd2 epfd2t
         timavg htflux2 htflux2t

         echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX FOR M $mfirst_band2 TO$mlast_band2
        x
              VERTICAL COMPONENT EP FLUX FOR M $mfirst_band2 TO$mlast_band2
        x
              EP FLUX DIVERGENCE FOR M $mfirst_band2 TO$mlast_band2
        x
              MERIDIONAL HEAT FLUX FOR M $mfirst_band2 TO$mlast_band2
        x" | ccc xsave epfile epfy2t epfz2t epfd2t htflux2t new
         mv new epfile

         if [ "$nbands" -ge 3 ] ; then

# SK: pure I/O should be avoided
#            cp udev up
#            cp vdev vp
#            cp wdev wp
#            cp tdev tp
           echo "  LLAFRB.  $maxf $mfirst_band3 $mlast_band3" | ccc llafrb udev um
           echo "  FRALL. $lon" | ccc frall  um up     
           echo "  LLAFRB.  $maxf $mfirst_band3 $mlast_band3" | ccc llafrb vdev vm
           echo "  FRALL. $lon" | ccc frall  vm vp
           echo "  LLAFRB.  $maxf $mfirst_band3 $mlast_band3" | ccc llafrb wdev wm
           echo "  FRALL. $lon" | ccc frall  wm wp
           echo "  LLAFRB.  $maxf $mfirst_band3 $mlast_band3" | ccc llafrb tdev tm
           echo "  FRALL. $lon" | ccc frall  tm tp
           rm um vm wm tm
           mlt up vp upvp
           mlt up wp upwp
           mlt vp tp vptp
           zonavg upvp upvpz
           zonavg upwp upwpz
           zonavg vptp vptpz
           rm upvp upwp vptp
           epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy3 epfz3 epfd3 vdum wdum vdum2 wdum2
           echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux3
           timavg epfy3 epfy3t
           timavg epfz3 epfz3t
           timavg epfd3 epfd3t
           timavg htflux3 htflux3t

           echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX FOR M $mfirst_band3 TO$mlast_band3
        x
              VERTICAL COMPONENT EP FLUX FOR M $mfirst_band3 TO$mlast_band3
        x
              EP FLUX DIVERGENCE FOR M $mfirst_band3 TO$mlast_band3
        x
              MERIDIONAL HEAT FLUX FOR M $mfirst_band3 TO$mlast_band3
        x" | ccc xsave epfile epfy3t epfz3t epfd3t htflux3t new
           mv new epfile

           if [ "$nbands" -ge 4 ] ; then

# SK: pure I/O should be avoided
#              cp udev up
#              cp vdev vp
#              cp wdev wp
#              cp tdev tp
             echo "  LLAFRB.  $maxf $mfirst_band4 $mlast_band4" | ccc llafrb udev um
             echo "  FRALL. $lon" | ccc frall  um up     
             echo "  LLAFRB.  $maxf $mfirst_band4 $mlast_band4" | ccc llafrb vdev vm
             echo "  FRALL. $lon" | ccc frall  vm vp
             echo "  LLAFRB.  $maxf $mfirst_band4 $mlast_band4" | ccc llafrb wdev wm
             echo "  FRALL. $lon" | ccc frall  wm wp
             echo "  LLAFRB.  $maxf $mfirst_band4 $mlast_band4" | ccc llafrb tdev tm
             echo "  FRALL. $lon" | ccc frall  tm tp
             rm um vm wm tm
             mlt up vp upvp
             mlt up wp upwp
             mlt vp tp vptp
             zonavg upvp upvpz
             zonavg upwp upwpz
             zonavg vptp vptpz
             rm upvp upwp vptp
             epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy4 epfz4 epfd4 vdum wdum vdum2 wdum2
             echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux4

             timavg epfy4 epfy4t
             timavg epfz4 epfz4t
             timavg epfd4 epfd4t
             timavg htflux4 htflux4t

             echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX FOR M $mfirst_band4 TO$mlast_band4
        x
              VERTICAL COMPONENT EP FLUX FOR M $mfirst_band4 TO$mlast_band4
        x
              EP FLUX DIVERGENCE FOR M $mfirst_band4 TO$mlast_band4
        x
              MERIDIONAL HEAT FLUX FOR M $mfirst_band4 TO$mlast_band4
        x" | ccc xsave epfile epfy4t epfz4t epfd4t htflux4t new
             mv new epfile

             if [ "$nbands" -ge 5 ] ; then

# SK: pure I/O should be avoided
#                cp udev up
#                cp vdev vp
#                cp wdev wp
#                cp tdev tp
               echo "  LLAFRB.  $maxf $mfirst_band5 $mlast_band5" | ccc llafrb udev um
               echo "  FRALL. $lon" | ccc frall  um up     
               echo "  LLAFRB.  $maxf $mfirst_band5 $mlast_band5" | ccc llafrb vdev vm
               echo "  FRALL. $lon" | ccc frall  vm vp
               echo "  LLAFRB.  $maxf $mfirst_band5 $mlast_band5" | ccc llafrb wdev wm
               echo "  FRALL. $lon" | ccc frall  wm wp
               echo "  LLAFRB.  $maxf $mfirst_band5 $mlast_band5" | ccc llafrb tdev tm
               echo "  FRALL. $lon" | ccc frall  tm tp
               rm um vm wm tm
               mlt up vp upvp
               mlt up wp upwp
               mlt vp tp vptp
               zonavg upvp upvpz
               zonavg upwp upwpz
               zonavg vptp vptpz
               rm upvp upwp vptp
               epflux_cmip6 upvpz upwpz vptpz uz tz vz wz epfy5 epfz5 epfd5 vdum wdum vdum2 wdum2
               echo "  RELABL.  
           ZONL           VPTP" | ccc relabl vptpz htflux5

               timavg epfy5 epfy5t
               timavg epfz5 epfz5t
               timavg epfd5 epfd5t
               timavg htflux5 htflux5t

               echo "  XSAVE.      MERIDIONAL COMPONENT EP FLUX FOR M $mfirst_band5 TO$mlast_band5
        x
              VERTICAL COMPONENT EP FLUX FOR M $mfirst_band5 TO$mlast_band5
        x
              EP FLUX DIVERGENCE FOR M $mfirst_band5 TO$mlast_band5
        x
              MERIDIONAL HEAT FLUX FOR M $mfirst_band5 TO$mlast_band5
        x" | ccc xsave epfile epfy5t epfz5t epfd5t htflux5t new
               mv new epfile

             fi 
           fi 
         fi 
       fi 
     fi

# Save in zp file
     save   epfile ${flabel}zp

# Check that sum of EP flux & div for all wavenumber bands equals total
# (works only for 4 or 5 bands and only if the sum of the wavenumber bands 
# equals the total)

     if [ "$epchecksum" = on ] ; then

       add epfy1 epfy2 sum1
       add epfy3 epfy4 sum2
       add sum1  sum2  epfy_sum
       if [ "$nbands" = 5 ] ; then
         mv epfy_sum sum3
         add epfy5 sum3  epfy_sum 
       fi
       timavg epfy epfyt
       timavg epfy_sum epfyt_sum
       sub epfyt epfyt_sum epfyt_diff 
       ggstat epfyt_diff  
       ggstat epfyt
       ggstat epfyt_sum 

       add epfz1 epfz2 sum1
       add epfz3 epfz4 sum2
       add sum1  sum2  epfz_sum
       if [ "$nbands" = 5 ] ; then
         mv epfz_sum sum3
         add epfz5 sum3  epfz_sum 
       fi
       timavg epfz epfzt
       timavg epfz_sum epfzt_sum
       sub epfzt epfzt_sum epfzt_diff 
       ggstat epfzt_diff  
       ggstat epfzt
       ggstat epfzt_sum

       add epfd1 epfd2 sum1
       add epfd3 epfd4 sum2
       add sum1  sum2  epfd_sum
       if [ "$nbands" = 5 ] ; then
         mv epfd_sum sum3
         add epfd5 sum3  epfd_sum 
       fi
       timavg epfd epfdt
       timavg epfd_sum epfdt_sum
       sub epfdt epfdt_sum epfdt_diff 
       ggstat epfdt_diff  
       ggstat epfdt
       ggstat epfdt_sum

       add htflux1 htflux2 sum1
       add htflux3 htflux4 sum2
       add sum1  sum2  htflux_sum
       if [ "$nbands" = 5 ] ; then
         mv htflux_sum sum3
         add htflux5 sum3  htflux_sum 
       fi
       timavg htflux htfluxt
       timavg htflux_sum htfluxt_sum 
       sub htfluxt htfluxt_sum htfluxt_diff 
       ggstat htfluxt_diff  
       ggstat htfluxt
       ggstat htfluxt_sum

     fi
     
# Plot time averages

     if [ "$plot" = "on" ] ; then
        libncar plunit=$plunit
	echo "2 
START - START - START - START - START - START - START - START - START 
 DELIVER THE FOLLOWING PLOTS TO...

          $user
          CCCMA
          3964 Gordon Head Road, Victoria, BC

          Job Name  = $jobname
          Host Name = $HOSTID
          Plot Date = `date '+%H.%M.%S %Z, %a, %b. %e, %Y.'`" | ccc txtplot
        timavg uz   uzt 
        echo " ZXPLOT.   NEXT    02   0    0        1.     -200.      300.       10.    1    1
    $run ${days}$yr TIME AVG ZONAL VELOCITY (M/S)
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot uzt
        timavg vres vrest 
        echo " ZXPLOT.   NEXT    02   0   -1        1.      -30.       30.        1.    1    1
    $run ${days}$yr TIME AVG RESIDUAL MERIDIONAL VELOCITY (M/S)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot vrest
        timavg wres wrest 
        echo " ZXPLOT.   NEXT    02   0   -1      100.     -100.      300.       0.2    1    1
    $run ${days}$yr TIME AVG RESIDUAL VERTICAL VELOCITY (CM/S) 
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot wrest
        timavg epfd epfdt 
        echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        5.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) (ALL M)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfdt
        if [ "$epchecksum" = on ] ; then
          timavg epfd_sum epfdt 
          echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        5.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) SUM CHECK (ALL M)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfdt
        fi
        if [ "$nbands" -ge 1 ] ; then
          timavg epfd1 epfd1t 
          echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        2.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) ($plotlab_band1)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.        2.       24." | ccc zxplot epfd1t
          if [ "$nbands" -ge 2 ] ; then
            timavg epfd2 epfd2t 
            echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        1.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) ($plotlab_band2)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfd2t
            if [ "$nbands" -ge 3 ] ; then
              timavg epfd3 epfd3t 
              echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        1.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) ($plotlab_band3)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfd3t
              if [ "$nbands" -ge 4 ] ; then
                timavg epfd4 epfd4t 
                echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        1.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) ($plotlab_band4)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfd4t
                if [ "$nbands" -ge 5 ] ; then
                  timavg epfd5 epfd5t 
                  echo " ZXPLOT.   NEXT    02   0   -1    86400.     -200.      200.        1.    1    1
    $run ${days}$yr DIV(F))/(A*COS(LAT) ($plotlab_band5)
                 90.      -90.$dpr1$dpr2    2
              $npat  $ipat1  $ipat2  $ipat3  $ipat4  $ipat5  $ipat6  $ipat7  
                  0.        4.        8.       12.       16.       20.       24." | ccc zxplot epfd5t
                fi
              fi
            fi
          fi
        fi

	echo "2 
 END OF PLOTTING. PLEASE SEND TO ...
          $user
          CCCMA
          3964 Gordon Head Road, Victoria, BC
 END - END - END - END - END - END - END - END - END - END" | ccc txtplot
.       plot.cdk
     fi

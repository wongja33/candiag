#/bin/sh
#                  gs6h_cmip6           sk - Jan 29/2020 - 
#  -----------------------------------  extract 6hr data on model levels

#  ----------------------------------- prepare input cards 

    if [ -z "$year_offset" ] ; then
      echo "ERROR in daily_cmip6.dk: undefined variable year_offset"
      exit 1
    fi
    if [ -z "$delt" ] ; then
      echo "ERROR in daily_cmip6.dk: undefined variable delt"
      exit 1
    fi
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h

    if [ "$gssave" = "on" ] ; then
#                                      sampled 6-hourly data on model levels
#                                      requires gssave=on
      # 3D sigma levels
      gsvars="ps gsq gst gsu gsv gsz"
      rm -f ic.xsave.gs
      for v in $gsvars ; do
        if [ "$v" = "gsq" ] ; then
          V="SHUM"
        elif [ "$v" = "gst" ] ; then
          V="TEMP"
        elif [ "$v" = "gsu" ] ; then
          V="U"
        elif [ "$v" = "gsv" ] ; then
          V="V"
        elif [ "$v" = "gsz" ] ; then
          V="PHI"
        elif [ "$v" = "ps" -o "$v" = "gsps" ] ; then
          V="PS"
        fi
        release $v
        access  $v ${flabel}_$v
#                                      relable time step
        tstep $v $v.1 input=ic.tstep_model_6h ; mv $v.1 $v
#                                      accumulate xsave input card
        echo "C*XSAVE       $V
               " >> ic.xsave.gs
      done

# ------------------------------------ xsave

      vars0=`echo "$gsvars" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rm -f old
      while [ -n "$vars0" ] ; do
        vars1=`echo "$vars0" | cut -f1-80 -d' '`
        head -160 ic.xsave.gs > ic.xsave.gs1
        xsave old $vars1 new input=ic.xsave.gs1
        mv new old
        vars0=`echo "$vars0" | cut -f81- -d' '`
        tail -n +161 ic.xsave.gs > ic.xsave.gs1 ; mv ic.xsave.gs1 ic.xsave.gs
      done

#   ---------------------------------- save results.

      save   old ${flabel}ds

    fi # gssave=on

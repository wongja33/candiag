#deck gcmsub
jobname=gcmsub ; time=$stime ; memory=$memory1  ; noxaprun=on
. comjcl.cdk
. suppress_compilers_setup
cat > Execute_Script <<'end_of_script'
# 
# Revised to call "gcmparm_sizes_extra.cdk".
# Added "fullmodsub" option support for inlining lssub subroutines. 
#                  gcmsub              EC,BD,FM,LS - Apr 22/15 - FM
# ------------------------------------ Script to pre-process and submit model
#                                      job script and source code.
# ------------------------------------ Input sections must be supplied by
#                                      the user.                                   

sed -n '
  /^ *###.*gcmparm/,/^ *###/{
    /^ *$/d
    /^ *#/!w gcmparm.in
  }
  /^ *###.*parmsub/,/^ *###/{
    /^ *###/!w parmsub.in
  }
  /^ *###.*condef/,/^ *###/{
    /^ *###/!w condef.in
  }
  /^ *###.*update.*script/,/^ *###/{
    /^ *$/d
    /^ *#/!w update_script.in
  }
  /^ *###.*update.*model/,/^ *###/{
    /^ *$/d
    /^[*%].[^d]/y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/
    /^ *###/!w update_model.in
  }
  /^ *###.*update *sub/,/^ *###/{
    /^ *$/d
    /^[*%].[^d]/y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/
    /^ *###/!w update_model_sub.in
  }
  /^ *###.*update.*ocean/,/^ *###/{
    /^ *$/d
    /^[*%].[^d]/y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/
    /^ *###/!w update_ocean.in
  }
  /^ *###.*update *osub/,/^ *###/{
    /^ *$/d
    /^[*%].[^d]/y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/
    /^ *###/!w update_ocean_sub.in
  }'                                     Model_Input

# ------------------------------------ Possibly setup for multitasking. 

. multisub.cdk

# ------------------------------------ Setup for propagating PARM/PARC 
#                                      records information in 
#                                      "txt2binm.in"/"txt2binc.in" 
#                                      files. 
#                                      Also, extract "CPP_I" file.

. parmxtrct.cdk

if [ "$samerun" != on ] ; then

  # ---------------------------------- Verify consistency of "itrac" 
  #                                    values specified.
# . check_itrac.cdk

  # ---------------------------------- Setup for appropriate model library.

  modver=${modver:=$DEFMODL}
  modver=${modver:='gcm6u'} 
  modlib=`find -L $CCRNSRC/source$OSbin/lsmod/agcm/. -name $modver -print 2>/dev/null | tail `
  . gcm_updt_extra.cdk
  if [ -n "$modlib" -a -d "$modlib/." ] ; then 
    modlib=`echo $modlib | sed 's/^.*\/source.*\/lsmod\/agcm/lsmod\/agcm/' ` 
  else 
    abort=abort ; touch haltit
    echo "" ; echo "gcmsub.dk: ERROR; modver(=$modver) subdirectory is not accessible!" ; echo ""
    (echo "" ; echo "gcmsub.dk: ERROR; modver(=$modver) subdirectory is not accessible!" ; echo "") >> haltit
    exit 1 
  fi

  #  --------------------------------- Update main routines for the GCM.
  hidecpp update_model.in
  EXTRA_LIB=${EXTRA_LIB:-"$HOME/bin${OSbin}"} ; EXTRA_LIB2=${EXTRA_LIB2:-"$HOME/bin"} ; update def list update_model.in $modlib extra_lib=$EXTRA_LIB extra_lb2=$EXTRA_LIB2 model_code
  hidecpp --undo model_code ; cat model_code
  echo "modver=$modver" >> parmsub.in

  #  --------------------------------- gcmparm input card must be specified
  #                                    for "gcmpr6u" ("gcm6u") case, 
  #                                    otherwise it will be generated from 
  #                                    parmsub parameters.

  gcmparm=${gcmparm:='gcmparm'}
  cat parmsub.in | sed -e '/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/,/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/d' > gcmparm_parmsub.in
  if [ "$gcmparm" = "gcmpr6u" ] ; then

   if [ ! -s gcmparm.in ] ; then
    touch haltit
    echo "gcmsub: ***ERROR*** A gcmparm input card must be specified in the submission job."
    echo "gcmsub: ***ERROR*** A gcmparm input card must be specified in the submission job." >> haltit
    exit 2
   fi

  else

   # ---------------------------------- Generate input cards for gcmparm
   #                                    from parmsub parameters after 
   #                                    applying consistency checking for 
   #                                    some of these variables.

   if [ -s gcmparm.in ] ; then
    touch haltit
    echo "gcmsub: ***ERROR*** A gcmparm input card exists in the submission job."
    echo "gcmsub: ***ERROR*** A gcmparm input card exists in the submission job." >> haltit
    echo "                    This is no longer supported. Use parmsub parameters instead."
    echo "                    This is no longer supported. Use parmsub parameters instead." >> haltit
    exit 2
   fi
   
   gcmparm_input gcmparm_parmsub.in
   echo 'Generated gcmparm.in file:'
   cat gcmparm.in
   echo 'End of generated gcmparm.in file.'

  fi

  #  --------------------------------- Generate and substitute suitable 
  #                                    dimensions into source code.
  
  $gcmparm sizes < gcmparm.in
  . gcmparm_sizes_extra.cdk
  parmsub model_code model.F sizes

  #  --------------------------------- Update main routines for the ocean 
  #                                    model, if requested.

  if [ -s update_ocean.in ] ; then
    EXTRA_LIB=${EXTRA_LIB:-"$HOME/bin${OSbin}"} ; EXTRA_LIB2=${EXTRA_LIB2:-"$HOME/bin"} ; hidecpp update_ocean.in ; update def list update_ocean.in $modlib extra_lib=$EXTRA_LIB extra_lb2=$EXTRA_LIB2 ocean_code
    hidecpp --undo ocean_code ; cat ocean_code
    parmsub ocean_code ocean.F sizes
  fi

  #  --------------------------------- Process subroutines to be changed,
  #                                    if any.

  if [ -s update_model_sub.in -o \( "$fullmodsub" = 'on' -o "$fullmodsub" = 'yes' \) ] ; then
   if [ "$fullmodsub" = 'on' -o "$fullmodsub" = 'yes' ] ; then modsublib=`find -L $CCRNSRC/source$OSbin/lssub/model/agcm/. -name $modver -print 2>/dev/null | tail -1 ` ; commsublib="$CCRNSRC/source$OSbin/lssub/comm" ; touch tmp_full_sub.in ; find -L $modsublib  -name '*.f' -exec basename {} .f \; | sed -e '/^xit$/d' -e '/^\.[a-zA-Z0-9]*/d' -e 's/^/%C /' | tr '[a-z]' '[A-Z]' >> tmp_full_sub.in ; find -L $commsublib -name '*.f' -exec basename {} .f \; | sed -e '/^xit$/d' -e '/^\.[a-zA-Z0-9]*/d' -e 's/^/%C /' | tr '[a-z]' '[A-Z]' >> tmp_full_sub.in ; if [ -s "./update_model_sub.in" ] ; then cat update_model_sub.in >> tmp_full_sub.in ; rm update_model_sub.in ; fi ; mv tmp_full_sub.in update_model_sub.in ; fi ; hidecpp update_model_sub.in ; gtupsub lnum update_model_sub.in model_sub.F $modver
    hidecpp --undo model_sub.F ; cat model_sub.F >> model.F
  fi

  if [ -s update_ocean_sub.in ] ; then
    hidecpp update_ocean_sub.in ; gtupsub update_ocean_sub.in ocean_sub.F $modver
    hidecpp --undo ocean_sub.F ; ocean_sub='ocean_sub.F'
  fi

  #  --------------------------------- Collect together all source code.
 
  if [ -s ocean.F ] ; then
    cat <<....endcat >> model.F
end_of_source
cat > ocean.F <<'end_of_source'
....endcat
    cat ocean.F $ocean_sub >> model.F
  fi
  if [ -s "CPP_I" ] ; then
    cat <<....endcat >> model.F
end_of_source
cat > CPP_I <<'end_of_source'
....endcat
    cat CPP_I | sed -e 's/^#cpp1/cpp1/g' >> model.F
  fi
  if [ -s "model_sub.F" ] ; then
    cat <<....endcat >> model.F
end_of_source
cat > model_sub.F <<'end_of_source'
....endcat
    cat model_sub.F >> model.F
  fi
  if [ -s "ocean_sub.F" ] ; then
    cat <<....endcat >> model.F
end_of_source
cat > ocean_sub.F <<'end_of_source'
....endcat
    cat ocean_sub.F >> model.F
  fi

else
  modver=${modver:=$DEFMODL} ; modver=${modver:='gcm6u'} ; echo "modver=$modver" >> parmsub.in ; gcm_inpfls_extra=${gcm_inpfls_extra:='gcm_inpfls_extra'} ; export gcm_inpfls_extra ; echo "gcm_inpfls_extra=$gcm_inpfls_extra" >> parmsub.in
  #  --------------------------------- Ensure "samerun" option is passed
  #                                    to the "gcmjcl" job.

  cat >> condef.in <<'  endcat'
    samerun=on
  endcat

fi

#  ------------------------------------- If "nextjob" option is set, pass on
#                                        to next job after model run.

if [ "$nextjob" = on ] ; then

  cat >> condef.in <<..endcat
    nextjob=on ; crawork=$crawork
..endcat
  cat > NOSTRING <<..endcat
    File used to suppress string continuation in endjcl.cdk
..endcat

fi

#  ------------------------------------ Include changes to script library.
OSbin=${OSbin-`ostarget bin`}
if [ -n "$oldiag" ] ; then  diaglib=$oldiag ; else diaglib=${DEFDIAG:='newdiag2'} ; fi
if [ ! -d "$CCRNSRC/source${OSbin}/${diaglib}/." ] ; then
 abort=abort ; touch haltit
 echo "" ; echo "gcmsub.dk: ERROR; $CCRNSRC/source${OSbin}/${diaglib} subdirectory is not accessible!" ; echo ""
 (echo "" ; echo "gcmsub.dk: ERROR; $CCRNSRC/source${OSbin}/${diaglib} subdirectory is not accessible!" ; echo "") >> haltit
 exit 2
fi
EXTRA_LIB=${EXTRA_LIB:-"$HOME/bin${OSbin}"} ; EXTRA_LIB2=${EXTRA_LIB2:-"$HOME/bin"} ; update update_script.in $CCRNSRC/source$OSbin/$diaglib update_script.out usrlib \
       extra_lib=$EXTRA_LIB extra_lb2=$EXTRA_LIB2
#  ------------------------------------ Construct model job.
#read_data < parmsub.in > parmsub.in2
  cat parmsub.in | sed -e '/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/,/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/d' > gcm_script
  cat condef.in update_script.out >> gcm_script
#  ------------------------------------ Setup for propagating "txt2binm.in"
#                                       and (if applicable) "txt2binc.in"
#                                       files into model job.

cat > sed_script <<'endcat'

  / *#.*Insert txt2bin PARM input file content/{
  r txt2binm.in
  d
  }

endcat

if [ -s "txt2binc.in" ] ; then

  cat >> sed_script <<'  endcat'

  / *#.*Insert txt2bin PARC input file content/{
  r txt2binc.in
  d
  }

  endcat

fi

if [ "$samerun" != on ] ; then

  #  ---------------------------------- And for insertion of fortran code.

  cat >> sed_script <<'  endcat'

  / *#.*Insert source code/{
  r model.F
  d
  }

  endcat
  
fi

#  ------------------------------------ Pass submission job through sed to 
#                                       insert the contents of the above
#                                       files into model job.

sed -f sed_script gcm_script > gcmjob

#  ------------------------------------- Submit the job via NQS and 
#                                        clean up temporary directory.
if [ -n "$topdog" ] ; then Qtopdog="topdog=$topdog" ; else Qtopdog=" " ; fi ; if [ -n "$premtn" ] ; then Qpremtn="premtn=$premtn" ; else Qpremtn=" " ; fi
if [ "$script" = on ] ; then

  submit3 gcmjob JHOME=$JHOME mdest=$mdest script log=$log oldiag=$oldiag $Qtopdog $Qpremtn 

  if [ "$host" = "$morigin" -o "$SITE_ID" = Victoria -o "$SITE_ID" = 'DrvlSC' -o \( "$SITE_ID" = 'Dorval' -a "$OS" = 'AIX' \) ] ; then

    (chmod g+r *.script || : ) ; mv *.script $output_dir

  else

##    ftp $morigin <<'      endftp'
##      cd .queue ; put *.script
##    endftp
      (chmod g+r *.script || : ) ; if [ -n "$JHOME" -a -d "${JHOME}/.queue/." -a \( "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' \)  ] ; then cp -p *.script ${JHOME}/.queue ; else cp *.script $HOME/.queue ; fi

  fi

else

  unset script queue 
  # unset script queue nproc nnode geometry
  submit3 gcmjob JHOME=$JHOME mdest=$mdest log=$log oldiag=$oldiag $Qtopdog $Qpremtn || unset noprint 

fi


end_of_script

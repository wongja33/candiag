#!/bin/sh
#                  wat_stat            RH,sk - mar 13/2007 - ec,sk
#   ---------------------------------- compute water fields and balances
#                                      for gcm13.
# ec,sk - add rcm support
#
#  Dictionary for output variables:
#
#   PCP : total precipitation rate
#  PCPN : snowfall rate
#  PCPC : convective precipitation rate
#  PWAT : precipitable water
#   QFS : moisture flux
#   BWG : fresh water flux (at ice-atmosphere interface above sea-ice)
#  OBWG : fresh water flux (at ice-ocean interface below sea-ice)
#    SQ : screen specific humidity
#
#  -------------------------------------------------------------------------
#
.     ggfiles.cdk
#
#  ---------------------------------- select input fields.
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME  PCP PCPC PCPN PWAT" | ccc   select npakgg  PCP PCPC PCPN PWAT
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME  QFS   SQ" | ccc   select npakgg  QFS   SQ
echo "SELECT    STEPS $g1 $g2 $g3 LEVS    1    1 NAME  BWG OBWG" | ccc   select npakgg  BWG OBWG
#
      rm -f npakgg .npakgg_Link

#
#   ---------------------------------- compute and save statistics.
#
      statsav PCP PCPC PCPN PWAT QFS BWG OBWG SQ \
              new_gp new_xp $stat2nd
      statsav PCP new_gp new_xp timmax
#
      rm      PCP PCPC PCPN PWAT QFS BWG OBWG SQ
#
#   ---------------------------------- save results.
#
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi

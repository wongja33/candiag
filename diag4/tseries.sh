#!/bin/sh
#
#             tseries                  L Solheim - Feb, 2008
#   ---------------------------------- Time series of history/diag file variables
#
#  Append a series of superlabeled sets containing time series to a file.
#
#  A number of shell variables may be set by the user to specify the time series
#  that are written to the output file. These variables are described as follows.
#
#  TS_file ...the name of a file to contain the output time series
#             If this file already exists it will be appended to and if
#             it does not exist it will be created (except see
#             TS_file_clobber below). Therefore using the same
#             value of TS_file for an entire run will result in a
#             single time series file for this run. Changing TS_file every
#             year will result in yearly files etc. The default, if the
#             user does not set TS_file, is to create a new time series
#             file every month named ${flabel}ts, analogous to the gp
#             and xp files. Note, flabel will default to the value in
#             the variable model if flabel is not set by the user.
#
#  TS_file_clobber ...flags a forced overwrite of TS_file, even if it exists
#
#  Each superlabeled set in the output file will represent a period of time
#  (typically 1 month) for one variable. There are several types of time series
#  that are supported. Each type is invoked by the user defining a set of options
#  to be associated with a particular variable from a particular file.
#
#  The combination of file type, variable name and options consitutes a
#  variable definition. Variable definitions may be supplied either through
#  variables passed from an invoking script (e.g. TS_gs, TS_ss, TS_gp, TS_xp,...)
#  or through a configuration file named in the variable TS_config. If TS_config
#  is set then variable definitions will be taken from this file only, any
#  definitions supplied in the TS_gs, TS_ss, etc variables will be ignored.
#  The file named in TS_config must contain a full pathname and the invoking
#  user must have read permission for that file.
#
#  The format of the config file is as follows.
#    - Each line represents a single variable definition or is a comment.
#    - A line in which the first non-whitespace character is "#" is a comment.
#    - All non comment lines are divided into fields delimited by colons (:).
#    - All fields except for the file type and variable name fields have
#      default values. Any field with a default value may be missing or empty.
#    - In all fields except the variable name field, whitespace is ignored.
#    - Field names and their definitions are as follows.
#  Tag:Var Catergory:File Type:Var Name:Var Def:Spatial Avg:Sampling:Temporal Avg:Coded Levels:Level Coord:Window
#        Tag ...an arbitrary string
#            Tag is ignored by this script but it is written to the output file.
#            It is intended to be used as an identifier that is meaningful
#            to the user (e.g. CF compliant variable name).
#        File Type ...suffix of the file in which this variable is found
#            This could be GS, SS, GP, XP, TD or GZ (case insensitive) and is used to
#            determine the file name as well as certain characteristics of the variables
#            found in this file (e.g. GP or XP implies monthly averaged fields).
#        Variable Name ...the NAME (ibuf3 value) as it appears in the file.
#                         In the case of a GP or XP file this is a super label.
#        Variable Definition ...an expression that defines a new variable in terms of
#                               other variables that exist in the input file
#            The desired variable may be something that does not exist
#            in the input file but can be derived from other variables
#            that are found in the input file. This field will tell this script
#            how to create these derived types from existing variables.
#            e.g. FLA+FLG would create a variable in the output file that is
#            the sum of the variables FLA and FLG found in the input file.
#            When this field is not empty the name of the variable that will appear in
#            the output file is the name specified in the Variable Name field.
#        Spatial Avg ...indicates the variable is to be a global average (g or G)
#                       or a zonal average (z or Z)
#            In multi level fields these spatial averages are applied on each level.
#            The default is to not perform any explicit spatial averaging so that the
#            variable will be spatially averaged, or not, depending on how it appears
#            in the input file.
#        Time Sampling ...a comma separated list of quantities (inc,orig,offset1,offset2)
#            inc is the sampling frequency, orig indicates the origin of the
#            selection, either start of month (M) or start of run (R), offset1 is a time
#            offset relative to the origin and offset2 is the largest time step to extract.
#            Any of inc, offset1 or offset2 may be of the form (here N is an integer)
#              N - a number of time steps (kount values)
#              Ns or NS - a number of save intervals
#              Nd or ND - a number of days
#        Temporal Avg ...indicates the variable is to be monthly averaged (m or M).
#            The default is to not perform any explicit temporal averaging
#            so that the variable will be temporally averaged, or not,
#            depending on how it appears in the input file.
#        Coded Levels ...a comma separted list of coded levels (ibuf4 values).
#            If levels are supplied then only these levels will appear in the output
#            time series file. The default is to extract every level found in the
#            input file.
#        Level Coord ...a single (case insensitive) character to indicate that the
#                       levels should be model eta (e or E) levels, pressure (p or P) levels
#                       or ocean depths (d or D).
#            If pressure levels are requested and the input file contains model levels
#            then an attempt will be made to interpolate to pressure levels.
#            This may not always be possible. If model levels are requested and the
#            input file contains pressure levels then an error will occur.
#            The default is to use the level coordinates found in the input file.
#        Window ...a comma separated list of the four grid indicies that identify a
#                  window, optionally followed by a switch indicating whether the grid
#                  contains an extra cyclic longitude (cycle=0) or not (cycle .ne. 0).
#            If cycle is not present then cycle=0 is used.
#            window = "left,right,lower,upper,cycle"
#            The letters "NH" and "SH" may be used in place of the list of integers
#            as shorthand for the northern or southern hemispheres.
#            The defualt is to use the entire grid found in the input file.
#        SplitName ...A file name into which the current variable will be written
#                     This overrides TS_file fo rthe current variable only.
#                     TS_file_split can modify the behavior.
#
#
#  TS_gs         ...a space separated list of GS file variable names.
#                   Each variable in this list is extracted from the
#                   current GS file and added to the output file as a
#                   single superlabeled set.
#
#  TS_ss         ...a space separated list of SS file variable names.
#                   Each variable in this list is extracted from the
#                   current SS file and added to the output file as a
#                   single superlabeled set.
#
#  TS_gp         ...a space separated list of GP file variable names.
#                   Each variable in this list is extracted from the
#                   current GP file and added to the output file as a
#                   single superlabeled set.
#
#  TS_gpNN (NN=01,02,...99)
#    - each variable of the form TS_gp01, TS_gp02, ... will contain a
#      superlabel found in the current GP file.
#    - Each set found with one of these superlabels is extracted from
#      the GP file and appended to the output file.
#    - It is assumed that each superlabeled set contains a single variable.
#
#  TS_xp         ...a space separated list of XP file variable names.
#                   Each variable in this list is extracted from the
#                   current XP file and added to the output file as a
#                   single superlabeled set.
#
#  TS_xpNN (NN=01,02,...99)
#    - Each variable of the form TS_xp01, TS_xp02, ... will contain a
#      superlabel found in the current XP file.
#    - Each set found with one of these superlabels is extracted from
#      the XP file and appended to the output file.
#    - It is assumed that each superlabeled set contains a single variable.
#
#  TS_td         ...a space separated list of TD file variable names.
#                   Each variable in this list is extracted from the
#                   current TD file and added to the output file as a
#                   single superlabeled set.
#
#  TS_gz         ...a space separated list of GZ file variable names.
#                   Each variable in this list is extracted from the
#                   current GZ file and added to the output file as a
#                   single superlabeled set.
#
#  TS_sp2gg     ...flags conversion of spectral records to grid records
#                  TS_sp2gg is either "on" of "off" (default is "on").
#
#  TS_clean     ...flags removal of certain intermediate files
#
#  -------------------------------------------------------------------------
#

#   ----------------------------------
#   start wall clock
    echo "\nSTART tseries:  "`date`"\n"

# -----------------------------------
# bail is a simple error exit routine
bail(){
    echo "tseries: "$1
    echo "tseries: "$1 >> haltit
    exit 1
}

# Define a local access command that it will look for files in a local dir
# named $access_dir if $access_dir is set, and create a local link.
access_dir=${TS_access_dir:=''}
local_access() {
    [ -z "$access_dir" ] && \
        bail "local_access: access_dir is not defined"
    [ x`expr $access_dir : '\(/\)'` != 'x/' ] && \
        bail "local_access: access_dir=$access_dir is not an absolute path name"
    [ ! -d "$access_dir" ] && \
        bail "local_access: access_dir=$access_dir is not a directory"
    [ -z "$1" ] && bail "local_access: 2 files names are required: $@"
    [ -z "$2" ] && bail "local_access: 2 files names are required: $@"
    [ ! -f "${access_dir}/$1" ] && \
        bail "local_access: ${access_dir}/$1 is not a regular file"
    ln -fs ${access_dir}/$1 $2
}

# Define a local release command that will simply remove the link
# created by the local access command
local_release() {
    [ -z "$1" ] && bail "local_release: A files name is required."
    rm -f $1
}

# Use the local access command if access_dir is defined
if [ -n "$access_dir" ]; then
  ACCESS=local_access
  RELEASE=local_release
else
  ACCESS=access
  RELEASE=release
fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Create a directory and populate it with various scripts that may be
# used below to define particular variables or perform certain tasks.
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    CWD=`pwd`
    # LOCAL_BIN must be a full path name
    LOCAL_BIN=$CWD/bin_$$
    mkdir $LOCAL_BIN
    cd $LOCAL_BIN

#   ----------------------------------
#   Create local scripts that may be used to create certain variables via
#   a vardef of the form def_scriptname

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # opld will determine Lid Pressure
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > opld << 'EOF'
      # requires ODPX and ODPY from GZ file
      selvar GZFILE ODPX x$lev_vals x$lev_coord x$tselpar
      selvar GZFILE ODPY x$lev_vals x$lev_coord x$tselpar

      # opld : Lid Pressure
      lidpres ODPX ODPY ocn_datadesc ocngrid_beta ocngrid_dy ocngrid_da NEWVAR
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # ti20 will determine Depth of 20C isotherm
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > ti20 << 'EOF'
      # requires TEMP from GZ file
      selvar GZFILE TEMP x$lev_vals x$lev_coord x$tselpar

      # ti20 = Depth of 20C isotherm
      echo "THETA_DEPTH 20.0" > INREC
      ccc  theta_depth ocngrid_dz ocngrid_beta TEMP NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # mxld will determine mixed layerdepth in meters
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > mxld << 'EOF'
      # requires TEMP and SALT from GZ file
      selvar GZFILE TEMP x$lev_vals x$lev_coord x$tselpar
      selvar GZFILE SALT x$lev_vals x$lev_coord x$tselpar

      echo "MIXLDEPTH   0.8" > INREC
      ccc mixldep TEMP SALT NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # hcon will determine 300m heat content
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > hcon << 'EOF'
      # requires TEMP from GZ file
      selvar GZFILE TEMP x$lev_vals x$lev_coord x$tselpar
    
      # hcon = 300m heat content
      echo "XLIN              1.   -273.16" > INREC
      ccc    xlin    TEMP x1 input=INREC

      mlt     x1  ocngrid_beta  temp3d 

      echo " VZINTV            0      3000" > INREC
      ccc    vzintv  temp3d   ocngrid_dz   x2 input=INREC

      ib2=`ggstat temp3d | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`
      ib2o=`ggstat x2 | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`

      echo "    RELABL       $ib2o
                 $ib2" > INREC      
      ccc relabl x2 x3 input=INREC

      echo " XLIN     0.00333333" > INREC 
      ccc    xlin    x3    NEWVAR input=INREC
   
      rm -f x1 temp3d x2 x3 ib2 ib2o
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # bnii will determine vertically integrating multi-level bni
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > bnii << 'EOF'
      # requires BNI from GZ file
      selvar GZFILE BNI x$lev_vals x$lev_coord x$tselpar
   
      # vertical integration
      mlt     BNI  ocngrid_beta  tmp
      
      vzint   tmp   ocngrid_dz  tmp1
      ib2=`ggstat tmp | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`
      echo "ib2=$ib2"
      ib2o=`ggstat tmp1 | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`
      echo "ib2o=$ib2o"

      echo "    RELABL       $ib2o
                 $ib2" > INREC
      ccc relabl tmp1 NEWVAR input=INREC

      rm -f tmp tmp1 ib2 ib2o
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # dici will determine vertically integrating multi-level dic
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > dici << 'EOF'
      # requires DIC from GZ file
      selvar GZFILE DIC x$lev_vals x$lev_coord x$tselpar
  
      # vertical integration
      mlt     DIC  ocngrid_beta  tmp
      vzint   tmp   ocngrid_dz   tmp1

      ib2=`ggstat tmp | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`
      ib2o=`ggstat tmp1 | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`

      echo "    RELABL       $ib2o
                 $ib2" > INREC
      ccc relabl tmp1 NEWVAR input=INREC

      rm -f tmp tmp1 ib2 ib2o
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # poci will determine vertically integrating multi-level poc
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > poci << 'EOF'
      # requires BPH, BZO and BDE from GZ file
      selvar GZFILE BPH x$lev_vals x$lev_coord x$tselpar
      selvar GZFILE BZO x$lev_vals x$lev_coord x$tselpar
      selvar GZFILE BDE x$lev_vals x$lev_coord x$tselpar

      # Build POC = BPH + BZO + BDE
      add  BPH   BZO   sum1
      add  sum1  BDE   poc
 
      # vertical integration
      mlt     poc  ocngrid_beta  tmp
      vzint   tmp   ocngrid_dz   tmp1

      ib2=`ggstat tmp | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`
      ib2o=`ggstat tmp1 | awk '{if($4=="MLT"){printf "%s",$3;exit}}'`

      echo "    RELABL       $ib2o
                 $ib2" > INREC
      ccc relabl tmp1 NEWVAR input=INREC

      rm -f sum1 poc tmp tmp1 ib2 ib2o
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sst will determine sea surface temperature
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sst << 'EOF'
      # requires TEMP from GZ file
      selvar GZFILE TEMP x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from TEMP and assume it is SST
      echo "RCOPY              1         1" > INREC
      ccc rcopy TEMP NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sss will determine sea surface salinity (converted to psu)
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sss << 'EOF'
      # requires SALT from GZ file
      selvar GZFILE SALT x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from SALT and assume it is SSS
      echo "RCOPY              1         1" > INREC
      ccc rcopy SALT SSS input=INREC

      # Convert to psu
      echo "XLIN           1000.       0.0" > INREC
      ccc xlin SSS NEWVAR input=INREC

      rm -f SSS
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sss2 will determine sea surface salinity (NOT converted to psu)
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sss2 << 'EOF'
      # requires SALT from GZ file
      selvar GZFILE SALT x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from SALT and assume it is SSS
      echo "RCOPY              1         1" > INREC
      ccc rcopy SALT NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sdic will determine sea surface dic
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sdic << 'EOF'
      # requires DIC from GZ file
      selvar GZFILE DIC x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from DIC and assume it is SDIC
      echo "RCOPY              1         1" > INREC
      ccc rcopy DIC NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # salk will determine sea surface alkalinity
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > salk << 'EOF'
      # requires ALK from GZ file
      selvar GZFILE ALK x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from ALK and assume it is SALK
      echo "RCOPY              1         1" > INREC
      ccc rcopy ALK NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sbni will determine sea surface nitrogen
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sbni << 'EOF'
      # requires BNI from GZ file
      selvar GZFILE BNI x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from BNI and assume it is SBNI
      echo "RCOPY              1         1" > INREC
      ccc rcopy BNI NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # sbch will determine sea surface chlorophyll
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > sbch << 'EOF'
      # requires BCH from GZ file
      selvar GZFILE BCH x$lev_vals x$lev_coord x$tselpar

      # Extract the first level from BCH and assume it is SBCH
      echo "RCOPY              1         1" > INREC
      ccc rcopy BCH NEWVAR input=INREC
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # ti01 will determine monthly ti01
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > ti01 << 'EOF'
      # requires VI01 and XF01 from GS file
      selvar GSFILE VI01 x$lev_vals x$lev_coord x$tselpar
      selvar GSFILE XF01 x$lev_vals x$lev_coord x$tselpar

      cp     VI01  vi01_2
      cp     XF01  xf01_2

      echo "  TIMLAG           1" > INREC
      ccc     timlag VI01  vi01_2  vi01_rm_end  vi01_rm_beg input=INREC

      echo "  TIMLAG           1" > INREC
      ccc     timlag XF01  xf01_2  xf01_rm_end  xf01_rm_beg input=INREC

      sub    vi01_rm_beg   vi01_rm_end  vi01_diff

      echo "    XLIN  1.15741E-5      0.00" > INREC 
      ccc     xlin   vi01_diff     dvi01_dt input=INREC

      add    xf01_rm_end  xf01_rm_beg   xf01_sum
      echo "    XLIN         0.5      0.00 XF01" > INREC
      ccc     xlin   xf01_sum     xf01_ave  input=INREC

      sub    dvi01_dt     xf01_ave     NEWVAR

      rm -f vi01_2 xf01_2 vi01_rm_end vi01_rm_beg xf01_rm_end xf01_rm_beg vi01_diff dvi01_dt xf01_sum xf01_ave
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # rsdscs will determine clear sky surface downwelling shortwave
    # flux in air
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > rsdscs << 'EOF'
      # rsdscs = FSGC/(1-SALB) = FSGC*FSS/FSG
      # requires FSGC, FSS and FSG from GS file
      selvar GSFILE FSGC x$lev_vals x$lev_coord x$tselpar
      selvar GSFILE FSS  x$lev_vals x$lev_coord x$tselpar
      selvar GSFILE FSG  x$lev_vals x$lev_coord x$tselpar

      div FSS FSG t_fssg
      mlt FSGC t_fssg t_rsdsc
      echo "    NEWNAM SDSC" > newnam_card
      newnam t_rsdsc NEWVAR input=newnam_card
      rm -f t_fssg t_rsdsc newnam_card
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # salb will determine surface albedo
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > salb << 'EOF'
      # Surface albedo SALB = (FSS-FSG)/FSS
      # requires FSS and FSG from GS file
      selvar GSFILE FSS x$lev_vals x$lev_coord x$tselpar
      selvar GSFILE FSG x$lev_vals x$lev_coord x$tselpar

      # Create a mask that is 0 where FSS-FSG <= 0 and 1 elsewhere
      echo ' FMASK            -1   -1   GT        0.' > fmask_input_card
      sub FSS FSG t_fsrgi
      fmask t_fsrgi t_msk input=fmask_input_card

      # Force FSS-FSG to be positive or zero everywhere
      mlt t_fsrgi t_msk t_fsrg

      # Evaluate the surface albedo and clean up
      div t_fsrg FSS t_salb
      echo "    NEWNAM SALB" > newnam_card
      newnam t_salb NEWVAR input=newnam_card
      rm -f t_fsrgi t_msk t_fsrg t_salb newnam_card fmask_input_card
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # ua will determine meridional wind
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > ua << 'EOF'
      # requires VORT and DIV from SS file

      # Convert VORT and DIV found in the SS file to U and V

      # If not already on disk, extract U,V and convert to gaussian grid
      if [ ! -s U_grid -o ! -s V_grid ]; then

        # Extract VORT and DIV from the SS file
        if [ ! -s VORT_spec ]; then
          cp SSFILE/VORT VORT_spec
        fi
        if [ ! -s  DIV_spec ]; then
          cp SSFILE/DIV DIV_spec
        fi

        # Convert spectral VORT and DIV to spectral (U,V)COS(LAT)/A
        # Note: cwinds will add an extra row to the U and V spectral fields
        # to hold derivative info.
        cwinds VORT_spec DIV_spec U_spec V_spec

        # Put spectral (U,V)COS(LAT)/A onto the gaussian grid and
        # convert to real winds U and V
        mv U_spec U_grid
        mv V_spec V_grid
        spec2grid U_grid 1
        spec2grid V_grid 1
      fi

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`


        # If not already on disk, extract LNSP and convert to gaussian grid
        if [ ! -s LNSP_grid ]; then
          cp SSFILE/LNSP LNSP_grid
          spec2grid LNSP_grid
        fi

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPL     $npl        0.        0.$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsapl U_grid LNSP_grid NEWVAR
      else
        cp U_grid NEWVAR
      fi
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # va will determine zonal wind
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > va << 'EOF'
      # requires VORT and DIV from SS file

      # Convert VORT and DIV found in the SS file to U and V

      # If not already on disk, extract U,V and convert to gaussian grid
      if [ ! -s U_grid -o ! -s V_grid ]; then

        # Extract VORT and DIV from the SS file
        if [ ! -s VORT_spec ]; then
          cp SSFILE/VORT VORT_spec
        fi
        if [ ! -s  DIV_spec ]; then
          cp SSFILE/DIV DIV_spec
        fi

        # Convert spectral VORT and DIV to spectral (U,V)COS(LAT)/A
        # Note: cwinds will add an extra row to the U and V spectral fields
        # to hold derivative info.
        cwinds VORT_spec DIV_spec U_spec V_spec

        # Put spectral (U,V)COS(LAT)/A onto the gaussian grid and
        # convert to real winds U and V
        mv U_spec U_grid
        mv V_spec V_grid
        spec2grid U_grid 1
        spec2grid V_grid 1
      fi

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`


        # If not already on disk, extract LNSP and convert to gaussian grid
        if [ ! -s LNSP_grid ]; then
          cp SSFILE/LNSP LNSP_grid
          spec2grid LNSP_grid
        fi

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPL     $npl        0.        0.$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsapl V_grid LNSP_grid NEWVAR
      else
        cp V_grid NEWVAR
      fi
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # hus will determine specific humidity
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > hus << 'EOF'
      # requires ES, TEMP and LNSP from SS file

      # If not already on disk, extract ES and convert to gaussian grid
      if [ ! -s ES_hu_grid ]; then
        cp SSFILE/ES ES_hu_grid
        spec2grid ES_hu_grid
      fi

      # If not already on disk, extract TEMP and convert to gaussian grid
      if [ ! -s TEMP_hu_grid ]; then
        cp SSFILE/TEMP TEMP_hu_grid
        spec2grid TEMP_hu_grid
      fi

      # If not already on disk, extract LNSP and convert to gaussian grid
      if [ ! -s LNSP_grid ]; then
        cp SSFILE/LNSP LNSP_grid
        spec2grid LNSP_grid
      fi

      if [ ! -s SHUM_grid -o ! -s RHUM_grid ]; then
        # Convert gridded moisture variable to gridded specific (SHUM_gs) and
        # relative (RHUM_gs) humidity

        # Format input variables
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_moist" ] && bail "curr_moist is not defined"
        curr_moist=`echo $curr_moist|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_sref" ] && bail "curr_sref is not defined"
        curr_sref=`echo $curr_sref|awk '{printf "%10.2E",$1}' -`
        [ -z "$curr_spow" ] && bail "curr_spow is not defined"
        curr_spow=`echo $curr_spow|awk '{printf "%10.1f",$1}' -`

        echo "gshumh input card"
        echo "$curr_coord$curr_moist$curr_plid$curr_sref$curr_spow"

        echo "GSHUMH    $curr_coord$curr_moist$curr_plid$curr_sref$curr_spow" |\
          ccc gshumh ES_hu_grid TEMP_hu_grid LNSP_grid SHUM_grid RHUM_grid
      fi

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPL     $npl        0.        0.$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsapl SHUM_grid LNSP_grid NEWVAR
      else
        cp SHUM_grid NEWVAR
      fi
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # Extract prerequisit fields for interpolation of TEMP and PHI
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > grid_temp_phi_etal << 'EOF'

      bail(){
        echo "grid_temp_phi_etal: "$1
        echo "grid_temp_phi_etal: "$1 >> haltit
        exit 1
      }

      # Determine spectral truncation parameters
      # Pick any spectral file from the SSFILE directory and extract LRLMT
      # from which the truncation parameters may be determined
      [ ! -s SSFILE/ES ] && bail "ES is missing from SSFILE."
      LRLMT=`ggstat SSFILE/ES|awk '{if($2=="SPEC"){printf "%d",$8;exit}}' -`
      [ -z "$LRLMT" ]  && bail "Unable to determine LRLMT"
      [ $LRLMT -le 0 ] && bail "LRLMT=$LRLMT is out of range"
      if [ $LRLMT -lt 100000 ]; then
        LR=`echo $LRLMT|awk '{n=int($1/1000);printf "%d",n}' -`
      else
        LR=`echo $LRLMT|awk '{n=int($1/10000);printf "%d",n}' -`
      fi
      kuv=0
      if [ $kuv -eq 1 ]; then
        # When converting model winds to real winds assume that the field to be
        # converted has been through cwinds which will have modified the ibuf7
        # value such that the value of LR calculated above will be 1 greater than
        # it should actually be. (e.g. an input value of ibuf7=48482 gets changed
        # to ibuf7=49482 on return from cwinds)
        LR=`expr $LR - 1`
      fi
  
      # Define parameters input to cofagg
      nlon=`echo $LR|awk '{printf "%5d",2*$1}' -`
      nlat=`echo $LR|awk '{printf "%5d",$1}' -`
      kuv=`echo $kuv|awk '{printf "%5d",$1}' -`

      # If not already on disk, extract ES and convert to gaussian grid
      if [ ! -s ES_grid ]; then
        cp SSFILE/ES ES_grid
        echo "   COFAGG $nlon$nlat$kuv    1" | ccc cofagg ES_grid XXX
        mv XXX ES_grid
      fi

      # If not already on disk, extract TEMP and convert to gaussian grid
      if [ ! -s TEMP_grid ]; then
        cp SSFILE/TEMP TEMP_grid
        echo "   COFAGG $nlon$nlat$kuv    1" | ccc cofagg TEMP_grid XXX
        mv XXX TEMP_grid
      fi

      # If not already on disk, extract LNSP and convert to gaussian grid
      if [ ! -s LNSP_grid ]; then
        cp SSFILE/LNSP LNSP_grid
        echo "   COFAGG $nlon$nlat$kuv    1" | ccc cofagg LNSP_grid XXX
        mv XXX LNSP_grid
      fi

      # If not already on disk, evaluate SHUM and RHUM on the gaussian grid
      if [ ! -s SHUM_grid -o ! -s RHUM_grid ]; then
        # Convert gridded moisture variable to gridded specific (SHUM_gs) and
        # relative (RHUM_gs) humidity

        # Format input variables
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_moist" ] && bail "curr_moist is not defined"
        curr_moist=`echo $curr_moist|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_sref" ] && bail "curr_sref is not defined"
        curr_sref=`echo $curr_sref|awk '{printf "%10.2E",$1}' -`
        [ -z "$curr_spow" ] && bail "curr_spow is not defined"
        curr_spow=`echo $curr_spow|awk '{printf "%10.1f",$1}' -`

        # echo "gshumh input card"
        # echo "$curr_coord$curr_moist$curr_plid$curr_sref$curr_spow"

        echo "GSHUMH    $curr_coord$curr_moist$curr_plid$curr_sref$curr_spow" |\
          ccc gshumh ES_grid TEMP_grid LNSP_grid SHUM_grid RHUM_grid
      fi

      # Convert SHUM to RGAS
      echo "XLIN.           176.      287. RGAS"|ccc xlin SHUM_grid RGAS_grid

      # If not already on disk, calculate PHI from fields in the ss file
      if [ ! -s PHI_grid ]; then

        # If not already on disk, extract PHIS and convert to gaussian grid
        if [ ! -s PHIS_grid ]; then
          cp SSFILE/PHIS PHIS_grid
          echo "   COFAGG $nlon$nlat$kuv    1" | ccc cofagg PHIS_grid XXX
          mv XXX PHIS_grid
        fi

        # Format input variables
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_lay" ] && bail "curr_lay is not defined"
        curr_lay=`echo $curr_lay|awk '{printf "%5d",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`

        # Derive PHI on eta levels from TEMP and moist gas constant
        echo "TAPHI.    $curr_coord$curr_lay$curr_plid" |\
          ccc taphi TEMP_grid PHIS_grid LNSP_grid RGAS_grid PHI_grid

      fi
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # temp will determine temperature
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > temp << 'EOF'
      # Extract all prerequisit gridded fields
      $LOCAL_BIN/grid_temp_phi_etal

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_sref" ] && bail "curr_sref is not defined"
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPZ(T)  $npl       0.0    6.5E-3$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsaplt TEMP_grid PHI_grid RGAS_grid LNSP_grid NEWVAR PHI_gtmp

      else
        cp TEMP_grid NEWVAR
      fi

      # Clean up
      rm -f TEMP_gtmp PHI_gtmp
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # phi will determine geopotential
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > phi << 'EOF'
      # Extract all prerequisit gridded fields
      $LOCAL_BIN/grid_temp_phi_etal

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_sref" ] && bail "curr_sref is not defined"
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPZ(T)  $npl       0.0    6.5E-3$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsaplt TEMP_grid PHI_grid RGAS_grid LNSP_grid TEMP_gtmp NEWVAR

      else
        cp PHI_grid NEWVAR
      fi

      # Clean up
      rm -f TEMP_gtmp PHI_gtmp
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # gz will determine geopotential height
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > gz << 'EOF'
      # Extract all prerequisit gridded fields
      $LOCAL_BIN/grid_temp_phi_etal

      # Interpolate to pressure levels if requested
      interp_to_pl=0
      case $lev_coord in
        P|p) interp_to_pl=1 ;;
      esac
      if [ $interp_to_pl -eq 1 ]; then
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_sref" ] && bail "curr_sref is not defined"
        PLEVELS="$DEFAULT_PLEVELS"
        if [ -n "$lev_vals" ]; then
          PLEVELS="$lev_vals"
        fi
        # Count the number of levels
        npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
        # Reformat for use with gsapl
        PLEVELS=`echo $PLEVELS|\
          awk -F',' '{for (i=1; i<=NF; i++) {
                      printf "%5d",$i
                      if (i%16 == 0) {printf "\n"}}}' -`

        # Convert gridded fields from eta levels to pressure levels
        echo "GSAPZ(T)  $npl       0.0    6.5E-3$curr_coord$curr_plid
$PLEVELS
 "      | ccc gsaplt TEMP_grid PHI_grid RGAS_grid LNSP_grid TEMP_gtmp PHI_gtmp

      else
        cp PHI_grid PHI_gtmp
      fi

      # Convert geopotential to geopotential height (divide by g=9.8106)
      echo "XLIN.       0.101931        0.   GZ"|ccc xlin PHI_gtmp NEWVAR

      # Clean up
      rm -f TEMP_gtmp PHI_gtmp
EOF

    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    # make_vardef will perform two different functions under control of
    # a commnad line option. In either case the input to make_vardef will
    # be a variable definition and, for option 1, an output variable name.
    # 1) Write a script that will create a file containing the variable
    #    that is described by the variable definition and return to
    #    stdout a list of variable names used in the variable definition
    #    and therfore required by the script.
    # 2) Write a file that contains a map between variable names found in
    #    the input variable definition and internally generated replacement
    #    variable names. The new variable definition, with new variable
    #    names, is returned to stdout. This is required for variable
    #    definitions that contain superlabels for variable names.
    #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    cat > make_vardef << 'EOF'
#!/usr/bin/perl -w
########################################################################
# Parse a user supplied variable definition and output various info
# extracted from this definition.
#
# Larry Solheim May,2008
########################################################################

  require 5.003;

  my $DEBUG = 0;

  if ($DEBUG) {
    # Open a file for debugging output
    die "Unable to create $cmds_file.\n  Stopped"
      unless (open(DBG_vardef, ">DBG_vardef"));
  }

  # verbose controls the amount of info written to stdout
  $verbose = 0;

  # If quiet is set then minimal info is written to stdout
  $quiet = 0;

  # var_out is the output variable name
  $var_out = '';

  # write_cmds flags the creation of a shell script in a file named $cmds_file
  # This shell script will create a file named $var_out containing
  # the variable defined by the variable definition found in $vardef
  # If write_cmds is set then a list of all variable names found in
  # vardef will be output to stdout. This is the default behaviour.
  $write_cmds = 1;
  $cmds_file = 'create_vname';

  # write_map flags creation of an ascii file in a file named $map_file
  # The contents of this file will map all variable names found in
  # $vardef to internally generated ibuf3 values.
  # Each line in this file will contain a colon (:) separated pair of strings.
  # The first string will be the superlabel found in the GP or XP file and
  # the second string will be an internally generated ibuf3 variable name.
  # This map will be used to replace superlabels with valid ibuf3 values in vardef
  # and the new vardef will be output to stdout.
  $write_map = 0;
  $map_file = 'var_slab_map';

  # Process command line args
  foreach $arg (@ARGV) {
    if ($arg =~ /^out=/)  {($var_out) = $arg =~ /^out=(.*)/; next}
    if ($arg =~ /^-cmds/) {$write_cmds=1; $write_map=0; next}
    if ($arg =~ /^-map/)  {$write_cmds=0; $write_map=1; next}
    if ($arg =~ /^-v$/)   {$verbose++; next}
    if ($arg =~ /^-q$/)   {$quiet = 1; next}
    push @Vardef, $arg;
  }

  if ($quiet) {$verbose = -1}

  unless ($var_out) {
    die "Output variable name (out=vname) required on command line.\n Stopped"
      if ($write_cmds);
  }

  die "A variable definition is required on the command line.\n  Stopped"
    unless scalar(@Vardef);

  # Use only the first definition found on the command line. Ignore the rest.
  $vardef = $Vardef[0];
  $new_vardef = $vardef;

  # regex to match any number
  $numex = q/[0-9.]+(?:(?:[eE]|[dD])[+-]?[0-9][0-9]*)?/;

  $tagcount = 0;
  my $tagnum = sub {
    # generate a token, hash the incomming number against this token
    # and return the token value
    my $num = shift;  # number found by regex
    $tagen{++$tagcount} = 1;
    my ($tag) = \$tagen{$tagcount} =~ /\(([^)]*)\)/;
    $numtag{$tag} = $num;
    return $tag;  # replacement token for input number
  };

  # Replace all numbers found in $vardef with tokens
  $vardef =~ s/($numex)/&$tagnum($1)/meig;

  # Split $vardef into terms separated by either '+' or '-'
  # retaining any leading sign
  my @terms = $vardef =~ /((?:^|[+-])[^+-]+)/g;

  # Replace any tokens in vardef with the original numbers
  foreach (keys %numtag) {$vardef =~ s/$_/$numtag{$_}/g}

  my $nterm = 0;
  my $nvar = 0;
  my $cmds = '';
  my $prev_term = '';
  my $sc_term = 0;
  my $slab_map = '';
  foreach $term (@terms) {
    $nterm++;
    if ($DEBUG) {
      print DBG_vardef "$nterm  term=$term\n";
    }
    # Determine the sign of this term
    my $lcoeff = ($term =~ /^\s*-/) ? -1 : 1;
    # Remove the leading sign and leading whitespace from each term
    $term =~ s/^\s*[+-]\s*//;
    # Replace any tokens in each term with the original numbers
    foreach (keys %numtag) {$term =~ s/$_/$numtag{$_}/g}
    # Split each term into atoms separated by either '*' or '/'
    # retaining any leading operator
    my @atoms = ();
    # TODO: modify this regex so that escaped '*' or '/' characters
    #       are not recognized as break points. This will allow these
    #       chars to be used in variable names (e.g. superlabels)
    push @atoms, $term =~ /((?:^|[*\/])[^*\/]+)/g;
    # Create a unique 4 character variable name for this term
    my $term_name = sprintf("ZZ%2.2d",$nterm);
    my $natom = 0;
    my $prev_var = '';
    foreach (@atoms) {
      if (++$natom == 1) {
        # The first atom in this term
        if (/^\s*[*\/]/) {
          # There should not be an operator prepended here
          die "Either '*' or '/' is prepended to $term.\n Stopped";
        } elsif (/^\s*[0-9.]/) {
          # If the first character is numeric or a '.'
          # then assume this is a number
          $lcoeff =  $lcoeff * $_;
        } else {
          # If it is not a number then assume it is a variable name
          push @req_var,$_;
          $prev_var = $_;
          $cmds .= "cp $prev_var $term_name\n";
          # Append to the variable name map and replace the current
          # variable name in new_vardef with its new name
          my $repl_var = sprintf("YZ%2.2d",++$nvar);
          $slab_map .= "$_:$repl_var\n";
          # Replace only the first name found
          $new_vardef =~ s/\Q$_\E/$repl_var/;
        }
        next;
      }

      # Determine which operator preceeds this atom and strip it off
      my $op = '';
      $op = "mlt" if /^\s*[*]/;
      $op = "div" if /^\s*[\/]/;
      die "Missing operator in term $term.\n   Stopped" unless $op;
      my ($atom) = /^\s*[*\/]\s*(.*)\s*/;
      $atom =~ s/\s*$//;

      # Determine what to do with this atom
      if ($atom =~ /^[0-9.]/) {
        # If the first character is numeric or a '.'
        # then assume this is a number
        $lcoeff = ($op eq "div") ? $lcoeff / $atom : $lcoeff * $atom;
      } else {
        # If it is not a number then assume it is a variable name
        push @req_var,$atom;
        # Append to the variable name map and replace the current
        # variable name in new_vardef with its new name
        my $repl_var = sprintf("YZ%2.2d",++$nvar);
        $slab_map .= "$atom:$repl_var\n";
        # Replace only the first name found
        $new_vardef =~ s/\Q$atom\E/$repl_var/;
        if ($prev_var) {
          # If a variable has previously been encountered in this term
          # then write apporiate commands to multiply or divide the previous
          # variable by the current variable.
          $cmds .= "$op $prev_var $atom XXX\n";
          $cmds .= "mv XXX $term_name\n";
        } else {
          # If this is the first variable encountered in this term
          # then simply copy it to the output variable
          $cmds .= "cp $atom $term_name\n";
        }
        $prev_var = $term_name;
      }
    }

    if ($DEBUG) {
      print DBG_vardef "$nterm  atoms=@atoms\n";
    }

    if ($prev_var) {
      # If any variable has been encountered then multiply by the leading coeff
      my $xlincard = sprintf("      XLIN%10.3E%10.3E %4s",$lcoeff,0,$term_name);
      $cmds .= "echo '" . $xlincard . "' > INREC\n";
      $cmds .= "ccc xlin $term_name XXX input=INREC\n";
      $cmds .= "mv XXX $term_name\n";
    }

    # Add each term as it is processed
    if ($prev_term) {
      # If this is not the first term encountered
      # then add this term to the previous total
      if ($DEBUG) {
        print DBG_vardef "$nterm   NOT first term  prev_var=$prev_var\n";
      }
      if ($prev_var) {
        # At least 1 variable name was found in this term
        $cmds .= "add $prev_term $term_name XXX\n";
        $cmds .= "mv XXX $prev_term\n";
      } else {
        # The current term is a scalar
        $sc_term += $lcoeff;
      }
    } else {
      # If this is the first term then simply copy it
      if ($DEBUG) {
        print DBG_vardef "$nterm   First term  prev_var=$prev_var\n";
      }
      if ($prev_var) {
        # At least 1 variable name was found in this term
        $cmds .= "cp $term_name $var_out\n";
        $prev_term = $term_name;
      } else {
        # The current term is a scalar
        $sc_term += $lcoeff;
      }
    }

    if ($DEBUG) {
      print DBG_vardef "$nterm  prev_term=$prev_term   prev_var=$prev_var\n";
      print DBG_vardef "$nterm\n$cmds\n\n";
    }

  }

  # Add the scalar term, if any
  if ($prev_term) {
    my $xlincard = sprintf("      XLIN%10.3E%10.3E %4s",1,$sc_term,$var_out);
    $cmds .= "echo '" . $xlincard . "' > INREC\n";
    $cmds .= "ccc xlin $prev_term XXX input=INREC\n";
    $cmds .= "mv XXX $var_out\n";
  } else {
    # There were no variable names found in any of the terms
    die "No variable names specified in vardef: $vardef\n";
  }

  if ($write_cmds) {
    # Write a script to create the new variable
    if (open(CMDS, ">$cmds_file")) {
      print CMDS $cmds;
    } else {
      die "Unable to create $cmds_file.\n  Stopped";
    }
    # Dump a space separated list of variable names found
    # in vardef to stdout.
    print join(" ",@req_var);
  }

  if ($write_map) {
    # Write a map of superlabels to internally generated variable
    # names that have replaced the superlabels in vardef to a file
    if (open(MAP, ">$map_file")) {
      print MAP $slab_map;
    } else {
      die "Unable to create $map_file.\n  Stopped";
    }
    # Dump the new vardef, with superlabels replaced by internally
    # generated variable names to stdout.
    print "$new_vardef";
  }

  unless ($DEBUG) {`rm -f DBG_vardef`};

exit 0;
########################################################
##################### End of main ######################
########################################################
EOF

    # Ensure that all scripts in LOCAL_BIN are executable
    chmod u+x *

    # Move back to the execution directory
    cd $CWD

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Define some functions used below
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

# ----------------------------------
# Format data record for input to chabin
fmt_data_rec(){
  # usage: fmt_data_rec file_in fmt_file_out

  # Create an ascii file containing numbers in 1P6E22.15 format
  # suitable for one data record read by chabin.
  # This function assumes that the input file contains only numeric data
  # with one or more whitespace separated values per line.
  [ -z "$1" ] && bail "fmt_data_rec: Missing input or output file name"
  [ -z "$2" ] && bail "fmt_data_rec: Missing output file name"
  awk 'BEGIN {col=0}
       {for (i=1;i<=NF;i++) {
          col++
          printf "%22.15E",$i
          if (col%6 == 0) {printf "\n"}
        }
       }
       END {if (col%6 != 0) {printf "\n"}}' $1 > $2
}

# ----------------------------------
# Extract a single variable (all records) from a file
rip_var(){
  # usage: rip_var file_in file_out var_name

  [ -z "$1" ] && bail "rip_var: Missing input file name"
  [ -z "$2" ] && bail "rip_var: Missing output file name"
  [ -z "$3" ] && bail "rip_var: Missing variable name"
  echo "rip_var $1 $2 $3"

  if [ -d "$1" ]; then
    [ ! -s $1/$3 ] && bail "rip_var: Empty input file $1/$3"
    # If the file is a directory create a symlink
    rm -f $2
    ln -s $1/$3 $2
  else
    # Use select on a regular file
    [ ! -s "$1" ] && bail "rip_var: Empty input file $1"
    # The output file will be overwritten
    rm -f $2
    vname_fmt=`echo "$3"|awk '{printf "%4.4s",$1}' -`
    echo "SELECT     STEP         0 999999999    1     -999999999 NAME $vname_fmt" |\
      ccc select $1 $2 || bail "rip_var: Cannot select $3 from $1"

#    ccc select $1 $2 <<EOF || bail "rip_var: Cannot select $3 from $1"
#SELECT     STEP         0 999999999    1     -999999999 NAME $vname_fmt
#EOF
  fi
}

# ----------------------------------
# Window a file and set certain shell variables
winfld(){
  # usage: winfld file_name winpar
  # This function is intended to be used only with files that contain
  # a single GRID variable (possibly multi level)

  [ -z "$1" ] && bail "winfld: Missing file name or window parameters"
  [ -z "$2" ] && bail "winfld: Missing window parameters"
  winfld_fname=$1
  winpar=$2

  [ ! -s "$winfld_fname" ] && bail "winfld: File $winfld_fname is missing"

  # Get the name of the first GRID record found in the file winfld_fname
  # It is assumed that only one variable is written to this file
  vname=`ggstat $winfld_fname|awk '{if($2=="GRID"){printf "%s",$4;exit}}' -`
  [ -z "$vname" ] && bail "winfld: No GRID records found in $winfld_fname"

  # Determine whether the grid contains a wrap around longitude
  # (cycl = 0) or not (cycl .ne. 0).
  cycl=`echo $winpar|awk -F',' '{if(NF<5){c=0}else{c=$5};printf "%5d",c}' -`

  # Determine the number of lon (ibuf5 - 1) and lat (ibuf6) values
  if [ $cycl -eq 0 ]; then
    # nlon = ibuf5-1
    nlon=`ggstat $winfld_fname|\
            awk -v fld="$vname" '{if($4==fld){printf "%d",$6-1;exit}}' -`
  else
    # nlon = ibuf5
    nlon=`ggstat $winfld_fname|\
            awk -v fld="$vname" '{if($4==fld){printf "%d",$6;exit}}' -`
  fi
  nlat=`ggstat $winfld_fname|\
            awk -v fld="$vname" '{if($4==fld){printf "%d",$7;exit}}' -`
  [ -z "$nlon" ] && bail "winfld: Unable to determine nlon"
  [ -z "$nlat" ] && bail "winfld: Unable to determine nlat"

  # Define parameters used by window
  # Certain secial cases for winpar are allowed
  case $winpar in
    NH) # Northern hemisphere
        lon1="    1"
        lon2=`echo $nlon|awk '{printf "%5d",$1}' -`
        lat1=`echo $nlat|awk '{lat=1+$1/2; printf "%5d",lat}' -`
        lat2=`echo $nlat|awk '{lat=$1;     printf "%5d",lat}' -`
        ;;
    SH) # Southern hemisphere
        lon1="    1"
        lon2=`echo $nlon|awk '{printf "%5d",$1}' -`
        lat1="    1"
        lat2=`echo $nlat|awk '{lat=$1/2; printf "%5d",lat}' -`
        ;;
     *) # Otherwise only commas and the digits 0-9 are allowed in winpar
        [ -n "`echo $winpar|sed 's/[,0-9]//g'`" ] &&\
        bail "winfld: Invalid windowing parameters -->${winpar}<--"

        # winpar should contain a comma separated list of the four
        # grid indicies that identify the window, optionally followed
        # by a switch indicating whether the grid contains an extra
        # cyclic longitude (cycle=0) or not (cycle .ne. 0). If cycle
        # is not present then cycle=0 is used.
        # winpar="left,right,lower,upper,cycle"
        lon1=`echo $winpar|awk -F',' '{printf "%5d",$1}' -`
        lon2=`echo $winpar|awk -F',' '{printf "%5d",$2}' -`
        lat1=`echo $winpar|awk -F',' '{printf "%5d",$3}' -`
        lat2=`echo $winpar|awk -F',' '{printf "%5d",$4}' -`
        cycl=`echo $winpar|awk -F',' '{printf "%5d",$5}' -`
        [ $lon1 -eq 0 ] && lon1="    1"
        [ $lon2 -eq 0 ] && lon2=`echo $nlon|awk '{printf "%5d",$1}' -`
        [ $lat1 -eq 0 ] && lat1="    1"
        [ $lat2 -eq 0 ] && lat2=`echo $nlat|awk '{printf "%5d",$1}' -`
        ;;
  esac

  # Extract the window
#LPSmodINREC
  echo "  WINDOW  $lon1$lon2$lat1$lat2$cycl    1      1E38" > INREC
  ccc window $winfld_fname XXX input=INREC
  mv XXX $winfld_fname

  # reset midlon and midlat to fall within this window
  midlon=`echo $lon1 $lon2 $nlon|\
            awk '{if ($1<$2) {m=0.5*($1+$2)}
                  else {
                    m=0.5*($1+$2+$3)
                    if (m>$3) {m=m-$3}}
                  printf "%d",m}' -`
  midlat=`echo $lat1 $lat2|awk '{m=0.5*($1+$2);printf "%d",m}' -`

  # Reset winpar so that a consistent format is written to the superlabel
  case $winpar in
    NH|SH) # Do not reformat winpar
       ;;
    *) # Reformat as a 12 character string
       lon1=`echo $lon1|awk '{printf "%3d",$1}' -`
       lon2=`echo $lon2|awk '{printf "%3d",$1}' -`
       lat1=`echo $lat1|awk '{printf "%3d",$1}' -`
       lat2=`echo $lat2|awk '{printf "%3d",$1}' -`
       winpar="$lon1$lon2$lat1$lat2"
       ;;
  esac
}

# ----------------------------------
# Convert a file containing SPEC records to GRID records
spec2grid(){
  # usage: spec2grid file_name [kuv]
  # Put spectral fields on a gaussian grid

  [ -z "$1" ] && bail "spec2grid: Missing file name"
  sp2gg_fname=$1
  if [ -n "$2" ]; then
    # Set kuv=1 to convert model winds to real winds
    # Otherwise it should be 0
    kuv=1
  else
    kuv=0
  fi

  [ ! -s "$sp2gg_fname" ] && bail "spec2grid: File $sp2gg_fname is missing"

  # Determine spectral truncation parameters
  LRLMT=`ggstat $sp2gg_fname|awk '{if($2=="SPEC"){printf "%d",$8;exit}}' -`
  [ -z "$LRLMT" ] && bail "spec2grid: Unable to determine LRLMT"
  [ $LRLMT -le 0 ] && bail "spec2grid: LRLMT=$LRLMT is out of range"
  if [ $LRLMT -lt 100000 ]; then
    LR=`echo $LRLMT|awk '{n=int($1/1000);printf "%d",n}' -`
  else
    LR=`echo $LRLMT|awk '{n=int($1/10000);printf "%d",n}' -`
  fi
  if [ $kuv -eq 1 ]; then
    # When converting model winds to real winds assume that the field to be
    # converted has been through cwinds which will have modified the ibuf7
    # value such that the value of LR calculated above will be 1 greater than
    # it should actually be. (e.g. an input value of ibuf7=48482 gets changed
    # to ibuf7=49482 on return from cwinds)
    LR=`expr $LR - 1`
  fi

  # Define parameters input to cofagg
  nlon=`echo $LR|awk '{printf "%5d",2*$1}' -`
  nlat=`echo $LR|awk '{printf "%5d",$1}' -`
  kuv=`echo $kuv|awk '{printf "%5d",$1}' -`
  rm -f XXX
#LPSmodINREC
  echo "   COFAGG $nlon$nlat$kuv    1" > INREC
  ccc cofagg $sp2gg_fname XXX input=INREC
  mv XXX $sp2gg_fname
}

# ----------------------------------
# Select a field at specified levels and at specified times from a file
# The selected records are put into a file named for the variable selected.
# This file will be overwritten if it exists.
selvar(){
  # usage: selvar file_name var_name [levels]
  # Select levels and/or times from a file

  [ -z "$1" ] && bail "selvar: Missing file name"
  [ -z "$2" ] && bail "selvar: Missing variable name"
  [ -z "$3" ] && bail "selvar: Missing level values"
  [ -z "$4" ] && bail "selvar: Missing level coord type"
  [ -z "$5" ] && bail "selvar: Missing time step selection info"
  loc_fname=$1
  loc_vname=$2
  loc_vfmt=`echo $loc_vname|awk '{printf "%4.4s",$1}' -`

  echo "selvar $loc_fname $loc_vname $3 $4 $5"

  # The remaining args will always begin with the character 'x'
  # This is used as a place holder when any of these arguments are null.
  loc_levv=`echo $3|sed 's/^x//'`
  loc_levc=`echo $4|sed 's/^x//'`
  loc_ksel=`echo $5|sed 's/^x//'`

  # empty_ok is used to tell subsequent script that it is ok to have an empty
  # output file from this function. This can happen when time section does not
  # find any valid records in the current input file.
  empty_ok=0

  # In the special case of internally derived fields, do not attempt to
  # select the field from the input file because it will not exist there.
  # Files containing these fields will have been created before calling getvar.

  if [ ! $derived_internal -eq 1 ]; then
    # Extract all levels and times from the input file
    rip_var $loc_fname $loc_vname $loc_vname
  fi

  ############################################################################
  # If only certain times are requested then extract these times from
  # the file named in the variable loc_vname.
  ############################################################################

  # If loc_ksel is non null and begins with a letter, underscore or backslash
  # then assume it is a file name of a file containing a list of kount values.
  # Only records with one of the kount values in this file will be selected.
  # loc_ksel_fname will be assigned the name of this file if it is supplied
  # otherwise loc_ksel_fname will be null.
  loc_ksel_fname=`echo "$loc_ksel"|sed -n '/^\s*[A-Za-z_\/]/p'`

  if [ -n "$loc_ksel" -a -z "$loc_ksel_fname" ]; then
    # loc_ksel will be a comma separated list of quantities (kinc,orig,koff,koff2)
    # kinc is the sampling frequency, koff is a time offset, koff2 is the
    # largest time step to extract and orig indicates the origin of the selection
    # (either start of month or start of run).

    # Determine the min/max kount values and save interval for this field.
    # This calculation assumes that the save interval is constant
    # between every save in the input file.
    rm -f TMP_kount
    touch TMP_kount
    loc_kinfo=`ggstat $loc_vname|awk -v var="$loc_vname" \
      'BEGIN {n=0; kmin=-999; kmax=-999}
       {if ($4==var) {
          gsub(/[ \t]/,"",$3)
          printf "%d\n",$3 >> "TMP_kount"
          if (n==0) {
            n=1; kmin=$3; kmax=$3
          } else {
            if ($3<kmin) {kmin=$3}
            if ($3>kmax) {kmax=$3}
          }
        }
       }
       END {printf "%d:%d",kmin,kmax}' -`
    loc_kmin=`echo "$loc_kinfo"|awk -F':' '{printf "%d",$1}' -`
    loc_kmax=`echo "$loc_kinfo"|awk -F':' '{printf "%d",$2}' -`

    # Determine the total number of unique kount values for this field
    loc_ntime=`cat TMP_kount|sort -bun|wc -l|sed 's/ //g'`
    [ -z "$loc_ntime" ]  && bail "selvar: loc_ntime is undefined"
    [ $loc_ntime -le 0 ] && bail "selvar: loc_ntime=$loc_ntime is out of range"
    rm -f TMP_kount

    # Determine the save interval for this field
    # This calculation assumes that the save interval is constant
    # between every save in the input file
    loc_delk=`echo $loc_kmin $loc_kmax $loc_ntime|\
                awk '{if ($3>1) {delk=($2-$1)/($3-1)} else {delk=0}
                      printf "%d",delk}' -`

    # When loc_delk = 0 there is only 1 time step in this file.
    # It is considered an error to attempt to time sample the file.
    [ $loc_delk -eq 0 ] &&\
      bail "selvar: Attempting to time sample a file that contains a single record."

    [ -z "$curr_delt" ]  && bail "selvar: curr_delt is undefined"
    [ $curr_delt -le 0 ] && bail "selvar: curr_delt=$curr_delt is out of range"

    # Create a file containing a program to calculate kount values suitable for
    # use with select, from user supplied kinc, koff and koff2 values
#    cat > XXX <<'EOF'
#      BEGIN {kperday=int(86400.0/(1.0*delt)); kpersave=int(delk)}
#      # If of the form N (N is integer) then output N steps
#      $1 ~ /^[ \t]*[0-9][0-9]*[ \t]*$/ {printf "%d",$1; exit}
#      # If of the form Nd or ND (N is integer) then
#      # output the number of steps in N days
#      $1 ~ /^[ \t]*[0-9][0-9]*[ \t]*[dD] *$/ {sub(/[ \t]*[dD][ \t]*$/,"",$1)
#                                    sub(/^[ \t]*/,"",$1)
#                                    k=$1*kperday
#                                    printf "%d",k
#                                    exit}
#      # If of the form Ns or NS (N is integer) then
#      # output the number of steps in N save intervals
#      $1 ~ /^[ \t]*[0-9][0-9]*[ \t]*[sS] *$/ {sub(/[ \t]*[sS][ \t]*$/,"",$1)
#                                    sub(/^[ \t]*/,"",$1)
#                                    k=$1*kpersave
#                                    printf "%d",k
#                                    exit}
#      # Otherwise output a negative value to indicate an error condtion
#      {printf "-999"}
#EOF
      echo 'BEGIN {kperday=int(86400.0/(1.0*delt)); kpersave=int(delk)}'             > XXX
      echo '# If of the form N (N is integer) then output N steps'                  >> XXX
      echo '$1 ~ /^[ \t]*[0-9][0-9]*[ \t]*$/ {printf "%d",$1; exit}'                >> XXX
      echo '# If of the form Nd or ND (N is integer) then'                          >> XXX
      echo '# output the number of steps in N days'                                 >> XXX
      echo '$1 ~ /^[ \t]*[0-9][0-9]*[ \t]*[dD] *$/ {sub(/[ \t]*[dD][ \t]*$/,"",$1)' >> XXX
      echo '                              sub(/^[ \t]*/,"",$1)'                     >> XXX
      echo '                              k=$1*kperday'                             >> XXX
      echo '                              printf "%d",k'                            >> XXX
      echo '                              exit}'                                    >> XXX
      echo '# If of the form Ns or NS (N is integer) then'                          >> XXX
      echo '# output the number of steps in N save intervals'                       >> XXX
      echo '$1 ~ /^[ \t]*[0-9][0-9]*[ \t]*[sS] *$/ {sub(/[ \t]*[sS][ \t]*$/,"",$1)' >> XXX
      echo '                              sub(/^[ \t]*/,"",$1)'                     >> XXX
      echo '                              k=$1*kpersave'                            >> XXX
      echo '                              printf "%d",k'                            >> XXX
      echo '                              exit}'                                    >> XXX
      echo '# Otherwise output a negative value to indicate an error condtion'      >> XXX
      echo '{printf "-999"}'                                                        >> XXX

    # loc_kinc will be the sampling frequency in time steps (kount values)
    loc_kinc=`echo "$loc_ksel"|awk -F',' '{gsub(/[ \t]/,"",$1); printf "%s",$1}' -`
    # Default loc_kinc is TS_kinc
    [ -z "$loc_kinc" ] && loc_kinc=$TS_kinc
    loc_kinc=`echo "$loc_kinc"|\
      awk -F',' -v delt="$curr_delt" -v delk="$loc_delk" -f XXX -`
    [ $loc_kinc -lt 1 ] && bail "selvar: Invalid sampling frequency in $loc_ksel"

    # loc_orig will indicate the origin for the first time step.
    # This will either be the start of the current month (kount=$curr_ksdoy)
    # or the start of the model run (kount=0).
    loc_orig=`echo "$loc_ksel"|awk -F',' '{gsub(/[ \t]/,"",$2); printf "%s",$2}' -`
    # Default loc_orig is 'M' indicating the start of the current month
    [ -z "$loc_orig" ] && loc_orig='M'

    # loc_koff will be an offset from the time step origin which will be used
    # to determine the minimum kount value to be extracted as well as
    # the starting point from which loc_kinc will begin incrementing
    loc_koff=`echo "$loc_ksel"|awk -F',' '{gsub(/[ \t]/,"",$3); printf "%s",$3}' -`
    # Default loc_koff is TS_koff
    [ -z "$loc_koff" ] && loc_koff=$TS_koff
    loc_koff=`echo "$loc_koff"|\
      awk -F',' -v delt="$curr_delt" -v delk="$loc_delk" -f XXX -`
    [ $loc_koff -lt 0 ] && bail "selvar: Min offset must be >= 0 (sampling $loc_ksel)"

    # loc_koff2 will be the largest time step (kount value) to be extracted relative
    # to the time step origin
    loc_koff2=`echo "$loc_ksel"|\
      awk -F',' '{sub(/^[ \t]*/,"",$4); sub(/[ \t]*$/,"",$4); printf "%s",$4}' -`
    # Default loc_koff2 is 0
    [ -z "$loc_koff2" ] && loc_koff2=0
    loc_koff2=`echo "$loc_koff2"|\
      awk -F',' -v delt="$curr_delt" -v delk="$loc_delk" -f XXX -`
    [ $loc_koff2 -lt 0 ] && bail "selvar: Max offset must be >= 0 (sampling $loc_ksel)"

    # Set start_kount based on the value of loc_orig
    # start_kount = $curr_ksdoy if loc_orig = [nM] ...(start of month)
    # start_kount = 0           if loc_orig = [rR] ...(start of run)
    [ -z "$curr_ksdoy" ] && bail "selvar: curr_ksdoy is missing"
    [ -n "`echo $curr_ksdoy|sed 's/[ 0-9]*//g'`" ] &&\
      bail "selvar: Invalid curr_ksdoy = $curr_ksdoy"
    start_kount=`echo $loc_orig $curr_ksdoy|\
      awk '$1 ~ /^[ \t]*[mM][ \t]*$/ {k=$2-1; printf "%d",k; exit}
           $1 ~ /^[ \t]*[rR][ \t]*$/ {k=0;    printf "%d",k; exit}
                                     {k=-999; printf "%d",k}' -`
    [ -z "$start_kount" ]  && bail "selvar: Missing start_kount (sampling $loc_ksel)"
    [ $start_kount -lt 0 ] && bail "selvar: Invalid start_kount (sampling $loc_ksel)"

    # Extract a subset of the records saved in loc_vname
    nt1=`echo $start_kount $loc_koff|awk '{printf "%10d",$1+$2}' -`
    nt2=`echo $start_kount $loc_koff2|\
      awk '{if ($2<=0) {k=999999999} else {k=$1+$2}; printf "%10d",k}' -`
    [ $loc_kinc -gt 99999 ] && bail "selvar: loc_kinc=$loc_kinc is out of range"
    ant1=`echo $loc_kinc|awk '{printf "%5d",$1}' -`
    ant2='     '
    rm -f XXX
    echo "SELECT     STEP$nt1$nt2$ant1${ant2}-9101 1100 NAME $loc_vfmt" |\
      ccc select $loc_vname XXX ||\
      bail "Cannot select sampled subset from $loc_vname (sampling $loc_ksel)"
#    ccc select $loc_vname XXX <<EOF ||\
#      bail "Cannot select sampled subset from $loc_vname (sampling $loc_ksel)"
#SELECT     STEP$nt1$nt2$ant1${ant2}-9101 1100 NAME $loc_vfmt
#EOF
    mv XXX $loc_vname

  elif [ -n "$loc_ksel" -a -n "$loc_ksel_fname" ]; then

    # loc_ksel_fname is assigned the name of a file that contains a list of
    # kount values. Only records that have an ibuf2 value that matches one of
    # the kount values in this file will be selected.

    # Access this file if it is not an absolute pathname or already in the cwd
    if [ ! -s "$loc_ksel_fname" ]; then
      $ACCESS $loc_ksel_fname $loc_ksel_fname
    fi

    [ ! -s "$loc_ksel_fname" ] && \
      bail "selvar: Time sampling file $loc_ksel_fname is missing or empty"

    # Read kount values from the inut file loc_vname and add them to a temporary
    # file named TMP_kount. This tmp file will contain a newline separated list
    # of currently available kount values.
    # Also determine the min/max kount values for this field.
    # This calculation assumes that the save interval is constant
    # between every save in the input file.
    rm -f TMP_kount
    touch TMP_kount
    loc_kinfo=`ggstat $loc_vname|awk -v var="$loc_vname" \
      'BEGIN {n=0; kmin=-999; kmax=-999}
       {if ($4==var) {
          gsub(/[ \t]/,"",$3)
          printf "%d\n",$3 >> "TMP_kount"
          if (n==0) {
            n=1; kmin=$3; kmax=$3
          } else {
            if ($3<kmin) {kmin=$3}
            if ($3>kmax) {kmax=$3}
          }
        }
       }
       END {printf "%d:%d",kmin,kmax}' -`
    loc_kmin=`echo "$loc_kinfo"|awk -F':' '{printf "%d",$1}' -`
    loc_kmax=`echo "$loc_kinfo"|awk -F':' '{printf "%d",$2}' -`

    # Read through the time selection file and extract a subset of that file
    # containing kount values that fall between loc_kmin and loc_kmax inclusive
    rm -f tmp_ksel_file
    touch tmp_ksel_file
    awk -v kmin="$loc_kmin" -v kmax="$loc_kmax" \
      '{if ($3 >= kmin && $3 <= kmax) {print $0 >>"tmp_ksel_file"}}' $loc_ksel_fname

    # Read the file line by line and select any requested fields
    rm -f YYY
    touch YYY
    if [ -s tmp_ksel_file ]; then
      # Do nothing unless there is at least 1 line in tmp_ksel_file
      nline=0
      while [ $nline -lt 9000 ]; do
        nline=`expr $nline + 1`
        tsel_line=`awk -v nline="$nline" '
          {if (NR==nline) {printf "%s",$0; exit}}
          END {if (nline>NR) {printf "END_OF_FILE"}}' tmp_ksel_file`
        [ -z "$tsel_line" ] && continue
        [ "$tsel_line" = "END_OF_FILE" ] && break

        # Ignore blank lines or lines whose first non whitespace char is "#"
        [ -n "`echo $tsel_line|sed -n '/^ *# /p'`" ] && continue
        [ -z "`echo $tsel_line|sed 's/ //g'`" ]      && continue

        # Each line contains 3 whitespace separated integers (year month kount)
        curr_kount=`echo $tsel_line|awk '{printf "%10d",$3}' -`
        if [ -z "`echo $curr_kount|sed 's/ //g'`" ]; then
          echo "Time sampling file tmp_ksel_file"
          cat tmp_ksel_file
          bail "selvar: Empty kount value found in file tmp_ksel_file"
        fi

        # Check to see if this kount value exists in the current file
        # If not then silently ignore
        have_kount=`awk -v k="$curr_kount" '
          BEGIN {have=0; k=1*k}
          $1 == k {have=1}
          END {printf "%d",have}' TMP_kount`
        [ $have_kount -eq 0 ] && continue

        # Select this record
        rm -f INREC
        echo "SELECT     STEP$curr_kount$curr_kount    1     -999999999 NAME $loc_vfmt" > INREC
        ccc select $loc_vname XXX input=INREC

        # Append this record to the tmp output file
        joinup ZZZ YYY XXX
        mv ZZZ YYY

      done
    fi
    mv YYY $loc_vname
    [ ! -s "$loc_vname" ] && empty_ok=1

  fi

  # If the time sampling done above results in an empty file then do not
  # attempt to select levels
  if [ -s "$loc_vname" ]; then

    ############################################################################
    # If a particular level coordinate (e.g. pressure levels, model levels, ...)
    # is requested then ensure that the output field contains this type of level.
    ############################################################################
    have_these_plevs=0
    if [ -n "$loc_levc" ]; then
      [ -n "`echo $loc_levc|sed 's/[eEpPdD]//g'`" ] &&\
        bail "selvar: loc_levc=$loc_levc is invalid. Valid value are E,P,D."
      loc_levc=`echo $loc_levc|tr '[a-z]' '[A-Z]'`
      case $loc_levc in
      D|d) [ "$ftype" != "GZ" -a "$ftype" != "CP" ] &&\
             bail "selvar: loc_levc=$loc_levc is incompatible with file type $ftype"
           ;;
      E|e) ( [ "$ftype" = "GP" ] || [ "$ftype" = "XP" ] || [ "$ftype" = "GZ" ] ) &&\
             bail "selvar: loc_levc=$loc_levc is incompatible with file type $ftype"
           ;;
      P|p) [ "$ftype" != "SS" -a "$ftype" != "GS" -a "$ftype" != "TD" -a "$ftype" != "GP" -a "$ftype" != "XP" -a "$ftype" != "CC" ] &&\
             bail "selvar: loc_levc=$loc_levc is incompatible with file type $ftype"

           if [ "$ftype" = "SS" -o "$ftype" = "GS" -o "$ftype" = "TD" ]; then

             [ -z "$curr_plid" ] && bail "selvar: curr_plid is not defined"
             curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
             [ -z "$curr_coord" ] && bail "curr_coord is not defined"
             curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`

             # If not already gridded convert SPEC records to GRID records
             loc_KIND=`ggstat $loc_vname|\
               awk -v fld="$loc_vname" '{if($4==fld){printf "%s",$2;exit}}' -`
             [ "$loc_KIND" = "SPEC" ] && spec2grid $loc_vname

             # If not already on disk, extract LNSP and convert to gaussian grid
             if [ ! -s LNSP_grid ]; then
               # Look for LNSP in SSFILE in case ftype is GS
               [ ! -s SSFILE ] && bail "selvar: SSFILE is required for LNSP"
               # rip_var SSFILE LNSP_spec LNSP
               cp SSFILE/LNSP LNSP_spec
               # Convert to GRID
               cp LNSP_spec LNSP_grid
               spec2grid LNSP_grid
             fi

             # Create a file that contains gridded LNSP values appropriate for
             # interpolation to pressure levels of the current variable
             rm -f TMP_LNSP_grid
             if [ "$ftype" = "GS" -o "$ftype" = "TD" ]; then
               # When ftype is GS or TD extract a subset of LNSP_grid records that
               # correspond to the times in $loc_vname

               # Read a unique sorted list of kount values from file $loc_vname
               kount_list=`ggstat $loc_vname| awk -v var="$loc_vname" \
                             '{if($4==var)printf "%d\n",$3}' -|sort -bun`

               # Extract these kount values from LNSP_grid
               for curr_kount in $kount_list; do
                 curr_kount=`echo $curr_kount|awk '{printf "%10d",$1}' -`
                 rm -f XXX
                 found_kount=0
                 echo "SELECT     STEP$curr_kount$curr_kount    1     -999999999 NAME LNSP" |\
                   ccc select LNSP_grid XXX && found_kount=1
#                 ccc select LNSP_grid XXX <<EOF && found_kount=1
#SELECT     STEP$curr_kount$curr_kount    1     -999999999 NAME LNSP
#EOF
                 if [ $found_kount -eq 1 ]; then
                   # Append LNSP at curr_kount to TMP_LNSP_grid
                   rm -f YYY
                   joinup YYY TMP_LNSP_grid XXX
                   mv YYY TMP_LNSP_grid
                 elif [ "$ftype" = "GS" ]; then
                   # If this is a GS file then substitue ln(PSA) for LNSP, if PSA
                   # exists in the gs file
                   echo "${loc_vname}: LNSP is missing for kount=$curr_kount"
                   echo "  Attempting to use ln(PSA) from GS file"
                   rm -f XXX
                   echo "SELECT     STEP$curr_kount$curr_kount    1     -999999999 NAME  PSA" |\
                     ccc select GSFILE XXX
#                   ccc select GSFILE XXX <<EOF
#SELECT     STEP$curr_kount$curr_kount    1     -999999999 NAME  PSA
#EOF
                   rm -f YYY
                   loge XXX YYY
                   echo "    NEWNAM LNSP" > INREC
                   ccc newnam YYY XXX input=INREC
                   joinup YYY TMP_LNSP_grid XXX
                   mv YYY TMP_LNSP_grid
                   echo "  Using PSA found in GS file. This will result in ERRORS."
                 else
                   bail "${loc_vname}: No surface pressure found for kount=$curr_kount"
                 fi
               done
             else
               # When ftype is SS simply use LNSP_grid as is
               cp LNSP_grid TMP_LNSP_grid
             fi

             # Create a list of pressure levels to use with gsapl.
             # If the user has specfied levels explicitly then use those levels
             # otherwise use the standard set of pressure levels.
             if [ -n "$loc_levv" ]; then
               # loc_levv is a comma separated list of user supplied levels
               PLEVELS="$loc_levv"
             else
               # Use the levels defined in the variable DEFAULT_PLEVELS
               PLEVELS="$DEFAULT_PLEVELS"
             fi
             # Count the number of levels
             npl=`echo $PLEVELS|awk -F',' '{printf "%5d",NF}' -`
             echo "npl=$npl"
             # Reformat for use with gsapl
             PLEVELS=`echo $PLEVELS|\
               awk -F',' '{for (i=1; i<=NF; i++) {
                           printf "%5d",$i
                           if (i%16 == 0) {printf "\n"}}}' -`

             # Convert gridded fields from eta levels to pressure levels
             rm -f XXX
             echo "GSAPL     $npl        0.        0.$curr_coord$curr_plid
$PLEVELS
 "           | ccc gsapl $loc_vname TMP_LNSP_grid XXX

             have_these_plevs=1
             mv XXX $loc_vname
           fi
           ;;
        *) bail "selvar: loc_levc=$loc_levc is invalid." ;;
      esac
    fi

    ############################################################################
    # If only certain levels are requested then extract these levels from
    # the file named loc_vname.
    ############################################################################
    if [ -n "$loc_levv" -a $have_these_plevs -eq 0 ]; then
      # loc_levv is a comma separated list of levels (ibuf4 values)
      loc_tmp=${loc_vname}$$
      rm -f $loc_tmp
      touch $loc_tmp
      for lev in `echo $loc_levv|sed 's/,/ /g'`; do
        lev1=`echo $lev|awk '{printf "%5d",$1}' -`
        lev2=$lev1
        rm -f XXX
        echo "SELECT     STEP         1 999999999    1     $lev1$lev2 NAME $loc_vfmt" |\
          ccc select $loc_vname XXX
#        ccc select $loc_vname XXX <<EOF
#SELECT     STEP         1 999999999    1     $lev1$lev2 NAME $loc_vfmt
#EOF
        rm -f YYY
        joinup YYY $loc_tmp XXX
        mv YYY $loc_tmp
      done
      mv $loc_tmp $loc_vname
    fi
  fi  # if [ -s "$loc_vname" ] ...end of level selection
}

# ----------------------------------
# Create a variable from a definition supplied by the user.
# This definition will either be created by an internally generated script
# or will be determined by a user supplied script.
parse_vardef(){
  # usage: parse_vardef fname vname vardef

  # Either create a variable file from a variable definition of the form
  # ...a string containing a linear combination of terms of the form
  #   a1 (op) a2 [(op) a3 ...]
  # where a1,a2,... are either constants or variable names
  # and (op) is either multiply '*' or divide '/'
  # Note that this precludes using variable names containing '*' or '/'
  # in the variable definition.
  # This variable definition must contain at least 1 variable name.
  #
  # or invoke a user supplied script to create the variable file.

  [ -z "$1" ] && bail "parse_vardef: Missing file name"
  [ -z "$2" ] && bail "parse_vardef: Missing variable name"
  [ -z "$3" ] && bail "parse_vardef: Missing variable definition"

  # turn off filename globbing (because vardef may contain '*')
  set -f
  loc_fname=`echo $1|sed 's/^x//'`
  vardef_vname=`echo $2|sed 's/^x//'`
  loc_vardef=`echo $3|sed 's/^x//'`
  [ -z "$loc_fname"  ] && bail "parse_vardef: Empty file name"
  [ -z "$vardef_vname"  ] && bail "parse_vardef: Empty variable name"
  [ -z "$loc_vardef" ] && bail "parse_vardef: Empty variable definition"
  echo "fname=$loc_fname   vname=$vardef_vname   vardef=$loc_vardef"

  # Check for an internal or user supplied script to be used
  # to define the variable
  def_script=`echo "$loc_vardef"|\
    awk '{if(match($0,/^script_/)>0 || match($0,/^def_/)>0){
            s=substr($0,RSTART+RLENGTH)
            if (length(s)>0) {printf "%s",s}
            else             {printf "NULL_STRING"} }}' -`
  if [ -n "$def_script" ]; then
    # The user has supplied the name of a file containing shell script
    # that will create a CCCma format file with the desired variable.
    # The file created must be named NEWVAR and contain a single variable.

    [ "$def_script" = "NULL_STRING" ] &&\
      bail "Missing variable definition script name"

    echo "def_script=$def_script"

    # determine a full pathname for this script
    CWD=`pwd`
    def_script_path=''
    internal_script=0
    if [ -s "$def_script" ]; then
      # def_script is a full pathname or relative to CWD
      def_script_path=$def_script
      # If the first character is not '/' then it is relative to CWD
      ch1=`echo $def_script|sed 's/^[ ]*\(.\).*/\1/'`
      [ x"$ch1" != x'/' ] && def_script_path=$CWD/$def_script
    elif [ -s "$LOCAL_BIN/$def_script" ]; then
      # def_script is in LOCAL_BIN
      def_script_path=$LOCAL_BIN/$def_script
      # Identify this as an internal script
      internal_script=1
    else
      # Look for def_script in the invoking users path
      for p in `echo $PATH|sed 's/:/ /g'`; do
        if [ -s ${p}/$def_script ]; then
          def_script_path=${p}/$def_script
          break
        fi
      done
    fi
    [ -z "$def_script_path" ] &&\
      bail "The variable definition file --> $def_script <-- is missing or empty"
    echo "def_script_path=$def_script_path"

    if [ $internal_script -eq 1 ]; then
      # Source this script in the current dir
      # It should create a file named NEWVAR contining a single variable
      . $def_script_path
    else
      # Create a temporary working directory and execute the user script there
      TWD="tmp_user_vdef_$$"
      mkdir $TWD
      cd $TWD

      # Create links for any GS, SS, TD, GP, XP or GZ files that are
      # present in CWD. The user script may then select variables from
      # any of these files.
      [ -s $CWD/GSFILE ] && ln -s $CWD/GSFILE GSFILE
      [ -s $CWD/SSFILE ] && ln -s $CWD/SSFILE SSFILE
      [ -s $CWD/TDFILE ] && ln -s $CWD/TDFILE TDFILE
      [ -s $CWD/GPFILE ] && ln -s $CWD/GPFILE GPFILE
      [ -s $CWD/XPFILE ] && ln -s $CWD/XPFILE XPFILE
      [ -s $CWD/GZFILE ] && ln -s $CWD/GZFILE GZFILE

      # Execute the user supplied script.
      # Note this is executed as a sub shell rather than being "sourced"
      # in the current shell. This means, among other things, that any
      # variables set in the script do not pollute this script.
      chmod u+x $def_script_path
      $def_script_path

      # Check for the user created file and copy it back to the CWD
      if [ ! -s NEWVAR ]; then
        cd $CWD
        rm -fr $TWD
        bail "User vardef script --> $def_script <-- failed to create file NEWVAR"
      fi

      # Move this file back to the CWD
      mv NEWVAR $CWD/NEWVAR

      # Get back to the CWD and clean up the temporary directory
      cd $CWD
      rm -fr $TWD
    fi

    # Ensure that the ibuf3 value is set to the variable name
    varfmt=`echo $vname|awk '{printf "%4.4s",$1}' -`
    rm -f $vname
    echo "    NEWNAM $varfmt" > INREC
    ccc newnam NEWVAR $vname input=INREC
    [ ! -s "$vname" ] && bail "Unable to rename $vname in NEWVAR"

  else

    # The user has supplied a variable definition.
    # This variable definition is a string containing a linear
    # combination of terms of the form
    #   a1 (op) a2 [(op) a3 ...]
    # where a1,a2,... are either constants or variable names
    # and (op) is either multiply '*' or divide '/'
    # Note that this precludes using variable names containing '*' or '/'
    # in the variable definition.
    # This variable definition must contain at least 1 variable name.

    # Write a shell script, that will create the variable defined
    # by this vardef, to the file create_vname.
    PARSE_VLIST=`$LOCAL_BIN/make_vardef out="$vardef_vname" "$vardef"`
    echo "${vardef_vname}: PARSE_VLIST=$PARSE_VLIST"
    [ -s DBG_vardef ] && cat DBG_vardef

    # Extract the required variables from the input file
    for reqvar in $PARSE_VLIST; do
      # First ensure that no files with these names exist
      echo "rm -f $reqvar"
      [ -n "$reqvar" ] && rm -f $reqvar
    done
    for reqvar in $PARSE_VLIST; do
      if [ -n "$reqvar" -a ! -s $reqvar ]; then
        # If this file does not already exist, create it
        selvar $loc_fname $reqvar x$lev_vals x$lev_coord x$tselpar
        KINDi=`ggstat $reqvar|\
                awk -v vname="$reqvar" '{if($4==vname){printf "%s",$2;exit}}' -`
        if [ "x$KINDi" = "xSPEC" -a $TS_sp2gg -eq 1 ]; then
          # Convert SPEC records to GRID records
          spec2grid $reqvar
        fi
      fi
    done

    echo "${vardef_vname}: Creating new field from definition vardef=$vardef"

    # Create the output file vardef_vname by executing the script written above
    # Prepend a shebang line and make executable
    setopts='-ae'
    [ $TS_verbose -gt 0 ] && setopts='-aex'
#    cat > TTT << EOF
##!/bin/sh
#set $setopts
#EOF
    echo '#!/bin/sh'     > TTT
    echo "set $setopts" >> TTT
    cat TTT create_vname > create_vname_x
    chmod u+x create_vname_x
    rm -f TTT
    echo "Script used to create ${vardef_vname}:"
    cat create_vname_x
    echo "Executing create_vname_x..."
    ./create_vname_x

  fi
  # turn on filename globbing
  set +f
}

# ----------------------------------
# Extract a variable from a file and define certain shell variables
# that provide information about this variable
getvar(){
  # usage: getvar file_name var_name

  # Extract a variable from a given file
  # Create auxillary ascii files containing kount values and levels
  # Set various shell variables
  [ -z "$1" ] && bail "getvar: Missing file or variable name"
  [ -z "$2" ] && bail "getvar: Missing variable name"
  getvar_fname=$1
  vname=$2
  echo "getvar:  fname=$getvar_fname  vname=$vname  vardef=$vardef"

  if [ -n "$vardef" ]; then
    # Create a new variable from a user supplied definition
    parse_vardef "x$getvar_fname" "x$vname" "x$vardef"
  else
    # Simply extract this variable from the file
    selvar $getvar_fname $vname x$lev_vals x$lev_coord x$tselpar
  fi

  [ ! -s "$vname" -a $empty_ok -eq 0 ] && bail "$vname is missing or empty"

  if [ -s "$vname" ]; then

    # Determine the KIND (ibuf1) value for this variable
    KIND=`ggstat $vname|\
            awk -v fld="$vname" '{if($4==fld){printf "%s",$2;exit}}' -`

    echo "KIND=$KIND"

    if [ "$KIND" = "SPEC" -a $TS_sp2gg -eq 1 ]; then
      # Convert SPEC records to GRID records
      echo "spec2grid $vname"
      spec2grid $vname
    fi

    # Read kount values from file $vname
    # The file ${vname}_kount will contain a <NEWLINE> separated list of
    # kount values, one kount value per record found in the file $vname
    rm -f ${vname}_kount
    ggstat ${vname}|\
      awk -v var="$vname" '{if($4==var)printf "%d\n",$3}' - > ${vname}_kount

    # Determine the total number of unique kount values for this field
    ntime=`cat ${vname}_kount|sort -bun|wc -l|sed 's/ //g'`
    [ -z "$ntime" ] && bail "getvar: ntime is undefined"
    [ $ntime -le 0 ] && bail "getvar: ntime=$ntime is out of range"
    echo "${vname}: ntime=$ntime"

    # Determine the min and max kount values for this field
    kount=`head -1 ${vname}_kount`
    min_kount=`awk -v kx="$kount" \
                 'BEGIN {k=kx}
                  {if ($1<k) {k=$1}}
                  END {printf "%d",k}' ${vname}_kount`
    max_kount=`awk -v kx="$kount" \
                 'BEGIN {k=kx}
                  {if ($1>k) {k=$1}}
                  END {printf "%d",k}' ${vname}_kount`

    # Determine the save interval for this field
    # This calculation assumes that the save interval is constant
    # between every save in the input file
    del_kount=`echo $min_kount $max_kount $ntime|\
                 awk '{if ($3>1) {delk=($2-$1)/($3-1)}
                       else {delk=0}
                       printf "%d",delk}' -`
    echo "${vname}: del_kount=$del_kount"

    # Determine the average kount value for this field
    # Set the default mid_kount to the arithmetic average
    # of min_kount and max_kount
    mid_kount=`echo $min_kount $max_kount|\
                 awk '{k=0.5*($1+$2);printf "%d",k}' -`
    if [ $max_kount -eq $curr_kedoy ]; then
      # Assume that the saves are at the end of a time step
      # Set mid_kount to the average of (min_kount minus
      # one save interval) and max_kount
      mid_kount=`echo $min_kount $max_kount $del_kount|\
                   awk '{k=0.5*($1+$2-$3);printf "%d",k}' -`
    fi
    echo "${vname}: min_kount=$min_kount  mid_kount=$mid_kount  max_kount=$max_kount"

    # Define a kount range for this variable
    range=`echo $min_kount $max_kount|awk '{printf "%10d%10d",$1,$2}' -`

    # Read levels from the file $vname
    # The file ${vname}_level will contain a <NEWLINE> separated list of
    # levels, one level per record found in the file $vname
    rm -f ${vname}_level
    ggstat ${vname}|\
      awk -v var="$vname" '{if($4==var)printf "%d\n",$5}' - > ${vname}_level

    # Determine the number of unique levels for this field
    nlev=`cat ${vname}_level|sort -bu|wc -l|sed 's/ //g'`
    echo "${vname}: nlev=$nlev"

    # Determine the number of lon (ibuf5) and lat (ibuf6) values on the grid
    ibuf5=`ggstat $vname|\
              awk -v fld="$vname" '{if($4==fld){printf "%d",$6;exit}}' -`
    ibuf6=`ggstat $vname|\
              awk -v fld="$vname" '{if($4==fld){printf "%d",$7;exit}}' -`

    # Determine the middle lat and lon grid index
    midlon=`ggstat $vname|\
              awk -v fld="$vname" \
                '{if($4==fld){
                  m=0.5*$6; if(m<1){m=1}
                  printf "%d",m;exit}}' -`
    midlat=`ggstat $vname|\
              awk -v fld="$vname" \
                '{if($4==fld){
                  m=0.5*$7; if(m<1){m=1}
                  printf "%d",m;exit}}' -`

  fi   # if [ -s "$vname" ]  ...the output file is empty
}

# ----------------------------------
# Create a file containing a pair of TIME records
make_TIME(){
  # usage: make_TIME file_in file_kount file_out

  # Create a file containing a pair of TIME records, one for the
  # time coordinates (kount values) and one for the function values
  # associated with each time coordinate.

  # It is assumed that:
  #   1) file_in contains a single value per record
  #   2) file_kount contains a <NEWLINE> separated list of kount
  #      values, one kount value per record found in file_in.
  #   3) The kount values in file_kount are in ascending order.
  #
  # file_out is overwritten if it exists

  [ -z "$1" ] && bail "make_TIME: Missing input file name"
  [ -z "$2" ] && bail "make_TIME: Missing kount file name"
  [ -z "$3" ] && bail "make_TIME: Missing output file name"
  file_in=$1
  file_kount=$2
  file_out=$3

  # write time series values from file_in to a single TIME record
  echo 'GGATIM        1    1    0    0    0' > INREC
  rm -f file_ts
  ccc ggatim $file_in file_ts input=INREC

  # Change the packing density to 1 for all TIME records
  echo 'REPACK        1' > INREC
  rm -f XXX
  ccc repack file_ts XXX input=INREC
  mv XXX file_ts

  # Determine the total number of unique kount values for this field
  ntime=`cat $file_kount|sort -bun|wc -l|sed 's/ //g'`
  [ -z "$ntime" ] && bail "make_TIME: ntime is undefined"
  [ $ntime -le 0 ] && bail "make_TIME: ntime=$ntime is out of range"

  # Determine the min and max kount values for this field
  kount=`head -1 $file_kount`
  min_kount=`awk -v kx="$kount" \
               'BEGIN {k=kx}
                {if ($1<k) {k=$1}}
                END {printf "%d",k}' $file_kount`
  max_kount=`awk -v kx="$kount" \
               'BEGIN {k=kx}
                {if ($1>k) {k=$1}}
                END {printf "%d",k}' $file_kount`

  # Determine the save interval for this field
  # This calculation assumes that the save interval is constant
  # between every save in the input file
  del_kount=`echo $min_kount $max_kount $ntime|\
               awk '{if ($3>1) {delk=($2-$1)/($3-1)}
                     else {delk=0}
                     printf "%d",delk}' -`

  # set the ibuf2 value to the minimum kount value
  ib2=$min_kount;   ib2=`echo $ib2|awk '{printf "%10d",$1}' -`
  # set the ibuf7 value to the save interval
  ib7=$del_kount;   ib7=`echo $ib7|awk '{printf "%5d",$1}' -`
  rm -f XXX
  echo "    RELABL TIME
           TIME$ib2                    $ib7" | ccc relabl file_ts XXX
  mv XXX file_ts

  # Create an ascii file containing a label and kount values
  # to be converted to CCCma format with chabin

  # Write the ascii label for a TIME record containing kount values
  ib2=$ib2;       ib2=`echo $ib2|awk '{printf "%10d",$1}' -`
  ib4=1;          ib4=`echo $ib4|awk '{printf "%10d",$1}' -`
  ib5=`cat $file_kount|sort -bu|wc -l|sed 's/ //g'`
  ib5=`echo $ib5|awk '{printf "%10d",$1}' -`
  ib6=1;          ib6=`echo $ib6|awk '{printf "%10d",$1}' -`
  ib7=$del_kount; ib7=`echo $ib7|awk '{printf "%10d",$1}' -`
  ib8=-1;         ib8=`echo $ib8|awk '{printf "%10d",$1}' -`
  echo " TIME${ib2} STEP${ib4}${ib5}${ib6}${ib7}${ib8}" > IBUF

  # Create a data record containing kount values in 1P6E22.15 format
  rm -f DATA
  fmt_data_rec $file_kount DATA

  # Combine IBUF and DATA and convert the ascii file to CCCma format
  cat IBUF DATA > XXX
  chabin XXX file_time ||\
    (cat XXX; bail "ERROR in chabin XXX file_time")
  rm -f IBUF DATA XXX

  rm -f $file_out
  joinup $file_out file_time file_ts
  rm -f file_time file_ts

}

# ----------------------------------
# Extract parmsub variable definitions from a PARM record
pardef(){
  # usage: pardef PARM_txt var1 [var2 ...]

  # Extract particular variable definitions from the PARM record

  # The first arg is the name of a text file containing the parmsub section.
  # The second arg is a list of variable names (it must be quoted on invocation).
  [ -z "$1" ] && bail "pardef: Missing PARM_txt file name."
  [ -z "$2" ] && bail "pardef: Missing variable name."
  loc_PARM_txt=$1
  shift
  loc_env_list="$*"
  [ -z "$loc_env_list" ] && bail "pardef: loc_env_list is missing"

  [ ! -s $loc_PARM_txt ] && bail "pardef: $loc_PARM_txt is missing or empty"

  rm -f parmsub1 parmsub.x

  # Remove any CPP_I definition that may appear in the parmsub section
  addr1='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/'
  addr2='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/'
  eval sed \'${addr1},${addr2}d\' $loc_PARM_txt > parmsub.x

  # Remove comments and blank lines
  # Remove any lines that source an external script via ". extscript"
  # Remove " +PARM" line inserted by bin2txt, if any
  sed 's/#.*$//; /^ *[+]/d; /^ *\. .*/d; /^ *$/d' parmsub.x > parmsub1

  # Prepend a shebang line
  echo '#!/bin/sh' | cat - parmsub1 > parmsub.x

  # Ensure the resulting script ends with a blank line
  echo 'echo " "' | cat parmsub.x - > parmsub1

  # Make sure parmsub1 and parmsub.x are equivalent and executable
  cat parmsub1 > parmsub.x
  chmod u+x parmsub.x parmsub1

  [ ! -s parmsub.x ] && bail "pardef: Unable to create parmsub.x"

  echo " "
  for loc_var in $loc_env_list; do

    # Determine parameter values from the parmsub record
    echo 'echo "$'${loc_var}'"' | cat parmsub.x - > parmsub1
    loc_val=`/bin/sh ./parmsub1|tail -1`

    # Create a variable of the same name with "_parm" appended
    # This can be used by the invoking script
    eval ${loc_var}_parm=\'$loc_val\'
    [ -n "$loc_val" ] &&\
      echo "Found in $loc_PARM_txt :: $loc_var = $loc_val"
  done
  rm -f parmsub1 parmsub.x
}

# ----------------------------------
# Determine kount values for start, middle and end of the current month
month_kount(){
  # usage: month_kount delt year start_doy end_day

  # Determine kount values for start, middle and end of the current month.
  # Assume kount=1 corresponds with the time step at the beginning
  # of Jan 1 of year 1.

  # arg 1: delt      = time step in seconds
  # arg 2: year      = the current year
  # arg 3: start_doy = day of year at start of month
  # arg 4: end_doy   = day of year at end of month
  [ -z "$1" ] && bail "month_kount: Missing delt."
  [ -z "$2" ] && bail "month_kount: Missing year."
  [ -z "$3" ] && bail "month_kount: Missing start_doy."
  [ -z "$4" ] && bail "month_kount: Missing end_doy."
  loc_delt=$1
  loc_year=$2
  loc_start_doy=$3
  loc_end_doy=$4

  # Ensure loc_delt is an integer
  loc_delt=`echo $loc_delt|awk '{printf "%d",$1}' -`

  if [ -z "$loc_delt" -o "$loc_delt" = "UNKNOWN" -o $loc_delt -le 0 ]; then
    echo "######### WARNING ##########"
    echo "month_kount: delt is missing"
    echo "######### WARNING ##########"
    ksdoy=0
    kmmd=0
    kedoy=0
  else
    # Number of time steps per day
    loc_kpd=`echo $loc_delt|awk '{p=86400.0/$1;printf "%d",p}' -`

    # kount at start of current month
    ksdoy=`echo $loc_year $loc_start_doy $loc_kpd|\
            awk '{k=1+(365.0*($1-1)+$2-1.0)*$3;printf "%d",k}' -`

    # kount at end of current month
    kedoy=`echo $loc_year $loc_end_doy $loc_kpd|\
            awk '{k=(365.0*($1-1)+$2)*$3;printf "%d",k}' -`

    # kount in the middle of current month
    kmmd=`echo $ksdoy $kedoy|awk '{k=0.5*($1+$2);printf "%d",k}' -`

    echo " "
  fi
  echo "delt=$loc_delt  current month kount: start=$ksdoy  middle=$kmmd  end=$kedoy"
}

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Set defualts for user supplied variables
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

#   Define a list of file types according to the file name suffix
    ftype_list="gs ss gp xp td gz cm cp ie cc"

#   ----------------------------------
#   Set a debugging flag
    TS_verbose=${TS_verbose:=0}
    [ "$TS_verbose" = 'on' ]  && eval TS_verbose\=1
    [ "$TS_verbose" = 'off' ] && eval TS_verbose\=0
    [ "$TS_verbose" = 'yes' ] && eval TS_verbose\=1
    [ "$TS_verbose" = 'no' ]  && eval TS_verbose\=0

#   ----------------------------------
#   flabel and model1 will default to the value of model
#   unless they are set by the user
    flabel=${flabel:=$model}
    model1=${model1:=$model}

#   ----------------------------------
#   Set the name of the file containing the output time series.
    [ -z "$flabel" -a -z "$TS_file" ] &&\
      bail "TS_file or flabel or model must be set."
    TS_file=${TS_file:=${flabel}ts}
    [ -z "$TS_file" ] && bail "TS_file must be set."
    echo "Create or append to file $TS_file"

#   ----------------------------------
#   TS_sp2gg flags conversion of spectral fields to gridded fields
    TS_sp2gg=${TS_sp2gg:=1}
    [ "$TS_sp2gg" = 'on' ]  && eval TS_sp2gg\=1
    [ "$TS_sp2gg" = 'off' ] && eval TS_sp2gg\=0
    [ "$TS_sp2gg" = 'yes' ] && eval TS_sp2gg\=1
    [ "$TS_sp2gg" = 'no' ]  && eval TS_sp2gg\=0

#   ----------------------------------
#   TS_clean flags removal of temporary directories that contain
#   "gs", "ss" or "td" split files
    TS_clean=${TS_clean:=0}
    [ "$TS_clean" = 'on' ]  && eval TS_clean\=1
    [ "$TS_clean" = 'off' ] && eval TS_clean\=0
    [ "$TS_clean" = 'yes' ] && eval TS_clean\=1
    [ "$TS_clean" = 'no' ]  && eval TS_clean\=0

#   ----------------------------------
#   TS_file_clobber flags overwrite of output file (TS_file) if it exists
    TS_file_clobber=${TS_file_clobber:=0}
    [ "$TS_file_clobber" = 'on' ]  && eval TS_file_clobber\=1
    [ "$TS_file_clobber" = 'off' ] && eval TS_file_clobber\=0
    [ "$TS_file_clobber" = 'yes' ] && eval TS_file_clobber\=1
    [ "$TS_file_clobber" = 'no' ]  && eval TS_file_clobber\=0

#   ----------------------------------
#   TS_file_split flags writing multiple output files with one variable
#   per output file. Each individual file will be created if it does not
#   exist or appended to if it exists and TS_file_clobber is false
    TS_file_split=${TS_file_split:=1}
    [ "$TS_file_split" = 'on' ]  && eval TS_file_split\=2
    [ "$TS_file_split" = 'off' ] && eval TS_file_split\=0
    [ "$TS_file_split" = 'yes' ] && eval TS_file_split\=2
    [ "$TS_file_split" = 'no' ]  && eval TS_file_split\=0

#   ----------------------------------
#   TS_kinc and TS_koff are the default time step increment and
#   time step offset used to select fields from a file
    TS_kinc=${TS_kinc:=1}
    TS_koff=${TS_koff:=0}

#   ----------------------------------
#   Set the name of decks that the user wishes to run.
#   These decks are simply sourced in the order they appear in the list.
#   This allows users to include customized time series in TS_file.
    TS_usr_decks=${TS_usr_decks:=''}
    if [ -n "$TS_usr_decks" ]; then
      echo "User supplied decks:"
      echo "$TS_usr_decks"
    fi

#   ----------------------------------
#   with_aerosols is found in the standard rtdiag config file. If set true then
#   aerosol run time diagnostics will be included. The variables required
#   to calculate these time series are only available in recent versions
#   of the model, hence the conditional inclusion in the rtdiag config file.
#   Aerosol run time diagnostics will be included by default.
#   The user must explicitly prohibit inclusion by setting with_aerosols=off (or 0)
    with_aerosols=${with_aerosols:=1}
    [ "$with_aerosols" = 'on' ]  && eval with_aerosols\=1
    [ "$with_aerosols" = 'off' ] && eval with_aerosols\=0
    [ "$with_aerosols" = 'yes' ] && eval with_aerosols\=1
    [ "$with_aerosols" = 'no' ]  && eval with_aerosols\=0

#   ----------------------------------
#   with_ocn is found in the standard rtdiag config file. If set true then
#   ocean related run time diagnostics will be included. The variables required
#   to calculate these time series are only available in the GZ file, hence
#   the conditional inclusion in the rtdiag config file.
#   Ocean run time diagnostics will be included by default.
#   The user must explicitly prohibit inclusion by setting with_ocn=off (or 0)
    with_ocn=${with_ocn:=1}
    [ "$with_ocn" = 'on' ]  && eval with_ocn\=1
    [ "$with_ocn" = 'off' ] && eval with_ocn\=0
    [ "$with_ocn" = 'yes' ] && eval with_ocn\=1
    [ "$with_ocn" = 'no' ]  && eval with_ocn\=0

#   ----------------------------------
#   Define a set of default pressure levels to use when pressure levels are
#   requested and specific levels have not been requested and/or the input file
#   does not already contain pressure levels (ie: neither a GP nor an XP file).
#   The user may override this either by setting TS_plev, adding a definition
#   to the config file (as a comma separated list of coded pressure levels) or
#   by specifying variables of the form pNN (NN=01,02,..99) in the environment.
    DEFAULT_PLEVELS="10,20,30,50,70,100,150,200,250,300,400,500,600,700,850,925,1000"
    TS_plevs=${TS_plevs:=''}
    DEFAULT_PLEVELS=${TS_plevs:=$DEFAULT_PLEVELS}

#   If either TS_date_offset or TS_date_format are set then all non TIME records
#   will have their ibuf2 values converted to TS_date_format (YYYYMMDDHH by default).
#   TS_date_offset must be in the same format (e.g. 1850010100)
    if [ -n "$TS_date_offset" ]; then
      date_offset="$TS_date_offset"
    elif [ -n "$TS_date_format" ]; then
      date_offset="0"
    else
      date_offset=''
    fi

#   Define file type specific TS_date_offset and TS_date_format values
#   These will be found in variables with names like TS_date_offset_gs or TS_date_format_gs
    for file_type in $ftype_list; do
      eval TS_date_offset_${file_type}=\$\{TS_date_offset_${file_type}:=\$date_offset\}
      eval TS_date_format_${file_type}=\$\{TS_date_format_${file_type}:=\$TS_date_format\}
    done

#   run_start_year, run_start_month, run_stop_year and run_stop_month
#   may be used in output file names below
    run_start_year=${run_start_year:=1}
    run_start_year=${TS_run_start_year:=$run_start_year}
    run_start_month=${run_start_month:=1}
    run_start_month=${TS_run_start_month:=$run_start_month}
    run_stop_year=${run_stop_year:=1}
    run_stop_year=${TS_run_stop_year:=$run_stop_year}
    run_stop_month=${run_stop_month:=12}
    run_stop_month=${TS_run_stop_month:=$run_stop_month}

    # format these variables appropriately
    xxx=`echo $run_start_year|awk '{printf "%04d",$1}' -`
    eval run_start_year\=$xxx
    xxx=`echo $run_start_month|awk '{printf "%02d",$1}' -`
    eval run_start_month\=$xxx
    xxx=`echo $run_stop_year|awk '{printf "%04d",$1}' -`
    eval run_stop_year\=$xxx
    xxx=`echo $run_stop_month|awk '{printf "%02d",$1}' -`
    eval run_stop_month\=$xxx

#   ymrange will be a string containing a year/month range based on
#   the values of the run_start/stop_year/month variables
    valid_ymrange=1
    [ -z "$run_start_year" ]  && valid_ymrange=0
    [ -z "$run_start_month" ] && valid_ymrange=0
    [ -z "$run_stop_year" ]   && valid_ymrange=0
    [ -z "$run_stop_month" ]  && valid_ymrange=0
    if [ $valid_ymrange -eq 1 ]; then
      # ymrange is a string of the form YYYmMM_YYYmMM
      ymrange=${ymrange:="${run_start_year}m${run_start_month}_${run_stop_year}m$run_stop_month"}
    else
      # ymrange is set to notify the user that there was a problem
      ymrange=${ymrange:='ymrange_not_set'}
    fi

#   yrange will be a string containing a year range based on
#   the values of the run_start/stop_year variables
    valid_yrange=1
    [ -z "$run_start_year" ]  && valid_yrange=0
    [ -z "$run_stop_year" ]   && valid_yrange=0
    if [ $valid_yrange -eq 1 ]; then
      # yrange is a string of the form YYY_YYY
      yrange=${yrange:="${run_start_year}_${run_stop_year}"}
    else
      # yrange is set to notify the user that there was a problem
      yrange=${yrange:='yrange_not_set'}
    fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Either create a config file locally in tsconfig or
# copy a user supplied config file to tsconfig
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    TS_config=${TS_config:=''}
    if [ -z "$TS_config" ]; then
      # The user has not provided a config file
      # Create a config file using info in TS_gs, TS_ss, etc.
      rm -f tsconfig
      touch tsconfig
      vtag=0
      head="# Tag : File Type : Var Name : Var Def : Spatial Avg ( |g|z) : Sampling :"
      head="$head Temporal Avg ( |m) : Coded Levels : Level Coord (e|p|d) : Window"
      echo "$head" >> tsconfig
      for fld in $TS_gs; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:GS:$fld" >> tsconfig
      done
      for fld in $TS_ss; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:SS:$fld" >> tsconfig
      done
      for fld in $TS_gp; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:GP:$fld" >> tsconfig
      done
      nn=0
      while [ $nn -lt 100 ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        eval fld=\$TS_gp$nn
        [ -z "$fld" ] && break
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:GP:$fld" >> tsconfig
      done
      for fld in $TS_xp; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:XP:$fld" >> tsconfig
      done
      nn=0
      while [ $nn -lt 100 ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        eval fld=\$TS_xp$nn
        [ -z "$fld" ] && break
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:XP:$fld" >> tsconfig
      done
      for fld in $TS_cp; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:CP:$fld" >> tsconfig
      done
      for fld in $TS_td; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:TD:$fld" >> tsconfig
      done
      for fld in $TS_gz; do
        vtag=`echo $vtag|awk '{printf "%3.3d",$1+1}' -`
        echo "${vtag}:GZ:$fld" >> tsconfig
      done
    else
      # The user has supplied a config file
      if [ -s $TS_config ]; then
        # This file exists and is not empty  ...get a local copy
        cp $TS_config tsconfig
      elif [ -s "`ls -1 $DATAPATH/${TS_config}.??? 2>/dev/null|tail -1`" ]; then
        # The config file is on DATAPATH/RUNPATH
        $ACCESS tsconfig $TS_config
      else
        bail "TS_config=$TS_config is missing"
      fi
    fi

    echo " "
    echo "Time series configuration file:"
    cat tsconfig

#   ----------------------------------
#   Check for a set of default pressure levels in the config file.
#   This must be a comma separated list of coded pressure levels on a single line
#   that begins with the string "# Default Pressure Levels:"
    cfg_plevs=`awk -F':' '
      $1 ~ /^[ \t]*#[ \t]*Default[ \t]*Pressure[ \t]*Levels[ \t]*$/ \
      {sub(/^[^:]*:/,"",$0); gsub(/[ \t]*/,"",$0); printf "%s",$0}' tsconfig`
    DEFAULT_PLEVELS=${cfg_plevs:-$DEFAULT_PLEVELS}

#   ----------------------------------
#   Check for a set of pressure levels defined in the current environment.
#   If variables of the form p01,p02,... exist in the env and are non null
#   then use the values of these variables, up to the first non null value,
#   as the default set of pressure levels.
#   These variables are typically found in a standard diagnostic deck.
    eval env_plev\=\$p01
    env_plev=`echo $env_plev|sed 's/ //g'`
    if [ -n "$env_plev" ]; then
      # There is at least one pressure level defined
      # Reset the default
      DEFAULT_PLEVELS=$env_plev
      nn=1
      while [ $nn -lt 1000 ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        eval env_plev\=\$p$nn
        env_plev=`echo $env_plev|sed 's/ //g'`
        [ -z "$env_plev" ] && break
        DEFAULT_PLEVELS="${DEFAULT_PLEVELS},$env_plev"
      done
    fi

    echo "DEFAULT_PLEVELS=$DEFAULT_PLEVELS"

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Determine which files are required
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    # fields_exits is used to determine if any files are required
    fields_exits=0

    # Allow the user to force a file to be required by setting
    # any of the following variables
    TS_require_gs=${TS_require_gs:=0}
    [ "$TS_require_gs" = 'on'  ] && eval TS_require_gs\=1
    [ "$TS_require_gs" = 'off' ] && eval TS_require_gs\=0
    [ "$TS_require_gs" = 'yes' ] && eval TS_require_gs\=1
    [ "$TS_require_gs" = 'no'  ] && eval TS_require_gs\=0
    TS_require_ss=${TS_require_ss:=0}
    [ "$TS_require_ss" = 'on' ]  && eval TS_require_ss\=1
    [ "$TS_require_ss" = 'off' ] && eval TS_require_ss\=0
    [ "$TS_require_ss" = 'yes' ] && eval TS_require_ss\=1
    [ "$TS_require_ss" = 'no' ]  && eval TS_require_ss\=0
    TS_require_gp=${TS_require_gp:=0}
    [ "$TS_require_gp" = 'on' ]  && eval TS_require_gp\=1
    [ "$TS_require_gp" = 'off' ] && eval TS_require_gp\=0
    [ "$TS_require_gp" = 'yes' ] && eval TS_require_gp\=1
    [ "$TS_require_gp" = 'no' ]  && eval TS_require_gp\=0
    TS_require_xp=${TS_require_xp:=0}
    [ "$TS_require_xp" = 'on' ]  && eval TS_require_xp\=1
    [ "$TS_require_xp" = 'off' ] && eval TS_require_xp\=0
    [ "$TS_require_xp" = 'yes' ] && eval TS_require_xp\=1
    [ "$TS_require_xp" = 'no' ]  && eval TS_require_xp\=0
    TS_require_cp=${TS_require_cp:=0}
    [ "$TS_require_cp" = 'on' ]  && eval TS_require_cp\=1
    [ "$TS_require_cp" = 'off' ] && eval TS_require_cp\=0
    [ "$TS_require_cp" = 'yes' ] && eval TS_require_cp\=1
    [ "$TS_require_cp" = 'no' ]  && eval TS_require_cp\=0
    TS_require_cm=${TS_require_cm:=0}
    [ "$TS_require_cm" = 'on' ]  && eval TS_require_cm\=1
    [ "$TS_require_cm" = 'off' ] && eval TS_require_cm\=0
    [ "$TS_require_cm" = 'yes' ] && eval TS_require_cm\=1
    [ "$TS_require_cm" = 'no' ]  && eval TS_require_cm\=0
    TS_require_td=${TS_require_td:=0}
    [ "$TS_require_td" = 'on' ]  && eval TS_require_td\=1
    [ "$TS_require_td" = 'off' ] && eval TS_require_td\=0
    [ "$TS_require_td" = 'yes' ] && eval TS_require_td\=1
    [ "$TS_require_td" = 'no' ]  && eval TS_require_td\=0
    TS_require_gz=${TS_require_gz:=0}
    [ "$TS_require_gz" = 'on' ]  && eval TS_require_gz\=1
    [ "$TS_require_gz" = 'off' ] && eval TS_require_gz\=0
    [ "$TS_require_gz" = 'yes' ] && eval TS_require_gz\=1
    [ "$TS_require_gz" = 'no' ]  && eval TS_require_gz\=0
    TS_require_ie=${TS_require_ie:=0}
    [ "$TS_require_ie" = 'on' ]  && eval TS_require_ie\=1
    [ "$TS_require_ie" = 'off' ] && eval TS_require_ie\=0
    [ "$TS_require_ie" = 'yes' ] && eval TS_require_ie\=1
    [ "$TS_require_ie" = 'no' ]  && eval TS_require_ie\=0
    TS_require_cc=${TS_require_cc:=0}
    [ "$TS_require_cc" = 'on' ]  && eval TS_require_cc\=1
    [ "$TS_require_cc" = 'off' ] && eval TS_require_cc\=0
    [ "$TS_require_cc" = 'yes' ] && eval TS_require_cc\=1
    [ "$TS_require_cc" = 'no' ]  && eval TS_require_cc\=0

    # The require_XX variables are used to determine if a file with the
    # extension XX is to be read.
    require_gs=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Gg][Ss] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_gs -eq 1 ] && require_gs=1
    [ $require_gs    -eq 1 ] && fields_exits=1
    require_ss=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Ss][Ss] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_ss -eq 1 ] && require_ss=1
    [ $require_ss    -eq 1 ] && fields_exits=1
    require_gp=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Gg][Pp] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_gp -eq 1 ] && require_gp=1
    [ $require_gp    -eq 1 ] && fields_exits=1
    require_xp=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Xx][Pp] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_xp -eq 1 ] && require_xp=1
    [ $require_xp    -eq 1 ] && fields_exits=1
    require_cm=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Cc][Mm] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_cm -eq 1 ] && require_cm=1
    [ $require_cm    -eq 1 ] && fields_exits=1
    require_cp=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Cc][Pp] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_cp -eq 1 ] && require_cp=1
    [ $require_cp    -eq 1 ] && fields_exits=1
    require_td=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Tt][Dd] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_td -eq 1 ] && require_td=1
    [ $require_td    -eq 1 ] && fields_exits=1
    require_gz=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Gg][Zz] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_gz -eq 1 ] && require_gz=1
    [ $require_gz    -eq 1 ] && fields_exits=1
    require_ie=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Ii][Ee] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_ie -eq 1 ] && require_ie=1
    [ $require_ie    -eq 1 ] && fields_exits=1
    require_cc=`awk 'BEGIN                  {req=0}
                     /^ *#/                 {next }
                     /^[^:]*: *[Cc][Cc] *:/ {req=1}
                     END {printf "%d",req}' tsconfig`
    [ $TS_require_cc -eq 1 ] && require_cc=1
    [ $require_cc    -eq 1 ] && fields_exits=1

#   ----------------------------------
#   If no time series were specified by the user then exit
    if [ $fields_exits -eq 0 ]; then
      echo "tseries: No fields were specified by the user"
      exit 1
    fi
    echo " "

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Ensure certain variables required to determine kount values at the
# start, middle and end of each month are available
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    [ -z "$year" ] && bail "year is not defined"
    [ -z "$mon"  ] && bail "mon is not defined"

    # number of days since Jan 1 to the start of each month
    sdoy01=1;   sdoy02=32;  sdoy03=60;  sdoy04=91;  sdoy05=121; sdoy06=152
    sdoy07=182; sdoy08=213; sdoy09=244; sdoy10=274; sdoy11=305; sdoy12=335

    # number of days since Jan 1 to the end of each month
    edoy01=31;  edoy02=59;  edoy03=90;  edoy04=120; edoy05=151; edoy06=181
    edoy07=212; edoy08=243; edoy09=273; edoy10=304; edoy11=334; edoy12=365

    eval sdoy=\$sdoy$mon  # first day of current month
    [ -z "$sdoy" ] && bail "Unable to determine first day of month $mon"
    eval edoy=\$edoy$mon  # last day of current month
    [ -z "$edoy" ] && bail "Unable to determine last day of month $mon"

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Access history and/or diagnostic files and process the PARM record
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

#   ----------------------------------
#   Access the gs file. This will only be used when TS_gs is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_gs -eq 1 ]; then
      TS_gsfile=${TS_gsfile:=${model1}gs}
      [ -z "$TS_gsfile" -o "$TS_gsfile" = "gs" ] &&\
        bail "Neither TS_gsfile nor model1 nor model are defined."
      # Check for the existence of a split dir that may have been created
      # by a diagnostic deck
      diag_spltdir=${splitdir:-"$RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npakgg"}
      if [ -d "$diag_spltdir" ]; then
        # If this deck is running as part of a diagnostic string and the GS
        # file has already been split into component files then simply create
        # a link to the diag split directory
        ln -s $diag_spltdir GSFILE
      else
        # Otherwise create a dir containg the split files
        mkdir GSFILE
        cd GSFILE
        $ACCESS gsfile$$ $TS_gsfile
        spltall gsfile$$
        $RELEASE gsfile$$
        cd ..
      fi
      echo "Using GS file $TS_gsfile"
    fi

#   ----------------------------------
#   Access the ss file. This will only be used when TS_ss is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_ss -eq 1 ]; then
      TS_ssfile=${TS_ssfile:=${model1}ss}
      [ -z "$TS_ssfile" -o "$TS_ssfile" = "ss" ] &&\
        bail "Neither TS_ssfile nor model1 nor model are defined."
      diag_spltdir=${splitdir:-"$RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaksp"}
      if [ -d "$diag_spltdir" ]; then
        # If this deck is running as part of a diagnostic string and the SS
        # file has already been split into component files then simply create
        # a link to the diag split directory
        ln -s $diag_spltdir SSFILE
      else
        # Otherwise create a dir containg the split files
        mkdir SSFILE
        cd SSFILE
        $ACCESS ssfile$$ $TS_ssfile
        spltall ssfile$$
        $RELEASE ssfile$$
        cd ..
      fi
      echo "Using SS file $TS_ssfile"

      # Extract LNSP and convert to gaussian grid creating files named
      # LNSP_spec and LNSP_grid. These files may be used to convert model
      # level fields to pressure levels etc.
      # rip_var SSFILE LNSP_spec LNSP
      cp SSFILE/LNSP LNSP_spec
      cp LNSP_spec LNSP_grid
      spec2grid LNSP_grid
    fi

#   ----------------------------------
#   Access the gp file. This will only be used when TS_gp[NN] is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_gp -eq 1 ]; then
      TS_gpfile=${TS_gpfile:=${flabel}gp}
      [ -z "$TS_gpfile" -o "$TS_gpfile" = "gp" ] &&\
        bail "Neither TS_gpfile nor flabel nor model are defined."
      $ACCESS GPFILE $TS_gpfile
      echo "Using GP file $TS_gpfile"
    fi

#   ----------------------------------
#   Access the xp file. This will only be used when TS_xp[NN] is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_xp -eq 1 ]; then
      TS_xpfile=${TS_xpfile:=${flabel}xp}
      [ -z "$TS_xpfile" -o "$TS_xpfile" = "xp" ] &&\
        bail "Neither TS_xpfile nor flabel nor model are defined."
      $ACCESS XPFILE $TS_xpfile
      echo "Using XP file $TS_xpfile"
    fi

#   ----------------------------------
#   Access the cm file, if required
    if [ $require_cm -eq 1 ]; then
      TS_cmfile=${TS_cmfile:=${model1}cm}
      [ -z "$TS_cmfile" -o "$TS_cmfile" = "cm" ] &&\
        bail "Neither TS_cmfile nor model1 nor model are defined."
      $ACCESS CMFILE $TS_cmfile
      echo "Using CM file $TS_cmfile"
    fi

#   ----------------------------------
#   Access the cp file, if required
    if [ $require_cp -eq 1 ]; then
      TS_cpfile=${TS_cpfile:=${flabel}cp}
      [ -z "$TS_cpfile" -o "$TS_cpfile" = "cp" ] &&\
        bail "Neither TS_cpfile nor flabel nor model are defined."
      $ACCESS CPFILE $TS_cpfile
      echo "Using CP file $TS_cpfile"
    fi

#   ----------------------------------
#   Access the td file. This will only be used when TS_td is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_td -eq 1 ]; then
      TS_tdfile=${TS_tdfile:=${model1}td}
      [ -z "$TS_tdfile" -o "$TS_tdfile" = "td" ] &&\
        bail "Neither TS_tdfile nor model1 nor model are defined."
      diag_spltdir=${splitdir:-"$RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaktd"}
      if [ -d "$diag_spltdir" ]; then
        # If this deck is running as part of a diagnostic string and the TD
        # file has already been split into component files then simply create
        # a link to the diag split directory
        ln -s $diag_spltdir TDFILE
      else
        # Otherwise create a dir containg the split files
        $ACCESS TDFILE $TS_tdfile
      fi
      echo "Using TD file $TS_tdfile"
    fi

#   ----------------------------------
#   Access the gz file. This will only be used when TS_gz is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_gz -eq 1 ]; then
      TS_gzfile=${TS_gzfile:=${model1}gz}
      [ -z "$TS_gzfile" -o "$TS_gzfile" = "gz" ] &&\
        bail "Neither TS_gzfile nor model1 nor model are defined."
      $ACCESS GZFILE $TS_gzfile
      echo "Using GZ file $TS_gzfile"

      # Put the ocean data description in a separate file for use below
      echo "  XFIND.  1   DATA DESCRIPTION" > INREC
      ccc xfind GZFILE ocn_datadesc input=INREC

      # If the GZ file is required then assume that ocean grid information
      # is also required. grido will calculate this info.
      grido GZFILE ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz

      # Extract the top level fields from ocngrid_beta
      echo "RCOPY              1         1" > INREC
      ccc rcopy ocngrid_beta ocngrid_beta1 input=INREC

      # Determine weights suitable for a horizontal global average of ocn data
      mlt ocngrid_beta1 ocngrid_da ocngrid_wts

      # Determine delt for GZ file
      echo "SELECT     STEP         0 999999999    1     -999999999 NAME DTTS" |\
        ccc select GZFILE ocn_DTTS || bail "Cannot select DTTS from $TS_gzfile"
      binach ocn_DTTS ocn_DTTS.txt
      # The first field of line 2 will be the value of delt
      dtts_ocn=`awk '{if(NR==2){printf "%g",$1}}' ocn_DTTS.txt`
      rm -f ocn_DTTS ocn_DTTS.txt

      # 1) Use year and mon to determine the working value of kstep for the current GZ file
      #    Assume KOUNT=0 on Jan 1 of year 1 at 00Z.
      # 2) Then relabel GZ file e.g. relabl GZFILE new_GZ ...
      # 3) Then 'mv new_GZ GZFILE'

    fi

#   ----------------------------------
#   Access the ie file. This will only be used when TS_ie is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_ie -eq 1 ]; then
      TS_iefile=${TS_iefile:=${flabel}ie}
      [ -z "$TS_iefile" -o "$TS_iefile" = "ie" ] &&\
        bail "Neither TS_iefile nor flabel nor model are defined."
      $ACCESS IEFILE $TS_iefile
      echo "Using IE file $TS_iefile"
    fi

#   ----------------------------------
#   Access the cc file. This will only be used when TS_cc is set.
#   It is also possible that a user supplied deck will want this file.
    if [ $require_cc -eq 1 ]; then
      TS_ccfile=${TS_ccfile:=${flabel}cc}
      [ -z "$TS_ccfile" -o "$TS_ccfile" = "cc" ] &&\
        bail "Neither TS_ccfile nor flabel nor model are defined."
      $ACCESS CCFILE $TS_ccfile
      echo "Using CC file $TS_ccfile"
    fi

#   ----------------------------------
#   Obtain local copies of the PARM and PARC records from each file
#   and extract certain parmsub parameter values for each file

    # Define values for specified paramsub parameters in the current environment

    # The variables found in env_list will be defined in the current env if they
    # exist in the parmsub record or they are defined by the user at run time
    env_list="delt plid coord moist sref spow lay"

    # Initialize all file type specific env_list variables (e.g. delt_gs) to the
    # null string unless the user has defined that variable (e.g. delt or delt_gs)
    # at run time. The file type specific variable definition (e.g. delt_gs) will
    # override the generic variable definition (e.g. delt).
    for envar in $env_list; do
      for file_type in $ftype_list; do
        eval var_xx=\$${envar}_${file_type}
        [ -z "$var_xx" ] && eval ${envar}_${file_type}=\$\{${envar}:=\'\'\}
      done
    done

    if [ $require_gs -eq 1 ]; then
      # Get PARM and PARC from the gs file
      rip_var GSFILE GSPARM PARM
      rip_var GSFILE GSPARC PARC

      # Determine values for parmsub parameters found in this file
      [ -s GSPARM ] && bin2txt GSPARM GSPARM_txt >/dev/null
      # It is not an error for GSPARM_txt to be missing or empty
      if [ -s GSPARM_txt ]; then
        # For each variable in $env_list, pardef will add a variable of the
        # form "var_parm" (e.g. delt_parm for variable delt) to the current
        # environment if that variable is found in the parmsub record
        pardef GSPARM_txt "$env_list"
      fi
      # Note: if GSPARM_txt is missing all *_parm vars will all be null
      for envar in $env_list; do
        eval ${envar}_gs=\"\$\{${envar}_gs:=\$${envar}_parm\}\"
      done

      # Determine start, middle and end kount values for current month
      month_kount $delt_gs $year $sdoy $edoy
      ksdoy_gs=$ksdoy
      kedoy_gs=$kedoy
      kmmd_gs=$kmmd
    fi

    if [ $require_ss -eq 1 ]; then
      # Get PARM and PARC from the ss file
      rip_var SSFILE SSPARM PARM
      rip_var SSFILE SSPARC PARC

      # Determine values for parmsub parameters found in this file
      [ -s SSPARM ] && bin2txt SSPARM SSPARM_txt >/dev/null
      # It is not an error for SSPARM_txt to be missing or empty
      if [ -s SSPARM_txt ]; then
        # For each variable in $env_list, pardef will add a variable of the
        # form "var_parm" (e.g. delt_parm for variable delt) to the current
        # environment if that variable is found in the parmsub record
        pardef SSPARM_txt "$env_list"
      fi
      # Note: if SSPARM_txt is missing all *_parm vars will all be null
      for envar in $env_list; do
        eval ${envar}_ss=\"\$\{${envar}_ss:=\$${envar}_parm\}\"
      done

      # Determine start, middle and end kount values for current month
      month_kount $delt_ss $year $sdoy $edoy
      ksdoy_ss=$ksdoy
      kedoy_ss=$kedoy
      kmmd_ss=$kmmd
    fi

    if [ $require_gp -eq 1 ]; then
      # Get PARM and PARC from the gp file
      rip_var GPFILE GPPARM PARM
      rip_var GPFILE GPPARC PARC

      # Determine values for parmsub parameters found in this file
      [ -s GPPARM ] && bin2txt GPPARM GPPARM_txt >/dev/null
      # It is not an error for GPPARM_txt to be missing or empty
      if [ -s GPPARM_txt ]; then
        # For each variable in $env_list, pardef will add a variable of the
        # form "var_parm" (e.g. delt_parm for variable delt) to the current
        # environment if that variable is found in the parmsub record
        pardef GPPARM_txt "$env_list"
      fi
      # Note: if GPPARM_txt is missing all *_parm vars will all be null
      for envar in $env_list; do
        eval ${envar}_gp=\"\$\{${envar}_gp:=\$${envar}_parm\}\"
      done

      # Determine start, middle and end kount values for current month
      month_kount $delt_gp $year $sdoy $edoy
      ksdoy_gp=$ksdoy
      kedoy_gp=$kedoy
      kmmd_gp=$kmmd
    fi

    if [ $require_xp -eq 1 ]; then
      # Get PARM and PARC from the xp file
      rip_var XPFILE XPPARM PARM
      rip_var XPFILE XPPARC PARC

      # Determine values for parmsub parameters found in this file
      [ -s XPPARM ] && bin2txt XPPARM XPPARM_txt >/dev/null
      # It is not an error for XPPARM_txt to be missing or empty
      if [ -s XPPARM_txt ]; then
        # For each variable in $env_list, pardef will add a variable of the
        # form "var_parm" (e.g. delt_parm for variable delt) to the current
        # environment if that variable is found in the parmsub record
        pardef XPPARM_txt "$env_list"
      fi
      # Note: if XPPARM_txt is missing all *_parm vars will all be null
      for envar in $env_list; do
        eval ${envar}_xp=\"\$\{${envar}_xp:=\$${envar}_parm\}\"
      done

      # Determine start, middle and end kount values for current month
      month_kount $delt_xp $year $sdoy $edoy
      ksdoy_xp=$ksdoy
      kedoy_xp=$kedoy
      kmmd_xp=$kmmd
    fi

    if [ $require_gz -eq 1 ]; then
      # Determine ocean time step delt_gz using the value of DTTS found
      # in the GZ file
      delt_gz=${delt_gz:=$dtts_ocn}
      echo "dtts_ocn=$dtts_ocn   delt_gz=$delt_gz"

      # Determine start, middle and end kount values for current month
      month_kount $delt_gz $year $sdoy $edoy
      ksdoy_gz=$ksdoy
      kedoy_gz=$kedoy
      kmmd_gz=$kmmd
    fi

    # Set default values for file types that do not contain parmsub records

    # Initialize all defults to the null string
    for envar in $env_list; do
      eval ${envar}_def=\'\'
    done

    # If any of gs, ss, gp, or xp file types are used then use the values
    # associated with one of these files types as default values.
    # If a gs file exists use values from it first,
    # then from ss, gp and xp in that order.
    if [ $require_gs -eq 1 ]; then
      for envar in $env_list; do
        eval ${envar}_def=\"\$\{${envar}:=\$${envar}_gs\}\"
      done
    elif [ $require_ss -eq 1 ]; then
      for envar in $env_list; do
        eval ${envar}_def=\"\$\{${envar}:=\$${envar}_ss\}\"
      done
    elif [ $require_gp -eq 1 ]; then
      for envar in $env_list; do
        eval ${envar}_def=\"\$\{${envar}:=\$${envar}_gp\}\"
      done
    elif [ $require_xp -eq 1 ]; then
      for envar in $env_list; do
        eval ${envar}_def=\"\$\{${envar}:=\$${envar}_xp\}\"
      done
    fi

    # Set variables for td, gz, cm, cp, ie and cc file types to default values
    # if any of these file types are used.
    for file_type in td gz cm cp ie cc; do
      eval in_use=\$require_$file_type
      [ $in_use -ne 1 ] && continue
      for envar in $env_list; do
        eval ${envar}_${file_type}=\"\$\{${envar}_${file_type}:=\$${envar}_def\}\"
      done
      eval delt_set=\$delt_$file_type
      if [ -z "$delt_set" ]; then
        # require delt to be set for every file type
        eval delt_${file_type}=UNKNOWN
        echo "The user has requested variables from the $file_type file but delt is unknown"
        bail "Set delt and try again"
      else
        # Determine start, middle and end kount values for current month
        month_kount $delt_set $year $sdoy $edoy
        eval ksdoy_${file_type}=$ksdoy
        eval kedoy_${file_type}=$kedoy
        eval kmmd_${file_type}=$kmmd
      fi
    done

    # Print out all variables for all file types in use
    for file_type in $ftype_list; do
      eval in_use=\$require_$file_type
      [ $in_use -ne 1 ] && continue
      for envar in $env_list; do
        eval echo ${envar}_${file_type}=\$${envar}_${file_type}
      done
    done

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Extract user requested fields from history/diagnostic files
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

#   ----------------------------------
#   Ensure TSFILE exists and has zero size
    rm -f TSFILE
    touch TSFILE

    # newsets will be the number of new superlabeld sets that will
    # be created in the following loop
    newsets=0

    # NEW_FILE_LIST and NEW_FILE_LIST_SPLIT will contain a lists of file names
    # for each of the new superlabeled sets created in the following loop
    # Files in NEW_FILE_LIST will be concatenated and saved in a single file
    # Files in NEW_FILE_LIST_SPLIT will be saved as individual files
    rm -f NEW_FILE_LIST
    touch NEW_FILE_LIST
    rm -f NEW_FILE_LIST_SPLIT
    touch NEW_FILE_LIST_SPLIT

    echo " "
    echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
    echo "# Extract fields"
    echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
    echo " "

    line=0
    while [ $line -lt 9000 ]; do
      # The lines must be read from the config file using awk (as below),
      # rather than via something like "cat tsfile|while read line" because
      # the "pipe to loop" results in unpredicable behaviour with a large
      # loop body on some *nxes (buffer overflow???).
      line=`expr $line + 1`
      config_line=`awk -v line="$line" '
        {if (NR==line) {printf "%s",$0; exit}}
        END {if (line>NR) {printf "END_OF_FILE"}}' tsconfig`
      [ -z "$config_line" ] && continue
      [ "$config_line" = "END_OF_FILE" ] && break

      # If the first non whitespace character is '#' then ignore this line
      ch1=`echo $config_line|sed 's/^[ ]*\(.\).*/\1/'`
      [ x"$ch1" = x'#' ] && continue

      echo " "
      echo "#######   Processing $config_line   #######"

      # Strip any conditional include flag              ...field #1
      # This is case sensitive with leading whitespace removed
      condition=`echo $config_line|awk -F':' '{sub(/^[ \t]*/,"",$1);printf "%s",$1}' -`

      # Test the condition and include or do not include this variable
      # according to the outcome of this test.
      if [ -n "$condition" ]; then
        # Extract the first space separated word, this will be a keyword
        # Currently, valid keywords are ifdef and ifndef
        kwd=`echo $condition|awk '{printf "%s",$1}' -`

        # Determine any expression remaining after the leading keyword is removed
        # Contiguous white space will collapse to a single blank in condex.
        condex=`echo $condition|awk '{for (i=2;i<=NF;i++) {printf "%s ",$i}}' -`

        if [ -n "$condex" ]; then
          case $kwd in
            ifdef)
              # Extract the first space separated word from this expression
              # Only 1 token is recognized, the rest of the line is ignored
              ctoken=`echo $condex|awk '{printf "%s",$1}' -`
              if [ -n "$ctoken" ]; then
                # check for the existence of a variable with this name
                eval tokenval=\$$ctoken
                # Check truth of the token value
                # A false token value is either null, "off" or "0"
                if [ -z "$tokenval" -o \
                     x"$tokenval" = 'xoff' -o \
                     x"$tokenval" = 'x0' ]; then
                  # false
                  echo "DO NOT INCLUDE   $config_line"
                  echo "  $ctoken = $tokenval is false or not defined"
                  continue
                else
                  echo "INCLUDE   $config_line"
                fi
              fi
              ;;
            ifndef)
              # Extract the first space separated word from this expression
              # Only 1 token is recognized, the rest of the line is ignored
              ctoken=`echo $condex|awk '{printf "%s",$1}' -`
              if [ -n "$ctoken" ]; then
                # check for the existence of a variable with this name
                eval tokenval=\$$ctoken
                # Check truth of the token value
                # A false token value is either null, "off" or "0"
                if [ -z "$tokenval" -o \
                     x"$tokenval" = 'xoff' -o \
                     x"$tokenval" = 'x0' ]; then
                  # false
                  echo "INCLUDE   $config_line"
                else
                  # true
                  echo "DO NOT INCLUDE   $config_line"
                  echo "  $ctoken = $tokenval is true"
                  continue
                fi
              fi
              ;;
            *)
              echo "Invalid keyword $kwd found in field 1: $condition  ...ignored" ;;
          esac
        fi
      fi

      # Strip file type                                 ...field #2
      # This is case insensitive with whitespace removed
      ftype=`echo $config_line|\
        awk -F':' '{gsub(/[ \t]*/,"",$2);printf "%s",$2}' -|tr '[a-z]' '[A-Z]'`

      # Strip the variable name                         ...field #3
      # This is case sensitive and preserves internal whitespace (leading and
      # trailing whitespace is removed).
      # vname will be either a variable name (ibuf3 value) or a superlabel.
      vname=`echo "$config_line"|\
        awk -F':' '{sub(/^[ \t]*/,"",$3);sub(/[ \t]*$/,"",$3);printf "%s",$3}' -`

      # Strip field definition info                     ...field #4
      # This is case sensitive and preserves internal whitespace (leading and
      # trailing whitespace is removed).
      # This allows superlabels to be used in a variable definition
      vardef=`echo "$config_line"|\
        awk -F':' '{sub(/^[ \t]*/,"",$4);sub(/[ \t]*$/,"",$4);printf "%s",$4}' -`

      # Strip spatial averaging options ( |g|z)         ...field #5
      # This is case insensitive with whitespace removed
      savgpar=`echo $config_line|\
        awk -F':' '{gsub(/[ \t]*/,"",$5); printf "%s",$5}' -|\
        tr '[a-z]' '[A-Z]'`
      if [ -n "$savgpar" ]; then
        savgpar_ok=`echo $savgpar|sed -n '/^[GgZz]$/p'`
        [ -z "$savgpar_ok" ] && \
          bail "spatial average specification -->${savgpar}<-- must be one of G, g, Z or z"
      fi

      # Strip time sampling parameters                  ...field #6
      # This is case sensitive with whitespace removed
      tselpar=`echo $config_line|\
        awk -F':' '{gsub(/[ \t]*/,"",$6); printf "%s",$6}' -`
      if [ -n "$tselpar" ]; then
        # When this is not a file name ensure it is all upper case
        tselpar_not_fname=`echo $tselpar|sed -n '/^ *[0-9,]/p'`
        [ -n "$tselpar_not_fname" ] && tselpar=`echo $tselpar|tr '[a-z]' '[A-Z]'`
      fi

      # Strip temporal averaging options ( |d|m|a)      ...field #7
      # This is case insensitive with whitespace removed
      tavgpar=`echo $config_line|\
        awk -F':' '{gsub(/[ \t]*/,"",$7); printf "%s",$7}' -|\
        tr '[a-z]' '[A-Z]'`
      if [ -n "$tavgpar" ]; then
        tavgpar_ok=`echo $tavgpar|sed -n '/^[DdMmAa]$/p'`
        [ -z "$tavgpar_ok" ] && \
          bail "temporal average specification -->${tavgpar}<-- must be one of d, m or a"
      fi

      # Strip level selection parameters (ibuf4 values) ...field #8
      # This is case insensitive with whitespace removed
      lev_vals=`echo $config_line |\
        awk -F':' '{gsub(/[ \t]*/,"",$8); printf "%s",$8}' -|\
        tr '[a-z]' '[A-Z]'`

      # Strip level coordinate type (E|P|D)             ...field #9
      # This is case insensitive with whitespace removed
      lev_coord=''
      lev_coord=`echo $config_line   |\
        awk -F':' '{gsub(/[ \t]*/,"",$9); printf "%s",$9}' -|\
        tr '[a-z]' '[A-Z]'`

      # Strip windowing parameters                      ...field #10
      # This is case insensitive with whitespace removed
      winpar=`echo $config_line |\
        awk -F':' '{gsub(/[ \t]*/,"",$10); printf "%s",$10}' -|\
        tr '[a-z]' '[A-Z]'`

      # Strip output file name, if any                   ...field #11
      # This is case insensitive with whitespace removed
      raw_outfname=`echo $config_line |\
        awk -F':' '{gsub(/[ \t]*/,"",$11); printf "%s",$11}' -|\
        tr '[A-Z]' '[a-z]'`
      # expand any embedded shell variables in outfname
      eval outfname=$raw_outfname

      # Set fld to the variable name with any whitespace removed
      fld=`echo $vname|sed 's/[ ]//g'`

      # Set avgpar equal to the concatenation of tavgpar and savgpar
      avgpar=$savgpar$tavgpar

      echo "##### ftype=$ftype  vname=$vname  fld=$fld  vardef=$vardef"
      echo "##### savgpar=$savgpar  tselpar=$tselpar  tavgpar=$tavgpar"
      echo "##### lev_vals=$lev_vals  lev_coord=$lev_coord  winpar=$winpar"
      echo "##### outfname=$outfname"

      # Set fname, slab and curr_* variables appropriately for the current field

      # slab is the superlabel to be read from fname when ftype is GP or XP
      slab=''

      # set all curr_* variables to the value associated with the current file type
      # env_list contains: delt, plid, coord, moist, sref, spow
      lc_ftype=`echo $ftype|tr '[A-Z]' '[a-z]'`
      for envar in $env_list; do
        eval curr_${envar}=\"\$${envar}_$lc_ftype\"
      done

      case $ftype in
        GS) fname=GSFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='E'}
            ;;
        SS) fname=SSFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='E'}
            ;;
        GP) # Extract a superlabeled set from the GP file
            slab="$vname"
            fname=SET_FROM_GPFILE
            rm -f $fname
            if [ -z "$vardef" ]; then
              # If there is no variable definition then extract
              # the superlabeled set named in slab
              echo "XFIND         $slab" > INREC
              ccc xfind GPFILE $fname input=INREC
              rm -f INREC

              # Use the variable name in the first record as the field name
              fld=`ggstat $fname|awk '{if(NF>12){printf "%s",$4;exit}}' -`
              fld=`echo $fld|sed 's/ //g'`
            else
              # If this variable is defined in terms of others then extract
              # the component fields from the GP file
              #
              # Create a file named var_slab_map containing a mapping
              # between GP superlabel sets used and set of internally
              # defined ibuf3 type variable names. Redefine vardef in
              # terms of these internally generated ibuf3 names.
              NEW_VARDEF=`$LOCAL_BIN/make_vardef -map "$vardef"`

              echo "${vname}: Resetting vardef to NEW_VARDEF=$NEW_VARDEF"
              vardef="$NEW_VARDEF"

              nvmap=0
              while [ $nvmap -lt 1000 ]; do
                nvmap=`expr $nvmap + 1`
                vmap_line=`awk -v line="$nvmap" '{if (NR==line) {
                             printf "%s",$0; exit}}' var_slab_map `
                [ -z "$vmap_line" ] && break

                # Each line will contain a colon (:) separated pair of strings
                # The first string will be the superlabel found in the GP file and
                # the second string will be internally generated ibuf3 variable name.
                oldslab=`echo "$vmap_line"|awk -F':' '{printf "%s",$1}' -`
                newvname=`echo "$vmap_line"|awk -F':' '{printf "%s",$2}' -`
                if [ -n "$oldslab" -a -n "$newvname" ]; then
                  # Extract this superlabeled set from the GP file
                  echo "XFIND         $oldslab" > INREC
                  rm -f XXX
                  ccc xfind GPFILE XXX input=INREC
                  rm -f INREC
                  # Ensure that the ibuf3 value is set to the new variable name
                  newvfmt=`echo $newvname|awk '{printf "%4.4s",$1}' -`
                  rm -f SSETi
                  echo "    NEWNAM $newvfmt" > INREC
                  ccc newnam XXX SSETi input=INREC
                  rm -f XXX
                  joinup XXX $fname SSETi
                  mv XXX $fname
                else
                  bail "Empty fields in var_slab_map"
                fi
              done

              # Use vname as the variable name
              fld=`echo $vname|sed 's/ //g'`
            fi

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $kmmd_gp|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL 
               $ib2" | ccc relabl $fname XXX
#            ccc relabl $fname XXX <<EOF
#    RELABL 
#               $ib2
#EOF
            mv XXX $fname
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='P'}
            ;;
        XP) # Extract a superlabeled set from the XP file
            slab="$vname"
            fname=SET_FROM_XPFILE
            rm -f $fname
            if [ -z "$vardef" ]; then
              # If there is no variable definition then extract
              # the superlabeled set named in slab
              echo "XFIND         $slab" > INREC
              ccc xfind XPFILE $fname input=INREC
              rm -f INREC

              # Use the variable name in the first record as the field name
              fld=`ggstat $fname|awk '{if(NF>12){printf "%s",$4;exit}}' -`
              fld=`echo $fld|sed 's/ //g'`
            else
              # If this variable is defined in terms of others then extract
              # the component fields from the XP file
              #
              # Create a file containing a mapping between XP superlabel sets used
              # and set of internally defined ibuf3 type variable names.
              # Redefine vardef in terms of these internally generated ibuf3 names.
              NEW_VARDEF=`$LOCAL_BIN/make_vardef -map "$vardef"`

              echo "${vname}: Resetting vardef to NEW_VARDEF=$NEW_VARDEF"
              vardef="$NEW_VARDEF"

              echo "var_slab_map:"
              cat var_slab_map

              nvmap=0
              while [ $nvmap -lt 1000 ]; do
                nvmap=`expr $nvmap + 1`
                vmap_line=`awk -v line="$nvmap" '{if (NR==line) {
                             printf "%s",$0; exit}}' var_slab_map`
                [ -z "$vmap_line" ] && break

                # Each line will contain a colon (:) separated pair of strings
                # The first string will be the superlabel found in the XP file and
                # the second string will be internally generated ibuf3 variable name.
                oldslab=`echo "$vmap_line"|awk -F':' '{printf "%s",$1}' -`
                newvname=`echo "$vmap_line"|awk -F':' '{printf "%s",$2}' -`
                if [ -n "$oldslab" -a -n "$newvname" ]; then
                  # Extract this superlabeled set from the XP file
                  echo "XFIND         $oldslab" > INREC
                  rm -f XXX
                  ccc xfind XPFILE XXX input=INREC
                  rm -f INREC
                  # Ensure that the ibuf3 value is set to the new variable name
                  newvfmt=`echo $newvname|awk '{printf "%4.4s",$1}' -`
                  rm -f SSETi
                  echo "    NEWNAM $newvfmt" > INREC
                  ccc newnam XXX SSETi input=INREC
                  rm -f XXX
                  joinup XXX $fname SSETi
                  mv XXX $fname
                else
                  bail "Empty fields in var_slab_map"
                fi
              done

              # Use vname as the variable name
              fld=`echo $vname|sed 's/ //g'`
            fi

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $kmmd_xp|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL 
               $ib2" | ccc relabl $fname XXX
#            ccc relabl $fname XXX <<EOF
#    RELABL 
#               $ib2
#EOF
            mv XXX $fname
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='P'}
            ;;
        CP) # Extract a superlabeled set from the CP file
            slab="$vname"
            fname=SET_FROM_CPFILE
            rm -f $fname
            if [ -z "$vardef" ]; then
              # If there is no variable definition then extract
              # the superlabeled set named in slab
              echo "XFIND         $slab" > INREC
              ccc xfind CPFILE $fname input=INREC
              rm -f INREC

              # Use the variable name in the first record as the field name
              fld=`ggstat $fname|awk '{if(NF>12){printf "%s",$4;exit}}' -`
              fld=`echo $fld|sed 's/ //g'`
            else
              # If this variable is defined in terms of others then extract
              # the component fields from the CP file
              #
              # Create a file named var_slab_map containing a mapping
              # between CP superlabel sets used and set of internally
              # defined ibuf3 type variable names. Redefine vardef in
              # terms of these internally generated ibuf3 names.
              NEW_VARDEF=`$LOCAL_BIN/make_vardef -map "$vardef"`

              echo "${vname}: Resetting vardef to NEW_VARDEF=$NEW_VARDEF"
              vardef="$NEW_VARDEF"

              nvmap=0
              while [ $nvmap -lt 1000 ]; do
                nvmap=`expr $nvmap + 1`
                vmap_line=`awk -v line="$nvmap" '{if (NR==line) {
                             printf "%s",$0; exit}}' var_slab_map `
                [ -z "$vmap_line" ] && break

                # Each line will contain a colon (:) separated pair of strings
                # The first string will be the superlabel found in the CP file and
                # the second string will be internally generated ibuf3 variable name.
                oldslab=`echo "$vmap_line"|awk -F':' '{printf "%s",$1}' -`
                newvname=`echo "$vmap_line"|awk -F':' '{printf "%s",$2}' -`
                if [ -n "$oldslab" -a -n "$newvname" ]; then
                  # Extract this superlabeled set from the CP file
                  echo "XFIND         $oldslab" > INREC
                  rm -f XXX
                  ccc xfind CPFILE XXX input=INREC
                  rm -f INREC
                  # Ensure that the ibuf3 value is set to the new variable name
                  newvfmt=`echo $newvname|awk '{printf "%4.4s",$1}' -`
                  rm -f SSETi
                  echo "    NEWNAM $newvfmt" > INREC
                  ccc newnam XXX SSETi input=INREC
                  rm -f XXX
                  joinup XXX $fname SSETi
                  mv XXX $fname
                else
                  bail "Empty fields in var_slab_map"
                fi
              done

              # Use vname as the variable name
              fld=`echo $vname|sed 's/ //g'`
            fi

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $kmmd_cp|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL 
               $ib2" | ccc relabl $fname XXX
#            ccc relabl $fname XXX <<EOF
#    RELABL 
#               $ib2
#EOF
            mv XXX $fname
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='D'}
            ;;
        CM) fname=CMFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='E'}
            ;;
        TD) fname=TDFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='E'}
            ;;
        GZ) fname=GZFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='D'}
            ;;
        IE) fname=IEFILE
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='E'}
            ;;
        CC) # Extract a superlabeled set from the CC file
            slab="$vname"
            fname=SET_FROM_CCFILE
            rm -f $fname
            if [ -z "$vardef" ]; then
              # If there is no variable definition then extract
              # the superlabeled set named in slab
              echo "XFIND         $slab" > INREC
              ccc xfind CCFILE $fname input=INREC
              rm -f INREC

              # Use the variable name in the first record as the field name
              fld=`ggstat $fname|awk '{if(NF>12){printf "%s",$4;exit}}' -`
              fld=`echo $fld|sed 's/ //g'`
            else
              # If this variable is defined in terms of others then extract
              # the component fields from the CC file
              #
              # Create a file named var_slab_map containing a mapping
              # between CC superlabel sets used and set of internally
              # defined ibuf3 type variable names. Redefine vardef in
              # terms of these internally generated ibuf3 names.
              NEW_VARDEF=`$LOCAL_BIN/make_vardef -map "$vardef"`

              echo "${vname}: Resetting vardef to NEW_VARDEF=$NEW_VARDEF"
              vardef="$NEW_VARDEF"

              nvmap=0
              while [ $nvmap -lt 1000 ]; do
                nvmap=`expr $nvmap + 1`
                vmap_line=`awk -v line="$nvmap" '{if (NR==line) {
                             printf "%s",$0; exit}}' var_slab_map `
                [ -z "$vmap_line" ] && break

                # Each line will contain a colon (:) separated pair of strings
                # The first string will be the superlabel found in the CC file and
                # the second string will be internally generated ibuf3 variable name.
                oldslab=`echo "$vmap_line"|awk -F':' '{printf "%s",$1}' -`
                newvname=`echo "$vmap_line"|awk -F':' '{printf "%s",$2}' -`
                if [ -n "$oldslab" -a -n "$newvname" ]; then
                  # Extract this superlabeled set from the CC file
                  echo "XFIND         $oldslab" > INREC
                  rm -f XXX
                  ccc xfind CCFILE XXX input=INREC
                  rm -f INREC
                  # Ensure that the ibuf3 value is set to the new variable name
                  newvfmt=`echo $newvname|awk '{printf "%4.4s",$1}' -`
                  rm -f SSETi
                  echo "    NEWNAM $newvfmt" > INREC
                  ccc newnam XXX SSETi input=INREC
                  rm -f XXX
                  joinup XXX $fname SSETi
                  mv XXX $fname
                else
                  bail "Empty fields in var_slab_map"
                fi
              done

              # Use vname as the variable name
              fld=`echo $vname|sed 's/ //g'`
            fi

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $kmmd_cc|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL 
               $ib2" | ccc relabl $fname XXX
#            ccc relabl $fname XXX <<EOF
#    RELABL 
#               $ib2
#EOF
            mv XXX $fname
            # Ensure that lev_coord has a non blank value
            lev_coord=${lev_coord:='P'}
            ;;
         *) bail "Invalid file type $ftype" ;;
      esac
      [ ! -s "$fname" ] && bail "File $fname is missing or empty"

      # Determine start, middle and end kount values for current month
      month_kount $curr_delt $year $sdoy $edoy
      curr_ksdoy=$ksdoy
      curr_kedoy=$kedoy
      curr_kmmd=$kmmd

      # Call getvar to select the field from the input file and
      # create $fld, ${fld}_kount and ${fld}_level as well as
      # certain variables (ntime,nlev,ibuf5,ibuf6,midlat,midlon,
      # min_kount,max_kount,range)

      # derived_internal will flag the existence of an internally derived quantity
      # such as winds (U,V) from the SS file or specific, relative humidity
      # (SHUM,RHUM) from the SS file. This flag is used by selvar to determine
      # whether or not to select the current variable from the input file. If
      # the variable is derived then it will not appear in the input file.
      derived_internal=0

#      if [ "$ftype" = "SS" -a \( "$vname" = "U" -o "$vname" = "V" \) ]; then
#
#        # Convert VORT and DIV found in the SS file to U and V
#        derived_internal=1
#
#        # If not already on disk, extract U,V and convert to gaussian grid
#        if [ ! -s U_grid -o ! -s V_grid ]; then
#
#          # Extract VORT and DIV from the SS file
#          [ ! -s VORT_spec ] && rip_var $fname VORT_spec VORT
#          [ ! -s  DIV_spec ] && rip_var $fname DIV_spec DIV
#
#          # Convert spectral VORT and DIV to spectral (U,V)COS(LAT)/A
#          # Note: cwinds will add an extra row to the U and V spectral fields
#          # to hold derivative info.
#          cwinds VORT_spec DIV_spec U_spec V_spec
#
#          # Put spectral (U,V)COS(LAT)/A onto the gaussian grid and
#          # convert to real winds U and V
#          cp U_spec U_grid
#          cp V_spec V_grid
#          spec2grid U_grid 1
#          spec2grid V_grid 1
#        fi
#        if [ "$vname" = "U" ]; then
#          rm -f U
#          cp U_grid U
#          fld='U'
#        fi
#        if [ "$vname" = "V" ]; then
#          rm -f V
#          cp V_grid V
#          fld='V'
#        fi
#
#        # Call getvar to perform any user requested time sampling, level
#        # interpolation etc on U or V
#        getvar $fname $fld
#
#      if [ "$ftype" = "SS" -a \( "$vname" = "SHUM" -o "$vname" = "RHUM" \) ]; then
#
#        # Convert ES and TEMP found in the SS file to specific humidity (SHUM)
#        # and relative humidity (RHUM)
#        derived_internal=1
#
#        # If not already on disk, extract ES and convert to gaussian grid
#        if [ ! -s ES_grid ]; then
#          [ ! -s ES_spec ] && rip_var $fname ES_spec ES
#          # Convert to GRID
#          cp ES_spec ES_grid
#          spec2grid ES_grid
#        fi
#
#        # If not already on disk, extract TEMP and convert to gaussian grid
#        if [ ! -s TEMP_grid ]; then
#          [ ! -s TEMP_spec ] && rip_var $fname TEMP_spec TEMP
#          # Convert to GRID
#          cp TEMP_spec TEMP_grid
#          spec2grid TEMP_grid
#        fi
#
#        # If not already on disk, extract LNSP and convert to gaussian grid
#        if [ ! -s LNSP_grid ]; then
#          # [ ! -s LNSP_spec ] && rip_var $fname LNSP_spec LNSP
#          [ ! -s LNSP_spec ] && cp SSFILE/LSNP LNSP_spec
#          # Convert to GRID
#          cp LNSP_spec LNSP_grid
#          spec2grid LNSP_grid
#        fi
#
#        if [ ! -s SHUM_grid -o ! -s RHUM_grid ]; then
#          # Convert gridded moisture variable to gridded specific (SHUM_gs) and
#          # relative (RHUM_gs) humidity
#
#          # Format input variables
#          [ -z "$curr_coord" ] && bail "curr_coord is not defined"
#          curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
#          [ -z "$curr_moist" ] && bail "curr_moist is not defined"
#          curr_moist=`echo $curr_moist|awk '{printf "%5s",$1}' -`
#          [ -z "$curr_plid" ] && bail "curr_plid is not defined"
#          curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
#          [ -z "$curr_sref" ] && bail "curr_sref is not defined"
#          curr_sref=`echo $curr_sref|awk '{printf "%10.2E",$1}' -`
#          [ -z "$curr_spow" ] && bail "curr_spow is not defined"
#          curr_spow=`echo $curr_spow|awk '{printf "%10.1f",$1}' -`
#
#          echo "gshumh input card"
#          echo "$curr_coord$curr_moist$curr_plid$curr_sref$curr_spow"
#
#          echo "GSHUMH    $curr_coord$curr_moist$curr_plid$curr_sref$curr_spow" |\
#            ccc gshumh ES_grid TEMP_grid LNSP_grid SHUM_grid RHUM_grid
##          ccc gshumh ES_grid TEMP_grid LNSP_grid SHUM_grid RHUM_grid <<EOF
##GSHUMH    $curr_coord$curr_moist$curr_plid$curr_sref$curr_spow
##EOF
#        fi
#        if [ "$vname" = "SHUM" ]; then
#          rm -f SHUM
#          cp SHUM_grid SHUM
#          fld='SHUM'
#        fi
#        if [ "$vname" = "RHUM" ]; then
#          rm -f RHUM
#          cp RHUM_grid RHUM
#          fld='RHUM'
#        fi
#
#        # Call getvar to perform any user requested time sampling,
#        # level interpolation, etc.
#        getvar $fname $fld
#
#
#      if [ "$ftype" = "SS" -a \( "$vname" = "PHI" -o  "$vname" = "GZ" \) ]; then
#
#        # Derive geopotential, PHI, from fields found in the SS file
#        derived_internal=1
#
#        # If not already on disk, calculate PHI from fields in the ss file
#        if [ ! -s PHI_grid ]; then
#
#          # If not already on disk, extract PHIS and convert to gaussian grid
#          if [ ! -s PHIS_grid ]; then
#            [ ! -s PHIS_spec ] && rip_var $fname PHIS_spec PHIS
#            # Convert to GRID
#            cp PHIS_spec PHIS_grid
#            spec2grid PHIS_grid
#          fi
#
#          # If not already on disk, extract ES and convert to gaussian grid
#          if [ ! -s ES_grid ]; then
#            [ ! -s ES_spec ] && rip_var $fname ES_spec ES
#            # Convert to GRID
#            cp ES_spec ES_grid
#            spec2grid ES_grid
#          fi
#
#          # If not already on disk, extract TEMP and convert to gaussian grid
#          if [ ! -s TEMP_grid ]; then
#            [ ! -s TEMP_spec ] && rip_var $fname TEMP_spec TEMP
#            # Convert to GRID
#            cp TEMP_spec TEMP_grid
#            spec2grid TEMP_grid
#          fi
#
#          # If not already on disk, extract LNSP and convert to gaussian grid
#          if [ ! -s LNSP_grid ]; then
#            # [ ! -s LNSP_spec ] && rip_var $fname LNSP_spec LNSP
#            [ ! -s LNSP_spec ] && cp SSFILE/LNSP LNSP_spec
#            # Convert to GRID
#            cp LNSP_spec LNSP_grid
#            spec2grid LNSP_grid
#          fi
#
#          # Format input variables
#          [ -z "$curr_coord" ] && bail "curr_coord is not defined"
#          curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
#          [ -z "$curr_moist" ] && bail "curr_moist is not defined"
#          curr_moist=`echo $curr_moist|awk '{printf "%5s",$1}' -`
#          [ -z "$curr_plid" ] && bail "curr_plid is not defined"
#          curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
#          [ -z "$curr_sref" ] && bail "curr_sref is not defined"
#          curr_sref=`echo $curr_sref|awk '{printf "%10.2E",$1}' -`
#          [ -z "$curr_spow" ] && bail "curr_spow is not defined"
#          curr_spow=`echo $curr_spow|awk '{printf "%10.1f",$1}' -`
#          [ -z "$curr_lay" ] && bail "curr_lay is not defined"
#          curr_lay=`echo $curr_lay|awk '{printf "%5d",$1}' -`
#
#          if [ ! -s SHUM_grid ]; then
#            # Convert gridded moisture variable to gridded specific (SHUM_gs) and
#            # relative (RHUM_gs) humidity
#
#            echo "GSHUMH    $curr_coord$curr_moist$curr_plid$curr_sref$curr_spow" > INREC
#            echo "gshumh input card:"
#            cat INREC
#
#            #ggstat ES_grid
#            #ggstat TEMP_grid
#            #ggstat LNSP_grid
#
#            ccc gshumh ES_grid TEMP_grid LNSP_grid SHUM_grid RHUM_grid input=INREC
#          fi
#
#          # Convert SHUM to moist gas constant, RGAS
#          echo "XLIN.           176.      287. RGAS" > INREC
#          xlin SHUM_grid RGAS_grid input=INREC
#
#          # Derive PHI on eta levels from TEMP and moist gas constant
#          echo "TAPHI.    $curr_coord$curr_lay$curr_plid" > INREC
#          taphi TEMP_grid PHIS_grid LNSP_grid RGAS_grid PHI_grid input=INREC
#
#        fi
#
#        if [ "$vname" = "PHI" ]; then
#          fld='PHI'
#          rm -f PHI
#          cp PHI_grid PHI
#        elif [ "$vname" = "GZ" ]; then
#          # Convert geopotential to geopotential height (divide by g=9.8106)
#          fld='GZ'
#          rm -f GZ
#          echo "XLIN.       0.101931        0.   GZ" > INREC
#          xlin PHI_grid GZ_grid input=INREC
#          cp GZ_grid GZ
#        else
#          bail "vname must be one of PHI or GZ"
#        fi
#
#        # Call getvar to perform any user requested time sampling,
#        # level interpolation, etc.
#        getvar $fname $fld

      if [ "$ftype" = "SS" -a \
           \( "$vname" = "TEMP" -o  "$vname" = "PHI" -o  "$vname" = "GZ" \) ]; then

        if [ "$vname" = "TEMP" ]; then
          vardef=def_temp
          fld='TEMP'
        elif [ "$vname" = "PHI" ]; then
          vardef=def_phi
          fld='PHI'
        elif [ "$vname" = "GZ" ]; then
          # Convert geopotential to geopotential height (divide by g=9.8106)
          vardef=def_gz
          fld='GZ'
        else
          bail "vname must be one of TEMP, PHI or GZ"
        fi

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc.
        getvar $fname $fld

      elif [ "$ftype" = "SS" -a "$vname" = "OMEG" ]; then

        # Use LNSP, TEMP, VORT and DIV to generate vertical motion (omega=dp/dt)
        # from spectral fields on eta levels to gridded omega (newtons/m**2/sec)
        # at mid layer positions.
        derived_internal=1

        # Create only the spectral files that do not already exist
        [ ! -s LNSP_spec ] && rip_var $fname LNSP_spec LNSP
        [ ! -s TEMP_spec ] && rip_var $fname TEMP_spec TEMP
        [ ! -s VORT_spec ] && rip_var $fname VORT_spec VORT
        [ ! -s  DIV_spec ] && rip_var $fname DIV_spec DIV

        # Format input variables
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_lay" ] && bail "curr_lay is not defined"
        curr_lay=`echo $curr_lay|awk '{printf "%5d",$1}' -`

        # Determine spectral truncation parameter (ibuf7) from the spectral file,
        # which can in turn be used to determine the grid size (nlon,nlat)
        # parameters that are input to gsomgah
        LRLMT=`ggstat LNSP_spec|awk '{if($2=="SPEC"){printf "%d",$8;exit}}' -`
        [ -z "$LRLMT" ] && bail "Unable to determine LRLMT"
        [ $LRLMT -le 0 ] && bail "LRLMT=$LRLMT is out of range"
        if [ $LRLMT -lt 100000 ]; then
          LR=`echo $LRLMT|awk '{n=int($1/1000);printf "%d",n}' -`
        else
          LR=`echo $LRLMT|awk '{n=int($1/10000);printf "%d",n}' -`
        fi
        nlon=`echo $LR|awk '{printf "%5d",2*$1}' -`
        nlat=`echo $LR|awk '{printf "%5d",$1}' -`

        # Note: gsomgah is hard coded to insert ibuf3=OMEG in the output file
        echo "GSOMGAH   $nlon$nlat    1$curr_coord$curr_plid$curr_lay" |\
          ccc gsomgah LNSP_spec TEMP_spec VORT_spec DIV_spec OMEG
#        ccc gsomgah LNSP_spec TEMP_spec VORT_spec DIV_spec OMEG <<EOF
#GSOMGAH   $nlon$nlat    1$curr_coord$curr_plid$curr_lay
#EOF

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc.
        fld='OMEG'
        getvar $fname $fld

      elif [ "$ftype" = "SS" -a "$vname" = "PMSL" ]; then

        # Use LNSP, TEMP and PHIS to determine mean sea level pressure
        # from spectral fields on eta levels to gridded pmsl
        derived_internal=1

        # If not already on disk, extract PHIS, LNSP and TEMP
        # from the ss file and convert to a gaussian grid
        if [ ! -s PHIS_grid ]; then
          [ ! -s PHIS_spec ] && rip_var $fname PHIS_spec PHIS
          # Convert to GRID
          cp PHIS_spec PHIS_grid
          spec2grid PHIS_grid
        fi
        if [ ! -s LNSP_grid ]; then
          [ ! -s LNSP_spec ] && rip_var $fname LNSP_spec LNSP
          # Convert to GRID
          cp LNSP_spec LNSP_grid
          spec2grid LNSP_grid
        fi
        if [ ! -s TEMP_grid ]; then
          [ ! -s TEMP_spec ] && rip_var $fname TEMP_spec TEMP
          # Convert to GRID
          cp TEMP_spec TEMP_grid
          spec2grid TEMP_grid
        fi

        # Format input variables
        [ -z "$curr_coord" ] && bail "curr_coord is not defined"
        curr_coord=`echo $curr_coord|awk '{printf "%5s",$1}' -`
        [ -z "$curr_plid" ] && bail "curr_plid is not defined"
        curr_plid=`echo $curr_plid|awk '{printf "%10.4E",$1}' -`
        [ -z "$curr_lay" ] && bail "curr_lay is not defined"
        curr_lay=`echo $curr_lay|awk '{printf "%5d",$1}' -`

        # Use gsmslph to determine mslp
        # Note: A lapse rate of 0.0065 and temp at lowest model level is used
        echo "   GSMSLPH     .0065    0$curr_lay$curr_coord$curr_plid" > INREC
        gsmslph TEMP_grid LNSP_grid PHIS_grid PMSL input=INREC

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc.
        fld='PMSL'
        getvar $fname $fld

      elif [ "$ftype" = "GS" -a "$vname" = "SICV" ]; then

        # Create total ice volume from SIC found in GS file
        derived_internal=1

        # Extract SIC from the GS file
        rip_var $fname SIC SIC

        # Split SIC into 2 files containing NH and SH grids
        gghems SIC XXXn XXXs

        if [ "$winpar" = "NH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from NH grid
          echo "GGLOB         1" > INREC
          gglob XXXn XSIC input=INREC
        elif [ "$winpar" = "SH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from SH grid
          echo "GGLOB         1" > INREC
          gglob XXXs XSIC input=INREC
        else
          bail "Must specify either NH or SH for winpar while using $vname"
        fi

        # Multiply by 2.7721E+11 to convert to total volume
        # !!!CHECK!!! does 2.7721E+11 = 2*pi*Re**2*0.0011
        echo "XLIN      2.7721E+11       0.0 SICV" > INREC
        ccc xlin XSIC SICV input=INREC
        rm -f SIC XXXn XXXs XSIC

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc
        fld='SICV'
        getvar $fname $fld

      elif [ "$ftype" = "GS" -a "$vname" = "SICA" ]; then

        # Create sea ice area from SIC found in GS file
        derived_internal=1

        # Extract SIC from the GS file
        rip_var $fname SIC SIC

        # Split SIC into 2 files containing NH and SH grids
        gghems SIC XXXn XXXs

        if [ "$winpar" = "NH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from NH grid
          echo "GGLOB         1" > INREC
          gglob XXXn XSIC input=INREC
        elif [ "$winpar" = "SH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from SH grid
          echo "GGLOB         1" > INREC
          gglob XXXs XSIC input=INREC
        else
          bail "Must specify either NH or SH for winpar while using $vname"
        fi

        # Create a mask in SICmsk that is 1 where SIC > 100 and 0 elsewhere
        echo "FMASK             -1 NEXT   GT      100." > INREC
        ccc fmask XSIC SICmsk input=INREC

        # Multiply this by 2.5503E+14 to convert to total area
        echo "XLIN      2.5503E+14       0.0 SICA" > INREC
        ccc xlin SICmsk SICA input=INREC

        rm -f SIC XXXn XXXs XSIC SICmx

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc
        fld='SICA'
        getvar $fname $fld

      elif [ "$ftype" = "GS" -a "$vname" = "SICM" ]; then

        # Create sea ice maximum from SIC found in GS file
        derived_internal=1

        # Extract SIC from the GS file
        rip_var $fname SIC SIC

        # Split SIC into 2 files containing NH and SH grids
        gghems SIC XXXn XXXs

        if [ "$winpar" = "NH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from NH grid
          echo "GGLOB         1" > INREC
          gglob XXXn XSIC input=INREC
        elif [ "$winpar" = "SH" ]; then
          # Create a file containing a global grid
          # (symmetric about EQ) from SH grid
          echo "GGLOB         1" > INREC
          gglob XXXs XSIC input=INREC
        else
          bail "Must specify either NH or SH for winpar while using $vname"
        fi

        # Set every grid point in SICmx to the maximum value found in XSIC
        rmax XSIC SICmx

        # Multiply this maximum value by 0.0011
        echo "XLIN          0.0011       0.0 SICM" > INREC
        ccc xlin SICmx SICM input=INREC

        rm -f SIC XXXn XXXs XSIC SICmx

        # Call getvar to perform any user requested time sampling,
        # level interpolation, etc
        fld='SICM'
        getvar $fname $fld

      else

        # Call getvar without any preprocessing for special cases
        getvar $fname $fld

      fi

      # If fld is empty then simply continue
      # This can happen when e.g. time sampling does not find any valid records
      # in the current file. Appropriate checking will be done in getvar.
      [ ! -s "$fld" ] && continue

      # lev_coord is the level coordinate type. It should be one of 'E'
      # for eta (ie model) levels, 'P' for pressure levels or 'D' for ocean depth.
      xxx=`echo $lev_coord|sed 's/^[EePpDd]//'`
      [ -n "$xxx" ] && bail "Invalid level cooordinate type $lev_coord"

      # Define a label to be used as the superlabel for this set

      # avgpar contains averaging options (G,Z,D,M,A...)
      #   G ...global average   (spatial)
      #   Z ...zonal average    (spatial)
      #   D ...daily average    (temporal)
      #   M ...monthly average  (temporal)
      #   A ...annual average   (temporal)
      # These letters may be combined in groups of 1 or 2 letters.
      # If 2 letters are used they must be 1 spatial average and
      # 1 temporal average indicator.
      # The following case statements will create a string to be used in the
      # super label for this set to identify any averaging done as well as
      # the dimensionality of the data in this set (e.g. 1D time series, 2D or 3D).
      # The default, if avgpar is not set is all fields at all times.
      avgpar=${avgpar:='full'}
      # nlev (set in getvar) contains the number of unique levels
      [ -z "$nlev" ] && bail "nlev is not set"
      if [ $nlev -eq 1 ]; then
        # Single level set, possibility of TIME records
        case $avgpar in
          full) avid='2D' ;;
             G) avid='GTS' ;;
             Z) avid='Z2D' ;;
             D) avid='D2D' ;;
             M) avid='M2D' ;;
             A) avid='A2D' ;;
         GD|DG) avid='GDTS' ;;
         ZD|DZ) avid='ZD2D' ;;
         GM|MG) avid='GMTS' ;;
         ZM|MZ) avid='ZM2D' ;;
         GA|AG) avid='GATS' ;;
         AM|AZ) avid='ZA2D' ;;
             *) bail "Invalid averaging options $avgpar" ;;
        esac
      else
        # Multi level set
        case $avgpar in
          full) avid='3D' ;;
             G) avid='G3D' ;;
             Z) avid='Z3D' ;;
             D) avid='D3D' ;;
             M) avid='M3D' ;;
             A) avid='A3D' ;;
         GD|DG) avid='GD3D' ;;
         ZD|DZ) avid='ZD3D' ;;
         GM|MG) avid='GM3D' ;;
         ZM|MZ) avid='ZM3D' ;;
         GA|AG) avid='GA3D' ;;
         ZA|AZ) avid='ZA3D' ;;
             *) bail "Invalid averaging options $avgpar" ;;
        esac
      fi
      avid=`echo $avid|sed 's/ //g'`

      # Usually fldlist will contain only one variable name. It is allowed to
      # contain more than one variable for special cases where 2 or more fields
      # need to be processed together
      fldlist=$fld

      for fld in $fldlist; do
        # Create an output file named SET

        # Define the label for this set
        fldfmt=`echo $fld|awk '{printf "%4.4s",$1}' -`
        avidfmt=`echo $avid|awk '{printf "%4.4s",$1}' -`
        lc_ftype=`echo $ftype|tr '[A-Z]' '[a-z]'`
        deltfmt=`echo $curr_delt|awk '{printf "%4d",$1}' -`

        # range (set in getvar) contains the time step (kount) range
        # winpar contains any windowing parameters

        LABEL="$avidfmt $lev_coord $fldfmt $lc_ftype $range $deltfmt"
        LABELx=" $winpar $tselpar $slab $lev_vals"
        if [ -n "`echo $LABELx|sed 's/ //g'`" ]; then
          lenLABx=`echo $LABELx|sed 's/ /x/g'|awk '{printf "%d",length($0)}' -`
          if [ $lenLABx -gt 20 ]; then
            # If the label extension would exceed the 60 character superlabel length,
            # encrypt LABELx to get a unique 13 character string beginning
            # with the characters "ID". This string is then used as part of the label
            # to ensure a unique superlabel for this variable definition.
            # Replace all spaces with lower case x so as not to confuse crypt.
            lenLABx=`echo $LABELx|sed 's/ /x/g'`
            labid=`perl -e 'print crypt($ARGV[0],"ID");' "$LABELx"`
            LABEL="$LABEL $labid"
          else
            LABEL="$LABEL $LABELx"
          fi
        fi
        echo "LABEL:$LABEL"

        # Ensure that nbin is zero at the end of this case statment 
        # unless a daily average is done inside the case statement
        nbin=0

        rm -f SET
        case $avid in

          2D|3D) ######################################################
            # Output fields as they appear in the history file

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld $fld $winpar
            mv $fld SET
            ;;

            GTS) ######################################################
            # Global average of a single level field

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld $fld $winpar

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # Determine a global average for this field
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in the history file.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw $fld ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg $fld ${fld}_gavg
            fi

            # window the global average file to get a single value per record
            # midlon and midlat are set in getvar and should identify the
            # midpoint of the window, if any, or or the whole grid
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            rm -f XXX
            ccc window ${fld}_gavg XXX input=INREC
            mv XXX ${fld}_gavg
            rm -f INREC

            # Create a set containing a pair of TIME records, one for the
            # time coordinate (kount values) and one for the global average values.
            make_TIME ${fld}_gavg ${fld}_kount SET
            rm -f ${fld}_gavg
            ;;

            G3D) ######################################################
            # Multi level field that is globally averaged

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld $fld $winpar

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # Determine a global average for this field
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in the history file.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw $fld ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg $fld ${fld}_gavg
            fi

            # window the global average file to get a single value per record
            # midlon and midlat are set in getvar and should identify the
            # midpoint of the window, if any, or or the whole grid
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            ccc window ${fld}_gavg SET input=INREC
            rm -f ${fld}_gavg INREC
            ;;

        Z2D|Z3D) ######################################################
            # Determine the zonal average of every field
            if [ "$ftype" = "XP" ]; then
              # XP files are already zonally averaged
              mv $fld SET
            else
              zonavg $fld SET
            fi

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar
            ;;

        D2D|D3D) ######################################################
            # Determine the daily average of every field
            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              # therefore daily averages are impossible
              bail "${fld}: Attempting a daily average of monthly averaged data ($ftype)."
            fi

            # nbin is the number of saving intervals per day
            [ -z "$curr_delt" ] && bail "delt is required for daily average of $fld"
            [ -z "$del_kount" ] && bail "A saving interval is required for daily average of $fld"
            nbin=`echo $del_kount $curr_delt|awk '{n=86400.0/(1.0*$1*$2)
                                                   if (int(n) != n) {n=-1}
                                                   printf "%d",n}' -`
            # If there is not an integer number of saving intervals per day
            # or there is less than 1 saving interval per day then nbin = -1
            if [ $nbin -eq -1 ]; then
              echo "${fld}: Daily average failed."
              bail "${fld}: Invalid saving interval or time step."
            fi

            # CM files are already daily averaged
            [ "x$ftype" = "xCM" ] && nbin=1

            if [ $nbin -gt 1 ]; then
              # Don't bin (average) data that already has a daily saving interval

              # nskp is the number of records to skip at the beginning of the file
              # ${fld}_kount will contain a <NEWLINE> separated list of kount
              # values, one kount value per record found in the file $fld
              nskp=`awk -v nbin="$nbin" -v delt="$curr_delt" '
                BEGIN {nskp=0; n=0; k[0]=-1}
                # If the first nbin kount values belong to the same day
                # then nskp=0 is returned.
                # If not then all the kount values that belong to the
                # first day (ie up to when the day changes) are skipped
                # and nskp is set appropriately
                {if (n  < nbin) {if ($1 != k[n]) {n=n+1; k[n] = $1}}
                 if (n == nbin) {
                   day1=k[1]*delt/86400.0
                   for (i=1; i<=nbin; i++) {
                     dayi=k[i]*delt/86400.0
                     if (int(day1) != int(dayi)) {
                       nskp=i-1
                     }
                   }
                   exit
                 }
                }
                END {printf "%d",nskp}' ${fld}_kount`

              # last_partial=0 means save the last incomplete bin, if any
              # last_partial=1 means do not save the last incomplete bin
              last_partial=1

              echo "${fld}: Daily average.   nbin=$nbin   nskp=$nskp"

              # Use binsml to average over each day of data in the file $fld
              nbin=`echo $nbin|awk '{printf "%5d",$1}' -`
              nskp=`echo $nskp|awk '{printf "%5d",$1}' -`
              last_partial=`echo $last_partial|awk '{printf "%5d",$1}' -`
              echo " BINSML   $nbin$nskp$last_partial" > INREC
              binsml $fld SET input=INREC
            else
              cp $fld SET
            fi

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar
            ;;

        M2D|M3D|A2D|A3D) ######################################################
            # Determine the monthly average of every field
            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              mv $fld SET
            else
              timavg $fld SET
            fi

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $mid_kount|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL GRID
           GRID$ib2" | ccc relabl SET XXX
#            ccc relabl SET XXX <<EOF
#    RELABL GRID
#           GRID$ib2
#EOF
            mv XXX SET
            ;;

        ZD2D|ZD3D) ######################################################
            # Zonal and daily average

            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              # therefore daily averages are impossible
              bail "${fld}: Attempting a daily average of monthly averaged data ($ftype)."
            fi

            # nbin is the number of saving intervals per day
            [ -z "$curr_delt" ] && bail "delt is required for daily average of $fld"
            [ -z "$del_kount" ] && bail "A saving interval is required for daily average of $fld"
            nbin=`echo $del_kount $curr_delt|awk '{n=86400.0/(1.0*$1*$2)
                                                   if (int(n) != n) {n=-1}
                                                   printf "%d",n}' -`
            # If there is not an integer number of saving intervals per day
            # or there is less than 1 saving interval per day then nbin = -1
            if [ $nbin -eq -1 ]; then
              echo "${fld}: Daily average failed."
              bail "${fld}: Invalid saving interval or time step."
            fi

            # CM files are already daily averaged
            [ "x$ftype" = "xCM" ] && nbin=1

            if [ $nbin -gt 1 ]; then
              # Don't bin (average) data that already has a daily saving interval

              # nskp is the number of records to skip at the beginning of the file
              # ${fld}_kount will contain a <NEWLINE> separated list of kount
              # values, one kount value per record found in the file $fld
              nskp=`awk -v nbin="$nbin" -v delt="$curr_delt" '
                BEGIN {nskp=0; n=0; k[0]=-1}
                # If the first nbin kount values belong to the same day
                # then nskp=0 is returned.
                # If not then all the kount values that belong to the
                # first day (ie up to when the day changes) are skipped
                # and nskp is set appropriately
                {if (n  < nbin) {if ($1 != k[n]) {n=n+1; k[n] = $1}}
                 if (n == nbin) {
                   day1=k[1]*delt/86400.0
                   for (i=1; i<=nbin; i++) {
                     dayi=k[i]*delt/86400.0
                     if (int(day1) != int(dayi)) {
                       nskp=i-1
                     }
                   }
                   exit
                 }
                }
                END {printf "%d",nskp}' ${fld}_kount`

              # last_partial=0 means save the last incomplete bin, if any
              # last_partial=1 means do not save the last incomplete bin
              last_partial=1

              echo "${fld}: Daily average.   nbin=$nbin   nskp=$nskp"

              # Use binsml to average over each day of data in the file $fld
              nbin=`echo $nbin|awk '{printf "%5d",$1}' -`
              nskp=`echo $nskp|awk '{printf "%5d",$1}' -`
              last_partial=`echo $last_partial|awk '{printf "%5d",$1}' -`
              echo " BINSML   $nbin$nskp$last_partial" > INREC
              binsml $fld SET input=INREC
            else
              cp $fld SET
            fi

            # Determine the zonal average of every field
            zonavg SET SETx
            mv SETx SET

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar
            ;;

      ZM2D|ZM3D|ZA2D|ZA3D) ######################################################
            # Zonal and monthly average

            if [ "$ftype" = "XP" ]; then
              # XP files are already zonally and monthly averaged
              mv $fld ${fld}_zavg
            else
              # Determine the time average of every field
              if [ "$ftype" = "GP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
                # GP, CP and GZ are already monthly averaged
                mv $fld ${fld}_tavg
              else
                timavg $fld ${fld}_tavg
              fi

              # Determine the zonal average of every field in ${fld}_tavg
              zonavg ${fld}_tavg ${fld}_zavg
              rm -f ${fld}_tavg
            fi

            # Window this field if winpar has been set.
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld ${fld}_zavg $winpar

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $mid_kount|awk '{printf "%10d",$1}' -`
            echo "    RELABL ZONL
           ZONL$ib2" | ccc relabl ${fld}_zavg SET
#            ccc relabl ${fld}_zavg SET <<EOF
#    RELABL ZONL
#           ZONL$ib2
#EOF
            rm -f ${fld}_zavg
            ;;

          GDTS) ######################################################
            # Global and daily average of single level fields

            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              # therefore daily averages are impossible
              bail "${fld}: Attempting a daily average of monthly averaged data ($ftype)."
            fi

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # nbin is the number of saving intervals per day
            [ -z "$curr_delt" ] && bail "delt is required for daily average of $fld"
            [ -z "$del_kount" ] && bail "A saving interval is required for daily average of $fld"
            nbin=`echo $del_kount $curr_delt|awk '{n=86400.0/(1.0*$1*$2)
                                                   if (int(n) != n) {n=-1}
                                                   printf "%d",n}' -`
            # If there is not an integer number of saving intervals per day
            # or there is less than 1 saving interval per day then nbin = -1
            if [ $nbin -eq -1 ]; then
              echo "${fld}: Daily average failed."
              bail "${fld}: Invalid saving interval or time step."
            fi

            if [ $nbin -gt 1 ]; then
              # Don't bin (average) data that already has a daily saving interval

              # nskp is the number of records to skip at the beginning of the file
              # ${fld}_kount will contain a <NEWLINE> separated list of kount
              # values, one kount value per record found in the file $fld
              nskp=`awk -v nbin="$nbin" -v delt="$curr_delt" '
                BEGIN {nskp=0; n=0; k[0]=-1}
                # If the first nbin kount values belong to the same day
                # then nskp=0 is returned.
                # If not then all the kount values that belong to the
                # first day (ie up to when the day changes) are skipped
                # and nskp is set appropriately
                {if (n  < nbin) {if ($1 != k[n]) {n=n+1; k[n] = $1}}
                 if (n == nbin) {
                   day1=k[1]*delt/86400.0
                   for (i=1; i<=nbin; i++) {
                     dayi=k[i]*delt/86400.0
                     if (int(day1) != int(dayi)) {
                       nskp=i-1
                     }
                   }
                   exit
                 }
                }
                END {printf "%d",nskp}' ${fld}_kount`

              # last_partial=0 means save the last incomplete bin, if any
              # last_partial=1 means do not save the last incomplete bin
              last_partial=1

              echo "${fld}: Daily average.   nbin=$nbin   nskp=$nskp"

              # Use binsml to average over each day of data in the file $fld
              nbin=`echo $nbin|awk '{printf "%5d",$1}' -`
              nskp=`echo $nskp|awk '{printf "%5d",$1}' -`
              last_partial=`echo $last_partial|awk '{printf "%5d",$1}' -`
              echo " BINSML   $nbin$nskp$last_partial" > INREC
              binsml $fld SET input=INREC
            else
              mv $fld SET
            fi

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar

            # Determine a global average of the daily average.
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in SET.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw SET ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg SET ${fld}_gavg
            fi

            # window the global average file to get a single value per record
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            rm -f XXX
            ccc window ${fld}_gavg XXX input=INREC
            mv XXX ${fld}_gavg
            rm -f INREC

            # Create a _kount file containing the set of kount values
            # found in the new file ${fld}_gavg. This may have been
            # shortened when the daily average was done above.
            ggstat ${fld}_gavg|\
              awk -v var="$vname" '{if($4==var)printf "%d\n",$3}' - > TMP_kount

            make_TIME ${fld}_gavg TMP_kount SET
            rm -f TMP_kount
            ;;

          GD3D) ######################################################
            # Global and daily average of multi level fields
            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              # therefore daily averages are impossible
              bail "${fld}: Attempting a daily average of monthly averaged data ($ftype)."
            fi

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # nbin is the number of saving intervals per day
            [ -z "$curr_delt" ] && bail "delt is required for daily average of $fld"
            [ -z "$del_kount" ] && bail "A saving interval is required for daily average of $fld"
            nbin=`echo $del_kount $curr_delt|awk '{n=86400.0/(1.0*$1*$2)
                                                   if (int(n) != n) {n=-1}
                                                   printf "%d",n}' -`
            # If there is not an integer number of saving intervals per day
            # or there is less than 1 saving interval per day then nbin = -1
            if [ $nbin -eq -1 ]; then
              echo "${fld}: Daily average failed."
              bail "${fld}: Invalid saving interval or time step."
            fi

            if [ $nbin -gt 1 ]; then
              # Don't bin (average) data that already has a daily saving interval

              # nskp is the number of records to skip at the beginning of the file
              # ${fld}_kount will contain a <NEWLINE> separated list of kount
              # values, one kount value per record found in the file $fld
              nskp=`awk -v nbin="$nbin" -v delt="$curr_delt" '
                BEGIN {nskp=0; n=0; k[0]=-1}
                # If the first nbin kount values belong to the same day
                # then nskp=0 is returned.
                # If not then all the kount values that belong to the
                # first day (ie up to when the day changes) are skipped
                # and nskp is set appropriately
                {if (n  < nbin) {if ($1 != k[n]) {n=n+1; k[n] = $1}}
                 if (n == nbin) {
                   day1=k[1]*delt/86400.0
                   for (i=1; i<=nbin; i++) {
                     dayi=k[i]*delt/86400.0
                     if (int(day1) != int(dayi)) {
                       nskp=i-1
                     }
                   }
                   exit
                 }
                }
                END {printf "%d",nskp}' ${fld}_kount`

              # last_partial=0 means save the last incomplete bin, if any
              # last_partial=1 means do not save the last incomplete bin
              last_partial=1

              echo "${fld}: Daily average.   nbin=$nbin   nskp=$nskp"

              # Use binsml to average over each day of data in the file $fld
              nbin=`echo $nbin|awk '{printf "%5d",$1}' -`
              nskp=`echo $nskp|awk '{printf "%5d",$1}' -`
              last_partial=`echo $last_partial|awk '{printf "%5d",$1}' -`
              echo " BINSML   $nbin$nskp$last_partial" > INREC
              binsml $fld SET input=INREC
            else
              mv $fld SET
            fi

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld SET $winpar

            # Determine a global average of the time average.
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in SET.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o  "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw SET ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg SET ${fld}_gavg
            fi

            # window the global average file to get a single value per record
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            ccc window ${fld}_gavg SET input=INREC
            rm -f INREC ${fld}_gavg
            ;;

           GMTS|GATS) ######################################################
            # Global and monthly average of single level fields
            # Determine the monthly average of every field
            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              mv $fld ${fld}_tavg
            else
              timavg $fld ${fld}_tavg
            fi

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld ${fld}_tavg $winpar

            # Determine a global average of the time average.
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in ${fld}_tavg.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw ${fld}_tavg ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg ${fld}_tavg ${fld}_gavg
            fi
            rm -f ${fld}_tavg

            # window the global average file to get a single value per record
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            rm -f XXX
            ccc window ${fld}_gavg XXX input=INREC
            mv XXX ${fld}_gavg
            rm -f INREC

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $mid_kount|awk '{printf "%10d",$1}' -`
            rm -f XXX
            echo "    RELABL GRID
           GRID$ib2" | ccc relabl ${fld}_gavg XXX
#            ccc relabl ${fld}_gavg XXX <<EOF
#    RELABL GRID
#           GRID$ib2
#EOF
            mv XXX ${fld}_gavg

            # The single record in this file will always contain a single data value.
            # Create a _kount file containing the average kount value
            # to be used as input for make_TIME
            echo "$mid_kount" > TMP_kount

            make_TIME ${fld}_gavg TMP_kount SET
            rm -f TMP_kount
            ;;

           GM3D|GA3D) ######################################################
            # Global and monthly average of multi level fields
            # Determine the monthly average of every field
            if [ "$ftype" = "GP" -o "$ftype" = "XP" -o "$ftype" = "CP" -o "$ftype" = "GZ" ]; then
              # GP, XP, CP and GZ files are already monthly averaged
              mv $fld ${fld}_tavg
            else
              timavg $fld ${fld}_tavg
            fi

            # CM files contain both ocn and atm gridded data
            # We need to know which is which before global averaging
            [ "x$ftype" = "xCM" ] && bail "Global average of CM file not allowed"

            # Window this field if winpar has been set.
            # globavg will respect "special values" inserted by window
            # This will also reset midlon and midlat.
            [ -n "$winpar" ] && winfld ${fld}_tavg $winpar

            # Determine a global average of the time average.
            # ${fld}_gavg will have 1 full size GRID record
            # for each save of $fld found in ${fld}_tavg.
            # Each record will contain the average value at every
            # point in the grid, except for possible special values
            # inserted during windowing.
            if [ "x$ftype" = "xGZ" -o  "x$ftype" = "xCP" ]; then
              # Ocean data requires a weighted average
              # If the GZ file is available then grido will already have been
              # called and ocean grid info will be available in the files
              # ocngrid_beta ocngrid_da ocngrid_dx ocngrid_dy ocngrid_dz
              # as well as ocngrid_wts used with globavw
              globavw ${fld}_tavg ocngrid_wts ${fld}_gavg
            else
              # Assume a gaussian grid
              globavg ${fld}_tavg ${fld}_gavg
            fi
            rm -f ${fld}_tavg

            # window the global average file to get a single value per record
            lon1=`echo $midlon|awk '{printf "%5d",$1}' -`
            lon2=`echo $midlon|awk '{printf "%5d",$1}' -`
            lat1=`echo $midlat|awk '{printf "%5d",$1}' -`
            lat2=`echo $midlat|awk '{printf "%5d",$1}' -`
            echo "  WINDOW  $lon1$lon2$lat1$lat2" > INREC
            rm -f XXX
            ccc window ${fld}_gavg XXX input=INREC
            mv XXX ${fld}_gavg
            rm -f INREC

            # set the ibuf2 value to the average kount value for this month
            ib2=`echo $mid_kount|awk '{printf "%10d",$1}' -`
            echo "    RELABL GRID
           GRID$ib2" | ccc relabl ${fld}_gavg SET
#            ccc relabl ${fld}_gavg SET <<EOF
#    RELABL GRID
#           GRID$ib2
#EOF
            rm -f ${fld}_gavg
            ;;

              *) ######################################################
            bail "Invalid set averaging id $avid"
            ;;
        esac

        # Define a current, file type specific value, for the date offset
        # and format to be used in this block
        # we do not want to convert ibuf2 values.
        lc_ftype=`echo $ftype|tr '[A-Z]' '[a-z]'`
        eval curr_date_offset=\$TS_date_offset_$lc_ftype
        eval curr_date_format=\$TS_date_format_$lc_ftype
        if [ -n "$curr_date_format" -a -z "$curr_date_offset" ]; then
          curr_date_offset="0"
        fi

        # Test to see if this file contains TIME records, if it does then
        # we do not want to convert ibuf2 values.
        is_ts=`echo $avid|sed -n '/TS/p'`
        date_format_id=''
        [ "x$ftype" = "xCM" ] && date_format_id='YYYYMMDD'
        # Ensure that TS records do not have a data_format specified
        [ -n "$is_ts" ] && date_format_id=''
        if [ -n "$curr_date_offset" -a -z "$is_ts" ]; then
          # Reformat the time (ibuf2 value), except in TIME records

          # Determine the time step in minutes (read by tstep as E5.0)
          minutes=`echo $curr_delt|awk '{dt=$1/60.0; printf "%5.2f",dt}' -`

          # Determine the input and output format for the current variable
          format_in="     MODEL"
          if [ "x$ftype" = "xGS" -a $nbin -le 1 ]; then
            format_in="  ACCMODEL"
          elif [ "x$ftype" = "xCM" ]; then
            format_in="  YYYYMMDD"
          fi
          format_out=`echo ${curr_date_format:='YYYYMMDDHH'}|tr '[a-z]' '[A-Z]'`

          # Set the offset in YYYYMMDDHH format or integer year format
          # (read by tstep as E15.0)

          # If the input format is MODEL or ACCMODEL then curr_date_offset must
          # be in YYYYMMDDHH format.
          # For all other input formats curr_date_offset is the year
          if [ "$format_in" = "     MODEL" -o "$format_in" = "  ACCMODEL" ]; then
            # If curr_date_offset=0 then tstep will assume Jan 1 00Z of year 1
            [ $curr_date_offset -le 0 ] && curr_date_offset=0
            # If curr_date_offset is less than 9999 assume that it is the year
            # and append '010100' (ie Jan 1 00Z)
            [ $curr_date_offset -gt 0 -a $curr_date_offset -le 9999 ] &&\
              curr_date_offset="${curr_date_offset}010100"
          fi
          curr_date_offset=`echo $curr_date_offset|awk '{printf "%15d",$1}' -`

          # Create the input card for tstep and reformat ibuf2 in SET
          format_in=`echo $format_in|awk '{printf "%15s",$1}' -`
          format_out=`echo $format_out|awk '{printf "%15s",$1}' -`
          echo "          $minutes$curr_date_offset$format_in$format_out" > INREC
          ccc tstep SET SETx input=INREC
          mv SETx SET
          rm -f INREC

          # Define a string to append to the DELT record to indicate that
          # ibuf2 values have been reformatted
          date_format_id=`echo $format_out|awk '{printf "%s",$1}' -`
        fi

        # If the input file is GS, SS, GP or XP then add the
        # corresponding PARM record to this set
        add_parm=0
        case $ftype in
          GS|SS|GP|XP) 
            # There should be a file in the cwd named GSPARM or SSPARM or ...
            # Determine the name of the file containing the PARM record
            eval parec=${ftype}PARM

            # The current input file name will be in the shell variable
            # TS_gsfile or TS_ssfile or TS_gpfile or TS_xpfile
            lc_ftype=`echo $ftype|tr '[A-Z]' '[a-z]'`
            eval curr_fname=\$TS_${lc_ftype}file

            # Flag addition of this file or issue a warning
            if [ -s $parec ]; then
              add_parm=1
            else
              echo "*** WARNING *** PARM record from $curr_fname is missing"
            fi
        esac
        if [ $add_parm -eq 1 -a -s "$parec" ]; then
          # Add a PARM record to the current set
          rm -f XXX
          joinup XXX $parec SET
          mv XXX SET
        fi

        # Create a CHAR record containing the current value of delt
        echo " +DELT" > DELT.txt
        echo $curr_delt|awk '{printf "    %10.2f",$1}' - >> DELT.txt
        if [ -n "$date_format_id" ]; then
          echo "ibuf2=$date_format_id"|awk '{printf "    %s",$1}' - >> DELT.txt
        fi
        rm -f DELT
        txt2bin DELT.txt DELT
        rm -f DELT.txt

        # Add DELT to the current set
        rm -f XXX
        joinup XXX DELT SET
        mv XXX SET

        # Create a file containing a single CHAR record with
        # certain variable specific information embedded in it
        # Note: A CHAR record is limited to 128 characters per line
        # and a maximum of 500 lines.

        echo " +VINF" > VARINFO.txt

        c_line="$config_line"
        # Wrap c_line if it exceeds the 128 char line length
        perl -e'use Text::Wrap; $Text::Wrap::columns = 128;
                print wrap("    ","    ",$ARGV[0]),"\n";' "$c_line" >> VARINFO.txt

        # Ensure a line break between the config line and the variable defs
        sfx=`echo $ftype|tr '[A-Z]' '[a-z]'`
        c_line='parm: '
        for envar in $env_list; do
          eval cdef=\"${envar}=\$${envar}_$sfx\;\"
          cdef=`echo $cdef|sed 's/= */=/'`
          if [ -z "$c_line" ]; then
            c_line=$cdef
          else
            c_line="$c_line $cdef"
          fi
        done
        # Wrap c_line if it exceeds the 128 char line length
        perl -e'use Text::Wrap; $Text::Wrap::columns=128; $Text::Wrap::break=q/[\s;]/;
          print wrap("    ","    ",$ARGV[0]),"\n";' "$c_line" >> VARINFO.txt

        # Ensure a line break between the variable defs and the file name
        eval echo "\ \ \ \ file: \$TS_${sfx}file" >> VARINFO.txt

        rm -f VARINFO
        txt2bin VARINFO.txt VARINFO
        rm -f VARINFO.txt

        # Add this variable info to the current set
        rm -f XXX
        joinup XXX VARINFO SET
        mv XXX SET

        # Determine a unique name for the current set
        newsets=`expr $newsets + 1`
# Use 3 values for TS_file_split
#   0 => never split
#   1 => split only when outfname is defined
#   2 => always split, generate any missing outfname internally
        if [ $TS_file_split -eq 2 ]; then
          split_this=1
        elif [ -n "$outfname" -a $TS_file_split -eq 1 ]; then
          split_this=1
        else
          split_this=0
        fi
        if [ $split_this -eq 1 ]; then
          # One variable per file will be saved
          if [ -n "$outfname" ]; then
            # Use the file name supplied in the configuration file
            newset=$outfname
          else
            # Compose a file name internally
            newset=`echo $newsets $$|awk '{printf "tseries_%4.4d_%d",$1,$2}' -`
          fi
          # Append this file name to the list of new files
          echo "$newset" >> NEW_FILE_LIST_SPLIT
          echo "$fld will be added to the file $newset"
        else
          # Use a default internal name
          newset=`echo $newsets|awk '{printf "SET%4.4d",$1}' -`

          # Append this file name to the list of new files
          echo "$newset" >> NEW_FILE_LIST
          echo "$fld will be added to the file $TS_file"
        fi

        # Create a superlabeled file containing only
        # the current set and name it $newset
        echo "XSAVE         $LABEL" > INREC
        rm -f EMPTY_FILE
        touch EMPTY_FILE
        ccc xsave EMPTY_FILE SET $newset input=INREC

        rm -f $fld ${fld}_kount ${fld}_level
      done

    done

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Create TSFILE from all the individual files created above
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    if [ -s NEW_FILE_LIST ]; then
      # Use tscat to concatenate the files created in the previous loop
      # This will simply copy these files record by record
      # using tscats' "raw" mode into TSFILE
      # No checking, reordering or combining of like sets is done here.
      tscat -raw flist=NEW_FILE_LIST out=TSFILE
    else
      rm -f TSFILE
    fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Save any files named in NEW_FILE_LIST_SPLIT one variable per file
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    if [ -s NEW_FILE_LIST_SPLIT ]; then
      # Save the files named in NEW_FILE_LIST_SPLIT one variable per file
      echo "Files to be created/appended"
      cat NEW_FILE_LIST_SPLIT

      echo " "
      echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
      line=0
      while [ $line -lt 9000 ]; do
        line=`expr $line + 1`
        file_name=`awk -v line="$line" '
          {if (NR==line) {printf "%s",$0; exit}}
          END {if (line>NR) {printf "END_OF_FILE"}}' NEW_FILE_LIST_SPLIT`
        [ -z "$file_name" ] && continue
        [ "$file_name" = "END_OF_FILE" ] && break

        [ ! -s "$file_name" ] && bail "Empty output file $file_name"

        rm -f oldTSFILE
        $ACCESS oldTSFILE $file_name na

        # If the file is empty or missing then ensure it exists
        # [ ! -s oldTSFILE ] && touch oldTSFILE

        echo "# Saving $file_name"
        if [ $TS_file_clobber -eq 1 ]; then
          # If the user has requested a new file then delete the old file
          delete oldTSFILE na
          save $file_name $file_name
        else
          # Concatenate using tscat
#          ls -alrt
          rm -f newTSFILE
          if [ -s oldTSFILE ]; then
#            ggstat oldTSFILE
#            ggstat $file_name
            tscat out=newTSFILE oldTSFILE $file_name
          else
            cp $file_name newTSFILE
          fi
          # Clean up temporary files created by tscat
          rm -f settmp* CHAR_tmp TS_TMP20 TS_TMP21
          save newTSFILE $file_name
          delete oldTSFILE na
          $RELEASE newTSFILE
        fi
      done
      echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
      echo " "
    fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Remove certain intermediate files if requested
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    if [ $TS_clean -eq 1 ]; then

      # Remove npakgg directory if present
      if [ -d $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npakgg ] ; then
        chmod -R u+w $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npakgg
        rm -rf $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npakgg
      fi

      # Remove npaksp directory if present
      if [ -d $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaksp ] ; then
        chmod -R u+w $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaksp
        rm -rf $RUNPATH_ROOT/../ccrn_tmp/tmp_${flabel}_${username}_npaksp
      fi

    fi

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# Save resulting TSFILE, if any
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

    if [ -s TSFILE ]; then

      echo " "
      echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
      echo "# Saving $TS_file"
      echo "#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#"
      echo " "

      rm -f oldTSFILE
      $ACCESS oldTSFILE $TS_file na
      if [ ! -s oldTSFILE -o $TS_file_clobber -eq 1 ]; then

        # If the user has requested a new file then delete the old
        # ts file before writing the HEADER
        [ $TS_file_clobber -eq 1 ] && delete oldTSFILE na

        # If TS_file does not already exist or is empty
        # then write a HEADER set.
        rm -f HEADER

        # Create a text file containing the value of delt from each
        # of the file types. If a file type is not used then set
        # the corresponding delt to zero.
        delt_gs=${delt_gs:=0}
        delt_ss=${delt_ss:=0}
        delt_gp=${delt_gp:=0}
        delt_xp=${delt_xp:=0}
        delt_td=${delt_td:=0}
        delt_gz=${delt_gz:=0}
        delt_cm=${delt_cm:=0}
        delt_cp=${delt_cp:=0}
        delt_ie=${delt_ie:=0}
        delt_cc=${delt_cc:=0}
        echo "$delt_gs $delt_ss $delt_gp $delt_xp $delt_td $delt_gz $delt_cm $delt_cp $delt_ie $delt_cc" > DATA_VALS
        ndat=10

        # Create a GRID record containing these values for delt
        rm -f IBUF
        ib2=0;     ib2=`echo $ib2|awk '{printf "%10d",$1}' -`
        ib4=1;     ib4=`echo $ib4|awk '{printf "%10d",$1}' -`
        ib5=$ndat; ib5=`echo $ib5|awk '{printf "%10d",$1}' -`
        ib6=1;     ib6=`echo $ib6|awk '{printf "%10d",$1}' -`
        ib7=0;     ib7=`echo $ib7|awk '{printf "%10d",$1}' -`
        ib8=-1;    ib8=`echo $ib8|awk '{printf "%10d",$1}' -`
        echo " GRID${ib2} DELT${ib4}${ib5}${ib6}${ib7}${ib8}" > IBUF

        # Create a data record containing kount values in 1P6E22.15 format
        fmt_data_rec DATA_VALS DATA
        cat IBUF DATA > DATAREC

        chabin DATAREC HEADER ||\
          (cat DATAREC; bail "ERROR in chabin DATAREC HEADER")
        rm -f DATAREC

        # Write a CHAR record containing the file name
        # of each input file used above.
        for ftype in GS SS TD GP XP CM CP GZ IE CC; do
          # If an input file of ftype was used then a local file
          # named ${ftype}FILE will be present.
          # If this file does not exist then do not add
          # a CHAR record containing the name of this input file.
          eval curr_local=${ftype}FILE
          [ ! -s $curr_local ] && continue

          # The current input file name will be in the shell variable
          # TS_gsfile or TS_ssfile or TS_tdfile or TS_gpfile or ...
          lc_ftype=`echo $ftype|tr '[A-Z]' '[a-z]'`
          eval curr_fname=\$TS_${lc_ftype}file

          # Create a file with a single CHAR record containing
          # the name of the current input file
          ftype=`echo $ftype|awk '{printf "%4s",$1}' -`
          echo " +$ftype" > INFNAME.txt
          echo "    $curr_fname" >> INFNAME.txt
          rm -f INFNAME
          txt2bin INFNAME.txt INFNAME
          rm -f INFNAME.txt

          # Add this file name to the header
          rm -f XXX
          joinup XXX HEADER INFNAME
          mv XXX HEADER
        done

        if [ -s HEADER ]; then
          rm -f XXX
          echo "  XSAVE       TS HEADER
          " | ccc xsave XXX HEADER NEW
          mv NEW oldTSFILE
        fi
        # If the file is still empty or missing then ensure it exists
        [ ! -s oldTSFILE ] && touch oldTSFILE
      fi

      # NOTE: If any superlabeled sets in TSFILE are duplicates of
      # sets in oldTSFILE then xjoin will replace those sets from
      # oldTSFILE with the corresponding sets from TSFILE. The new
      # sets are always appended, so set order may change.
      xjoin oldTSFILE TSFILE newTSFILE

      save newTSFILE $TS_file
      delete oldTSFILE na
      $RELEASE newTSFILE
    fi

#   ----------------------------------
#   stop wall clock
    echo "\nSTOP tseries:  "`date`"\n"

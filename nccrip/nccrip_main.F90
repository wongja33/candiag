program nccrip_main

  !--- Convert CCCma binary files to netcdf
  !--- Larry Solheim June 7, 2012

  !--- crec_data defines types and subroutines and assoicated crec data
  use crec_data

  !--- Global parameters
  use nccrip_params

  !--- Routines for processing CCCma time series files
  use ccc_time_series

  use ccc_io,    only : newunit, cc_open, cc_close
  use ccc_subs,  only : query_file
  use file_subs, only : fin, nfin
  use crec_subs, only : print_cvar_t
  use time_subs, only : time_in_secs
  !use cmor_users_functions, only : cmor_close

  !--- use netcdf

  implicit none

  include "netcdf.inc"

  !--- Unit number for input CCCma binary data file
  integer, parameter :: iudata = 1

  !--- Integer work space
  integer, allocatable :: iwrk(:)

  !--- Space to hold a temporary copy of ibuf
  integer :: xbuf(8)

  integer :: nx, ny, nz
  integer :: idx1, idx2, ret
  integer(kind=4) :: vid_cmor
  integer :: first_ymd_hms(6), last_ymd_hms(6)
  character(12) :: range1, range2
  logical :: remote, found
  character(1024) :: strng, strng2

  !--- CCCma io specific
  integer :: machine, me32o64
  integer :: intsize, inteflt
  external me32o64, inteflt
  common /machtyp/ machine,intsize

  !===============================================================================
  !===============================================================================

  ! -----------------------------------------------------------------------
  ! Initialize machine and intsize (required for CCCma io routines)
  machine = me32o64(idx1)
  intsize = inteflt(idx1)

  !--- Read command line args and initialize control parameters
  call nccrip_cmdline

  if ( cccma_time_series_in ) then
    !--- All input files are CCCma time series
    call read_time_series()

    if (use_cmor) then
      !!--- A single file was read and the cmor variable id was put
      !!--- in var%vids(7) of the first variable in var_list
      !if (nvar .ne.1) then
      !  write(6,'(a,i6)') &
      !    'nccrip_main: Reading time series and writing netcdf using CMOR but nvar = ',nvar
      !  write(6,'(a,i6)') '             Expecting nvar = 1'
      !  call abort
      !endif
      !vid_cmor = var_list(1)%vids(7)

      !!--- Call cmor_close once to get the name of the file that was created
      !ret = cmor_close(var_id=vid_cmor, file_name=strng)
      !if (ret.ne.0) then
      !  write(6,'(a,i6)')"nccrip_main: error from cmor_close = ",ret
      !  call abort
      !endif

      !!--- Call cmor_close a second time with no args to actually close the file
      !ret = cmor_close()
      !if (ret.ne.0) then
      !  write(6,'(a,i6)')"nccrip_main: error from cmor_close = ",ret
      !  call abort
      !endif

      !!--- Replace any trailing nulls with spaces in the file name strng
      !!--- and write this file name to stdout
      !do idx1=len(strng),1,-1
      !  if (strng(idx1:idx1).eq.' '  .or. strng(idx1:idx1).eq.char(0)) then
      !    strng(idx1:idx1)=' '
      !  else
      !    exit
      !  endif
      !enddo
      !write(6,'(2a)')'Created netcdf file ',trim(strng)

      !!--- Stop here when cmor was used to write the netcdf file
      !stop

    endif

  else

    !--- Loop over input files and process each one indivudually
    IN_FILE: do idx1 = 1,nfin

      if ( only_query_file ) then
        !--- Set verbose to avoid extra output
        verbose = -1

        !--- Attach the input CCCma binary data file to iudata
        call cc_open(iudata,trim(fin(idx1)%name),remote,0)

        !--- Read enough records to extract level info and write to stdout
        allocate( iwrk(1000) )
        call query_file(iudata, xbuf, nx, ny, nz, iwrk, query_file_for_this)
        deallocate( iwrk )

        !--- Close the input file and clean up any remote access links
        call cc_close(iudata,trim(fin(idx1)%name),remote,0)

        !--- Jump to the next file
        cycle
      endif

      !--- Attach the input CCCma binary data file to iudata
      call cc_open(iudata,trim(fin(idx1)%name),remote,verbose)
      fin(idx1)%unit   = iudata
      fin(idx1)%remote = remote

      !--- Set certain parameters according to the type of input file

      if ( .not. add_label_attr_from_cmdl ) then
        !--- Reset this for each file read
        add_label_attr = .false.
      endif
      if ( .not. time_interval_from_cmdl ) then
        !--- Reset this for each file read
        time_interval = " "
        time_interval_secs = -1
      endif

      select case (trim(adjustl(fin(idx1)%ftype)))
        case ("gs")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif

        case ("ss")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif

        case ("gz")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMM"
            if ( fin(idx1)%year .gt. -1 ) then
              int_ib2_year = fin(idx1)%year
            else
              int_ib2_year = -1
              !--- If the year is unknown then revert to OMODEL
              int_ib2_fmt = "OMODEL"
            endif
            if ( fin(idx1)%mon  .gt. -1 ) then
              int_ib2_mon  = fin(idx1)%mon
            else
              int_ib2_mon  = -1
              !--- If the month is unknown then revert to OMODEL
              int_ib2_fmt = "OMODEL"
            endif
          endif

        case ("cm")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMMDD"
          endif

        case ("gp")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMM"
            if ( fin(idx1)%year .gt. -1 ) then
              int_ib2_year = fin(idx1)%year
            else
              int_ib2_year = -1
            endif
            if ( fin(idx1)%mon  .gt. -1 ) then
              int_ib2_mon  = fin(idx1)%mon
            else
              int_ib2_mon  = -1
            endif
          endif
          !--- Set a logical flag to indicate that surface pressure is always added
          !--- to output netcdf files when model levels are output.
          add_ps_to_nc = .true.

        case ("xp")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMM"
            if ( fin(idx1)%year .gt. -1 ) then
              int_ib2_year = fin(idx1)%year
            else
              int_ib2_year = -1
            endif
            if ( fin(idx1)%mon  .gt. -1 ) then
              int_ib2_mon  = fin(idx1)%mon
            else
              int_ib2_mon  = -1
            endif
          endif
          !--- Set a logical flag to indicate that surface pressure is always added
          !--- to output netcdf files when model levels are output.
          add_ps_to_nc = .true.

        case ("gp6")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif
          !--- Set a logical flag to indicate that surface pressure is always added
          !--- to output netcdf files when model levels are output.
          add_ps_to_nc = .true.

        case ("cp")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMM"
          endif

        case ("td")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif

        case ("ts")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif

        case ("rs")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "MODEL"
          endif

        case ("cs")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "YYYYMMDD"
          endif

        case ("os")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "OMODEL"
          endif

        case ("an")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "DOY"
          endif

        case ("rtd")
          !--- Set the internal ibuf2 format
          if ( len_trim(fin(idx1)%ib2_fmt) .gt. 0 ) then
            int_ib2_fmt = trim(adjustl(fin(idx1)%ib2_fmt))
          else
            int_ib2_fmt = "RAW"
          endif
          !--- Always add a label attribute to variables read from
          !--- and rtd file unless the user has set this explicitly
          if ( .not. add_label_attr_from_cmdl ) then
            add_label_attr = .true.
          endif
          !--- Set time_interval to 1 year unless set by the user
          if (  .not. time_interval_from_cmdl ) then
            time_interval = "1 year"
            time_interval_secs = time_in_secs(time_interval)
          endif
          if ( .not. add_date_to_ncout_from_cmdl ) then
            add_date_to_ncout = .false.
          endif
!xxx          !--- Set time units unless set by user
!xxx          if (  .not. time_units_attrib_from_cmdl ) then
!xxx            time_units_attrib = "years since 1978-1-1"
!xxx            !--- Ensure time_units_attrib is lower case
!xxx            call tolower(time_units_attrib,strng)
!xxx            time_units_attrib = trim(strng)
!xxx            !--- Extract info from this string and set other parameters
!xxx            !--- such as time_units and gref_ymd_hms
!xxx            call parse_time_units(time_units_attrib)
!xxx          endif

        case default
          !--- Set the internal ibuf2 format
          int_ib2_fmt  = "RAW"
          int_ib2_year = -1
          int_ib2_mon  = -1

      end select

      !--- Reinitialize (year|mon)_from_file for each new file so that the current
      !--- year/mon values will be used (if present in the file's PARM record)
      year_from_file = .false.
      mon_from_file  = .false.

      !--- Read the current input file and write records to output files
      if (verbose.gt.0) then
        write(6,'(2a)')'Processing ',trim(fin(idx1)%name)
      endif
      call proc_file(iudata, fin(idx1))

      !--- Close the input file and clean up any remote access links
      call cc_close(iudata,trim(fin(idx1)%name),remote,verbose)

    enddo IN_FILE

  endif

  !--- Close all netcdf files
  if (verbose.gt.10) write(6,*)'nccrip_main: Closing netcdf files.'
  do idx1=1,nvar
    ret = nf_close( var_list(idx1)%fid )
  enddo

  !--- Close any open temporary or CCCma binary output files
  do idx1=1,nvar
    !--- Temporary file
    if (var_list(idx1)%iutmp .gt. 0) close( var_list(idx1)%iutmp )
    var_list(idx1)%iutmp = -1
    if (var_list(idx1)%alt_iutmp .gt. 0) close( var_list(idx1)%alt_iutmp )
    var_list(idx1)%alt_iutmp = -1
    !--- Output CCCma binary files
    !--- Do not reset the unit number for CCCma binary files, it will be used below
    if (var_list(idx1)%iuccout .gt. 0) close( var_list(idx1)%iuccout )
  enddo

  if (add_date_to_ncout) then
    !--- Rename output netcdf files so that a date range is part of each file name
    if (verbose.gt.10) write(6,*)'nccrip_main: Renaming netcdf files to include date range.'
    do idx1=1,nvar
      if ( .not. associated( var_list(idx1)%rec ) ) then
        write(6,'(a)')'nccrip_main: rec is not associated with variable'
        call print_cvar_t( var_list(idx1) )
        write(6,'(2a)')'*********** Cannot add time range to output file name ', &
          trim(var_list(idx1)%file)
        cycle
      endif

      call get_first_and_last_time(var_list(idx1)%rec, first_ymd_hms, last_ymd_hms)

      if ( all( first_ymd_hms .eq. -1 ) ) then
        call print_cvar_t( var_list(idx1) )
        write(6,'(2a)')'*********** Cannot add time range to output file name ', &
          trim(var_list(idx1)%file)
        cycle
      endif

      if ( all( last_ymd_hms .eq. -1 ) ) then
        call print_cvar_t( var_list(idx1) )
        write(6,'(2a)')'*********** Cannot add time range to output file name ', &
          trim(var_list(idx1)%file)
        cycle
      endif

      if ( first_ymd_hms(1).eq.0 .and. &
           first_ymd_hms(2).eq.1 .and. &
           first_ymd_hms(3).eq.1 .and. &
            last_ymd_hms(1).eq.0 .and. &
            last_ymd_hms(2).eq.1 .and. &
            last_ymd_hms(3).eq.1 ) then
        !--- This is an invariant variable ...do not change the file name
        cycle
      endif

      strng = " "
      strng = var_list(idx1)%file

      !--- Get date range as char
      if ( first_ymd_hms(1).le.9999 ) then
        !--- YYYYMMDDHH
        write(range1,'(i4.4,i2.2,i2.2,i2.2)')first_ymd_hms(1:4)
      else if ( first_ymd_hms(1).gt.9999 .and. first_ymd_hms(1).le.99999 ) then
        !--- YYYYYMMDDHH
        write(range1,'(i5.5,i2.2,i2.2,i2.2)')first_ymd_hms(1:4)
      else if ( first_ymd_hms(1).gt.99999 .and. first_ymd_hms(1).le.999999 ) then
        !--- YYYYYYMMDDHH
        write(range1,'(i6.6,i2.2,i2.2,i2.2)')first_ymd_hms(1:4)
      else
        write(6,'(2a)')'nccrip_main: First year is out of range in file ', &
                       trim(var_list(idx1)%file)
        write(6,*)' Y-M-D = ',first_ymd_hms(1),'-',first_ymd_hms(2),'-',first_ymd_hms(3),'-'
        call print_cvar_t( var_list(idx1) )
        cycle
      endif
      if ( last_ymd_hms(1).le.9999 ) then
        !--- YYYYMMDDHH
        write(range2,'(i4.4,i2.2,i2.2,i2.2)')last_ymd_hms(1:4)
      else if ( last_ymd_hms(1).gt.9999 .and. last_ymd_hms(1).le.99999 ) then
        !--- YYYYYMMDDHH
        write(range2,'(i5.5,i2.2,i2.2,i2.2)')last_ymd_hms(1:4)
      else if ( last_ymd_hms(1).gt.99999 .and. last_ymd_hms(1).le.999999 ) then
        !--- YYYYYYMMDDHH
        write(range2,'(i6.6,i2.2,i2.2,i2.2)')last_ymd_hms(1:4)
      else
        write(6,'(2a)')'nccrip_main: Last year is out of range in file ', &
                       trim(var_list(idx1)%file)
        write(6,*)' Y-M-D = ',last_ymd_hms(1),'-',last_ymd_hms(2),'-',last_ymd_hms(3),'-'
        call print_cvar_t( var_list(idx1) )
        cycle
      endif

      idx2 = len_trim(strng)
      idx2 = idx2 - 3  ! move idx2 back to the last char before the trailing ".nc"
      strng(idx2+1:idx2+3) = '_  '
      idx2 = len_trim(strng)
      strng(idx2+1:idx2+len_trim(range1)) = trim(range1)
      idx2 = len_trim(strng)
      strng(idx2+1:idx2+1) = '-'
      idx2 = len_trim(strng)
      strng(idx2+1:idx2+len_trim(range2)) = trim(range2)
      idx2 = len_trim(strng)
      strng(idx2+1:idx2+3) = '.nc'

      !--- Rename this file on disk
      strng2=" "
      strng2="mv "//trim(var_list(idx1)%file)//" "//trim(strng)
      call system(trim(strng2))

      !--- Replace the file name in var_list
      var_list(idx1)%file = trim(strng)

      if (var_list(idx1)%iuccout .gt. 0) then
        !--- There is also a CCCma binary output file containing this variable
        strng = " "
        strng = var_list(idx1)%ccout
        idx2 = len_trim(strng)
        idx2 = idx2 - 3  ! move idx2 back to the last char before the trailing "_ts"
        strng(idx2+1:idx2+3) = '_  '
        idx2 = len_trim(strng)
        strng(idx2+1:idx2+len_trim(range1)) = trim(range1)
        idx2 = len_trim(strng)
        strng(idx2+1:idx2+1) = '-'
        idx2 = len_trim(strng)
        strng(idx2+1:idx2+len_trim(range2)) = trim(range2)
        idx2 = len_trim(strng)
        strng(idx2+1:idx2+3) = '_ts'

        !--- Rename this file on disk
        strng2=" "
        strng2="mv "//trim(var_list(idx1)%ccout)//" "//trim(strng)
        call system(trim(strng2))

        !--- Replace the file name in var_list
        var_list(idx1)%ccout = trim(strng)
      endif

    enddo
  endif

  if ( len_trim(dataout_dir).gt.0 ) then
    !--- Move output files to another directory
    do idx1=1,nvar
      !--- If the file name is missing then ignore this variable
      if (len_trim(var_list(idx1)%file).le.0) cycle

      !--- Move this file to dataout_dir
      strng=" "
      strng="mv "//trim(var_list(idx1)%file)//" "//trim(dataout_dir)
      call system(trim(strng))

      !--- prepend the output dir name to the file name
      strng=" "
      strng=trim(adjustl(dataout_dir))//"/"//trim(adjustl(var_list(idx1)%file))
      var_list(idx1)%file = trim(strng)

      if (var_list(idx1)%iuccout .gt. 0) then
        !--- There is also a CCCma binary output file containing this variable
        strng=" "
        strng="mv "//trim(var_list(idx1)%ccout)//" "//trim(dataout_dir)
        call system(trim(strng))

        !--- prepend the output dir name to the file name
        strng=" "
        strng=trim(adjustl(dataout_dir))//"/"//trim(adjustl(var_list(idx1)%ccout))
        var_list(idx1)%ccout = trim(strng)
      endif

    enddo
  endif

  if (write_file_list) then
    !--- Create a text file containing a list of all output files

    idx2 = newunit(1)
    if ( len_trim(flist_out) .le. 0 ) then
      !--- Use a default file name for this list
      flist_out = 'nccrip_file_list_netcdf'
    endif
    open(idx2,file=trim(flist_out),form='formatted')
    rewind(idx2)
    found=.false.
    do idx1=1,nvar
      write(idx2,'(a)')trim(adjustl(var_list(idx1)%file))
      if ( var_list(idx1)%iuccout .gt. 0 ) found = .true.
    enddo
    close(idx2)
    write(6,'(2a)')'Created netcdf output file list in ',trim(flist_out)

    if (found) then
      !--- There is at least 1 output CCCma binary file
      strng = " "
      if ( len_trim(flist_out) .le. 0 .or. &
           trim(adjustl(flist_out)) .eq. 'nccrip_file_list_netcdf' ) then
        !--- Use a default file name for this list
        strng = 'nccrip_file_list_cccbin'
      else
        !--- Simply append '_cccbin' to the existing value
        strng = trim(adjustl(flist_out)) // '_cccbin'
      endif
      open(idx2,file=trim(strng),form='formatted')
      rewind(idx2)
      do idx1=1,nvar
        if ( var_list(idx1)%iuccout .gt. 0 ) then
          write(idx2,'(a)')trim(adjustl(var_list(idx1)%ccout))
        endif
      enddo
      close(idx2)
      write(6,'(2a)')'Created CCCma  output file list in ',trim(strng)
    endif

  else

    if (verbose.gt.-1 .and. nvar.le.10) then
      do idx1=1,nvar
        write(6,'(2a)')'Created netcdf file ',trim(var_list(idx1)%file)
        if (var_list(idx1)%iuccout .gt. 0) then
          write(6,'(2a)')'Created CCCma  file ',trim(var_list(idx1)%ccout)
        endif
      enddo
    endif

  endif

  contains

  subroutine get_first_and_last_time(crec, first_ymd_hms, last_ymd_hms)
    use crec_subs, only : crec_t

    implicit none

    type(crec_t), pointer :: crec
    !--- date and time as (Y,M,D,H,M,S)
    integer :: first_ymd_hms(6), last_ymd_hms(6)

    !--- Local
    type (crec_t), pointer :: curr, last

    first_ymd_hms(:) = -1
    last_ymd_hms(:)  = -1

    if ( .not. associated(crec%time) ) then
      write(6,'(a)')'Time is not associated with the first record.'
      return
    endif

    !--- get ymd_hms from the first node in the list
    first_ymd_hms(1:6) = crec%time%ymd_hms(1:6)

    !--- Find the last node in the list
    last => crec
    curr => crec
    do while ( associated(curr) )
      last => curr
      curr => curr%next
    enddo

    if ( .not. associated(last%time) ) then
      write(6,'(a)')'Time is not associated with the last record.'
      return
    endif

    last_ymd_hms(1:6) = last%time%ymd_hms(1:6)

  end subroutine get_first_and_last_time


end program nccrip_main
